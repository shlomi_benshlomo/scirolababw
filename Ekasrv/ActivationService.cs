using System;
using System.Collections.Generic;
using System.Text;
using Ekasrv.Server;

namespace Ekasrv
{
    public class ActivationService
    {
        public const int MODE_NOT_APPLICABLE = 0;
        public const int MODE_LICENSE_ACTIVATION = 1;
        public const int MODE_LICENSE_TRANSFER = 2;
        public const int MODE_LICENSE_CUSTOMIZATION = 3;
        public const int MODE_LICENSE_REPLACEMENT_OR_DEACTIVATION = 4;
        public const int MODE_LICENSE_DEACTIVATION = 5;
        public const int MODE_LICENSE_DESTROYED = 6;
        public const int MODE_LICENSE_REACTIVATION = 7;
        public const int ERR_EXCEPTION = 90;
        public ActivationService()
        {
            InitProp();
            InitStatusProp();
            InitResProp();
        }
        public ActivationService(string strServiceUrl)
        {
            InitProp();
            InitStatusProp();
            InitResProp();

            m_strServiceUrl = strServiceUrl;
        }
        public bool ActivateLicense(string strActivationKey, string strRegistrationID)
        {
            InitResProp();
            m_strLicenseKey = "";
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                ActivateLicenseRes resActivateLicense = As.ActivateLicense(strActivationKey, strRegistrationID);
                m_nErrCode = resActivateLicense.ErrorCode;
                m_strErrStr = resActivateLicense.ErrorMessage;
                m_strLicenseKey = resActivateLicense.LicenseKey;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool ReactivateLicense(string strActivationKey, string strLicKeyTran, string strRegistrationID)
        {
            InitResProp();
            m_strLicenseKey = "";
            try
            {
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                Server.ReactivateLicenseRes resReactivateLicense = As.ReactivateLicense(strActivationKey, strLicKeyTran, strRegistrationID);
                m_nErrCode = resReactivateLicense.ErrorCode;
                m_strErrStr = resReactivateLicense.ErrorMessage;
                m_strLicenseKey = resReactivateLicense.LicenseKey;
                if (m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool CheckRegName(string strActivationKey, string strRegName, string strRegOrg)
        {
            InitResProp();
            m_strRegIDPer = "";
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                CheckRegNameRes resCheckRegName = As.CheckRegName(strActivationKey, strRegName, strRegOrg);
                m_nErrCode = resCheckRegName.ErrorCode;
                m_strErrStr = resCheckRegName.ErrorMessage;
                m_strRegIDPer = resCheckRegName.RegIDPer;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool CheckUserID(string strActivationKey, string strUserID)
        {
            InitResProp();
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                CheckUserIDRes resCheckUserID = As.CheckUserID(strActivationKey, strUserID);
                m_nErrCode = resCheckUserID.ErrorCode;
                m_strErrStr = resCheckUserID.ErrorMessage;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool ConfirmDestroyCode(string strActivationKey, string strDestroyCode)
        {
            InitResProp();
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                ConfirmDestroyCodeRes resConfirmDestroyCode = As.ConfirmDestroyCode(strActivationKey, strDestroyCode);
                m_nErrCode = resConfirmDestroyCode.ErrorCode;
                m_strErrStr = resConfirmDestroyCode.ErrorMessage;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool DeactivateLicense(string strActivationKey, string strLicKeyTran)
        {
            InitResProp();
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                DeactivateLicenseRes resDeactivateLicense = As.DeactivateLicense(strActivationKey, strLicKeyTran);
                m_nErrCode = resDeactivateLicense.ErrorCode;
                m_strErrStr = resDeactivateLicense.ErrorMessage;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool GetRegIDTran(string strActivationKey)
        {
            InitResProp();
            m_strRegIDTran = "";
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                GetRegIDTranRes resGetRegIDTran = As.GetRegIDTran(strActivationKey);
                m_nErrCode = resGetRegIDTran.ErrorCode;
                m_strErrStr = resGetRegIDTran.ErrorMessage;
                m_strRegIDTran = resGetRegIDTran.RegIDTran;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool GetStatus(string strActivationKey)
        {
            InitResProp();
            InitStatusProp();
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                GetStatusRes resGetStatus = As.GetStatus(strActivationKey);
                m_nErrCode = resGetStatus.ErrorCode;
                m_strErrStr = resGetStatus.ErrorMessage;
                m_nActivationMode = resGetStatus.ActivationMode;
                m_bActivated = resGetStatus.Activated;
                m_bDestroyed = resGetStatus.Destroyed;
                m_nMaxActivation = resGetStatus.MaxActivation;
                m_nActivationCount = resGetStatus.ActivationCount;
                m_strRegIDTran = resGetStatus.RegIDTran;
                m_nLicUpgradeID = resGetStatus.LicUpgradeID;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool SaveRegName(string strActivationKey, string strRegName, string strRegOrg)
        {
            InitResProp();
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                SaveRegNameRes resSaveRegName = As.SaveRegName(strActivationKey, strRegName, strRegOrg);
                m_nErrCode = resSaveRegName.ErrorCode;
                m_strErrStr = resSaveRegName.ErrorMessage;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool UpdateLog(string strActivationKey, string strLogMessage)
        {
            InitResProp();
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                UpdateLogRes resUpdateLog = As.UpdateLog(strActivationKey, strLogMessage);
                m_nErrCode = resUpdateLog.ErrorCode;
                m_strErrStr = resUpdateLog.ErrorMessage;
                if(m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public string HelloWorld()
        {
            InitResProp();
            try{
                Ekasrv.Server.Service As = new Ekasrv.Server.Service();
                if (m_strServiceUrl != "")
                    As.Url = m_strServiceUrl;
                return As.HelloWorld();
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return "";
            }
        }
        
        protected void HandleException(string strMessage)
        {
            m_nErrCode = ERR_EXCEPTION;
            m_strErrStr = strMessage;
        }

        protected void InitProp()
        {
            m_strServiceUrl = "";   
        }

        protected void InitStatusProp()
        {
            m_nActivationMode = 0;
            m_bActivated = false;
            m_bDestroyed = false;
            m_nMaxActivation = 0;
            m_nActivationCount = 0;
            m_strLicenseKey = "";
            m_strRegIDTran = "";
            m_strRegIDPer = "";
            m_nLicUpgradeID = 0;
        }

        protected void InitResProp()
        {
	        m_nErrCode = 0;
	        m_strErrStr = "";
        }

        public string ServiceURL
        {
            get { return m_strServiceUrl; }
            set { m_strServiceUrl = value; }
        }
        // Activation Prop
        public string LicenseKey
        {
			get {return m_strLicenseKey;}
		}
        // Status Prop
        public int ActivationMode
        {   
            get {return m_nActivationMode;}
        }
        public bool Activated
        {
            get {return m_bActivated;}
        }
        public bool Destroyed
        {   
            get {return m_bDestroyed;}
        }
        public int MaxActivation
        {
            get {return m_nMaxActivation;}
        }
        public int ActivationCount
        {
            get { return m_nActivationCount; }
        }
        public string RegIDTran
        {
            get { return m_strRegIDTran; }
        }
        public int LicUpgradeID
        {
            get { return m_nLicUpgradeID; }
        }
        //
        public string RegIDPer
        {
            get { return m_strRegIDPer; }
        }
        // Response
		public int ErrCode
        {
            get { return m_nErrCode; }
        }
        public string ErrStr
        {
            get { return m_strErrStr; }
        }

        protected string m_strServiceUrl;
        // Activation Prop
        protected string m_strLicenseKey;
        // Status Prop
        protected int m_nActivationMode;
        protected bool m_bActivated;
        protected bool m_bDestroyed;
        protected int m_nMaxActivation;
        protected int m_nActivationCount;
        protected string m_strRegIDTran;
        protected int m_nLicUpgradeID;
        //
        protected string m_strRegIDPer;
        // Response Prop
        protected int m_nErrCode;
        protected string m_strErrStr;
    }
}
