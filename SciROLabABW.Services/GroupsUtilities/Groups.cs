﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SciROLabABW.Web;

namespace SciROLabABW.Services
{
    public class Groups
    {
        public static bool IsUserInGroup(SciROLabGroup group, string userName)
        {
            foreach (AspNetUser GroupUser in group.AspNetUsers)
            {
                if (userName == GroupUser.UserName)
                {
                    return true;
                }
            }

            return false;
        }
    }
}