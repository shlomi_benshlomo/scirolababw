﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SciROLabABW.Web.Startup))]
namespace SciROLabABW.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
