﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.Models
{
    public class LicenseObject
    {
        public int m_nLicenseMode;
        public int m_nMaxUser;
        public int m_nMaxExec;
        public int m_nExecCount;
        public int m_nExecLeft;
        public int m_nMaxDayPeriod;
        public int m_nDayCountPeriod;
        public int m_nDayLeftPeriod;
        public int m_nExpYear;
        public int m_nExpMonth;
        public int m_nExpDay;
        public uint m_lModules;
        public string m_strRegistrationID;
        public string m_strLicKeyTran;
        public string m_strDestroyCode;

        public int m_nErrCode;
        public string m_strErrStr;

        private LicenseManager Lm;
        public LicenseObject()
        {
            Lm = new LicenseManager(0);
            Lm.UpdateLimitKey();
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            UpdatePermission();
        }
        private HttpRequest Rq;
        private HttpResponse Rp;
        public LicenseObject(int nModuleID, HttpRequest objRequest, HttpResponse objResponse)
        {
            Lm = new LicenseManager(nModuleID);
            Rq = objRequest;
            Rp = objResponse;
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
        }
        private void UpdatePermission()
        {
            // Updating the permission value to show on the front-end
            m_nLicenseMode = Lm.LicenseMode;
            m_nMaxExec = Lm.MaxExec;
            m_nExecCount = Lm.ExecCount;
            m_nMaxDayPeriod = Lm.MaxDayPeriod;
            m_nDayCountPeriod = Lm.DayCountPeriod;
            m_nDayLeftPeriod = Lm.DayLeftPeriod;
            m_nExpYear = Lm.ExpYear;
            m_nExpMonth = Lm.ExpMonth;
            m_nExpDay = Lm.ExpDay;
            m_lModules = Lm.Modules;
            m_strRegistrationID = Lm.RegistrationID;
            m_nMaxUser = Lm.MaxUser;
        }

        private void UpdateProperties()
        {
            // Updating the permission value to show on the front-end
            m_nLicenseMode = Lm.LicenseMode;
            m_nMaxExec = Lm.MaxExec;
            m_nExecCount = Lm.ExecCount;
            m_nExecLeft = Lm.ExecLeft;
            m_nMaxDayPeriod = Lm.MaxDayPeriod;
            m_nDayCountPeriod = Lm.DayCountPeriod;
            m_nDayLeftPeriod = Lm.DayLeftPeriod;
            m_nExpYear = Lm.ExpYear;
            m_nExpMonth = Lm.ExpMonth;
            m_nExpDay = Lm.ExpDay;
            m_lModules = Lm.Modules;
            m_nMaxUser = Lm.MaxUser;
        }

        public bool NKLSLogin(string strAppName, string strLoginUser, string strSessionID, int nSessionTimeout, int nInterval)
        {
            bool res = false;
            res = Lm.UpdateLimitKey();
            if (res)
            {
                string username;
                if (strLoginUser != "")
                    username = strLoginUser;
                else
                    username = Rq.ServerVariables["REMOTE_USER"];
                //res = Lm.NetKeySessionLogin(strAppName, "", Ekc.KeyCheck.CHECK_DUPLICATE_LOGIN_USER, username, Rq.ServerVariables["REMOTE_USER"], // in case of checking for duplicate login user
                res = Lm.NetKeySessionLogin(strAppName, "", 0, username, Rq.ServerVariables["REMOTE_USER"],
                        Rq.ServerVariables["LOCAL_ADDR"], Rq.ServerVariables["REMOTE_ADDR"],
                        strLoginUser, strSessionID, nSessionTimeout);
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            UpdateProperties();
            return res;
        }

        public bool NKLSLogout(string strLoginUser, string strSessionID)
        {
            bool res = false;
            res = Lm.NetKeySessionLogout(Rq.ServerVariables["LOCAL_ADDR"], Rq.ServerVariables["REMOTE_ADDR"], strLoginUser, strSessionID);
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool NKLSRefresh(string strLoginUser, string strSessionID, int nInterval)
        {
            bool res = false;
            res = Lm.NetKeySessionRefresh(Rq.ServerVariables["LOCAL_ADDR"], Rq.ServerVariables["REMOTE_ADDR"], strLoginUser, strSessionID);
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool NKLSUpdate(string strLoginUser, string strSessionID, int nInterval)
        {
            bool res = false;
            res = Lm.NetKeySessionRefresh(Rq.ServerVariables["LOCAL_ADDR"], Rq.ServerVariables["REMOTE_ADDR"], strLoginUser, strSessionID);
            if (res)
            {
                res = Lm.UpdateLimitKey();
                UpdateProperties();
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool GetKeyProperties()
        {
            bool res = false;
            res = Lm.GetKeyProperties();
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            UpdateProperties();
            return res;
        }

        public bool GetRegistrationID()
        {
            bool res = false;
            res = Lm.GetRegID(0);
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            m_strRegistrationID = Lm.RegistrationID;
            return res;
        }

        public bool InitLicense()
        {
            bool res = false;
            res = Lm.InitLicKey();
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            m_strRegistrationID = Lm.RegistrationID;
            return res;
        }

        public bool ManualActivate(string strLicenseKey)
        {
            bool res = false;
            if (Lm.PutLicKey(strLicenseKey))
            {
                Lm.UpdateLimitKey();
                UpdatePermission();
                res = true;
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool AutoActivate(string strActivationKey)
        {
            bool res = false;
            if (Lm.AutoActivate(strActivationKey))
            {
                Lm.UpdateLimitKey();
                UpdatePermission();
                res = true;
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool ManualTransfer(string strRegID)
        {
            bool res = false;
            m_strLicKeyTran = "";
            if (Lm.TransferLicense(strRegID))
            {
                m_strLicKeyTran = Lm.LicKeyTran;
                res = true;
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool AutoTransfer(string strActivationKey)
        {
            bool res = false;
            if (Lm.AutoTransfer(strActivationKey))
            {
                res = true;
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool ManualDestroy()
        {
            bool res = false;
            m_strDestroyCode = "";
            if (Lm.DestroyLicense())
            {
                m_strDestroyCode = Lm.DestroyCode;
                res = true;
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool AutoDestroy(string strActivationKey)
        {
            bool res = false;
            if (Lm.AutoTransfer(strActivationKey))
            {
                res = true;
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }

        public bool TerminateAgent()
        {
            bool res = false;
            if (Lm.TerminateKeySrv(LicenseKeyCheck.LOCAL_SRV))
            {
                res = true;
            }
            m_nErrCode = Lm.ErrCode;
            m_strErrStr = Lm.ErrStr;
            return res;
        }
    }
}