﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.Models
{
    public class LicenseAutoActivation : Ekasrv.ActivationService
    {
        public LicenseAutoActivation()
        {

        }

        public LicenseAutoActivation(string strServiceUrl)
        {
            m_strServiceUrl = strServiceUrl;
        }

        public bool GetActivationStatus(string strActivationKey)
        {
            InitResProp();
            return GetStatus(strActivationKey);
        }

        public bool AutoActivate(LicenseKeyCheck Kc, string strActivationKey)
        {
            InitResProp();
            if (Kc.InitLicKey())
            {
                if (ActivateLicense(strActivationKey, Kc.RegistrationID))
                {
                    if (Kc.PutLicKey(LicenseKey, LicUpgradeID))
                    {
                        return true;
                    }
                    else
                    {
                        m_nErrCode = Kc.ErrCode;
                        m_strErrStr = Kc.ErrStr;
                    }
                }
            }
            else
            {
                m_nErrCode = Kc.ErrCode;
                m_strErrStr = Kc.ErrStr;
            }
            return false;
        }
        public bool AutoReactivate(LicenseKeyCheck Kc, string strActivationKey)
        {
            InitResProp();
            if (Kc.InitLicKey())
            {
                if (GetStatus(strActivationKey))
                {
                    if ((ActivationMode == MODE_LICENSE_REPLACEMENT_OR_DEACTIVATION) ||
                        (ActivationMode == MODE_LICENSE_DEACTIVATION) ||
                        (ActivationMode == MODE_LICENSE_REACTIVATION))
                    {
                        if (Kc.GetKeyProperties())
                        {
                            if (CheckUserID(strActivationKey, Kc.UserID.ToString()))
                            {
                                if (Kc.TransferLicense(RegIDTran))
                                {
                                    if (Kc.InitLicKey())
                                    {
                                        if (ReactivateLicense(strActivationKey, Kc.LicKeyTran, Kc.RegistrationID))
                                        {
                                            if (Kc.PutLicKey(LicenseKey, LicUpgradeID))
                                            {
                                                return true;
                                            }
                                            else
                                            {
                                                m_nErrCode = Kc.ErrCode;
                                                m_strErrStr = Kc.ErrStr;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        m_nErrCode = Kc.ErrCode;
                                        m_strErrStr = Kc.ErrStr;
                                    }
                                }
                                else
                                {
                                    m_nErrCode = Kc.ErrCode;
                                    m_strErrStr = Kc.ErrStr;
                                }
                            }
                        }
                        else
                        {
                            m_nErrCode = Kc.ErrCode;
                            m_strErrStr = Kc.ErrStr;
                        }
                    }
                    else
                        m_strErrStr = "Reactivation not allowed";
                }
            }
            else
            {
                m_nErrCode = Kc.ErrCode;
                m_strErrStr = Kc.ErrStr;
            }
            return false;
        }
        public bool AutoActivateUpgrade(LicenseKeyCheck Kc, string strActivationKey)
        {
            InitResProp();
            if (Kc.InitLicKey())
            {
                if (GetStatus(strActivationKey))
                {
                    if (Kc.IsRegistered())
                    {
                        if (CheckUserID(strActivationKey, Kc.UserID.ToString()))
                        {
                            if ((ActivationMode == MODE_LICENSE_REACTIVATION) ||
                                (LicUpgradeID != Kc.LicUpgradeID))
                            {   // To reactivate license
                                if (Kc.TransferLicense(RegIDTran))
                                {
                                    if (Kc.InitLicKey())
                                    {
                                        if (ReactivateLicense(strActivationKey, Kc.LicKeyTran, Kc.RegistrationID))
                                        {
                                            if (Kc.PutLicKey(LicenseKey, LicUpgradeID))
                                            {
                                                m_strErrStr = "Upgrade Complete";
                                                return true;
                                            }
                                            else
                                            {
                                                m_nErrCode = Kc.ErrCode;
                                                m_strErrStr = Kc.ErrStr;
                                                return false;
                                            }
                                        }
                                        else
                                            return false;
                                    }
                                    else
                                    {
                                        m_nErrCode = Kc.ErrCode;
                                        m_strErrStr = Kc.ErrStr;
                                        return false;
                                    }
                                }
                                else
                                {
                                    m_nErrCode = Kc.ErrCode;
                                    m_strErrStr = Kc.ErrStr;
                                    return false;
                                }
                                //
                            }
                            else
                            {
                                m_nErrCode = 0;
                                m_strErrStr = "License Complete";
                                return true;
                            }
                        }
                    }
                    // To activate license
                    if (ActivateLicense(strActivationKey, Kc.RegistrationID))
                    {
                        if (Kc.PutLicKey(LicenseKey, LicUpgradeID))
                        {
                            m_strErrStr = "Activation Complete";
                            return true;
                        }
                        else
                        {
                            m_nErrCode = Kc.ErrCode;
                            m_strErrStr = Kc.ErrStr;
                        }
                    }
                    //
                }
            }
            else
            {
                m_nErrCode = Kc.ErrCode;
                m_strErrStr = Kc.ErrStr;
            }
            return false;
        }
        public bool AutoTransfer(LicenseKeyCheck Kc, string strActivationKey)
        {
            InitResProp();
            if (Kc.InitLicKey())
            {
                if (GetStatus(strActivationKey))
                {
                    if ((ActivationMode == MODE_LICENSE_REPLACEMENT_OR_DEACTIVATION) || (ActivationMode == MODE_LICENSE_DEACTIVATION))
                    {
                        if (Kc.GetKeyProperties())
                        {
                            if (CheckUserID(strActivationKey, Kc.UserID.ToString()))
                            {
                                if (Kc.TransferLicense(RegIDTran))
                                {
                                    if (DeactivateLicense(strActivationKey, Kc.LicKeyTran))
                                    {
                                        return true;
                                    }
                                }
                                else
                                {
                                    m_nErrCode = Kc.ErrCode;
                                    m_strErrStr = Kc.ErrStr;
                                }
                            }
                            else
                                m_strErrStr = "User ID mismatch";
                        }
                        else
                        {
                            m_nErrCode = Kc.ErrCode;
                            m_strErrStr = Kc.ErrStr;
                        }
                    }
                    else
                        m_strErrStr = "Transfer not allowed";
                }
            }
            else
            {
                m_nErrCode = Kc.ErrCode;
                m_strErrStr = Kc.ErrStr;
            }
            return false;
        }

        public bool AutoDestroy(LicenseKeyCheck Kc, string strActivationKey)
        {
            InitResProp();
            if (Kc.GetKeyProperties())
            {
                if (CheckUserID(strActivationKey, Kc.UserID.ToString()))
                {
                    if (Kc.DestroyLicense())
                    {
                        if (ConfirmDestroyCode(strActivationKey, Kc.DestroyCode))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        m_nErrCode = Kc.ErrCode;
                        m_strErrStr = Kc.ErrStr;
                    }
                }
                else
                    m_strErrStr = "User ID mismatch";
            }
            else
            {
                m_nErrCode = Kc.ErrCode;
                m_strErrStr = Kc.ErrStr;
            }
            return false;
        }

    }
}