﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SciROLabABW.Web.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class SciROLabWebEntities : DbContext
    {
        public SciROLabWebEntities()
            : base("name=SciROLabWebEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<WBReferencesTable> WBReferencesTable { get; set; }
        public virtual DbSet<WBChangesTable> WBChangesTables { get; set; }
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<WBMainTable> WBMainTable { get; set; }
        public virtual DbSet<WBMessagesTable> WBMessagesTables { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<SciROLabGroup> SciROLabGroups { get; set; }
        public virtual DbSet<WBRowPermission> WBRowPermissions { get; set; }
        public virtual DbSet<MethodsTable> MethodsTables { get; set; }
        public virtual DbSet<SciROLabTeam> SciROLabTeams { get; set; }
        public virtual DbSet<AspNetUserSubscription> AspNetUserSubscriptions { get; set; }
        public virtual DbSet<SciROLabPhrases> SciROLabPhrases { get; set; }
    }
}
