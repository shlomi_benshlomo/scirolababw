//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SciROLabABW.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MethodsTable
    {
        public System.Guid methodId { get; set; }
        public string methodName { get; set; }
        public string text1Name { get; set; }
        public Nullable<bool> text1Display { get; set; }
        public string text2Name { get; set; }
        public Nullable<bool> text2Display { get; set; }
        public string date1Name { get; set; }
        public Nullable<bool> date1Display { get; set; }
        public string bool1Name { get; set; }
        public Nullable<bool> bool1Display { get; set; }
        public string text3Name { get; set; }
        public Nullable<bool> text3Display { get; set; }
        public string text4Name { get; set; }
        public Nullable<bool> text4Display { get; set; }
        public string date2Name { get; set; }
        public Nullable<bool> date2Display { get; set; }
        public string date3Name { get; set; }
        public Nullable<bool> date3Display { get; set; }
        public string bool2Name { get; set; }
        public Nullable<bool> bool2Display { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public string text5Name { get; set; }
        public Nullable<bool> text5Display { get; set; }
        public string text6Name { get; set; }
        public Nullable<bool> text6Display { get; set; }
        public string text7Name { get; set; }
        public Nullable<bool> text7Display { get; set; }
        public string text8Name { get; set; }
        public Nullable<bool> text8Display { get; set; }
        public string text9Name { get; set; }
        public Nullable<bool> text9Display { get; set; }
        public string text10Name { get; set; }
        public Nullable<bool> text10Display { get; set; }
        public string text11Name { get; set; }
        public Nullable<bool> text11Display { get; set; }
        public string text12Name { get; set; }
        public Nullable<bool> text12Display { get; set; }
        public string text13Name { get; set; }
        public Nullable<bool> text13Display { get; set; }
        public string text14Name { get; set; }
        public Nullable<bool> text14Display { get; set; }
        public string text15Name { get; set; }
        public Nullable<bool> text15Display { get; set; }
        public string bool3Name { get; set; }
        public Nullable<bool> bool3Display { get; set; }
        public string bool4Name { get; set; }
        public Nullable<bool> bool4Display { get; set; }
        public string bool5Name { get; set; }
        public Nullable<bool> bool5Display { get; set; }
        public string bool6Name { get; set; }
        public Nullable<bool> bool6Display { get; set; }
        public string bool7Name { get; set; }
        public Nullable<bool> bool7Display { get; set; }
        public string bool8Name { get; set; }
        public Nullable<bool> bool8Display { get; set; }
    }
}
