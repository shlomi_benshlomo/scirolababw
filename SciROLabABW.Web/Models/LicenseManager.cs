﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.Models
{
    class LicenseManager : LicenseKeyCheck // this class should be private
    {
        private LicenseAutoActivation Aa = null;
        public LicenseManager(int nModuleID)
        {
            Aa = new LicenseAutoActivation();

            KeyID = 1280241850; // Key ID
            ProgramID = 0;  // Program ID
            ModuleID = nModuleID;   // Module ID
            KeyLocation = LicenseKeyCheck.DEFAULT_KEY_LOCATION;    // Key Location
            InitLicenseKey = "8O7LUJMXCLIOI8OMKKNKFTI6LG";

            AppPath = AppDomain.CurrentDomain.BaseDirectory + @"bin\MyBusinessProcess.dll"; // Set the directory of the ElecKey system files.
            // Please place Ekc3220.dll, Ekc6420.dll, Nkag20.exe and NETKEY.INI to the Bin\ directory where the MyBusinessProcess.dll resides.

            /* Option to show Hardware Signatures
            RegIDFormat = KeyCheck.TRegIDFormat.fmtShowBIOSSig | KeyCheck.TRegIDFormat.fmtShowCPUSig | KeyCheck.TRegIDFormat.fmtShowEthernetSig | KeyCheck.TRegIDFormat.fmtShowHDDSig;
            */
            // Opening the Key service
            AgentStartup = "1"; // Set to run the agent without termination
            // Select the Network Key service.
            SrvIdx = LicenseKeyCheck.NETWORK_SRV;

            if (!OpenKeySrv(LicenseKeyCheck.NETWORK_SRV))
            {
                throw new InvalidOperationException(ErrStr);
            }
            
            // Connecting to the NetKey License Server
            if (!NetKeyConnect("NETKEY.INI"))
            {
                throw new InvalidOperationException(ErrStr);
            }
            // Initializing License Key
            if (!InitLicKey())
            {
                throw new Exception(ErrStr);
            }
            SetServerBackgroundCheck(10, 0);
        }
        public bool AutoActivate(string strActivationKey)
        {
            bool res = Aa.AutoActivateUpgrade(this, strActivationKey);
            m_nErrCode = Aa.ErrCode;
            m_strErrStr = Aa.ErrStr;
            return res;
        }
        public bool AutoTransfer(string strActivationKey)
        {
            bool res = Aa.AutoTransfer(this, strActivationKey);
            m_nErrCode = Aa.ErrCode;
            m_strErrStr = Aa.ErrStr;
            return res;
        }
        public bool AutoDestroy(string strActivationKey)
        {
            bool res = Aa.AutoDestroy(this, strActivationKey);
            m_nErrCode = Aa.ErrCode;
            m_strErrStr = Aa.ErrStr;
            return res;
        }
    }
}