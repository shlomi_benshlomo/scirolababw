﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;

namespace SciROLabABW.Web.Models
{
    public class LicenseKeyCheck
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct KEY_PROPERTIES
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3)]
            public string KeyType;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3)]
            public string LicenseMode;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 11)]
            public string UserID;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
            public string MaxInstall;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
            public string InstallCount;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
            public string MaxUser;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
            public string MaxExec;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
            public string ExecCount;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
            public string MaxDay;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
            public string DayCount;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
            public string ExpYear;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3)]
            public string ExpMonth;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3)]
            public string ExpDay;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 9)]
            public string Options;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 9)]
            public string Modules;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 65)]
            public string KeyData;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3)]
            public string MaxYear;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 3)]
            public string YearCount;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
            public string LicUpgradeID;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string Reserved;
        }
        public const string EKCDLL32 = "Ekc3220.dll";
        public const string EKCDLL64 = "Ekc6420.dll";
        public const int AGENT_MAJOR_VER = 2;
        public const int AGENT_MINOR_VER = 0;
        public const int AGENT_RELEASE_VER = 8;
        public const int AGENT_BUILD_VER = 24;
        public const int MAX_REGID = 240;
        public const int MAX_LICKEY = 240;
        public const int MAX_USER = 999;
        public const int MAX_INSTALL = 999;
        public const int MAX_INIT = 999;
        public const int MAX_EXEC = 9999;
        public const int MAX_DAY = 999;
        public const int MAX_DAY_PERIOD = -1;
        public const int MAX_MODULE = 30;
        public const int NUMDAYS_YEAR = 365;
        public const int LOCAL_SRV = 0;
        public const int NETWORK_SRV = 1;
        public const int DEFAULT_KEY_LOCATION = 0;
        public const int USB_KEY_LOCATION = 30;
        public const int DEFAULT_DIALOG_BOX = 0;
        public const int HTML_DIALOG_BOX = 1;
        public const int RESTRICT_TERMINAL_SERVER = 1;
        public const int CHECK_DUPLICATE_LOGIN_USER = 2;
        public const int NETKEY_TIMEOUT = 60000;
        public const int BLOCK_MULTI_PROCESSES = 0x00000001;
        public const int BLOCK_TERMINAL_SERVER = 0x00000002;
        public const int DETECTION_PROMPT = 0x00000020;
        public const int LOCAL_RESTRICT_TERMINAL_SERVER = 0x00000100;
        public const int LOCAL_RKU = 0x00000200;
        public const int NETWORK_RESTRICT_TERMINAL_SERVER = 0x00010000;
        public const int NETWORK_RKU = 0x00020000;
        public const int SILENT_MODE_FLAG = 0x00000001;
        public const int FAIL_IF_EXISTS = 0x00000001;
        public const int FAIL_IF_ALREADY = 0x00000001;
        public const int CHECK_READY_UPGRADE = 0x00000002;
        public const int INST_UPGRADE_LICENSE = 0x00000004;
        public const int OPT_LICENSE_UPGRADE = 0x00000040;
        public const int OPT_DONGLE_WITH_MACHINE = 0x00000010;
        public const int OPT_NON_GENUINE_FLAG = 0x40000000;
        public const uint NO_UPDATING_EXEC_COUNT = 1;
        //--- Common error codes:
        // Success
        public const int SUCCESS = 0;               // "The operation completed successfully"
        // Expiration and limitation
        public const int EXPIRED_DATE = 37;         // "The program has expired" The protected application has expired or reached the limit.]
        public const int ERR_INVALID_DATE = 294;    // "Invalid date" The system clock has been set back to the date before the most recent date stamp in the Key. This error occurs when the protected application is under an evaluation or leasing license with a date limit or an expiration date.
        public const int EXPIRED_EXEC = 53;         // "The program has reached its execution limit" The protected application has reached its execution limit.
        public const int ERR_MODULE = 68;           // "The module is not licensed to execute" This error occurs when the particular module program is not licensed, as specified in the Key.
        public const int ERR_NOT_ALLOW_REEVAL = 42; // "Not allowed to re-evaluate" The evaluation is over. Re-evaluation is not allowed.
        // Hardware detection
        public const int ERR_LOAD_KEYDEVICE = 315;  // "Error loading Key device" The system cannot get a hardware signature.
        public const int ERR_KEYDEVICE = 316;       // "Invalid Key device" A hardware signature has changed and does not match the one saved in the Key.
        public const int ERR_MATCH_KEYDEVICE = 317; // "Error matching Key device" A hardware signature has changed and does not match the one saved in the Key.
        public const int ERR_DISK = 262;            // "Invalid Key" The Key is invalid.
        // Key interface
        public const int ERR_LOAD_KEY = 283;        // "Key not found" The system cannot find the Key, or the Key is invalid.
        public const int ERR_WRITE_KEY = 286;       // "Error writing Key" The system cannot write to the Key.
        public const int ERR_MAKE_KEY = 289;        // "Error making Key"  The system cannot make the Key to the hard drive.
        public const int ERR_DESTROY_KEY = 292;     // "Error destroying Key" The Destroy utility cannot destroy the Key.
        // Licensing
        public const int ERR_LICKEY = 63;           // "Invalid License Key" The entered License Key is invalid.
        public const int ERR_REGID = 62;            // "Invalid Registration ID" This error occurs when entering an invalid Registration ID to the Transfer utility.
        public const int ERR_LICENSING = 74;        // "License is required" This error usually occurs on the development machine in which testing the protected programs cause the Key to be overwritten improperly.
        //   For example, you protect the first program with 30-day evaluation; and then, protect the second program using the same Program ID with 15-day evaluation.
        //   During the test, it is recommended to keep Key Inspector running. Key Inspector allows you to see all the Keys installed on your machine.
        //   A good practice is to delete the Keys before beginning a new test. This ensures that the machine is in a fresh state (with no Key) like the end-user's computer.
        // Virtual Machine detection
        public const int ERR_NOT_ALLOW_VM = 44;     // "The license of the application is not allowed on Virtual Machine" The Key is not created on virtual machines and the protected application is not running. See the Disable making Key on Virtual Machine.
        // Process check
        public const int ERR_ALREADY_RUN = 65;      // "The program is already running" The system detects another process of the protected application. This error occurs when the system is set to block multiple processes.
        public const int ERR_ILLEGAL_OPR = 66;      // "Illegal operation" The protected application is running under Terminal Services. This error occurs when the system is set to restrict or block Terminal Services.
        // USB License (Dongle) detection
        public const int ERR_KEY_NOT_FOUND = 18;    // "Key not found" The system cannot find the USB License Key, or the Key is invalid.
        // USB License installation and initialization
        public const int ERR_INSTALL = 12;          // "Key installation is not available"
        public const int ERR_INIT = 94;             // "Invalid node-locked dongle"
        public const int ERR_KEY_EXISTS = 529;      // "Key already exists"
        public const int ERR_INIT_EXISTS = 607;     // "The USB dongle is already initialized"
        // Evaluation reminder
        public const int ERR_UNREG_VER = 77;        // "Unregistered version" The background process calls the ResultProc with this error code to perform an evaluation reminder.
        // NetKey License Server
        public const int ERR_NETKEY_CONFIG = 80;    // "Error reading Network Key configuration file" The system cannot read the Network Key configuration file (NETKEY.INI).
        public const int ERR_NETKEY_CONNECT = 337;  // "Cannot connect to the NetKey License Server" This error is usually caused by a network connection problem, for example, incorrect IP address or port number of the NetKey License Server.
        public const int ERR_NETKEY_KEY = 338;      // "Invalid Network Key" The system connects to the NetKey License Server, but cannot find the Network Key.
        public const int ERR_NETKEY_COUNTER = 83;   // "Number of concurrent network users has reached the limit" Number of concurrent network users of the protected application has reached the limit.
        public const int ERR_NETKEY_LOCAL = 84;     // "The client is out of the local network" This error occurs when the NetKey License Server is set to allow only the connection from the clients on the same subnet. The error may be caused by the following situations:
        //   The client on another subnet attempts to run the protected application.
        //   Or, the client attempts to run the protected application in which it connects through a proxy server.
        //   Or, the server connects 2 subnets (via 2 Ethernet adapters). In this case, the subnet connected by the client first is considered the allowed subnet. The other subnet is blocked.
        public const int ERR_NETKEY_PRIVATE = 85;   // "The client is out of the private network" The client on a public network (using the public IP address) attempts to run the protected application, where only the clients on private networks (using the private IP address) are allowed.
        public const int ERR_NETKEY_ACCESS = 86;    // "Unauthorized user is not allowed to access" An unauthorized client (its IP address is not listed in the Server Properties) attempts to run the protected application.
        public const int ERR_NETKEY_IDENT = 87;     // "Network Key identification error" The system connects to the NetKey License Server, but cannot identify the Network Key. This error occurs when the NetKey License Server is serving another Network Key.
        // Internal error
        public const int EXCEPTION_ERR = 90;
        public const int UNKNOWN_ERR = 99;

        public enum TRegIDFormat
        {
            fmtDefault = 0x00000000,
            fmtShowAllSig = 0x0000000F,
            fmtShowHDDSig = 0x00000001,
            fmtShowCPUSig = 0x00000002,
            fmtShowBIOSSig = 0x00000004,
            fmtShowEthernetSig = 0x00000008,
            fmtShowLicenseStatus = 0x00000100
        }
        public enum TErrType
        {
            errSuccess = 0,
            errError = 1,
            errExpire = 2,
            errSystemChange = 3,
            errLicenseRequired = 4,
            errKeyExists = 5,
            errKeyNotFound = 6
        }

        public enum TInstantType
        {
            insDetection = 0,
            insDetectionNoDlg = 0x0040,
            insDetectionSilent = 0x0080,
            insCheckIsRegistered = 0x0010,
            insUtilRegister = 0x0100,
            insUtilTransfer = 0x0200,
            insUtilDestroy = 0x0300,
            insUtilRemake = 0x0400
        }

        public enum TInternalKeyCmd
        {
            inkSetNonGenuineFlag = 0x00000001,
            inkInvalidateLicense = 0x00000002
        };

        public enum TValidationCmd
        {
            vldAutoLicenseUpdateValidation = 0x00000001,
            vldGenuineLicenseValidation = 0x00000002,	// bit-30 of the Options property is set if not genuine
            vldInvalidateLicenseIfNotGenuine = 0x00000004,
            vldServerConnectionRequiredEvery001Day = 0x00000100,
            vldServerConnectionRequiredEvery002Days = 0x00000200,
            vldServerConnectionRequiredEvery003Days = 0x00000300,
            vldServerConnectionRequiredEvery004Days = 0x00000400,
            vldServerConnectionRequiredEvery005Days = 0x00000500,
            vldServerConnectionRequiredEvery006Days = 0x00000600,
            vldServerConnectionRequiredEvery007Days = 0x00000700,
            vldServerConnectionRequiredEvery030Days = 0x00000900,
            vldServerConnectionRequiredEvery060Days = 0x00000a00,
            vldServerConnectionRequiredEvery090Days = 0x00000b00,
            vldServerConnectionRequiredEvery120Days = 0x00000c00,
            vldServerConnectionRequiredEvery150Days = 0x00000d00,
            vldServerConnectionRequiredEvery180Days = 0x00000e00,
            vldServerConnectionRequiredEvery210Days = 0x00000f00
        };

        public enum TBackgroundCheckCmd
        {
            bgcDefault = 0,
            bgcCheckAtBeginning = 0x001,
            bgcCallbackAllResults = 0x002,
            bgcEvaluationReminder01Minute = 0x014,
            bgcEvaluationReminder03Minutes = 0x034,
            bgcEvaluationReminder05Minutes = 0x054,
            bgcEvaluationReminder10Minutes = 0x0A4,
            bgcEvaluationReminder15Minutes = 0x0F4,
            bgcEvaluationReminder20Minutes = 0x144,
            bgcEvaluationReminder30Minutes = 0x1E4,
            bgcEvaluationReminder01Hour = 0x010
        };

        public

            delegate void cbResultProc(ushort nType, ushort nErrCode, string strErrStr);
        public delegate void evResultProc(int nType, int nErrCode, string strErrStr);
        static event evResultProc OnResult;
        static event evResultProc OnClientResult;
        static cbResultProc fnCallBack;
        static cbResultProc fnClientCallBack;

        public LicenseKeyCheck()
        {
            m_i64Items = new UInt64[4];

            InitProp();
            InitKeyProp();
            InitResProp();

            LastResultProc = null;
            fnCallBack = new cbResultProc(ResultProc);

            LastClientResultProc = null;
            fnClientCallBack = new cbResultProc(ClientResultProc);

        }
        public LicenseKeyCheck(uint nKeyID, int nProgramID, int nModuleID, int nKeyLocation,
                            string strInitLicenseKey, string strAppPath)
        {
            m_i64Items = new UInt64[4];

            InitProp();
            InitKeyProp();
            InitResProp();

            KeyID = nKeyID;
            ProgramID = nProgramID;
            ModuleID = nModuleID;
            KeyLocation = nKeyLocation;
            InitLicenseKey = strInitLicenseKey;
            AppPath = strAppPath;

            LastResultProc = null;
            fnCallBack = new cbResultProc(ResultProc);

            LastClientResultProc = null;
            fnClientCallBack = new cbResultProc(ClientResultProc);


        }

        public LicenseKeyCheck(uint nKeyID, int nProgramID, int nModuleID, int nKeyLocation,
                            string strInitLicenseKey, string strAppPath, string strKeyValueCode)
        {

            InitProp();
            InitKeyProp();
            InitResProp();

            KeyID = nKeyID;
            ProgramID = nProgramID;
            ModuleID = nModuleID;
            KeyLocation = nKeyLocation;
            InitLicenseKey = strInitLicenseKey;
            AppPath = strAppPath;
            KeyValueCode = strKeyValueCode;

            LastResultProc = null;
            fnCallBack = new cbResultProc(ResultProc);

            LastClientResultProc = null;
            fnClientCallBack = new cbResultProc(ClientResultProc);


        }

        // Main Functions
        public bool OpenKeySrv(int nSrvType)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncOpenKeySrv64((ushort)nSrvType, (uint)m_lKeyID, (ushort)m_nProgramID);
                else
                    m_nErrCode = (int)fncOpenKeySrv32((ushort)nSrvType, (uint)m_lKeyID, (ushort)m_nProgramID);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public void CloseKeySrv(int nSrvType)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncCloseKeySrv64((ushort)nSrvType);
                else
                    m_nErrCode = (int)fncCloseKeySrv32((ushort)nSrvType);
                m_strErrStr = GetErrStr(m_nErrCode);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
            }
        }

        public bool TerminateKeySrv(int nSrvType)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncTerminateKeySrv64((ushort)nSrvType);
                else
                    m_nErrCode = (int)fncTerminateKeySrv32((ushort)nSrvType);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public uint SetKeyCache(uint dwFlag)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    return (uint)fncSetKeyCache64((ushort)m_nSrvIdx, (ushort)dwFlag);
                else
                    return (uint)fncSetKeyCache32((ushort)m_nSrvIdx, (ushort)dwFlag);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return EXCEPTION_ERR;
            }
        }

        public string GetErrStr(int nErrCode)
        {
            try
            {
                StringBuilder cstrBuf = new StringBuilder(128);

                if (isProcess64())
                    fncGetErrStr64((ushort)m_nSrvIdx, (ushort)nErrCode, cstrBuf);
                else
                    fncGetErrStr32((ushort)m_nSrvIdx, (ushort)nErrCode, cstrBuf);

                return cstrBuf.ToString();
            }
            catch
            {
                return "";
            }
        }
        public int GetLastErrCode()
        {
            try
            {
                int nLastErrCode;
                if (isProcess64())
                    nLastErrCode = fncGetLastErrCode64();
                else
                    nLastErrCode = fncGetLastErrCode32();

                if (nLastErrCode == EXCEPTION_ERR)
                    HandleException(GetLastErrStr());

                return nLastErrCode;
            }
            catch
            {
                return EXCEPTION_ERR;
            }
        }

        public string GetLastErrStr()
        {
            try
            {
                StringBuilder cstrBuf = new StringBuilder(128);

                if (isProcess64())
                    fncGetLastErrStr64(cstrBuf);
                else
                    fncGetLastErrStr32(cstrBuf);

                return cstrBuf.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // Licensing Functions
        public bool InitLicKey()
        {
            try
            {
                StringBuilder cstrRegIDBuf = new StringBuilder(MAX_REGID + 1);
                InitResProp();
                m_strRegistrationID = "";
                if (isProcess64())
                    m_nErrCode = (int)fncInitLicKeyEx64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, m_strInitLicenseKey, cstrRegIDBuf, (ushort)m_dwRegIDFormat);
                else
                    m_nErrCode = (int)fncInitLicKeyEx32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, m_strInitLicenseKey, cstrRegIDBuf, (ushort)m_dwRegIDFormat);
                //
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_strRegistrationID = cstrRegIDBuf.ToString();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool GetRegID(uint dwRegIDFlag)
        {
            try
            {
                StringBuilder cstrRegIDBuf = new StringBuilder(MAX_REGID + 1);
                InitResProp();
                m_strRegistrationID = "";
                if (isProcess64())
                    m_nErrCode = (int)fncGetRegID64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, cstrRegIDBuf, (ushort)((ushort)m_dwRegIDFormat | (ushort)dwRegIDFlag));
                else
                    m_nErrCode = (int)fncGetRegID32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, cstrRegIDBuf, (ushort)((ushort)m_dwRegIDFormat | (ushort)dwRegIDFlag));
                //
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_strRegistrationID = cstrRegIDBuf.ToString();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool IsRegistered()
        {
            if (!GetKeyProperties())
                return false;
            if (LicenseMode == 0)
                return false;
            return true;
        }

        public bool CheckLicKey(string strLicKey)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncCheckLicKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strLicKey);
                else
                    m_nErrCode = (int)fncCheckLicKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strLicKey);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool PutLicKey(string strLicKey)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncPutLicKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strLicKey);
                else
                    m_nErrCode = (int)fncPutLicKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strLicKey);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool PutLicKey(string strLicKey, int nLicUpgradeID)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncPutLicKeyEx64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strLicKey, (ushort)nLicUpgradeID, 0);
                else
                    m_nErrCode = (int)fncPutLicKeyEx32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strLicKey, (ushort)nLicUpgradeID, 0);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool TransferLicense(string strRegIDTran)
        {
            try
            {
                StringBuilder cstrLicKeyBuf = new StringBuilder(MAX_LICKEY + 1);
                InitResProp();
                m_strLicKeyTran = "";
                if (isProcess64())
                    m_nErrCode = (int)fncTransferLicense64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation,
                        strRegIDTran, cstrLicKeyBuf);
                else
                    m_nErrCode = (int)fncTransferLicense32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation,
                        strRegIDTran, cstrLicKeyBuf);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_strLicKeyTran = cstrLicKeyBuf.ToString();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        //	Key Interface Functions
        public bool CheckKey()
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncCheckKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation);
                else
                    m_nErrCode = (int)fncCheckKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool GetKeyProperties()
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncGetKeyProperties64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, KeyPropPtr);
                else
                    m_nErrCode = (int)fncGetKeyProperties32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, KeyPropPtr);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public string GetKeyData(int nDataAddr, int nDataCount)
        {
            try
            {
                StringBuilder cstrDataBuf = new StringBuilder(65);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncGetKeyData64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (ushort)nDataAddr, (ushort)nDataCount, cstrDataBuf);
                else
                    m_nErrCode = (int)fncGetKeyData32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (ushort)nDataAddr, (ushort)nDataCount, cstrDataBuf);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    return cstrDataBuf.ToString().Substring(0, nDataCount);
                }
                return "";
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return "";
            }
        }

        public bool PutKeyData(int nDataAddr, int nDataCount, string strDataIn)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncPutKeyData64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (ushort)nDataAddr, (ushort)nDataCount, strDataIn);
                else
                    m_nErrCode = (int)fncPutKeyData32(
                                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                                        (ushort)m_nKeyLocation, (ushort)nDataAddr, (ushort)nDataCount, strDataIn);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        // Overload
        public bool UpdateLimitKey()
        {
            return UpdateLimitKey(0);
        }

        public bool UpdateLimitKey(uint dwCmd)
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncUpdateLimitKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (ushort)dwCmd, KeyPropPtr);
                else
                    m_nErrCode = (int)fncUpdateLimitKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (ushort)dwCmd, KeyPropPtr);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        // Overload
        public bool UpdateLimitKeyAndProcessKeyValue(ushort wKeyOfKeyValue, ref ushort wValueOfKeyValue)
        {
            return UpdateLimitKeyAndProcessKeyValue(wKeyOfKeyValue, ref wValueOfKeyValue, 0);
        }

        public bool UpdateLimitKeyAndProcessKeyValue(ushort wKeyOfKeyValue, ref ushort wValueOfKeyValue, uint dwCmd)
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncUpdateLimitKeyAndProcessKeyValue64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (ushort)dwCmd, KeyPropPtr, m_strKeyValueCode, wKeyOfKeyValue, ref wValueOfKeyValue);
                else
                    m_nErrCode = (int)fncUpdateLimitKeyAndProcessKeyValue32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (ushort)dwCmd, KeyPropPtr, m_strKeyValueCode, wKeyOfKeyValue, ref wValueOfKeyValue);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        //
        // Overload
        public bool UpdateRemoteKey(string strRKUFile)
        {
            return UpdateRemoteKey(strRKUFile, 0);
        }

        public bool UpdateRemoteKey(string strRKUFile, uint dwReserved)
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncUpdateRemoteKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strRKUFile, (ushort)dwReserved, KeyPropPtr);
                else
                    m_nErrCode = (int)fncUpdateRemoteKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strRKUFile, (ushort)dwReserved, KeyPropPtr);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        // Overload
        public bool SetBackgroundCheck(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd)
        {
            try
            {
                InitResProp();

                if (isProcess64())
                    m_nErrCode = (int)fncSetBackgroundCheckDef64(
                            (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                            (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, null);
                else
                    m_nErrCode = (int)fncSetBackgroundCheckDef32(
                            (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                            (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, null);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool SetBackgroundCheck(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd, evResultProc Event)
        {
            try
            {
                if (LastResultProc == null)
                    LastResultProc = Event;
                else
                    OnResult -= LastResultProc;
                OnResult += Event;
                InitResProp();

                if (isProcess64())
                    m_nErrCode = (int)fncSetBackgroundCheck64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, fnCallBack);
                else
                    m_nErrCode = (int)fncSetBackgroundCheck32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, fnCallBack);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool SetBackgroundCheckAndValidation(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd, string strWebServiceURL, uint lKeyIDEx1, uint lUserID, TValidationCmd dwValidationCmd)
        {
            try
            {
                dwValidationCmd |= (TValidationCmd)((uint)m_dwRegIDFormat << 12);
                if (isProcess64())
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL64((ushort)m_nSrvIdx, strWebServiceURL);
                    fncSetAgentUserID64((ushort)m_nSrvIdx, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                else
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL32((ushort)m_nSrvIdx, strWebServiceURL);
                    fncSetAgentUserID32((ushort)m_nSrvIdx, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                return SetBackgroundCheck(nIntervals, dwBackgroundCheckCmd);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool SetBackgroundCheckAndValidation(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd, evResultProc Event, string strWebServiceURL, uint lKeyIDEx1, uint lUserID, TValidationCmd dwValidationCmd)
        {
            try
            {
                dwValidationCmd |= (TValidationCmd)((uint)m_dwRegIDFormat << 12);
                if (isProcess64())
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL64((ushort)m_nSrvIdx, strWebServiceURL);
                    fncSetAgentUserID64((ushort)m_nSrvIdx, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                else
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL32((ushort)m_nSrvIdx, strWebServiceURL);
                    fncSetAgentUserID32((ushort)m_nSrvIdx, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                return SetBackgroundCheck(nIntervals, dwBackgroundCheckCmd, Event);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        // Callback
        private void ResultProc(ushort sType, ushort ErrCodeIn, string ErrStrIn)
        {
            OnResult((int)sType, (int)ErrCodeIn, ErrStrIn);
        }

        private void ClientResultProc(ushort sType, ushort ErrCodeIn, string ErrStrIn)
        {
            OnClientResult((int)sType, (int)ErrCodeIn, ErrStrIn);
        }

        // Overload
        public bool SetClientBackgroundCheck(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd)
        {
            try
            {

                InitResProp();

                if (isProcess64())
                    m_nErrCode = (int)fncSetBackgroundCheckDef64(
                            (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                            (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, null);
                else
                    m_nErrCode = (int)fncSetBackgroundCheckDef32(
                            (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                            (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, null);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool SetClientBackgroundCheck(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd, evResultProc Event)
        {
            try
            {
                if (LastClientResultProc == null)
                    LastClientResultProc = Event;
                else
                    OnClientResult -= LastClientResultProc;
                OnClientResult += Event;
                InitResProp();

                if (isProcess64())
                    m_nErrCode = (int)fncSetBackgroundCheck64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, fnClientCallBack);
                else
                    m_nErrCode = (int)fncSetBackgroundCheck32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)dwBackgroundCheckCmd, fnClientCallBack);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }

        }

        public bool SetClientBackgroundCheckAndValidation(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd, string strWebServiceURL, uint lKeyIDEx1, uint lUserID, TValidationCmd dwValidationCmd)
        {
            try
            {
                dwValidationCmd |= (TValidationCmd)((uint)m_dwRegIDFormat << 12);
                if (isProcess64())
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL64((ushort)NETWORK_SRV, strWebServiceURL);
                    fncSetAgentUserID64((ushort)NETWORK_SRV, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                else
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL32((ushort)NETWORK_SRV, strWebServiceURL);
                    fncSetAgentUserID32((ushort)NETWORK_SRV, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                return SetClientBackgroundCheck(nIntervals, dwBackgroundCheckCmd);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool SetClientBackgroundCheckAndValidation(uint nIntervals, TBackgroundCheckCmd dwBackgroundCheckCmd, evResultProc Event, string strWebServiceURL, uint lKeyIDEx1, uint lUserID, TValidationCmd dwValidationCmd)
        {
            try
            {
                dwValidationCmd |= (TValidationCmd)((uint)m_dwRegIDFormat << 12);
                if (isProcess64())
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL64((ushort)NETWORK_SRV, strWebServiceURL);
                    fncSetAgentUserID64((ushort)NETWORK_SRV, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                else
                {
                    if (strWebServiceURL != "")
                        fncInitWebServiceURL32((ushort)NETWORK_SRV, strWebServiceURL);
                    fncSetAgentUserID32((ushort)NETWORK_SRV, lKeyIDEx1, lUserID.ToString(), (ushort)dwValidationCmd);
                }
                return SetClientBackgroundCheck(nIntervals, dwBackgroundCheckCmd, Event);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        // Overload
        public bool RemakeLicense()
        {
            return RemakeLicense(0);
        }

        public bool RemakeLicense(uint dwCmd)
        {
            try
            {
                StringBuilder cstrRegIDBuf = new StringBuilder(MAX_REGID + 1);
                InitResProp();
                m_strRegistrationID = "";
                if (isProcess64())
                    m_nErrCode = (int)fncRemakeLicenseEx64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, m_strInitLicenseKey, cstrRegIDBuf, (ushort)dwCmd, (ushort)m_dwRegIDFormat);
                else
                    m_nErrCode = (int)fncRemakeLicenseEx32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, m_strInitLicenseKey, cstrRegIDBuf, (ushort)dwCmd, (ushort)m_dwRegIDFormat);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_strRegistrationID = cstrRegIDBuf.ToString();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool DestroyLicense()
        {
            try
            {
                StringBuilder cstrDesCodeBuf = new StringBuilder(MAX_LICKEY + 1);
                InitResProp();
                m_strDestroyCode = "";
                if (isProcess64())
                    m_nErrCode = (int)fncDestroyLicense64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID,
                        (ushort)m_nKeyLocation, cstrDesCodeBuf);
                else
                    m_nErrCode = (int)fncDestroyLicense32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID,
                        (ushort)m_nKeyLocation, cstrDesCodeBuf);
                m_strErrStr = GetErrStr(m_nErrCode);

                if (m_nErrCode == SUCCESS)
                {
                    m_strDestroyCode = cstrDesCodeBuf.ToString();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool InstallKey(int nHDKeyLocation)
        {
            return InstallKey(nHDKeyLocation, 0);
        }

        public bool InstallKey(int nHDKeyLocation, uint dwInstFlag)
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncInstallKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation, (ushort)nHDKeyLocation,
                        KeyPropPtr, (ushort)dwInstFlag);
                else
                    m_nErrCode = (int)fncInstallKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation, (ushort)nHDKeyLocation,
                        KeyPropPtr, (ushort)dwInstFlag);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool RemoveKey(int nHDKeyLocation)
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncRemoveKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation, (ushort)nHDKeyLocation,
                        KeyPropPtr, 0);
                else
                    m_nErrCode = (int)fncRemoveKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation, (ushort)nHDKeyLocation,
                        KeyPropPtr, 0);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool InitMachSig()
        {
            return InitMachSig(0);
        }

        public bool InitMachSig(uint dwInitFlag)
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncInitMachSig64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation,
                        KeyPropPtr, (ushort)dwInitFlag);
                else
                    m_nErrCode = (int)fncInitMachSig32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation,
                        KeyPropPtr, (ushort)dwInitFlag);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }

        }

        public bool UninitMachSig()
        {
            try
            {
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncUninitMachSig64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation,
                        KeyPropPtr, 0);
                else
                    m_nErrCode = (int)fncUninitMachSig32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation,
                        KeyPropPtr, 0);
                KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                Marshal.FreeHGlobal(KeyPropPtr);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    KeyPropStrcToKeyProp(KeyPropStrc);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }

        }
        // Server Interface Functions
        // Overload
        public bool NetKeyConnect()
        {
            return NetKeyConnect(NETKEY_TIMEOUT);
        }
        public bool NetKeyConnect(int nTimeOut)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeyConnectEx64(
                        (ushort)NETWORK_SRV,
                        m_strServerAddress, m_nServerPort, m_strServerHost, "",
                        m_strServerAddress2, m_nServerPort2, m_strServerHost2, "",
                        m_strServerAddress3, m_nServerPort3, m_strServerHost3, "",
                        m_strServerAddress4, m_nServerPort4, m_strServerHost4, "",
                        nTimeOut, 0);
                else
                    m_nErrCode = (int)fncNetKeyConnectEx32(
                        (ushort)NETWORK_SRV,
                        m_strServerAddress, m_nServerPort, m_strServerHost, "",
                        m_strServerAddress2, m_nServerPort2, m_strServerHost2, "",
                        m_strServerAddress3, m_nServerPort3, m_strServerHost3, "",
                        m_strServerAddress4, m_nServerPort4, m_strServerHost4, "",
                        nTimeOut, 0);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool NetKeyConnect(string strConfigFile)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeyConnectAdv64(
                        (ushort)NETWORK_SRV,
                        strConfigFile);
                else
                    m_nErrCode = (int)fncNetKeyConnectAdv32(
                        (ushort)NETWORK_SRV,
                        strConfigFile);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public void NetKeyDisconnect()
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    fncNetKeyDisconnect64((ushort)NETWORK_SRV);
                else
                    fncNetKeyDisconnect32((ushort)NETWORK_SRV);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
            }
        }

        public bool NetKeyCheckConnection()
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeyCheckConnection64((ushort)NETWORK_SRV);
                else
                    m_nErrCode = (int)fncNetKeyCheckConnection32((ushort)NETWORK_SRV);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool NetKeyLogin(string strAppName, string strModuleName, uint dwCmd)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeyLogin64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strAppName, strModuleName, (ushort)dwCmd);
                else
                    m_nErrCode = (int)fncNetKeyLogin32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strAppName, strModuleName, (ushort)dwCmd);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_bLoggedIn = true;
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool NetKeyLogout()
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeyLogout64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation);
                else
                    m_nErrCode = (int)fncNetKeyLogout32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_bLoggedIn = false;
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool NetKeyCheckLogin()
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeyCheckLogin64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation);
                else
                    m_nErrCode = (int)fncNetKeyCheckLogin32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool NetKeySessionLogin(string strAppName, string strModuleName, uint dwCmd,
                string strUserName, string strComputerName, string strServerLocalAddr, string strServerRemoteAddr,
                string strLoginUserID, string strSessionID, int nSessionTimeout)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeySessionLogin64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strAppName, strModuleName, (ushort)dwCmd,
                        strUserName, strComputerName, strServerLocalAddr, strServerRemoteAddr,
                        strLoginUserID, strSessionID, nSessionTimeout);
                else
                    m_nErrCode = (int)fncNetKeySessionLogin32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, strAppName, strModuleName, (ushort)dwCmd,
                        strUserName, strComputerName, strServerLocalAddr, strServerRemoteAddr,
                        strLoginUserID, strSessionID, nSessionTimeout);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_bLoggedIn = true;
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool NetKeySessionLogout(string strServerLocalAddr, string strServerRemoteAddr,
                string strLoginUserID, string strSessionID)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeySessionLogout64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, 0,
                        strServerLocalAddr, strServerRemoteAddr,
                        strLoginUserID, strSessionID);
                else
                    m_nErrCode = (int)fncNetKeySessionLogout32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, 0,
                        strServerLocalAddr, strServerRemoteAddr,
                        strLoginUserID, strSessionID);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                {
                    m_bLoggedIn = false;
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool NetKeySessionRefresh(string strServerLocalAddr, string strServerRemoteAddr,
                string strLoginUserID, string strSessionID)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncNetKeySessionRefresh64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, 0,
                        strServerLocalAddr, strServerRemoteAddr,
                        strLoginUserID, strSessionID);
                else
                    m_nErrCode = (int)fncNetKeySessionRefresh32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, 0,
                        strServerLocalAddr, strServerRemoteAddr,
                        strLoginUserID, strSessionID);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        // Overload
        public bool SetServerBackgroundCheck(uint nIntervals, int nReserved)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncSetServerBackgroundCheck64(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID,
                        (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)nReserved);
                else
                    m_nErrCode = (int)fncSetServerBackgroundCheck32(
                        (ushort)NETWORK_SRV, (uint)m_lKeyID, (ushort)m_nProgramID,
                        (ushort)m_nKeyLocation, (uint)nIntervals, (ushort)nReserved);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        // Instant Functions
        public bool InstantProtection(IntPtr hWnd)
        {
            try
            {
                bool bRes;
                InitResProp();
                if (isProcess64())
                    bRes = fncInstantProtection64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        m_strSystemImageFile);
                else
                    bRes = fncInstantProtection32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        m_strSystemImageFile);
                if (!bRes)
                    GetLastErrCode();
                return bRes;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        // Overload
        public ushort InstantProtection(IntPtr hWnd, TInstantType tInstantType)
        {
            try
            {
                ushort res;
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    res = fncInstantProtectionEx64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        m_strSystemImageFile, (ushort)tInstantType, KeyPropPtr);
                else
                    res = fncInstantProtectionEx32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        m_strSystemImageFile, (ushort)tInstantType, KeyPropPtr);

                if (res == 0)
                {
                    KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                    KeyPropStrcToKeyProp(KeyPropStrc);
                }
                else if (res == 2)
                    GetLastErrCode();
                Marshal.FreeHGlobal(KeyPropPtr);
                return res;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return 2;
            }
        }

        // Overload
        public ushort InstantProtection(IntPtr hWnd, TInstantType tInstantType, string strParam)
        {
            try
            {
                ushort res;
                KEY_PROPERTIES KeyPropStrc = new KEY_PROPERTIES();
                IntPtr KeyPropPtr = Marshal.AllocHGlobal(Marshal.SizeOf(KeyPropStrc));
                Marshal.StructureToPtr(KeyPropStrc, KeyPropPtr, false);
                InitResProp();
                if (isProcess64())
                    res = fncInstantProtectionParam64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        m_strSystemImageFile, (ushort)tInstantType, strParam, KeyPropPtr);
                else
                    res = fncInstantProtectionParam32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        m_strSystemImageFile, (ushort)tInstantType, strParam, KeyPropPtr);

                if (res == 0)
                {
                    KeyPropStrc = (KEY_PROPERTIES)Marshal.PtrToStructure(KeyPropPtr, typeof(KEY_PROPERTIES));
                    KeyPropStrcToKeyProp(KeyPropStrc);
                }
                else if (res == 2)
                    GetLastErrCode();
                Marshal.FreeHGlobal(KeyPropPtr);
                return res;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return 2;
            }
        }

        // Overload
        public bool InstantLocalKey(IntPtr hWnd, int nDialogBoxStyle, int nKeyLocation)
        {
            try
            {
                InitResProp();
                uint dwHDLGFlag = 0;
                if (nDialogBoxStyle == 1)
                    dwHDLGFlag = 0x1000000;	// HTML DLG
                if (isProcess64())
                    return fncInstantLocalKeyDef64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation, m_strInitLicenseKey,
                        null, null,
                        null, null, null, null,
                        dwHDLGFlag, 0, 0,
                        "UPDATE.ENC", null);
                else
                    return fncInstantLocalKeyDef32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation, m_strInitLicenseKey,
                        null, null,
                        null, null, null, null,
                        dwHDLGFlag, 0, 0,
                        "UPDATE.ENC", null);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool InstantLocalKey(IntPtr hWnd, int nDialogBoxStyle, int nKeyLocation,
            string strLicText,
            string strPmtText,
            string strRegText,
            string strLimText,
            string strExpText,
            string strInfMsg,
            uint dwTextOptions,
            uint dwLockOptions,
            uint dwLocalIntervals,
            string strLocalRKUFile,
            string strModuleName
            )
        {
            try
            {
                InitResProp();
                uint dwHDLGFlag = 0;
                if (nDialogBoxStyle == 1)
                    dwHDLGFlag = 0x1000000;	// HTML DLG
                if (isProcess64())
                    return fncInstantLocalKey64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation, m_strInitLicenseKey,
                        strLicText, strPmtText,
                        strRegText, strLimText, strExpText, strInfMsg,
                        dwTextOptions | dwHDLGFlag, (ushort)dwLockOptions, dwLocalIntervals,
                        strLocalRKUFile, strModuleName);
                else
                    return fncInstantLocalKey32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation, m_strInitLicenseKey,
                        strLicText, strPmtText,
                        strRegText, strLimText, strExpText, strInfMsg,
                        dwTextOptions | dwHDLGFlag, (ushort)dwLockOptions, dwLocalIntervals,
                        strLocalRKUFile, strModuleName);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }

        }

        // Overload
        public bool InstantNetworkKey(IntPtr hWnd, int nDialogBoxStyle, int nKeyLocation)
        {
            try
            {
                InitResProp();
                uint dwHDLGFlag = 0;
                if (nDialogBoxStyle == 1)
                    dwHDLGFlag = 0x1000000;	// HTML DLG
                if (isProcess64())
                    return fncInstantNetworkKeyDef64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation,
                        null, null, null,
                        dwHDLGFlag, 0, 0,
                        "UPDATE.ENC", null, "NETKEY.INI");
                else
                    return fncInstantNetworkKeyDef32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation,
                        null, null, null,
                        dwHDLGFlag, 0, 0,
                        "UPDATE.ENC", null, "NETKEY.INI");
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool InstantNetworkKey(IntPtr hWnd, int nDialogBoxStyle, int nKeyLocation,
            string strLimText,
            string strExpText,
            string strInfMsg,
            uint dwTextOptions,
            uint dwLockOptions,
            uint dwServerIntervals,
            string strServerRKUFile,
            string strModuleName,
            string strNetConfigFile
            )
        {
            try
            {
                InitResProp();
                uint dwHDLGFlag = 0;
                if (nDialogBoxStyle == 1)
                    dwHDLGFlag = 0x1000000;	// HTML DLG
                if (isProcess64())
                    return fncInstantNetworkKey64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation,
                        strLimText, strExpText, strInfMsg,
                        dwTextOptions | dwHDLGFlag, dwLockOptions, dwServerIntervals,
                        strServerRKUFile, strModuleName, strNetConfigFile);
                else
                    return fncInstantNetworkKey32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nKeyLocation,
                        strLimText, strExpText, strInfMsg,
                        dwTextOptions | dwHDLGFlag, dwLockOptions, dwServerIntervals,
                        strServerRKUFile, strModuleName, strNetConfigFile);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }

        }
        // Overload
        public bool InstantLocalNetworkKey(IntPtr hWnd, int nDialogBoxStyle, int nLocalKeyLocation, int nServerKeyLocation)
        {
            try
            {
                InitResProp();
                uint dwHDLGFlag = 0;
                if (nDialogBoxStyle == 1)
                    dwHDLGFlag = 0x1000000;	// HTML DLG
                if (isProcess64())
                    return fncInstantLocalNetworkKeyDef64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nLocalKeyLocation, (ushort)nServerKeyLocation, m_strInitLicenseKey,
                        null, null,
                        null, null, null,
                        null, dwHDLGFlag, 0,
                        0, 0, "UPDATE.ENC",
                        "UPDATE.ENC", null, "NETKEY.INI");
                else
                    return fncInstantLocalNetworkKeyDef32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nLocalKeyLocation, (ushort)nServerKeyLocation, m_strInitLicenseKey,
                        null, null,
                        null, null, null,
                        null, dwHDLGFlag, 0,
                        0, 0, "UPDATE.ENC",
                        "UPDATE.ENC", null, "NETKEY.INI");
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public bool InstantLocalNetworkKey(IntPtr hWnd, int nDialogBoxStyle, int nLocalKeyLocation, int nServerKeyLocation,
            string strLicText,
            string strLocalPmtText,
            string strLocalRegText,
            string strLimText,
            string strExpText,
            string strInfMsg,
            uint dwTextOptions,
            uint dwLockOptions,
            uint dwLocalIntervals,
            uint dwServerIntervals,
            string strLocalRKUFile,
            string strServerRKUFile,
            string strModuleName,
            string strNetConfigFile
            )
        {
            try
            {
                InitResProp();
                uint dwHDLGFlag = 0;
                if (nDialogBoxStyle == 1)
                    dwHDLGFlag = 0x1000000;	// HTML DLG
                if (isProcess64())
                    return fncInstantLocalNetworkKey64(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nLocalKeyLocation, (ushort)nServerKeyLocation,
                        m_strInitLicenseKey, strLicText, strLocalPmtText,
                        strLocalRegText, strLimText, strExpText,
                        strInfMsg, dwTextOptions | dwHDLGFlag, dwLockOptions,
                        dwLocalIntervals, dwServerIntervals, strLocalRKUFile,
                        strServerRKUFile, strModuleName, strNetConfigFile);
                else
                    return fncInstantLocalNetworkKey32(
                        hWnd, m_strAppPath,
                        (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)nLocalKeyLocation, (ushort)nServerKeyLocation,
                        m_strInitLicenseKey, strLicText, strLocalPmtText,
                        strLocalRegText, strLimText, strExpText,
                        strInfMsg, dwTextOptions | dwHDLGFlag, dwLockOptions,
                        dwLocalIntervals, dwServerIntervals, strLocalRKUFile,
                        strServerRKUFile, strModuleName, strNetConfigFile);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        // Miscellaneous Functions
        public int CheckProcess(string strModuleName, uint dwCmd)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncCheckProcess64(
                            (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                            strModuleName, (ushort)dwCmd);
                else
                    m_nErrCode = (int)fncCheckProcess32(
                            (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                            strModuleName, (ushort)dwCmd);
                m_strErrStr = GetErrStr(m_nErrCode);
                return m_nErrCode;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return EXCEPTION_ERR;
            }
        }

        public bool CheckAgentVersion(int nMajorVer, int nMinorVer, int nReleaseVer, int nBuildVer)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncCheckAgentVersion64(
                            (ushort)m_nSrvIdx, (ushort)nMajorVer, (ushort)nMinorVer, (ushort)nReleaseVer, (ushort)nBuildVer);
                else
                    m_nErrCode = (int)fncCheckAgentVersion32(
                            (ushort)m_nSrvIdx, (ushort)nMajorVer, (ushort)nMinorVer, (ushort)nReleaseVer, (ushort)nBuildVer);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }

        public bool SetInternalKey(TInternalKeyCmd dwInternalKeyCmd)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncSetInternalKey64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation, (ushort)dwInternalKeyCmd);
                else
                    m_nErrCode = (int)fncSetInternalKey32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nKeyLocation, (ushort)dwInternalKeyCmd);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == 0)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }

        }

        public bool IsModuleEnabled(int nModuleID)
        {
            if ((Modules & (1 << (nModuleID - 1))) != 0)
                return true;
            return false;
        }

        public bool IsItemEnabled(int nItemsNum, int nItemID)
        {
            if (nItemsNum < 1)
                nItemsNum = 1;
            else if (nItemsNum > 4)
                return false;
            UInt64 i = 1;
            i = i << (nItemID - 1);
            if ((m_i64Items[nItemsNum - 1] & i) != 0)
                return true;
            return false;
        }

        public bool IsConnected()
        {
            return NetKeyCheckConnection();
        }

        public bool IsLoggedIn()
        {
            return NetKeyCheckLogin();
        }

        // Extended
        public bool SetKeyValue()
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    m_nErrCode = (int)fncSetKeyValue64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, m_strKeyValueCode);
                else
                    m_nErrCode = (int)fncSetKeyValue32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, m_strKeyValueCode);
                m_strErrStr = GetErrStr(m_nErrCode);
                if (m_nErrCode == SUCCESS)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return false;
            }
        }
        public ushort GetKeyValue(ushort wKey)
        {
            try
            {
                InitResProp();
                if (isProcess64())
                    return fncGetKeyValue64(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, wKey);
                else
                    return fncGetKeyValue32(
                        (ushort)m_nSrvIdx, (uint)m_lKeyID, (ushort)m_nProgramID, (ushort)m_nModuleID,
                        (ushort)m_nKeyLocation, wKey);
            }
            catch (Exception e)
            {
                HandleException(e.Message);
                return 0;
            }
        }

        public string InitLicenseKey
        {
            get { return m_strInitLicenseKey; }
            set { m_strInitLicenseKey = value; }
        }

        public int SrvIdx
        {
            get { return m_nSrvIdx; }
            set { m_nSrvIdx = value; }
        }

        public uint KeyID
        {
            get { return m_lKeyID; }
            set { m_lKeyID = value; }
        }

        public int ProgramID
        {
            get { return m_nProgramID; }
            set { m_nProgramID = value; }
        }

        public int ModuleID
        {
            get { return m_nModuleID; }
            set { m_nModuleID = value; }
        }

        public int KeyLocation
        {
            get { return m_nKeyLocation; }
            set { m_nKeyLocation = value; }
        }

        public TRegIDFormat RegIDFormat
        {
            get { return m_dwRegIDFormat; }
            set { m_dwRegIDFormat = value; }
        }

        // Instant
        public string SystemImageFile
        {
            get { return m_strSystemImageFile; }
            set { m_strSystemImageFile = value; }
        }

        public string AppPath
        {
            get { return m_strAppPath; }
            set
            {
                try
                {
                    if (isProcess64())
                        fncSetAppPath64(value);
                    else
                        fncSetAppPath32(value);
                    m_strAppPath = value;
                }
                catch
                {
                }
            }
        }

        public uint InternalFlag
        {
            get { return m_dwInternalFlag; }
            set
            {
                try
                {
                    if (isProcess64())
                        fncSetInternalFlag64(value);
                    else
                        fncSetInternalFlag32(value);
                    m_dwInternalFlag = value;
                }
                catch
                {
                }
            }
        }

        public string AgentStartup
        {
            get { return m_strAgentStartup; }
            set
            {
                try
                {
                    if (isProcess64())
                        fncSetAgentStartup64(value);
                    else
                        fncSetAgentStartup32(value);
                    m_strAgentStartup = value;
                }
                catch
                {
                }
            }
        }
        //
        public string RegistrationID
        {
            get { return m_strRegistrationID; }
        }
        public string DestroyCode
        {
            get { return m_strDestroyCode; }
        }
        public string LicKeyTran
        {
            get { return m_strLicKeyTran; }
        }
        //

        // Key Prop
        public int KeyType
        {
            get { return m_nKeyType; }
        }
        public int LicenseMode
        {
            get { return m_nLicenseMode; }
        }
        public uint UserID
        {
            get { return m_lUserID; }
        }
        public int MaxInstall
        {
            get { return m_nMaxInstall; }
        }
        public int InstallCount
        {
            get { return m_nInstallCount; }
        }
        public int InstallLeft
        {
            get { return m_nInstallLeft; }
        }
        public int MaxInit
        {
            get { return m_nMaxInit; }
        }
        public int InitCount
        {
            get { return m_nInitCount; }
        }
        public int InitLeft
        {
            get { return m_nInitLeft; }
        }
        public int MaxUser
        {
            get { return m_nMaxUser; }
        }
        public int MaxExec
        {
            get { return m_nMaxExec; }
        }
        public int ExecCount
        {
            get { return m_nExecCount; }
        }
        public int ExecLeft
        {
            get { return m_nExecLeft; }
        }
        public int MaxDay
        {
            get { return m_nMaxDay; }
        }
        public int DayCount
        {
            get { return m_nDayCount; }
        }
        public int MaxYear
        {
            get { return m_nMaxYear; }
        }
        public int YearCount
        {
            get { return m_nYearCount; }
        }
        public int MaxDayPeriod
        {
            get { return m_nMaxDayPeriod; }
        }
        public int DayCountPeriod
        {
            get { return m_nDayCountPeriod; }
        }
        public int ExpYear
        {
            get { return m_nExpYear; }
        }

        public int ExpMonth
        {
            get { return m_nExpMonth; }
        }

        public int ExpDay
        {
            get { return m_nExpDay; }
        }

        public int DayLeft
        {
            get { return m_nDayLeft; }
        }
        public int YearLeft
        {
            get { return m_nYearLeft; }
        }
        public int DayLeftPeriod
        {
            get { return m_nDayLeftPeriod; }
        }

        public uint Options
        {
            get { return m_lOptions; }
        }

        public uint Modules
        {
            get { return m_lModules; }
        }

        public string KeyData
        {
            get { return m_strKeyData; }
        }

        public UInt64 Items1
        {
            get { return m_i64Items[0]; }
        }

        public UInt64 Items2
        {
            get { return m_i64Items[1]; }
        }

        public UInt64 Items3
        {
            get { return m_i64Items[2]; }
        }

        public UInt64 Items4
        {
            get { return m_i64Items[3]; }
        }

        public int LicUpgradeID
        {
            get { return m_nLicUpgradeID; }
        }

        // Extended Prop
        public string KeyValueCode
        {
            get { return m_strKeyValueCode; }
            set { m_strKeyValueCode = value; }
        }
        // Server Prop
        public string ServerAddress
        {
            get { return m_strServerAddress; }
            set { m_strServerAddress = value; }
        }
        public int ServerPort
        {
            get { return m_nServerPort; }
            set { m_nServerPort = value; }
        }
        public string ServerHost
        {
            get { return m_strServerHost; }
            set { m_strServerHost = value; }
        }
        //
        public string ServerAddress2
        {
            get { return m_strServerAddress2; }
            set { m_strServerAddress2 = value; }
        }
        public int ServerPort2
        {
            get { return m_nServerPort2; }
            set { m_nServerPort2 = value; }
        }
        public string ServerHost2
        {
            get { return m_strServerHost2; }
            set { m_strServerHost2 = value; }
        }
        //
        public string ServerAddress3
        {
            get { return m_strServerAddress3; }
            set { m_strServerAddress3 = value; }
        }
        public int ServerPort3
        {
            get { return m_nServerPort3; }
            set { m_nServerPort3 = value; }
        }
        public string ServerHost3
        {
            get { return m_strServerHost3; }
            set { m_strServerHost3 = value; }
        }
        //
        public string ServerAddress4
        {
            get { return m_strServerAddress4; }
            set { m_strServerAddress4 = value; }
        }
        public int ServerPort4
        {
            get { return m_nServerPort4; }
            set { m_nServerPort4 = value; }
        }
        public string ServerHost4
        {
            get { return m_strServerHost4; }
            set { m_strServerHost4 = value; }
        }
        public bool LoggedIn
        {
            get { return m_bLoggedIn; }
        }
        // Response Prop
        public int ErrCode
        {
            get { return m_nErrCode; }
        }
        public TErrType ErrType
        {
            get
            {
                if (ErrCode == 0)
                    return TErrType.errSuccess;
                else if (ErrCode == EXPIRED_DATE || ErrCode == EXPIRED_EXEC)
                    return TErrType.errExpire;
                else if (ErrCode == ERR_LOAD_KEYDEVICE || ErrCode == ERR_KEYDEVICE || (ErrCode == ERR_MATCH_KEYDEVICE) || ErrCode == ERR_DISK)
                    return TErrType.errSystemChange;
                else if (ErrCode == ERR_LICENSING)
                    return TErrType.errLicenseRequired;
                else if (ErrCode == ERR_KEY_EXISTS)
                    return TErrType.errKeyExists;
                else if (ErrCode == ERR_KEY_NOT_FOUND)
                    return TErrType.errKeyNotFound;
                else
                    return TErrType.errError;
            }
        }
        public string ErrStr
        {
            get { return m_strErrStr; }
        }

        private
            evResultProc LastResultProc;
        evResultProc LastClientResultProc;
        // Main Prop
        protected string m_strInitLicenseKey;
        protected int m_nSrvIdx;
        protected uint m_lKeyID;
        protected int m_nProgramID;
        protected int m_nModuleID;
        protected int m_nKeyLocation;
        protected TRegIDFormat m_dwRegIDFormat;
        //
        protected string m_strSystemImageFile;
        protected string m_strAppPath;
        protected uint m_dwInternalFlag;
        protected string m_strAgentStartup;
        //
        protected string m_strRegistrationID;
        protected string m_strDestroyCode;
        protected string m_strLicKeyTran;
        // Key Prop
        protected int m_nKeyType;
        protected int m_nLicenseMode;
        protected uint m_lUserID;
        protected int m_nMaxInstall;
        protected int m_nInstallCount;
        protected int m_nInstallLeft;
        protected int m_nMaxInit;
        protected int m_nInitCount;
        protected int m_nInitLeft;
        protected int m_nMaxUser;
        protected int m_nMaxExec;
        protected int m_nExecCount;
        protected int m_nExecLeft;
        protected int m_nMaxDay;
        protected int m_nDayCount;
        protected int m_nMaxYear;
        protected int m_nYearCount;
        protected int m_nMaxDayPeriod;
        protected int m_nDayCountPeriod;
        protected int m_nExpYear;
        protected int m_nExpMonth;
        protected int m_nExpDay;
        protected int m_nDayLeft;
        protected int m_nYearLeft;
        protected int m_nDayLeftPeriod;
        protected uint m_lOptions;
        protected uint m_lModules;
        protected string m_strKeyData;
        protected UInt64[] m_i64Items;
        protected int m_nLicUpgradeID;
        // Extended Prop
        protected string m_strKeyValueCode;
        // Server Prop
        protected string m_strServerAddress;
        protected int m_nServerPort;
        protected string m_strServerHost;
        protected string m_strServerAddress2;
        protected int m_nServerPort2;
        protected string m_strServerHost2;
        protected string m_strServerAddress3;
        protected int m_nServerPort3;
        protected string m_strServerHost3;
        protected string m_strServerAddress4;
        protected int m_nServerPort4;
        protected string m_strServerHost4;
        protected bool m_bLoggedIn;
        // Response Prop
        protected int m_nErrCode;
        protected string m_strErrStr;
        // Private Methods
        protected void HandleException(string strMessage)
        {
            m_nErrCode = EXCEPTION_ERR;
            m_strErrStr = strMessage;
        }

        protected UInt64 KeyDataToItems(string strKeyData, int nItemsNum)
        {
            if (nItemsNum < 1)
                nItemsNum = 1;
            else if (nItemsNum > 4)
                return 0;
            if (strKeyData.Length < 64)
                return 0;
            if ((strKeyData.Substring(((nItemsNum - 1) * 16), 1) == "[") &&
                (strKeyData.Substring(15 + ((nItemsNum - 1) * 16), 1) == "]"))
            {
                return Convert.ToUInt64(strKeyData.Substring(1 + ((nItemsNum - 1) * 16), 14), 16);
            }
            return 0;
        }

        protected void KeyPropStrcToKeyProp(KEY_PROPERTIES KeyProp)
        {
            m_nKeyType = Convert.ToInt32(KeyProp.KeyType);
            m_nLicenseMode = Convert.ToInt32(KeyProp.LicenseMode);
            m_lUserID = Convert.ToUInt32(KeyProp.UserID);
            m_nMaxInstall = Convert.ToInt32(KeyProp.MaxInstall);
            m_nInstallCount = Convert.ToInt32(KeyProp.InstallCount);
            m_nMaxUser = Convert.ToInt32(KeyProp.MaxUser);
            m_nMaxExec = Convert.ToInt32(KeyProp.MaxExec);
            m_nExecCount = Convert.ToInt32(KeyProp.ExecCount);
            m_nMaxDay = Convert.ToInt32(KeyProp.MaxDay);
            m_nDayCount = Convert.ToInt32(KeyProp.DayCount);
            m_nExpYear = Convert.ToInt32(KeyProp.ExpYear);
            m_nExpMonth = Convert.ToInt32(KeyProp.ExpMonth);
            m_nExpDay = Convert.ToInt32(KeyProp.ExpDay);
            m_lOptions = Convert.ToUInt32(KeyProp.Options, 16);
            m_lModules = Convert.ToUInt32(KeyProp.Modules, 16);
            m_strKeyData = KeyProp.KeyData;

            for (int i = 0; i < 4; i++)
                m_i64Items[i] = KeyDataToItems(m_strKeyData, i + 1);

            // Setting extended Key Propertis
            m_nInstallLeft = m_nMaxInstall == MAX_INSTALL ? MAX_INSTALL : m_nMaxInstall - m_nInstallCount;

            if ((m_lOptions & OPT_DONGLE_WITH_MACHINE) != 0)
            {
                m_nMaxInit = m_nMaxInstall;
                m_nInitCount = m_nInstallCount;
                m_nInitLeft = m_nInstallLeft;

                m_nMaxInstall = 0;
                m_nInstallCount = 0;
                m_nInstallLeft = 0;
            }
            else
            {
                m_nMaxInit = 0;
                m_nInitCount = 0;
                m_nInitLeft = 0;
            }

            if (KeyProp.MaxYear != "")
                m_nMaxYear = Convert.ToInt32(KeyProp.MaxYear);
            if (KeyProp.YearCount != "")
                m_nYearCount = Convert.ToInt32(KeyProp.YearCount);

            if (KeyProp.LicUpgradeID != "")
                m_nLicUpgradeID = Convert.ToInt32(KeyProp.LicUpgradeID, 16);

            if (m_nMaxExec < MAX_EXEC)
                m_nExecLeft = m_nMaxExec - m_nExecCount;
            else
                m_nExecLeft = MAX_EXEC;

            m_nMaxDayPeriod = m_nMaxDay == MAX_DAY ? MAX_DAY_PERIOD : m_nMaxDay + (m_nMaxYear * NUMDAYS_YEAR);
            m_nDayCountPeriod = m_nDayCount + (m_nYearCount * NUMDAYS_YEAR);

            try
            {
                if (isProcess64())
                    m_nDayLeftPeriod = fncGetDayLeft64(m_nMaxDayPeriod, m_nDayCountPeriod, (ushort)m_nExpYear, (ushort)m_nExpMonth, (ushort)m_nExpDay);
                else
                    m_nDayLeftPeriod = fncGetDayLeft32(m_nMaxDayPeriod, m_nDayCountPeriod, (ushort)m_nExpYear, (ushort)m_nExpMonth, (ushort)m_nExpDay);

                if (m_nDayLeftPeriod == MAX_DAY_PERIOD)
                {
                    m_nYearLeft = 0;
                    m_nDayLeft = MAX_DAY;
                }
                else if (m_nMaxYear > 0) // if year mode
                {
                    m_nYearLeft = m_nDayLeftPeriod / NUMDAYS_YEAR;
                    m_nDayLeft = m_nDayLeftPeriod % NUMDAYS_YEAR;
                }
                else // if day mode
                {
                    m_nYearLeft = 0;
                    m_nDayLeft = m_nDayLeftPeriod;
                }
            }
            catch
            {
            }


        }

        protected void InitProp()
        {
            m_nSrvIdx = 0;
            m_lKeyID = 0;
            m_nProgramID = 0;
            m_nModuleID = 0;
            m_nKeyLocation = 0;
            m_dwRegIDFormat = 0;
            m_strInitLicenseKey = "";
            m_strRegistrationID = "";
            m_strDestroyCode = "";
            m_strLicKeyTran = "";
            m_strKeyValueCode = "";
            m_strServerAddress = "";
            m_nServerPort = 0;
            m_strServerHost = "";
            m_strServerAddress2 = "";
            m_nServerPort2 = 0;
            m_strServerHost2 = "";
            m_strServerAddress3 = "";
            m_nServerPort3 = 0;
            m_strServerHost3 = "";
            m_strServerAddress4 = "";
            m_nServerPort4 = 0;
            m_strServerHost4 = "";
            m_strSystemImageFile = "";
            m_strAppPath = "";
            m_dwInternalFlag = 0;
            m_strAgentStartup = "";
            m_bLoggedIn = false;
        }
        protected void InitKeyProp()
        {
            m_nKeyType = 0;
            m_nLicenseMode = 0;
            m_lUserID = 0;
            m_nMaxInstall = 0;
            m_nInstallCount = 0;
            m_nMaxUser = 0;
            m_nMaxExec = 0;
            m_nExecCount = 0;
            m_nMaxDay = 0;
            m_nDayCount = 0;
            m_nMaxYear = 0;
            m_nYearCount = 0;
            m_nExpYear = 0;
            m_nExpMonth = 0;
            m_nExpDay = 0;
            m_lOptions = 0;
            m_lModules = 0;
            m_nDayLeft = 0;
            m_nYearLeft = 0;
            m_nDayLeftPeriod = 0;
            m_nInstallLeft = 0;
            m_nInitLeft = 0;
            m_nLicUpgradeID = 0;
            m_strKeyData = "";
            for (int i = 0; i < 4; i++)
                m_i64Items[i] = 0;
        }
        protected void InitResProp()
        {
            m_nErrCode = 0;
            m_strErrStr = "";
        }
        protected bool isProcess64()
        {
            if (IntPtr.Size == 8)
                return true;
            else
                return false;
        }
        // Main functions
        [DllImport(EKCDLL32, EntryPoint = "H33", CharSet = CharSet.Ansi)]
        static extern ushort fncOpenKeySrv32(ushort sType, uint KeyID, ushort ProgramID);
        [DllImport(EKCDLL64, EntryPoint = "H33", CharSet = CharSet.Ansi)]
        static extern ushort fncOpenKeySrv64(ushort sType, uint KeyID, ushort ProgramID);

        [DllImport(EKCDLL32, EntryPoint = "H34", CharSet = CharSet.Ansi)]
        static extern ushort fncCloseKeySrv32(ushort sType);
        [DllImport(EKCDLL64, EntryPoint = "H34", CharSet = CharSet.Ansi)]
        static extern ushort fncCloseKeySrv64(ushort sType);

        [DllImport(EKCDLL32, EntryPoint = "H99", CharSet = CharSet.Ansi)]
        static extern ushort fncTerminateKeySrv32(ushort sType);
        [DllImport(EKCDLL64, EntryPoint = "H99", CharSet = CharSet.Ansi)]
        static extern ushort fncTerminateKeySrv64(ushort sType);

        [DllImport(EKCDLL32, EntryPoint = "H11", CharSet = CharSet.Ansi)]
        static extern ushort fncSetKeyCache32(ushort sType, ushort Flag);
        [DllImport(EKCDLL64, EntryPoint = "H11", CharSet = CharSet.Ansi)]
        static extern ushort fncSetKeyCache64(ushort sType, ushort Flag);

        [DllImport(EKCDLL32, EntryPoint = "H35", CharSet = CharSet.Ansi)]
        static extern void fncGetErrStr32(ushort sType, ushort ErrCode, StringBuilder ErrStr);
        [DllImport(EKCDLL64, EntryPoint = "H35", CharSet = CharSet.Ansi)]
        static extern void fncGetErrStr64(ushort sType, ushort ErrCode, StringBuilder ErrStr);

        // Licensing functions
        [DllImport(EKCDLL32, EntryPoint = "H5A", CharSet = CharSet.Ansi)]
        static extern ushort fncInitLicKey32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, string InitLicKey, StringBuilder RegIdOut);
        [DllImport(EKCDLL64, EntryPoint = "H5A", CharSet = CharSet.Ansi)]
        static extern ushort fncInitLicKey64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, string InitLicKey, StringBuilder RegIdOut);

        [DllImport(EKCDLL32, EntryPoint = "H6A", CharSet = CharSet.Ansi)]
        static extern ushort fncInitLicKeyEx32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, string InitLicKey, StringBuilder RegIdOut, ushort RegIDFormat);
        [DllImport(EKCDLL64, EntryPoint = "H6A", CharSet = CharSet.Ansi)]
        static extern ushort fncInitLicKeyEx64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, string InitLicKey, StringBuilder RegIdOut, ushort RegIDFormat);

        [DllImport(EKCDLL32, EntryPoint = "H6B", CharSet = CharSet.Ansi)]
        static extern ushort fncGetRegID32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, StringBuilder RegIdOut, ushort RegIDFormat);
        [DllImport(EKCDLL64, EntryPoint = "H6B", CharSet = CharSet.Ansi)]
        static extern ushort fncGetRegID64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, StringBuilder RegIdOut, ushort RegIDFormat);

        [DllImport(EKCDLL32, EntryPoint = "H5C", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckLicKey32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string LicKey);
        [DllImport(EKCDLL64, EntryPoint = "H5C", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckLicKey64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string LicKey);

        [DllImport(EKCDLL32, EntryPoint = "H5B", CharSet = CharSet.Ansi)]
        static extern ushort fncPutLicKey32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string LicKey);
        [DllImport(EKCDLL64, EntryPoint = "H5B", CharSet = CharSet.Ansi)]
        static extern ushort fncPutLicKey64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string LicKey);

        [DllImport(EKCDLL32, EntryPoint = "H6D", CharSet = CharSet.Ansi)]
        static extern ushort fncPutLicKeyEx32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string LicKey, ushort LicUpgradeID, ushort Flag);
        [DllImport(EKCDLL64, EntryPoint = "H6D", CharSet = CharSet.Ansi)]
        static extern ushort fncPutLicKeyEx64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string LicKey, ushort LicUpgradeID, ushort Flag);

        [DllImport(EKCDLL32, EntryPoint = "H61", CharSet = CharSet.Ansi)]
        static extern ushort fncTransferLicense32(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, string RegIDIn, StringBuilder LicKeyOut);
        [DllImport(EKCDLL64, EntryPoint = "H61", CharSet = CharSet.Ansi)]
        static extern ushort fncTransferLicense64(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, string RegIDIn, StringBuilder LicKeyOut);

        [DllImport(EKCDLL32, EntryPoint = "H60", CharSet = CharSet.Ansi)]
        static extern ushort fncRemakeLicense32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string InitLicKey, StringBuilder RegIDOut, ushort Cmd);
        [DllImport(EKCDLL64, EntryPoint = "H60", CharSet = CharSet.Ansi)]
        static extern ushort fncRemakeLicense64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string InitLicKey, StringBuilder RegIDOut, ushort Cmd);

        [DllImport(EKCDLL32, EntryPoint = "H67", CharSet = CharSet.Ansi)]
        static extern ushort fncRemakeLicenseEx32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string InitLicKey, StringBuilder RegIDOut, ushort Cmd, ushort RegIDFormat);
        [DllImport(EKCDLL64, EntryPoint = "H67", CharSet = CharSet.Ansi)]
        static extern ushort fncRemakeLicenseEx64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string InitLicKey, StringBuilder RegIDOut, ushort Cmd, ushort RegIDFormat);

        [DllImport(EKCDLL32, EntryPoint = "H3E", CharSet = CharSet.Ansi)]
        static extern ushort fncInstallKey32(ushort sType, uint KeyID, ushort ProgramID, ushort FromKeyLocation, ushort ToKeyLocation, IntPtr KeyPropertiesOut, ushort InstFlag);
        [DllImport(EKCDLL64, EntryPoint = "H3E", CharSet = CharSet.Ansi)]
        static extern ushort fncInstallKey64(ushort sType, uint KeyID, ushort ProgramID, ushort FromKeyLocation, ushort ToKeyLocation, IntPtr KeyPropertiesOut, ushort InstFlag);

        [DllImport(EKCDLL32, EntryPoint = "H3F", CharSet = CharSet.Ansi)]
        static extern ushort fncRemoveKey32(ushort sType, uint KeyID, ushort ProgramID, ushort FromKeyLocation, ushort ToKeyLocation, IntPtr KeyPropertiesOut, ushort RemoveFlag);
        [DllImport(EKCDLL64, EntryPoint = "H3F", CharSet = CharSet.Ansi)]
        static extern ushort fncRemoveKey64(ushort sType, uint KeyID, ushort ProgramID, ushort FromKeyLocation, ushort ToKeyLocation, IntPtr KeyPropertiesOut, ushort RemoveFlag);

        [DllImport(EKCDLL32, EntryPoint = "H28", CharSet = CharSet.Ansi)]
        static extern ushort fncInitMachSig32(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, IntPtr KeyPropertiesOut, ushort InitFlag);
        [DllImport(EKCDLL64, EntryPoint = "H28", CharSet = CharSet.Ansi)]
        static extern ushort fncInitMachSig64(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, IntPtr KeyPropertiesOut, ushort InitFlag);

        [DllImport(EKCDLL32, EntryPoint = "H29", CharSet = CharSet.Ansi)]
        static extern ushort fncUninitMachSig32(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, IntPtr KeyPropertiesOut, ushort UninitFlag);
        [DllImport(EKCDLL64, EntryPoint = "H29", CharSet = CharSet.Ansi)]
        static extern ushort fncUninitMachSig64(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, IntPtr KeyPropertiesOut, ushort UninitFlag);

        // Key Interface functions
        [DllImport(EKCDLL32, EntryPoint = "H36", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckKey32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation);
        [DllImport(EKCDLL64, EntryPoint = "H36", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckKey64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation);

        [DllImport(EKCDLL32, EntryPoint = "H39", CharSet = CharSet.Ansi)]
        static extern ushort fncGetKeyProperties32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, IntPtr KeyPropertiesOut);
        [DllImport(EKCDLL64, EntryPoint = "H39", CharSet = CharSet.Ansi)]
        static extern ushort fncGetKeyProperties64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, IntPtr KeyPropertiesOut);

        [DllImport(EKCDLL32, EntryPoint = "H3A", CharSet = CharSet.Ansi)]
        static extern ushort fncGetKeyData32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort DataAddress, ushort DataCount, StringBuilder DataOut);
        [DllImport(EKCDLL64, EntryPoint = "H3A", CharSet = CharSet.Ansi)]
        static extern ushort fncGetKeyData64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort DataAddress, ushort DataCount, StringBuilder DataOut);

        [DllImport(EKCDLL32, EntryPoint = "H3B", CharSet = CharSet.Ansi)]
        static extern ushort fncPutKeyData32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort DataAddress, ushort DataCount, string DataIn);
        [DllImport(EKCDLL64, EntryPoint = "H3B", CharSet = CharSet.Ansi)]
        static extern ushort fncPutKeyData64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort DataAddress, ushort DataCount, string DataIn);

        [DllImport(EKCDLL32, EntryPoint = "H3C", CharSet = CharSet.Ansi)]
        static extern ushort fncUpdateLimitKey32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort uCmd, IntPtr KeyPropertiesOut);
        [DllImport(EKCDLL64, EntryPoint = "H3C", CharSet = CharSet.Ansi)]
        static extern ushort fncUpdateLimitKey64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort uCmd, IntPtr KeyPropertiesOut);

        [DllImport(EKCDLL32, EntryPoint = "H77", CharSet = CharSet.Ansi)]
        static extern ushort fncUpdateLimitKeyAndProcessKeyValue32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort uCmd, IntPtr KeyPropertiesOut, string KeyValueCode, ushort wKeyOfKeyValue, ref ushort wValueOfKeyValue);
        [DllImport(EKCDLL64, EntryPoint = "H77", CharSet = CharSet.Ansi)]
        static extern ushort fncUpdateLimitKeyAndProcessKeyValue64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort uCmd, IntPtr KeyPropertiesOut, string KeyValueCode, ushort wKeyOfKeyValue, ref ushort wValueOfKeyValue);

        [DllImport(EKCDLL32, EntryPoint = "H3D", CharSet = CharSet.Ansi)]
        static extern ushort fncUpdateRemoteKey32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string RKUFile, ushort uReserved, IntPtr KeyPropertiesOut);
        [DllImport(EKCDLL64, EntryPoint = "H3D", CharSet = CharSet.Ansi)]
        static extern ushort fncUpdateRemoteKey64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string RKUFile, ushort uReserved, IntPtr KeyPropertiesOut);

        [DllImport(EKCDLL32, EntryPoint = "H37", CharSet = CharSet.Ansi)]
        static extern ushort fncSetBackgroundCheckDef32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, uint Intervals, ushort cCmd, cbResultProc ResultProc);
        [DllImport(EKCDLL64, EntryPoint = "H37", CharSet = CharSet.Ansi)]
        static extern ushort fncSetBackgroundCheckDef64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, uint Intervals, ushort cCmd, cbResultProc ResultProc);

        [DllImport(EKCDLL32, EntryPoint = "H37", CharSet = CharSet.Ansi)]
        static extern ushort fncSetBackgroundCheck32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, uint Intervals, ushort cCmd, cbResultProc CallBack);
        [DllImport(EKCDLL64, EntryPoint = "H37", CharSet = CharSet.Ansi)]
        static extern ushort fncSetBackgroundCheck64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, uint Intervals, ushort cCmd, cbResultProc CallBack);

        [DllImport(EKCDLL32, EntryPoint = "H5F", CharSet = CharSet.Ansi)]
        static extern ushort fncDestroyLicense32(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, StringBuilder DesCodeOut);
        [DllImport(EKCDLL64, EntryPoint = "H5F", CharSet = CharSet.Ansi)]
        static extern ushort fncDestroyLicense64(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, StringBuilder DesCodeOut);

        // Server Interface functions
        [DllImport(EKCDLL32, EntryPoint = "H46", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyConnect32(ushort sType, string ServerAddress, int ServerPort, string ServerHost, string sReserved, int TimeOut, ushort fReserved);
        [DllImport(EKCDLL64, EntryPoint = "H46", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyConnect64(ushort sType, string ServerAddress, int ServerPort, string ServerHost, string sReserved, int TimeOut, ushort fReserved);

        [DllImport(EKCDLL32, EntryPoint = "H4C", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyConnectAdv32(ushort sType, string ConfigFile);
        [DllImport(EKCDLL64, EntryPoint = "H4C", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyConnectAdv64(ushort sType, string ConfigFile);

        [DllImport(EKCDLL32, EntryPoint = "H4D", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyConnectEx32(ushort sType,
            string ServerAddress1, int ServerPort1, string ServerHost1, string sReserved1,
            string ServerAddress2, int ServerPort2, string ServerHost2, string sReserved2,
            string ServerAddress3, int ServerPort3, string ServerHost3, string sReserved3,
            string ServerAddress4, int ServerPort4, string ServerHost4, string sReserved4,
            int TimeOut, ushort fReserved);
        [DllImport(EKCDLL64, EntryPoint = "H4D", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyConnectEx64(ushort sType,
            string ServerAddress1, int ServerPort1, string ServerHost1, string sReserved1,
            string ServerAddress2, int ServerPort2, string ServerHost2, string sReserved2,
            string ServerAddress3, int ServerPort3, string ServerHost3, string sReserved3,
            string ServerAddress4, int ServerPort4, string ServerHost4, string sReserved4,
            int TimeOut, ushort fReserved);

        [DllImport(EKCDLL32, EntryPoint = "H47", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyDisconnect32(ushort sType);
        [DllImport(EKCDLL64, EntryPoint = "H47", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyDisconnect64(ushort sType);

        [DllImport(EKCDLL32, EntryPoint = "HCB", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyCheckConnection32(ushort sType);
        [DllImport(EKCDLL64, EntryPoint = "HCB", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyCheckConnection64(ushort sType);

        [DllImport(EKCDLL32, EntryPoint = "H48", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyLogin32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string AppName, string ModuleName, ushort sCmd);
        [DllImport(EKCDLL64, EntryPoint = "H48", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyLogin64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string AppName, string ModuleName, ushort sCmd);

        [DllImport(EKCDLL32, EntryPoint = "H49", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyLogout32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation);
        [DllImport(EKCDLL64, EntryPoint = "H49", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyLogout64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation);

        [DllImport(EKCDLL32, EntryPoint = "HCC", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyCheckLogin32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation);
        [DllImport(EKCDLL64, EntryPoint = "HCC", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeyCheckLogin64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation);

        [DllImport(EKCDLL32, EntryPoint = "HC8", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeySessionLogin32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string AppName, string ModuleName, ushort sCmd,
                                string UserName, string ComputerName, string ServerLocalAddr, string ServerRemoteAddr, string LoginUserID, string SessionID, int SessionTimeout);
        [DllImport(EKCDLL64, EntryPoint = "HC8", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeySessionLogin64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, string AppName, string ModuleName, ushort sCmd,
                                string UserName, string ComputerName, string ServerLocalAddr, string ServerRemoteAddr, string LoginUserID, string SessionID, int SessionTimeout);

        [DllImport(EKCDLL32, EntryPoint = "HC9", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeySessionLogout32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort sCmd,
                                string ServerLocalAddr, string ServerRemoteAddr, string LoginUserID, string SessionID);
        [DllImport(EKCDLL64, EntryPoint = "HC9", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeySessionLogout64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort sCmd,
                                string ServerLocalAddr, string ServerRemoteAddr, string LoginUserID, string SessionID);

        [DllImport(EKCDLL32, EntryPoint = "HCA", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeySessionRefresh32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort sCmd,
                                string ServerLocalAddr, string ServerRemoteAddr, string LoginUserID, string SessionID);
        [DllImport(EKCDLL64, EntryPoint = "HCA", CharSet = CharSet.Ansi)]
        static extern ushort fncNetKeySessionRefresh64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID, ushort KeyLocation, ushort sCmd,
                                string ServerLocalAddr, string ServerRemoteAddr, string LoginUserID, string SessionID);

        [DllImport(EKCDLL32, EntryPoint = "H55", CharSet = CharSet.Ansi)]
        static extern ushort fncSetServerBackgroundCheck32(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, uint Intervals, ushort cReserved);
        [DllImport(EKCDLL64, EntryPoint = "H55", CharSet = CharSet.Ansi)]
        static extern ushort fncSetServerBackgroundCheck64(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, uint Intervals, ushort cReserved);

        // Instant functions 
        [DllImport(EKCDLL32, EntryPoint = "H81", CharSet = CharSet.Ansi)]
        static extern bool fncInstantProtection32(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, string SystemImageFile);
        [DllImport(EKCDLL64, EntryPoint = "H81", CharSet = CharSet.Ansi)]
        static extern bool fncInstantProtection64(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, string SystemImageFile);

        [DllImport(EKCDLL32, EntryPoint = "H85", CharSet = CharSet.Ansi)]
        static extern ushort fncInstantProtectionEx32(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, string SystemImageFile, ushort CallFlag, IntPtr KeyPropertiesOut);
        [DllImport(EKCDLL64, EntryPoint = "H85", CharSet = CharSet.Ansi)]
        static extern ushort fncInstantProtectionEx64(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, string SystemImageFile, ushort CallFlag, IntPtr KeyPropertiesOut);

        [DllImport(EKCDLL32, EntryPoint = "H86", CharSet = CharSet.Ansi)]
        static extern ushort fncInstantProtectionParam32(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, string SystemImageFile, ushort CallFlag, string Param, IntPtr KeyPropertiesOut);
        [DllImport(EKCDLL64, EntryPoint = "H86", CharSet = CharSet.Ansi)]
        static extern ushort fncInstantProtectionParam64(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, string SystemImageFile, ushort CallFlag, string Param, IntPtr KeyPropertiesOut);

        [DllImport(EKCDLL32, EntryPoint = "H82", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalKey32(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string InitLicKey, string LicText, string PmtText,
                        string RegText, string LimText, string ExpText, string InfMsg,
                        uint TextOptions, ushort LockOptions, uint LocalIntervals,
                        string LocalRKUFile, string ModuleName);
        [DllImport(EKCDLL64, EntryPoint = "H82", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalKey64(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string InitLicKey, string LicText, string PmtText,
                        string RegText, string LimText, string ExpText, string InfMsg,
                        uint TextOptions, ushort LockOptions, uint LocalIntervals,
                        string LocalRKUFile, string ModuleName);

        [DllImport(EKCDLL32, EntryPoint = "H82", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalKeyDef32(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string InitLicKey, string LicText, string PmtText,
                        string RegText, string LimText, string ExpText, string InfMsg,
                        uint TextOptions, uint LockOptions, uint LocalIntervals,
                        string LocalRKUFile, string ModuleName);
        [DllImport(EKCDLL64, EntryPoint = "H82", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalKeyDef64(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string InitLicKey, string LicText, string PmtText,
                        string RegText, string LimText, string ExpText, string InfMsg,
                        uint TextOptions, uint LockOptions, uint LocalIntervals,
                        string LocalRKUFile, string ModuleName);

        [DllImport(EKCDLL32, EntryPoint = "H83", CharSet = CharSet.Ansi)]
        static extern bool fncInstantNetworkKey32(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string LimText, string ExpText, string InfMsg,
                        uint TextOptions, uint LockOptions, uint ServerIntervals,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);
        [DllImport(EKCDLL64, EntryPoint = "H83", CharSet = CharSet.Ansi)]
        static extern bool fncInstantNetworkKey64(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string LimText, string ExpText, string InfMsg,
                        uint TextOptions, uint LockOptions, uint ServerIntervals,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);

        [DllImport(EKCDLL32, EntryPoint = "H83", CharSet = CharSet.Ansi)]
        static extern bool fncInstantNetworkKeyDef32(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string LimText, string ExpText, string InfMsg,
                        uint TextOptions, uint LockOptions, uint ServerIntervals,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);
        [DllImport(EKCDLL64, EntryPoint = "H83", CharSet = CharSet.Ansi)]
        static extern bool fncInstantNetworkKeyDef64(IntPtr hWnd, string AppPath, uint KeyID, ushort ProgramID,
                        ushort ModuleID, ushort KeyLocation,
                        string LimText, string ExpText, string InfMsg,
                        uint TextOptions, uint LockOptions, uint ServerIntervals,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);

        [DllImport(EKCDLL32, EntryPoint = "H84", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalNetworkKey32(IntPtr hWnd, string AppPath, uint KeyID,
                        ushort ProgramID, ushort ModuleID, ushort LocalKeyLocation, ushort ServerKeyLocation,
                        string InitLicKey, string LicText, string LocalPmtText,
                        string LocalRegText, string LimText, string ExpText,
                        string InfMsg, uint TextOptions, uint LockOptions,
                        uint LocalIntervals, uint ServerIntervals, string LocalRKUFile,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);
        [DllImport(EKCDLL64, EntryPoint = "H84", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalNetworkKey64(IntPtr hWnd, string AppPath, uint KeyID,
                        ushort ProgramID, ushort ModuleID, ushort LocalKeyLocation, ushort ServerKeyLocation,
                        string InitLicKey, string LicText, string LocalPmtText,
                        string LocalRegText, string LimText, string ExpText,
                        string InfMsg, uint TextOptions, uint LockOptions,
                        uint LocalIntervals, uint ServerIntervals, string LocalRKUFile,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);

        [DllImport(EKCDLL32, EntryPoint = "H84", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalNetworkKeyDef32(IntPtr hWnd, string AppPath, uint KeyID,
                        ushort ProgramID, ushort ModuleID, ushort LocalKeyLocation, ushort ServerKeyLocation,
                        string InitLicKey, string LicText, string LocalPmtText,
                        string LocalRegText, string LimText, string ExpText,
                        string InfMsg, uint TextOptions, uint LockOptions,
                        uint LocalIntervals, uint ServerIntervals, string LocalRKUFile,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);
        [DllImport(EKCDLL64, EntryPoint = "H84", CharSet = CharSet.Ansi)]
        static extern bool fncInstantLocalNetworkKeyDef64(IntPtr hWnd, string AppPath, uint KeyID,
                        ushort ProgramID, ushort ModuleID, ushort LocalKeyLocation, ushort ServerKeyLocation,
                        string InitLicKey, string LicText, string LocalPmtText,
                        string LocalRegText, string LimText, string ExpText,
                        string InfMsg, uint TextOptions, uint LockOptions,
                        uint LocalIntervals, uint ServerIntervals, string LocalRKUFile,
                        string ServerRKUFile, string ModuleName, string NetConfigFile);

        // Miscellaneous functions
        [DllImport(EKCDLL32, EntryPoint = "H42", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckProcess32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                        string ModuleName, ushort cCmd);
        [DllImport(EKCDLL64, EntryPoint = "H42", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckProcess64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                        string ModuleName, ushort cCmd);

        [DllImport(EKCDLL32, EntryPoint = "H01", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckAgentVersion32(ushort sType, ushort nMajorVer, ushort nMinorVer, ushort nReleaseVer, ushort nBuildVer);
        [DllImport(EKCDLL64, EntryPoint = "H01", CharSet = CharSet.Ansi)]
        static extern ushort fncCheckAgentVersion64(ushort sType, ushort nMajorVer, ushort nMinorVer, ushort nReleaseVer, ushort nBuildVer);

        [DllImport(EKCDLL32, EntryPoint = "H62", CharSet = CharSet.Ansi)]
        static extern ushort fncSetInternalKey32(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, ushort Flag);
        [DllImport(EKCDLL64, EntryPoint = "H62", CharSet = CharSet.Ansi)]
        static extern ushort fncSetInternalKey64(ushort sType, uint KeyID, ushort ProgramID, ushort KeyLocation, ushort Flag);

        [DllImport(EKCDLL32, EntryPoint = "H9A", CharSet = CharSet.Ansi)]
        static extern void fncSetAppPath32(string AppPath);
        [DllImport(EKCDLL64, EntryPoint = "H9A", CharSet = CharSet.Ansi)]
        static extern void fncSetAppPath64(string AppPath);

        [DllImport(EKCDLL32, EntryPoint = "H9B", CharSet = CharSet.Ansi)]
        static extern void fncSetInternalFlag32(uint Flag);
        [DllImport(EKCDLL64, EntryPoint = "H9B", CharSet = CharSet.Ansi)]
        static extern void fncSetInternalFlag64(uint Flag);

        [DllImport(EKCDLL32, EntryPoint = "H9C", CharSet = CharSet.Ansi)]
        static extern void fncSetAgentStartup32(string StartupParam);
        [DllImport(EKCDLL64, EntryPoint = "H9C", CharSet = CharSet.Ansi)]
        static extern void fncSetAgentStartup64(string StartupParam);

        // Extended functions
        [DllImport(EKCDLL32, EntryPoint = "H78", CharSet = CharSet.Ansi)]
        static extern ushort fncSetKeyValue32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, string KeyValueCode);
        [DllImport(EKCDLL64, EntryPoint = "H78", CharSet = CharSet.Ansi)]
        static extern ushort fncSetKeyValue64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, string KeyValueCode);

        [DllImport(EKCDLL32, EntryPoint = "H79", CharSet = CharSet.Ansi)]
        static extern ushort fncGetKeyValue32(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, ushort KeyIn);
        [DllImport(EKCDLL64, EntryPoint = "H79", CharSet = CharSet.Ansi)]
        static extern ushort fncGetKeyValue64(ushort sType, uint KeyID, ushort ProgramID, ushort ModuleID,
                                ushort KeyLocation, ushort KeyIn);

        [DllImport(EKCDLL32, EntryPoint = "HFB", CharSet = CharSet.Ansi)]
        static extern ushort fncSetAgentUserID32(ushort sType, uint KeyIDEx1, string UserID, ushort Flag);
        [DllImport(EKCDLL64, EntryPoint = "HFB", CharSet = CharSet.Ansi)]
        static extern ushort fncSetAgentUserID64(ushort sType, uint KeyIDEx1, string UserID, ushort Flag);

        [DllImport(EKCDLL32, EntryPoint = "HA0", CharSet = CharSet.Ansi)]
        static extern ushort fncInitWebServiceURL32(ushort sType, string WebServiceURL);
        [DllImport(EKCDLL64, EntryPoint = "HA0", CharSet = CharSet.Ansi)]
        static extern ushort fncInitWebServiceURL64(ushort sType, string WebServiceURL);

        [DllImport(EKCDLL32, EntryPoint = "H9F", CharSet = CharSet.Ansi)]
        static extern int fncGetDayLeft32(int MaxDayPeriod, int DayCountPeriod, ushort ExpYear, ushort ExpMonth, ushort ExpDay);
        [DllImport(EKCDLL64, EntryPoint = "H9F", CharSet = CharSet.Ansi)]
        static extern int fncGetDayLeft64(int MaxDayPeriod, int DayCountPeriod, ushort ExpYear, ushort ExpMonth, ushort ExpDay);

        [DllImport(EKCDLL32, EntryPoint = "H03", CharSet = CharSet.Ansi)]
        static extern ushort fncGetLastErrCode32();
        [DllImport(EKCDLL64, EntryPoint = "H03", CharSet = CharSet.Ansi)]
        static extern ushort fncGetLastErrCode64();

        [DllImport(EKCDLL32, EntryPoint = "H04", CharSet = CharSet.Ansi)]
        static extern void fncGetLastErrStr32(StringBuilder ErrStr);
        [DllImport(EKCDLL64, EntryPoint = "H04", CharSet = CharSet.Ansi)]
        static extern void fncGetLastErrStr64(StringBuilder ErrStr);

    }
}