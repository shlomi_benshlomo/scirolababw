﻿using Newtonsoft.Json;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.ViewModels.LicenseManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.Controllers
{
    public class LicenseManagerController : Controller
    {
        // GET: LicenseManager
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index(Activate_VM vm)
        {
            var objLnc = new LicenseObject();
            vm.strRegistrationID = objLnc.m_strRegistrationID;
            TempData["message"] = objLnc.m_strErrStr;
            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AutoActivate(Activate_VM vm)
        {
            string msg = "";            
            var objLnc = new LicenseObject();
            string activationKey = vm.strActivationKey;

            vm.strRegistrationID = objLnc.m_strRegistrationID;
            if (activationKey == null)
            {
                TempData["message"] = "Please Enter ActivationKey.";
                return View("Index", vm);
            }

            if (objLnc.AutoActivate(activationKey))
            {
                msg = objLnc.m_strErrStr;
            }
            else 
            {
                msg = objLnc.m_strErrStr;
            }

            TempData["message"] = msg;
            return View("Index", vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ManualActivate(Activate_VM vm)
        {
            string msg = "";
            var objLnc = new LicenseObject();
            string licenseKey = vm.strLicenseKey;

            vm.strRegistrationID = objLnc.m_strRegistrationID;
            if(licenseKey == null)
            {
                TempData["message"] = "Please Enter  LicenseKey.";
                return View("Index", vm);
            }
            if (objLnc.ManualActivate(licenseKey))
            {
                msg = objLnc.m_strErrStr;
            }
            else
            {
                msg = objLnc.m_strErrStr;
            }
            TempData["message"] = msg;
            return View("Index", vm);
        }

        // GET: LicenseManager
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Details()
        {
            var objLnc = new LicenseObject();
            Details_VM details = new Details_VM();

            details.strLicenseMode = objLnc.m_nLicenseMode;

            if (objLnc.m_nLicenseMode == 1)
            {
                details.strLicenseStatus = "Unlimited";
                details.strMaxUser = objLnc.m_nMaxUser;
            }
            else
            {
                if (objLnc.m_nLicenseMode == 0)
                {
                    details.strLicenseStatus = "Unregistered";
                }

                details.nMaxDayPeriod = objLnc.m_nMaxDayPeriod.ToString();
                details.nDayCountPeriod = objLnc.m_nDayCountPeriod.ToString();
                details.nDayLeftPeriod = objLnc.m_nDayLeftPeriod.ToString();
                details.strMaxExec = objLnc.m_nMaxExec;
                details.strExecCount = objLnc.m_nExecCount;
                details.strMaxUser = objLnc.m_nMaxUser;

                if (objLnc.m_nExpDay > 0 || objLnc.m_nExpMonth > 0 || objLnc.m_nExpYear > 0)
                {
                    details.strExpiredDate = objLnc.m_nExpYear + "/" + objLnc.m_nExpMonth + "/" + objLnc.m_nExpDay;
                }
                else
                {
                    details.strExpiredDate = null;
                }

            }
            return View(details);
        }

        [HttpPost]
        [Authorize(Roles = "Owner, Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult AutoDestroy(Activate_VM vm)
        {
            string msg = "";
            var objLnc = new LicenseObject();
            string activationKey = vm.strActivationKey;

            vm.strRegistrationID = objLnc.m_strRegistrationID;
            if (activationKey == null)
            {
                TempData["message"] = "Please Enter ActivationKey.";
                return View("Index", vm);
            }

            if (objLnc.AutoDestroy(activationKey))
            {
                msg = objLnc.m_strErrStr;
            }
            else
            {
                msg = objLnc.m_strErrStr;
            }

            TempData["message"] = msg;
            return View("Index", vm);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult RefreshLicenseServer()
        {
            
            if (ConfigurationManager.AppSettings["cloudService"] != "1963")
            {
                if ((User.Identity.Name == null) || (Session == null) || (Session.SessionID == null))
                {
                    throw new Exception("<NetKey: ErrCode>90</ErrCode>" + "<ErrStr>Bad command</ErrStr>");
                }
                try
                {
                    bool res = false;
                    LicenseObject objLnc = new LicenseObject(0, System.Web.HttpContext.Current.Request, System.Web.HttpContext.Current.Response);
                    res = objLnc.NKLSRefresh(User.Identity.Name, Session.SessionID, 0);
                    if (res != true)
                    {
                        throw new Exception(objLnc.m_strErrStr);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, responseText = ex.Message });
                }
            }
            return Json(new { success = true });
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult UpdateLicenseServer()
        {
            if (ConfigurationManager.AppSettings["cloudService"] != "1963")
            {
                try
                {
                    LicenseObject objLnc = new LicenseObject(0, System.Web.HttpContext.Current.Request, System.Web.HttpContext.Current.Response);
                    if (Session["LoginUserID"] != null)
                    {
                        if (!objLnc.NKLSUpdate(Session["LoginUserID"].ToString(), Session.SessionID, 20))
                        {
                            Session.Abandon();
                            throw new Exception(objLnc.m_strErrStr);
                        }
                    }
                    else
                    {
                        throw new Exception("NetKey: Error - User is not logged in to the license server. Please log off from the system and login again. If problem persists, contact your system administrator.");
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, responseText = ex.Message });
                }
            }

            return Json(new { success = true });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult LicenseError(string error)
        {
            var errMsg = TempData["LicenseKeyErrMsg"] as string;
            if(!string.IsNullOrEmpty(errMsg))
            {
                ViewBag.LicenseKeyErrMsg = errMsg;
            }
            else if (!string.IsNullOrEmpty(error))
            {
                ViewBag.LicenseKeyErrMsg = error;
            }
            return View();
        }
    }
}