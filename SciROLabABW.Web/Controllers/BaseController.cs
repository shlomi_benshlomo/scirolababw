﻿using SciROLabABW.Web.Models;
using System;
using System.Configuration;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SciROLabABW.Web.Controllers
{
    public class LicenseAuthorization : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (ConfigurationManager.AppSettings["cloudService"] != "1963")
            {
#if false
                LicenseObject objLnc = new LicenseObject();
                if (objLnc.m_nErrCode != 0)
                {
                    filterContext.Result = new RedirectToRouteResult(new
                        RouteValueDictionary(new { controller = "LicenseManager", action = "Index" }));
                }
#endif
            }
        }
    }

    [LicenseAuthorization]
    public class BaseController : Controller
    {

    }
}