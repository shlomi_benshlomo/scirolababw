﻿using AutoMapper;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.ViewModels.Teams;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SciROLabABW.Web.TeamsUtilities;
using SciROLabABW.Web.GroupsUtilities;
using SciROLabABW.Web.Authorization;

namespace SciROLabABW.Web.Controllers
{
    // Only Owner can manage teams
    [Authorize(Roles = "Owner")]
    public class TeamsController : Controller
    {
        private SciROLabWebEntities db = new SciROLabWebEntities();

        // GET: Teams
        public ActionResult Index()
        {
            var teamList = Mapper.Map<List<SciROLabTeam>, List<TeamManage_VM>>(db.SciROLabTeams.ToList());
            Mapper.AssertConfigurationIsValid();

            return View(teamList);
        }

        /*Not in use
        // GET: Teams/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }*/

        // GET: Teams/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Teams/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TeamCreate_VM teamCreate_VM)
        {
            if (ModelState.IsValid)
            {
                var teamList = Mapper.Map<List<SciROLabTeam>, List<TeamManage_VM>>(db.SciROLabTeams.ToList());

                foreach (var team in teamList)
                {
                    if (team.Name == teamCreate_VM.Name)
                    {
                        ViewBag.TeamExistsMessage = "Team with the same name already exists! Please try another name.";
                        return View(teamCreate_VM);
                    }
                }

                SciROLabTeam sciROLabTeam = Mapper.Map<SciROLabTeam>(teamCreate_VM); // Map view model items to domain model
                Mapper.AssertConfigurationIsValid();
                db.SciROLabTeams.Add(sciROLabTeam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(teamCreate_VM);

        }

        // GET: Teams/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SciROLabTeam sciROLabTeam = db.SciROLabTeams.Find(id);
            if (sciROLabTeam == null)
            {
                return HttpNotFound();
            }

            TeamManage_VM team = Mapper.Map<TeamManage_VM>(sciROLabTeam);
            Mapper.AssertConfigurationIsValid();

            return View(team);
        }

        // POST: Teams/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TeamManage_VM teamManage_VM)
        {
            if (ModelState.IsValid)
            {
                SciROLabTeam sciROLabTeam = Mapper.Map<SciROLabTeam>(teamManage_VM); // Map view model items to domain model
                Mapper.AssertConfigurationIsValid();

                db.Entry(sciROLabTeam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(teamManage_VM);

        }

        // GET: Teams/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SciROLabTeam sciROLabTeam = db.SciROLabTeams.Find(id);
            if (sciROLabTeam == null)
            {
                return HttpNotFound();
            }

            TeamManage_VM team = Mapper.Map<TeamManage_VM>(sciROLabTeam);
            Mapper.AssertConfigurationIsValid();
            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            SciROLabTeam sciROLabTeam = db.SciROLabTeams.Find(id);

            // Remove all users from the team
            foreach (var user in sciROLabTeam.AspNetUsers.ToList())
            {
                sciROLabTeam.AspNetUsers.Remove(user);
                user.TeamName = null;
            }
            // Remove all groups created under this team
            Groups.RemoveAllTeamGroups(db, id);
            // Remove team
            db.SciROLabTeams.Remove(sciROLabTeam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AddUsers(string teamId)
        {
            try
            {
                if (teamId == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find any team!");
                }

                SciROLabTeam team = db.SciROLabTeams.Find(teamId);

                if (team == null)
                {
                    return HttpNotFound();
                }

                // Build a list of all users that are not in any team
                List<SelectListItem> listItems = new List<SelectListItem>();
                List<AspNetUser> users = db.AspNetUsers.ToList();
                foreach (AspNetUser user in users)
                {
                    if (user.TeamId == null)
                    {
                        SelectListItem selectList = new SelectListItem()
                        {
                            Text = user.UserName,
                            Value = user.Id
                        };
                        listItems.Add(selectList);
                    }
                }

                var viewModel = new TeamManageUsers_VM
                {
                    teamId = team.Id,
                    teamName = team.Name,
                    Users = listItems
                };

                return View("TeamAddUsers", viewModel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUsers(TeamManageUsers_VM viewModel, IEnumerable<string> SelectedUsers)
        {
            try
            {
                SciROLabTeam sciROLabTeam = db.SciROLabTeams.Find(viewModel.teamId);
                if (sciROLabTeam == null)
                {
                    return HttpNotFound();
                }

                // Add selected users to current team
                if (SelectedUsers != null)
                {
                    foreach (string strUser in SelectedUsers)
                    {
                        AspNetUser user = db.AspNetUsers.Find(strUser);
                        user.TeamId = viewModel.teamId;
                        user.TeamName = viewModel.teamName;
                        sciROLabTeam.ActualUsersNum ++;
                    }
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteUsers(string teamId)
        {
            try
            {
                if (teamId == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find any team!");
                }

                SciROLabTeam team = db.SciROLabTeams.Find(teamId);

                if (team == null)
                {
                    return HttpNotFound();
                }

                // Build a list of all users that are in the current team
                List<SelectListItem> listItems = new List<SelectListItem>();
                foreach (AspNetUser user in team.AspNetUsers)
                {
                    SelectListItem selectList = new SelectListItem()
                    {
                        Text = user.UserName,
                        Value = user.Id
                    };
                    listItems.Add(selectList);
                }

                var viewModel = new TeamManageUsers_VM
                {
                    teamId = team.Id,
                    teamName = team.Name,
                    Users = listItems
                };

                return View("TeamDeleteUsers", viewModel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUsers(TeamManageUsers_VM viewModel, IEnumerable<string> SelectedUsers)
        {
            bool rv = true;
            SciROLabTeam sciROLabTeam = db.SciROLabTeams.Find(viewModel.teamId);
            
            if (sciROLabTeam == null)
            {
                return HttpNotFound();
            }

            if (SelectedUsers != null)
            {
                foreach (string strUser in SelectedUsers)
                {
                    AspNetUser user = db.AspNetUsers.Find(strUser);
                    user.TeamId = null;
                    user.TeamName = null;
                    GroupsController grp = new GroupsController();
                    rv = grp.DeleteUserFromAllGroups(user.Id);
                    sciROLabTeam.ActualUsersNum--;
                }
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public bool DeleteUserFromAllTeams(string userID)
        {
            AspNetUser user = db.AspNetUsers.Find(userID);
            List<SciROLabTeam> teams = db.SciROLabTeams.ToList();
            foreach (SciROLabTeam team in teams)
            {
                if (Teams.IsUserInTeam(team, user.UserName))
                {
                    team.AspNetUsers.Remove(user);
                }
            }
            db.SaveChanges();
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
