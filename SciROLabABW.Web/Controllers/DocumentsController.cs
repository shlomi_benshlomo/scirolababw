﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.Controllers
{
    public class DocumentsController : BaseController
    {
        [AllowAnonymous]
        public ActionResult DisplayText(string file_path, string modal_title)
        {
            ViewBag.FileFullPath = file_path;
            ViewBag.ModalTitle = modal_title;
            return PartialView("_DisplayTextModal");
        }
    }
}