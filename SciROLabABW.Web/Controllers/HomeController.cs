﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.TeamsUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (var context = new ApplicationDbContext())
            {
                // Create basic roles (if they don't exist)
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var basicRolesArr = ProjectCommons.BasicRoles;

                for (int i = 0; i < basicRolesArr.Length; i++)
                {
                    if (roleManager.FindByName(basicRolesArr[i]) == null)
                    {
                        roleManager.Create(new IdentityRole(basicRolesArr[i]));
                    }
                }

                // Note: this code is to create the first owner in the system, use only if can't do this in any other way.                
                // If there is no owner user in the system add the default user as default owner
                #region AddFirstOwner
                var defaultUserName = "shlomibe@hotmail.com";
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var usersNames = (from u in userManager.Users select u.UserName).ToList();
                bool ownerFound = false;
                ApplicationUser user = null;

                // Check if there is already owner user in the system
                foreach(var userName in usersNames)
                {
                    user = userManager.FindByName(userName);
                    if (userManager.IsInRole(user.Id, "Owner"))
                    {
                        // There is at least one owner user in the system, mark as found and quit
                        ownerFound = true;
                        break;
                    }
                }
                // If no owner user found add the default user as the default owner
                if (ownerFound == false)
                {
                    ApplicationUser defaultUser = null;
                    // Check if user already exist, if not create it
                    if (!context.Users.Any(u => u.UserName == defaultUserName))
                    {
                        defaultUser = new ApplicationUser {
                            UserName = defaultUserName,
                            Email = "shlomibe@hotmail.com"
                        };
                        userManager.Create(defaultUser, "Sbs12#");
                    }
                    else // User already exists, find user in database by username
                    {
                        defaultUser = userManager.FindByName(defaultUserName);
                    }
                    if (defaultUser != null)
                    {
                        roleManager.Create(new IdentityRole("Owner"));
                        userManager.AddToRole(defaultUser.Id, "Owner");
                        context.SaveChanges();
                    }
                }
                #endregion
            }
            return View();
        }

        public ActionResult LogOut()
        {
            if (ConfigurationManager.AppSettings["cloudService"] != "1963")
            {
                // Log out from the license server.
                LicenseObject objLnc = new LicenseObject(0, System.Web.HttpContext.Current.Request, System.Web.HttpContext.Current.Response);
                if (Session["LoginUserID"] != null)
                {
                    objLnc.NKLSLogout(Session["LoginUserID"].ToString(), Session.SessionID);
                }
                Session.Abandon();
            }
            return View("Index");
        }

        public ActionResult About()
        {
            string currentUsrName = User.Identity.Name;
            // If user exists display some information about the user
            if (!string.IsNullOrEmpty(currentUsrName))
            {
                SciROLabWebEntities db = new SciROLabWebEntities();
                if (db != null)
                {
                    SciROLabTeam team = Teams.GetUserTeam(db);
                    if (team != null)
                    {
                        ViewBag.TeamName = team.Name;
                    }
                    else
                    {
                        ViewBag.TeamName = "You don't belong to any team";
                    }
                }
            }

            ViewBag.Message = ProjectCommons.productVersionName;
            ViewBag.Version = "Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}