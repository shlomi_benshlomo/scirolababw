﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SciROLabABW.Web.Models;
using System.IO;
using SciROLabABW.Web.ViewModels;
using SciROLabABW.Web.GroupsUtilities;
using AutoMapper;
using System.Threading.Tasks;
using System.Web.Hosting;
using SciROLabABW.Web.Mappings;
using PagedList;
using Microsoft.AspNet.Identity;
using System.Linq.Expressions;
using SciROLabABW.Web.Authorization;
using SciROLabABW.Web.MethodsUtilities;
using SciROLabABW.Web.TeamsUtilities;
using SciROLabABW.Web.RowsUtilities;

namespace SciROLabABW.Web.Controllers
{
    [Authorize]
    public class WesternBlotController : BaseController
    {
        private SciROLabWebEntities db = new SciROLabWebEntities();

        // GET: WesternBlot
        public ActionResult Index(WBIndex_VM model, string wbSortOrder, int? page, 
                                  string pageSizeStr, string startSearch, 
                                  string clearSearch, string searchType)
        {
            bool searchState = false;

            if (!string.IsNullOrEmpty(clearSearch))
            {
                model.ClearSearch(model);
                Session["freeSearchStr"] = null;
            }

            if ((searchType == null) && (model.CurrentSearchType == null))
            {
                if (Session["searchType"] == null)
                {
                    Session["searchType"] = "freeSearch";
                }
                searchType = Session["searchType"].ToString();
                model.CurrentSearchType = Session["searchType"].ToString();
            }
            else if (searchType == null)
            {
                searchType = model.CurrentSearchType;
                Session["searchType"] = searchType;
            }
            else if (model.CurrentSearchType != searchType)
            {
                model.CurrentSearchType = searchType;
                Session["searchType"] = searchType;
            }

            // Set the sort parameters
            if (wbSortOrder != null)
            {
                model.CurrentSortOrder = wbSortOrder;
                Session["sortOrder"] = wbSortOrder;
            }
            else if (model.CurrentSortOrder != null)
            {
                wbSortOrder = model.CurrentSortOrder;
                Session["sortOrder"] = wbSortOrder;
            }
            else
            {
                if(Session["sortOrder"] == null)
                {
                    Session["sortOrder"] = "Image ID, Descending"; // Set to default value
                }
                wbSortOrder = Session["sortOrder"].ToString();
                model.CurrentSortOrder = wbSortOrder;
            }

            // Set pagination parameters
            if (pageSizeStr != null)
            {
                model.CurrentPageSize = pageSizeStr;
                Session["pageSizeStr"] = pageSizeStr;
            }
            else if (model.CurrentPageSize != null)
            {
                pageSizeStr = model.CurrentPageSize;
                Session["pageSizeStr"] = pageSizeStr;
            }
            
            else
            {
                if (Session["pageSizeStr"] == null)
                {
                    Session["pageSizeStr"] = 5; // Set to default value
                }
                pageSizeStr = Session["pageSizeStr"].ToString();
                model.CurrentPageSize = pageSizeStr;
            }           

            // Get all main table rows that the user has permission to view.
            var wbMainTable = Rows.getUserPermmitedRowsViewMainTable(db);

            // If there is something in search boxes, update the query according to this data 
            // Set the page to the first page in case of a new search
            // else use currrent info to update search fields to the value they were set to on the last change
            if ((!string.IsNullOrEmpty(model.freeSearchStr)) ||
                (!string.IsNullOrEmpty(model.searchUserName)) ||
                (!string.IsNullOrEmpty(model.searchMethod)) ||
                (!string.IsNullOrEmpty(model.searchExperimenter)) ||
                (!string.IsNullOrEmpty(model.searchPrimaryAntibody)) ||
                (model.dateAddedMinDate != null) ||
                (model.dateAddedMaxDate != null) ||
                (model.expMinDate != null) ||
                (model.expMaxDate != null))
            {
                page = 1;
            }
            else
            {
                model.freeSearchStr = model.FreeSearchCurrentFilter;
                model.searchUserName = model.UserNameCurrentFilter;
                model.searchMethod = model.MethodCurrentFilter;
                model.searchExperimenter = model.ExperimenterCurrentFilter;
                model.searchPrimaryAntibody = model.PrimaryAntibodyCurrentFilter;
                model.dateAddedMinDate = model.DateAddedCurrentMinDate;
                model.dateAddedMaxDate = model.DateAddedCurrentMaxDate;
                model.expMinDate = model.ExperimentCurrentMinDate;
                model.expMaxDate = model.ExperimentCurrentMaxDate;
            }

            if (searchType == "freeSearch")
            {
                model.ClearAdvancedSearch(model);
                // If user is pressing on search and the search box is empty - reset Session state
                if ((!string.IsNullOrEmpty(startSearch)) && (String.IsNullOrEmpty(model.freeSearchStr)))
                {
                    Session["freeSearchStr"] = null;
                }
                else if ((String.IsNullOrEmpty(model.freeSearchStr)) && (Session["freeSearchStr"] != null))
                {
                    model.freeSearchStr = Session["freeSearchStr"].ToString();
                }

                if (!String.IsNullOrEmpty(model.freeSearchStr))
                {
                    Session["freeSearchStr"] = model.freeSearchStr;
                    // TODO:2: still need to work on this one to make the code more readable using Lambada like I did with the 'ContainsOrExpression'!!!
                    wbMainTable = wbMainTable.Where(sl =>
                        sl.mainID.ToString().Contains(model.freeSearchStr.ToUpper()) ||
                        sl.userName.ToString().Contains(model.freeSearchStr.ToUpper()) ||
                        sl.experimentMethod.ToString().Contains(model.freeSearchStr.ToUpper()) ||
                        sl.experimentID.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.experimentInfo.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.performer.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.blockID.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.primaryAntibodyId.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.primaryAntibody.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.secondaryAntibody.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.membraneInfo.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.membraneID.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.blot.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.comments.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.text1.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.text2.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.text3.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.text4.Contains(model.freeSearchStr.ToUpper()) ||
                        sl.imageName.Contains(model.freeSearchStr.ToUpper()));

                    searchState = true;
                }
            }
            else if (searchType == "advancedSearch")
            {
                model.ClearFreeSearch(model);
                Session["freeSearchStr"] = null;

                if (!string.IsNullOrEmpty(model.searchUserName))
                {
                    wbMainTable = wbMainTable.Where(sl => sl.userName.ToString().Contains(model.searchUserName.ToUpper()));
                    searchState = true;
                }

                if (!string.IsNullOrEmpty(model.searchMethod))
                {
                    wbMainTable = wbMainTable.Where(sl => sl.experimentMethod.ToString().Contains(model.searchMethod.ToUpper()));
                    searchState = true;
                }

                if (!string.IsNullOrEmpty(model.searchExperimenter))
                {
                    wbMainTable = wbMainTable.Where(sl => sl.performer.ToString().Contains(model.searchExperimenter.ToUpper()));
                    searchState = true;
                }

                if (!string.IsNullOrEmpty(model.searchPrimaryAntibody))
                {
                    wbMainTable = wbMainTable.Where(sl => sl.primaryAntibody.ToString().Contains(model.searchPrimaryAntibody.ToUpper()));
                    searchState = true;
                }

                // Filter data according to min. and max. date
                if ((model.expMinDate != null) && (model.expMaxDate != null))
                {
                    wbMainTable = wbMainTable.Where(x => x.experimentDate <= model.expMaxDate && x.experimentDate >= model.expMinDate);
                    searchState = true;
                }

                // Filter data according to min. and max. date
                if ((model.dateAddedMinDate != null) && (model.dateAddedMaxDate != null))
                {
                    wbMainTable = wbMainTable.Where(x => x.dateAdded <= model.dateAddedMaxDate && x.dateAdded >= model.dateAddedMinDate);
                    searchState = true;
                }
            }            

            // Sort data according to the selected value
            switch (wbSortOrder)
            {
                case "Image ID, Descending":
                default:
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.mainID);
                    break;
                case "Image ID, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.mainID);
                    break;
                case "User Name, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.userName);
                    break;
                case "User Name, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.userName);
                    break;
                case "Image Name, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb .imageName);
                    break;
                case "Image Name, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.imageName);
                    break;
                case "Share Row Mode":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.shareRow);
                    break;
                case "Date Added, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.dateAdded);
                    break;
                case "Date Added, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.dateAdded);
                    break;
                case "Method, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.experimentMethod);
                    break;
                case "Method, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.experimentMethod);
                    break;
                case "Experiment ID, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.experimentID);
                    break;
                case "Experiment ID, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.experimentID);
                    break;
                case "Experiment Date, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.experimentDate);
                    break;
                case "Experiment Date, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.experimentDate);
                    break;
                case "Experimenter, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.performer);
                    break;
                case "Experimenter, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.performer);
                    break;
                case "Primary Antibody Id, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.primaryAntibodyId);
                    break;
                case "Primary Antibody Id, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.primaryAntibodyId);
                    break;
            }

            // Set number of items per page according to page size selection
            int pageSize;
            switch (pageSizeStr)
            {
                case "5":
                default:
                    pageSize = 5;
                    break;
                case "10":
                    pageSize = 10;
                    break;
                case "15":
                    pageSize = 15;
                    break;
                case "25":
                    pageSize = 25;
                    break;
                // 50 rows per page is causing the system to be slow and for some reason 
                // system can't delete 50 items using javascript at the same time
                /*case "50":
                    pageSize = 50;
                    break;*/
            }

            var WBMainTableIndex_VM_list = Mapper.Map<List<WBMainTable>, List<WBMainTableIndex_VM>>(wbMainTable.ToList());
            Mapper.AssertConfigurationIsValid();

            // Update all parameters to pass to the view
            model.CurrentSortOrder = wbSortOrder;
            model.FreeSearchCurrentFilter = model.freeSearchStr;
            model.UserNameCurrentFilter = model.searchUserName;
            model.MethodCurrentFilter = model.searchMethod;
            model.ExperimenterCurrentFilter = model.searchExperimenter;
            model.PrimaryAntibodyCurrentFilter = model.searchPrimaryAntibody;
            model.DateAddedCurrentMinDate = model.dateAddedMinDate;
            model.DateAddedCurrentMaxDate = model.dateAddedMaxDate;
            model.ExperimentCurrentMinDate = model.expMinDate;
            model.ExperimentCurrentMaxDate = model.expMaxDate;

            if (searchState == true)
            {
                model.CurrentSearchStatMsg = "Search is ON";
            }
            else
            {
                model.CurrentSearchStatMsg = "";
            }

            model.CurrentSearchType = searchType;

            // Populate sort dropdownlist and set selected value
            ViewData["WBSortOrder"] = new SelectList(model.GetWBSortList(), wbSortOrder);
            // Populate page size dropdownlist and set selected value
            ViewData["pageSizeStr"] = new SelectList(model.GetWBPageSizeList(), pageSizeStr);
            // Update page number with page value if not null, otherwise set to 1
            int pageNumber = (page ?? 1);

            model.WBMainTableIndex_VM = WBMainTableIndex_VM_list.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult TileView(int? id, string freeSearchStr, string startSearch)
        {
            const int recordsPerPage = 16;
            bool searchState = false;

            // Get all main table rows that the user has permission to view.
            var wbMainTable = Rows.getUserPermmitedRowsViewMainTable(db);

            // If user is pressing on search and the search box is empty - reset Session state
            if ((!string.IsNullOrEmpty(startSearch)) && (String.IsNullOrEmpty(freeSearchStr)))
            {
                Session["freeSearchStr"] = null;
            }
            else if ((String.IsNullOrEmpty(freeSearchStr)) && (Session["freeSearchStr"] != null)) // else - update freeSearchStr with the value in Session["freeSearchStr"]
            {
                freeSearchStr = Session["freeSearchStr"].ToString();
            }

            if (!String.IsNullOrEmpty(freeSearchStr))
            {
                Session["freeSearchStr"] = freeSearchStr;
                wbMainTable = wbMainTable.Where(sl =>
                    sl.imageName.Contains(freeSearchStr.ToUpper()));
            }

            var WBMainTableIndex_VM_list = Mapper.Map<List<WBMainTable>, List<WBMainTableIndex_VM>>(wbMainTable.ToList());
            Mapper.AssertConfigurationIsValid();

            var page = id ?? 0;

            ViewBag.CurrentSearchText = freeSearchStr;
            if (searchState == true)
            {
                ViewData["CurrentSearchStatMsg"] = "Search is ON";
            }
            else
            {
                ViewData["CurrentSearchStatMsg"] = "";
            }

            if (Request.IsAjaxRequest())
            {
                var skipRecords = page * recordsPerPage;
                if (skipRecords <= WBMainTableIndex_VM_list.Count)
                {
                    return PartialView("_TileView", WBMainTableIndex_VM_list.OrderBy(m => m.imageName).Skip(skipRecords).Take(recordsPerPage).ToList());
                }
                return null;
            }

            return View(WBMainTableIndex_VM_list.OrderBy(m => m.imageName).Take(recordsPerPage).ToList());
        }

        [HttpGet]
        public async Task<ActionResult> SelectedRowsTileView(Guid[] selectedRows)
        {
            try
            {
                // Build a list of all the selected rows
                var wbMainTable = new List<WBMainTable>();
                foreach (var id in selectedRows)
                {
                    WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
                    wbMainTable.Add(wBMainTableRow);
                }

                var WBMainTableIndex_VM_list = Mapper.Map<List<WBMainTable>, List<WBMainTableIndex_VM>>(wbMainTable.ToList());
                Mapper.AssertConfigurationIsValid();

                return PartialView("_TileViewSelectedRows", WBMainTableIndex_VM_list);
            }
            catch
            {
                return Json(new { ok = true, method = "Can't display files in Tile View" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TileViewSelected(Guid[] selectedRows)
        {
            var wbMainTable = db.WBMainTable;

            var WBMainTableIndex_VM_list = Mapper.Map<List<WBMainTable>, List<WBMainTableIndex_VM>>(wbMainTable.ToList());
            Mapper.AssertConfigurationIsValid();

            return View(WBMainTableIndex_VM_list);
        }

        // GET: WesternBlot/Details/5
        public async Task<ActionResult> Details(Guid? id, string viewstyle)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
            if (wBMainTableRow == null)
            {
                return HttpNotFound();
            }

            var wBMainTableViewModel = Mapper.Map<WBMainTableDetails_VM>(wBMainTableRow);
            Mapper.AssertConfigurationIsValid();

            if ((Request.IsAjaxRequest()) && (viewstyle != null))
            {
                if (viewstyle == "large")
                {
                    return PartialView("_DetailsViewStyleLarge", wBMainTableViewModel);
                }
                else if (viewstyle == "mid")
                {
                    return PartialView("_DetailsViewStyleMid", wBMainTableViewModel);
                }
                else
                {
                    return PartialView("_DetailsViewStyleSmall", wBMainTableViewModel);
                }
            }

            return View(wBMainTableViewModel);
        }

        // GET: WesternBlot/Create
        public ActionResult Create()
        {
            WBMainTableCreate_VM model = new WBMainTableCreate_VM();

            // Populate methods list based on methods in MethodsTable
            List<MethodsTable> methods = Methods.GetPermittedMethodsList(db);
            foreach (var method in methods)
            {
                model.MethodsList.Add(new SelectListItem { Text = method.methodName, Value = method.methodName, Selected = false });
            }
            // Add predefined methods to the end of the list.
            model.MethodsList.Add(new SelectListItem { Text = "Western Blot", Value = "Western Blot" });
            model.MethodsList.Add(new SelectListItem { Text = "Immunostaining", Value = "Immunostaining" });
            return View(model);
        }

        // POST: WesternBlot/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(WBMainTableCreate_VM wBMainTableCreate_VM)
        {
            string imgDestPath = null;
            string storedImgFullPath = null;
            string downSampleImgPath = null;
            string labelFileName = null;
            string macroFileName = null;
            string lableImgPath = null;
            string macroImgPath = null;
            string errMsg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    HttpPostedFileBase file = wBMainTableCreate_VM.file; // Get image file, added by the user, from the view
                    if ((file != null) && (file.ContentLength > 0))
                    {
                        imgDestPath = FileServices.SetFileDestPath("image", db);
                        if (String.IsNullOrEmpty(imgDestPath))
                        {
                            throw new InvalidOperationException("Uploading image to the server failed.");
                        }
                        // Upload the image to the server
                        string storedFileName = FileServices.FileUpload(file, imgDestPath);
                        // If upload failed there is no point to continue with creating the row - print a message and quit
                        if (String.IsNullOrEmpty(storedFileName))
                        {
                            throw new InvalidOperationException("Uploading image to the server failed.");
                        }
                        storedImgFullPath = Path.Combine(Url.Content(imgDestPath), storedFileName);

                        if (storedImgFullPath != null)
                        {
                            // Check if there is a downsample image (like in case of openslide images).
                            // If yes, upload the downsample image to the server to improve performance.
                            string downSampleFileName = FileServices.uploadDownSampleImage(storedImgFullPath, imgDestPath);
                            if (!String.IsNullOrEmpty(downSampleFileName))
                            {
                                downSampleImgPath = Path.Combine(Url.Content(imgDestPath), downSampleFileName);
                            }

                            // Check if there are more associated images (like in case of openslide images).
                            // If yes, upload them to the server to improve performance.
                            labelFileName = FileServices.uploadAssociatedImage(storedImgFullPath, imgDestPath, "label");
                            if (!String.IsNullOrEmpty(labelFileName))
                            {
                                lableImgPath = Path.Combine(Url.Content(imgDestPath), labelFileName);
                            }
                            macroFileName = FileServices.uploadAssociatedImage(storedImgFullPath, imgDestPath, "macro");
                            if (!String.IsNullOrEmpty(macroFileName))
                            {
                                macroImgPath = Path.Combine(Url.Content(imgDestPath), macroFileName);
                            }

                            // Add a new row to the table and update it with the current data
                            wBMainTableCreate_VM.imageType = file.ContentType;
                            wBMainTableCreate_VM.image = FileServices.imageToThumbnail(file, storedImgFullPath); // Save thumbnail of the original image in database
                            wBMainTableCreate_VM.imageName = Path.GetFileName(file.FileName);

                            // If user selected a specific Method from the list assign the method to the new row
                            if ((wBMainTableCreate_VM.SelectedMethod != null) && (wBMainTableCreate_VM.SelectedMethod != "No Method"))
                            {
                                wBMainTableCreate_VM.experimentMethod = wBMainTableCreate_VM.SelectedMethod;
                            }

                            WBMainTable wBMainTable = Mapper.Map<WBMainTable>(wBMainTableCreate_VM); // Map view model items to domain model
                            Mapper.AssertConfigurationIsValid();
                            // Save original image name to display to user in database
                            wBMainTable.storedImageName = storedFileName;
                            // Save the path of the image in database
                            wBMainTable.storedImagePath = storedImgFullPath;

                            // Save downsample image info to database
                            wBMainTable.storedDownSampleImagePath = downSampleImgPath;

                            // Save associated images info to database
                            wBMainTable.storedLabelImagePath = lableImgPath;
                            wBMainTable.storedMacroImagePath = macroImgPath;

                            // Populate methods list based on methods in MethodsTable
                            List<MethodsTable> methods = Methods.GetPermittedMethodsList(db);
                            foreach (var method in methods)
                            {
                                if (method.methodName == wBMainTable.experimentMethod)
                                {
                                    wBMainTable.text1Name = method.text1Name;
                                    wBMainTable.text2Name = method.text2Name;
                                    wBMainTable.text3Name = method.text3Name;
                                    wBMainTable.text4Name = method.text4Name;
                                    wBMainTable.text5Name = method.text5Name;
                                    wBMainTable.text6Name = method.text6Name;
                                    wBMainTable.text7Name = method.text7Name;
                                    wBMainTable.text8Name = method.text8Name;
                                    wBMainTable.text9Name = method.text9Name;
                                    wBMainTable.text10Name = method.text10Name;
                                    wBMainTable.text11Name = method.text11Name;
                                    wBMainTable.text12Name = method.text12Name;
                                    wBMainTable.text13Name = method.text13Name;
                                    wBMainTable.text14Name = method.text14Name;
                                    wBMainTable.text15Name = method.text15Name;
                                    wBMainTable.date1Name = method.date1Name;
                                    wBMainTable.date2Name = method.date2Name;
                                    wBMainTable.date3Name = method.date3Name;
                                    wBMainTable.bool1Name = method.bool1Name;
                                    wBMainTable.bool2Name = method.bool2Name;
                                    wBMainTable.bool3Name = method.bool3Name;
                                    wBMainTable.bool4Name = method.bool4Name;
                                    wBMainTable.bool5Name = method.bool5Name;
                                    wBMainTable.bool6Name = method.bool6Name;
                                    wBMainTable.bool7Name = method.bool7Name;
                                    wBMainTable.bool8Name = method.bool8Name;
                                }
                            }

                            db.WBMainTable.Add(wBMainTable);
                            await db.SaveChangesAsync();
                            return Json(new { ok = true });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errMsg = "Error! " + ex.Message + " ";
                if ((ex.InnerException != null) && (ex.InnerException.InnerException != null))
                {
                    errMsg = errMsg + ex.InnerException.InnerException.Message;
                }
            }
            // If we got here it means something went wrong, mark as BadRequest and return error message
            if (errMsg == "")
            {
                errMsg = "Invalid image or data - upload failed!";
            }
            // If create failed check which image was already uploaded to the server and delete it
            if (!String.IsNullOrEmpty(storedImgFullPath))
            {
                FileServices.deleteFile(Url.Content(storedImgFullPath));
            }
            if (!String.IsNullOrEmpty(downSampleImgPath))
            {
                FileServices.deleteFile(Url.Content(downSampleImgPath));
            }
            if (!String.IsNullOrEmpty(lableImgPath))
            {
                FileServices.deleteFile(Url.Content(lableImgPath));
            }
            if (!String.IsNullOrEmpty(macroImgPath))
            {
                FileServices.deleteFile(Url.Content(macroImgPath));
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(errMsg);
        }

        [HttpGet]
        public ActionResult ExperimentMethodCreateView(string selectedmethodid)
        {
            if (selectedmethodid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (selectedmethodid == "No Method")
            {
                return Json(new { ok = true, method = "no method" }, JsonRequestBehavior.AllowGet);
            }
            if (selectedmethodid == "Western Blot")
            {
                return PartialView("_WBRowDBCreate");
            }
            else if (selectedmethodid == "Immunostaining")
            {
                return PartialView("_ISRowDBCreate");
            }
            else
            {
                var wBMainTableCreate_VM = new WBMainTableCreate_VM();

                List<MethodsTable> methods = Methods.GetPermittedMethodsList(db);
                foreach (var method in methods)
                {
                    if (method.methodName == selectedmethodid)
                    {
                        wBMainTableCreate_VM.Text1Name = method.text1Name;
                        wBMainTableCreate_VM.Text2Name = method.text2Name;
                        wBMainTableCreate_VM.Text3Name = method.text3Name;
                        wBMainTableCreate_VM.Text4Name = method.text4Name;
                        wBMainTableCreate_VM.Text5Name = method.text5Name;
                        wBMainTableCreate_VM.Text6Name = method.text6Name;
                        wBMainTableCreate_VM.Text7Name = method.text7Name;
                        wBMainTableCreate_VM.Text8Name = method.text8Name;
                        wBMainTableCreate_VM.Text9Name = method.text9Name;
                        wBMainTableCreate_VM.Text10Name = method.text10Name;
                        wBMainTableCreate_VM.Text11Name = method.text11Name;
                        wBMainTableCreate_VM.Text12Name = method.text12Name;
                        wBMainTableCreate_VM.Text13Name = method.text13Name;
                        wBMainTableCreate_VM.Text14Name = method.text14Name;
                        wBMainTableCreate_VM.Text15Name = method.text15Name;
                        wBMainTableCreate_VM.Date1Name = method.date1Name;
                        wBMainTableCreate_VM.Date2Name = method.date2Name;
                        wBMainTableCreate_VM.Date3Name = method.date3Name;
                        wBMainTableCreate_VM.Bool1Name = method.bool1Name;
                        wBMainTableCreate_VM.Bool2Name = method.bool2Name;
                        wBMainTableCreate_VM.Bool3Name = method.bool3Name;
                        wBMainTableCreate_VM.Bool4Name = method.bool4Name;
                        wBMainTableCreate_VM.Bool5Name = method.bool5Name;
                        wBMainTableCreate_VM.Bool6Name = method.bool6Name;
                        wBMainTableCreate_VM.Bool7Name = method.bool7Name;
                        wBMainTableCreate_VM.Bool8Name = method.bool8Name;
                        break;
                    }
                }
                return PartialView("_RowDBCreate", wBMainTableCreate_VM);
            }
        }

        // GET: WesternBlot/Edit/5
        [EditDelAuthorize]
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
            if (wBMainTableRow == null)
            {
                return HttpNotFound();
            }

            var wBMainTableViewModel = Mapper.Map<WBMainTableEdit_VM>(wBMainTableRow);
            Mapper.AssertConfigurationIsValid();

            // Populate methods list based on methods in MethodsTable
            List<MethodsTable> methods = Methods.GetPermittedMethodsList(db);
            foreach (var method in methods)
            {
                wBMainTableViewModel.MethodsList.Add(new SelectListItem { Text = method.methodName, Value = method.methodName, Selected = false });
            }
            // Add predefined methods to the end of the list.
            wBMainTableViewModel.MethodsList.Add(new SelectListItem { Text = "Western Blot", Value = "Western Blot" });
            wBMainTableViewModel.MethodsList.Add(new SelectListItem { Text = "Immunostaining", Value = "Immunostaining" });

            // Get the previous url and store it in the view model to be able to go back to the same location if needed
            wBMainTableViewModel.previousUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View(wBMainTableViewModel);
        }

        // SET: WesternBlot/Edit/5
        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(WBMainTableEdit_VM wBMainTableEdit_VM)
        {
            string errMsg = "";
            try
            {
                if (ModelState.IsValid)
                {
                    var wBMainTable = Mapper.Map<WBMainTable>(wBMainTableEdit_VM);
                    Mapper.AssertConfigurationIsValid();
                    db.Entry(wBMainTable).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    // Redirect to previous URL
                    return Json(new { ok = true, redirectUrl = wBMainTableEdit_VM.previousUrl, isRedirect = true });
                }
            }
            catch (Exception ex)
            {
                errMsg = "Error! " + ex.Message + " ";
                if ((ex.InnerException != null) && (ex.InnerException.InnerException != null))
                {
                    errMsg = errMsg + ex.InnerException.InnerException.Message;
                }
            }

            // If we got here it means something went wrong, mark as BadRequest and return error message
            if (errMsg == "")
            {
                errMsg = "Invalid image or data - upload failed!";
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(errMsg);
        }

        // GET: WesternBlot/EditMultiple/5
        [EditDelAuthorize]
        public ActionResult EditMultiple(string listSortOrder, string currentSortOrder, 
                                         string currentSearchText, string freeSearchStr, 
                                         int? page, string pageSizeStr,
                                         string startSearch, string currentPageSize)
        {
            WBIndex_VM pagingModel = new WBIndex_VM();
            bool searchState = false;

            // Set the sort parameters
            if (listSortOrder != null)
            {
                currentSortOrder = listSortOrder;
                Session["sortOrder"] = listSortOrder;
            }
            else if (currentSortOrder != null)
            {
                listSortOrder = currentSortOrder;
                Session["sortOrder"] = listSortOrder;
            }
            else
            {
                if (Session["sortOrder"] == null)
                {
                    Session["sortOrder"] = "Image ID, Descending"; // Set to default value
                }
                listSortOrder = Session["sortOrder"].ToString();
                currentSortOrder = listSortOrder;
            }

            // Set pagination parameters
            if (pageSizeStr != null)
            {
                currentPageSize = pageSizeStr;
                Session["pageSizeStr"] = pageSizeStr;
            }
            else if (currentPageSize != null)
            {
                pageSizeStr = currentPageSize;
                Session["pageSizeStr"] = pageSizeStr;
            }
            else
            {
                if (Session["pageSizeStr"] == null)
                {
                    Session["pageSizeStr"] = 5; // Set to default value
                }
                pageSizeStr = Session["pageSizeStr"].ToString();
                currentPageSize = pageSizeStr;
            }

            // Load from the DB server only rows that belong to the current user
            string usrName = User.Identity.GetUserName();
            var wbMainTable = db.WBMainTable.Where(u => u.userName == usrName);       

            // Note: At this point we don't let the user to modify experiment methods in this view so we don't need this.
            // Build a list of methods that the user has permission to view/use
            /*List<MethodsTable> methods = Methods.GetPermittedMethodsList(db);
            foreach (var item in WBMainTableEdit_VM_list)
            {
                // Populate methods list
                foreach (var method in methods)
                {
                    item.MethodsList.Add(new SelectListItem { Text = method.methodName, Value = method.methodName, Selected = false });
                }
            }*/

            // If there is something in search boxes, update the query according to this data 
            // Set the page to the first page in case of a new search
            // else use currrent info to update search fields to the value they were set to on the last change
            if (!string.IsNullOrEmpty(freeSearchStr))
            {
                page = 1;
            }
            else
            {
                freeSearchStr = currentSearchText;
            }

            // If user is pressing on search and the search box is empty - reset Session state
            if ((!string.IsNullOrEmpty(startSearch)) && (String.IsNullOrEmpty(freeSearchStr)))
            {
                Session["freeSearchStr"] = null;
            }
            else if ((String.IsNullOrEmpty(freeSearchStr)) && (Session["freeSearchStr"] != null))
            {
                freeSearchStr = Session["freeSearchStr"].ToString();
            }

            if (!String.IsNullOrEmpty(freeSearchStr))
            {
                Session["freeSearchStr"] = freeSearchStr;
                // TODO:2: still need to work on this one to make the code more readable using Lambada like I did with the 'ContainsOrExpression'!!!
                wbMainTable = wbMainTable.Where(sl =>
                    sl.mainID.ToString().Contains(freeSearchStr.ToUpper()) ||
                    sl.userName.ToString().Contains(freeSearchStr.ToUpper()) ||
                    sl.experimentMethod.ToString().Contains(freeSearchStr.ToUpper()) ||
                    sl.experimentID.Contains(freeSearchStr.ToUpper()) ||
                    sl.experimentInfo.Contains(freeSearchStr.ToUpper()) ||
                    sl.performer.Contains(freeSearchStr.ToUpper()) ||
                    sl.blockID.Contains(freeSearchStr.ToUpper()) ||
                    sl.primaryAntibodyId.Contains(freeSearchStr.ToUpper()) ||
                    sl.primaryAntibody.Contains(freeSearchStr.ToUpper()) ||
                    sl.secondaryAntibody.Contains(freeSearchStr.ToUpper()) ||
                    sl.membraneInfo.Contains(freeSearchStr.ToUpper()) ||
                    sl.membraneID.Contains(freeSearchStr.ToUpper()) ||
                    sl.blot.Contains(freeSearchStr.ToUpper()) ||
                    sl.comments.Contains(freeSearchStr.ToUpper()) ||
                    sl.text1.Contains(freeSearchStr.ToUpper()) ||
                    sl.text2.Contains(freeSearchStr.ToUpper()) ||
                    sl.text3.Contains(freeSearchStr.ToUpper()) ||
                    sl.text4.Contains(freeSearchStr.ToUpper()) ||
                    sl.imageName.Contains(freeSearchStr.ToUpper()));

                searchState = true;
            }

            // Sort data according to the selected value
            switch (listSortOrder)
            {
                case "Image ID, Descending":
                default:
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.mainID);
                    break;
                case "Image ID, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.mainID);
                    break;
                case "User Name, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.userName);
                    break;
                case "User Name, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.userName);
                    break;
                case "Image Name, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.imageName);
                    break;
                case "Image Name, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.imageName);
                    break;
                case "Share Row Mode":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.shareRow);
                    break;
                case "Date Added, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.dateAdded);
                    break;
                case "Date Added, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.dateAdded);
                    break;
                case "Method, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.experimentMethod);
                    break;
                case "Method, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.experimentMethod);
                    break;
                case "Experiment ID, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.experimentID);
                    break;
                case "Experiment ID, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.experimentID);
                    break;
                case "Experiment Date, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.experimentDate);
                    break;
                case "Experiment Date, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.experimentDate);
                    break;
                case "Experimenter, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.performer);
                    break;
                case "Experimenter, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.performer);
                    break;
                case "Primary Antibody Id, Ascending":
                    wbMainTable = wbMainTable.OrderBy(wb => wb.primaryAntibodyId);
                    break;
                case "Primary Antibody Id, Descending":
                    wbMainTable = wbMainTable.OrderByDescending(wb => wb.primaryAntibodyId);
                    break;
            }

            // Set number of items per page according to page size selection
            int pageSize;
            switch (pageSizeStr)
            {
                case "5":
                default:
                    pageSize = 5;
                    break;
                case "10":
                    pageSize = 10;
                    break;
                case "15":
                    pageSize = 15;
                    break;
                case "25":
                    pageSize = 25;
                    break;
                // 50 rows per page is causing the system to be slow and for some reason 
                // system can't delete 50 items using javascript at the same time
                /*case "50":
                    pageSize = 50;
                    break;*/
            }

            // Populate sort dropdownlist and set selected value
            ViewData["ListSortOrder"] = new SelectList(pagingModel.GetWBSortList(), listSortOrder);
            // Populate page size dropdownlist and set selected value
            ViewData["pageSizeStr"] = new SelectList(pagingModel.GetWBPageSizeList(), pageSizeStr);
            // Update page number with page value if not null, otherwise set to 1
            int pageNumber = (page ?? 1);

            ViewBag.CurrentSearchText = freeSearchStr;
            if (searchState == true)
            {
                ViewData["CurrentSearchStatMsg"] = "Search is ON";
            }
            else
            {
                ViewData["CurrentSearchStatMsg"] = "";
            }

            ViewBag.CurrentSortOrder = listSortOrder;
            ViewBag.CurrentPageSize = pageSizeStr;            

            var WBMainTableEdit_VM_list = Mapper.Map<List<WBMainTable>, List<WBMainTableEdit_VM>>(wbMainTable.ToList());
            Mapper.AssertConfigurationIsValid();

            var model = WBMainTableEdit_VM_list.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditMultiple(List<WBMainTableEdit_VM> model)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in model)
                {
                    var row = await db.WBMainTable.FindAsync(item.rowID);

                    row = Mapper.Map<WBMainTable>(item);
                    Mapper.AssertConfigurationIsValid();

                    // Before setting the state of the entity to EntityState.Modified
                    // We should first verify if the entity is already present inside the DBSet and if yes to detache the local entity.
                    // Otherwise we'll get the following error message: 
                    // Attaching an entity of type 'X' failed because another entity of the same type already has the same primary key value. 
                    var local = db.Set<WBMainTable>().Local.FirstOrDefault(f => f.rowID == item.rowID);
                    if (local != null)
                    {
                        db.Entry(local).State = EntityState.Detached;
                    }
                    db.Entry(row).State = EntityState.Modified;
                }
                db.SaveChanges();
                //return RedirectToAction("Index");
                return Json(new { success = true });
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(errors);
            }            
        }

        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> EditSelectedRows(Guid[] selectedRows)
        {
            string errMsg = null;

            if (selectedRows == null)
            {
                errMsg = "No row has been selected!";
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(errMsg, JsonRequestBehavior.AllowGet);
            }

            try
            {
                // Build a list of all the selected rows that current user has permission to edit
                var wbMainTable = new List<WBMainTable>();
                foreach (var id in selectedRows)
                {
                    WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
                    if (Rows.userEditMainRowPermission(wBMainTableRow.userName) == true)
                    {
                        wbMainTable.Add(wBMainTableRow);
                    }
                }

                List<WBMainTableEdit_VM> WBMainTableEdit_VM_list = Mapper.Map<List<WBMainTable>, List<WBMainTableEdit_VM>>(wbMainTable.ToList());
                Mapper.AssertConfigurationIsValid();

                // Note: At this point we don't let the user to modify experiment methods in this view so we don't need this.
                // Build a list of methods that the user has permission to view/use
                /*List<MethodsTable> methods = Methods.GetPermittedMethodsList(db);
                foreach (var item in WBMainTableEdit_VM_list)
                {
                    // Populate methods list
                    foreach (var method in methods)
                    {
                        item.MethodsList.Add(new SelectListItem { Text = method.methodName, Value = method.methodName, Selected = false });
                    }
                }*/

                return PartialView("_EditSelectedRows", WBMainTableEdit_VM_list);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> ExperimentMethodEditView(Guid? id, string selectedmethodid)
        {
            if (selectedmethodid == null || id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
            if (wBMainTableRow == null)
            {
                return HttpNotFound();
            }

            var wBMainTableViewModel = Mapper.Map<WBMainTableEdit_VM>(wBMainTableRow);
            Mapper.AssertConfigurationIsValid();

            wBMainTableViewModel.experimentMethod = selectedmethodid;

            List<MethodsTable> methods = Methods.GetPermittedMethodsList(db);
            foreach (var method in methods)
            {
                if (method.methodName == wBMainTableViewModel.experimentMethod)
                {
                    wBMainTableViewModel.Text1Name = method.text1Name;
                    wBMainTableViewModel.Text2Name = method.text2Name;
                    wBMainTableViewModel.Text3Name = method.text3Name;
                    wBMainTableViewModel.Text4Name = method.text4Name;
                    wBMainTableViewModel.Text5Name = method.text5Name;
                    wBMainTableViewModel.Text6Name = method.text6Name;
                    wBMainTableViewModel.Text7Name = method.text7Name;
                    wBMainTableViewModel.Text8Name = method.text8Name;
                    wBMainTableViewModel.Text9Name = method.text9Name;
                    wBMainTableViewModel.Text10Name = method.text10Name;
                    wBMainTableViewModel.Text11Name = method.text11Name;
                    wBMainTableViewModel.Text12Name = method.text12Name;
                    wBMainTableViewModel.Text13Name = method.text13Name;
                    wBMainTableViewModel.Text14Name = method.text14Name;
                    wBMainTableViewModel.Text15Name = method.text15Name;
                    wBMainTableViewModel.Date1Name = method.date1Name;
                    wBMainTableViewModel.Date2Name = method.date2Name;
                    wBMainTableViewModel.Date3Name = method.date3Name;
                    wBMainTableViewModel.Bool1Name = method.bool1Name;
                    wBMainTableViewModel.Bool2Name = method.bool2Name;
                    wBMainTableViewModel.Bool3Name = method.bool3Name;
                    wBMainTableViewModel.Bool4Name = method.bool4Name;
                    wBMainTableViewModel.Bool5Name = method.bool5Name;
                    wBMainTableViewModel.Bool6Name = method.bool6Name;
                    wBMainTableViewModel.Bool7Name = method.bool7Name;
                    wBMainTableViewModel.Bool8Name = method.bool8Name;
                }
            }

            if (selectedmethodid == "Western Blot")
            {
                return PartialView("_WBRowDBEdit", wBMainTableViewModel);
            }
            else if (selectedmethodid == "Immunostaining")
            {
                return PartialView("_ISRowDBEdit", wBMainTableViewModel);
            }
            else
            {
                return PartialView("_RowDBEdit", wBMainTableViewModel);
            }
        }

        // GET: WesternBlot/Delete/5
        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
            if (wBMainTableRow == null)
            {
                return HttpNotFound();
            }

            var viewModel = new EntityDelete_VM
            {
                EntityId = wBMainTableRow.rowID,
                EntityName = wBMainTableRow.imageName,
                ActionName = "Delete",
                ControllerName = "WesternBlot",
            };
            return PartialView("_EntityDeleteModal", viewModel);
        }

        // POST: WesternBlot/Delete/5
        [HttpPost, ActionName("Delete")]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteConfirmed(EntityDelete_VM viewModel)
        {
            string errMsg = null;

            errMsg = await deleteMainTableRow(viewModel.EntityId);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return Json(new { success = false, responseText = errMsg });
            }
            return Json(new { success = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
        }

        private async Task<string> deleteMainTableRow(Guid id)
        {
            string errMsg = null;
            try
            {
                WBMainTable wBMainTableRow = db.WBMainTable.Find(id);
                if (wBMainTableRow == null)
                {
                    return "Can't find row in main table";
                }

                if(Rows.userEditMainRowPermission(wBMainTableRow.userName) == false)
                {
                    return("Can't delete the file: " + wBMainTableRow.imageName + ". You don't have the right permission for that!");
                }

                // Delete all other related data from other tables before deleting data from main table
                // TODO:2: move this to a method that should remove all related data when user removes row from main table. 
                foreach (var reference in wBMainTableRow.WBReferencesTable.ToList())
                {
                    // Delete reference's data from database
                    db.WBReferencesTable.Remove(reference);
                    // Check if this is an actual file and delete it if yes
                    DeleteRefFile(reference);
                }

                // Delete all images related to this row
                errMsg = deleteRowImages(wBMainTableRow);
                if (!string.IsNullOrEmpty(errMsg))
                {
                    // If there was an error deleting the image from the server, display error message and don't delete data from database
                    return errMsg;
                }
                // Delete data from main table
                db.WBMainTable.Remove(wBMainTableRow);
                await db.SaveChangesAsync();

                return errMsg;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return errMsg;
            }
        }

        [HttpPost]
        [EditDelAuthorize]
        public async Task<JsonResult> DeleteMultiple(Guid[] selectedRows)
        {
            string errMsg = null;
            string combinedErrMsgs = null;
            try
            {
                if (selectedRows == null)
                {
                    return Json(new { success = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
                }

                foreach (var row in selectedRows)
                {
                    errMsg = await deleteMainTableRow(row);
                    if (!string.IsNullOrEmpty(errMsg))
                    {
                        if (string.IsNullOrEmpty(combinedErrMsgs))
                        {
                            combinedErrMsgs = "*** " + errMsg;
                        }
                        else
                        {
                            combinedErrMsgs = combinedErrMsgs + "\n\n" + "*** " + errMsg;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(combinedErrMsgs))
                {
                    combinedErrMsgs = ex.Message;
                }
                else
                {
                    combinedErrMsgs = ex.Message + "\n\n" + combinedErrMsgs;
                }
            }
            if (!string.IsNullOrEmpty(combinedErrMsgs))
            {
                return Json(new { success = false, responseText = combinedErrMsgs });
            }
            return Json(new { success = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
        }

        [HttpPost]
        [EditDelAuthorize]
        public async Task<JsonResult> SetRowsSharedMode(Guid[] selectedRows, bool status)
        {
            string errMsg = null;
            string combinedErrMsgs = null;

            try
            {
                if (selectedRows == null)
                {
                    return Json(new { success = true });
                }

                foreach (var row in selectedRows)
                {
                    WBMainTable wBMainTableRow = db.WBMainTable.Find(row);
                    if (Rows.userEditMainRowPermission(wBMainTableRow.userName) == false)
                    {
                        errMsg = "Can't change 'Share Row' status of the file: " + wBMainTableRow.imageName + ". You don't have the right permission for that!";
                        if (string.IsNullOrEmpty(combinedErrMsgs))
                        {
                            combinedErrMsgs = "*** " + errMsg;
                        }
                        else
                        {
                            combinedErrMsgs = combinedErrMsgs + "\n\n" + "*** " + errMsg;
                        }
                    }
                    else
                    {
                        wBMainTableRow.shareRow = status;
                        db.Entry(wBMainTableRow).State = EntityState.Modified;
                    }                    
                }

                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(combinedErrMsgs))
                {
                    combinedErrMsgs = ex.Message;
                }
                else
                {
                    combinedErrMsgs = ex.Message + "\n\n" + combinedErrMsgs;
                }
            }
            if (!string.IsNullOrEmpty(combinedErrMsgs))
            {
                return Json(new { success = false, responseText = combinedErrMsgs });
            }
            return Json(new { success = true });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult GetImg(Guid id)
        {
            try
            {
                WBMainTable wBMainTableRow = db.WBMainTable.Find(id);
                if (wBMainTableRow == null)
                {
                    throw new InvalidOperationException("Image info can't be found!");
                }

                // If there is a downsample image display this one otherwise display the full image
                string imagePath = wBMainTableRow.storedDownSampleImagePath != null ? wBMainTableRow.storedDownSampleImagePath : wBMainTableRow.storedImagePath;
                if ((imagePath == null) || (imagePath == ""))
                {
                    throw new InvalidOperationException(("Image not found! Image path is missing."));
                }
                // Convert the image to a format that is supported by all browsers
                var img = FileServices.ImageToJpgBase64(imagePath);
                if (img == null)
                {
                    throw new InvalidOperationException("Image not found!");
                }
                var jsonResult = Json(new { Image = img });
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (FileNotFoundException)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Couldn't find image to display. Please make sure that image exists!");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
        }

        /// <summary>
        /// Get the 'Immunostaining control image' according to the id and the type of the current image
        /// </summary>
        /// <param name="id" Note: this is the id of the current image not the control's ></param>
        /// <param name="ctrltype"></param> 
        /// <returns> the control image </returns>
        [HttpPost]
        public JsonResult GetImnstCtrl(Guid id, string ctrltype)
        {
            try
            {
                string imagePath = "";
                string imageName = "";
                var ctrlFound = false;

                WBMainTable wBMainTableRow = db.WBMainTable.Find(id);
                if (wBMainTableRow == null)
                {
                    throw new InvalidOperationException("Control info can't be found!");
                }

                var primaryAntibodyId = wBMainTableRow.primaryAntibodyId;
                if (string.IsNullOrEmpty(primaryAntibodyId))
                {
                    throw new InvalidOperationException(("Can't display control if image doesn't have Primary Antibody ID!"));
                }

                foreach (WBMainTable m in db.WBMainTable.ToList())
                {
                    if ((m.primaryAntibodyId == primaryAntibodyId))
                    {
                        if (ctrltype == "positive")
                        {
                            if (m.posControl == true)
                            {
                                ctrlFound = true;
                            }
                        }
                        else
                        {
                            if (m.negControl == true)
                            {
                                ctrlFound = true;
                            }
                        }
                        if (ctrlFound == true)
                        {
                            // If there is a downsample image display this one otherwise display the full image
                            imagePath = m.storedDownSampleImagePath != null ? m.storedDownSampleImagePath : m.storedImagePath;
                            imageName = m.imageName;
                            break; // There should be only one control for each experiment
                        }
                    }
                }

                if (string.IsNullOrEmpty(imagePath))
                {
                    throw new InvalidOperationException("Couldn't find any " + ctrltype + " control for this image!");
                }

                // Convert the image to a format that is supported by all browsers
                var img = FileServices.ImageToJpgBase64(imagePath);

                var jsonResult = Json(new { Image = img, Name = imageName });
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (FileNotFoundException)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Couldn't find control to display. Please make sure that control exists!");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
        }

        /// <summary>
        /// Get the 'control image' according to the id of the current image
        /// </summary>
        /// <param name="id" Note: this is the id of the current image not the control's ></param>
        /// <returns> the control image </returns>
        [HttpPost]
        public JsonResult GetCtrl(Guid id)
        {
            try
            {
                string imagePath = "";
                string imageName = "";

                WBMainTable wBMainTableRow = db.WBMainTable.Find(id);
                if (wBMainTableRow == null)
                {
                    throw new InvalidOperationException("Control info can't be found!");
                }

                var experimentId = wBMainTableRow.experimentID;
                var membraneId = wBMainTableRow.membraneID;
                if ((experimentId == null) || (experimentId == ""))
                {
                    throw new InvalidOperationException(("Can't display control if image doesn't have Experiment ID!"));
                }
                if ((membraneId == null) || (membraneId == ""))
                {
                    throw new InvalidOperationException(("Can't display control if image doesn't have Membrane ID!"));
                }

                foreach (WBMainTable m in db.WBMainTable.ToList())
                {
                    if ((m.experimentID == experimentId) && (m.membraneID == membraneId) && (m.control == true))
                    {
                        // If there is a downsample image display this one otherwise display the full image
                        imagePath = m.storedDownSampleImagePath != null ? m.storedDownSampleImagePath : m.storedImagePath;
                        imageName = m.imageName;
                        break; // There should be only one control for each experiment
                    }
                }

                if ((imagePath == null) || (imagePath == ""))
                {
                    throw new InvalidOperationException("Couldn't find any control for this image!");
                }

                // Convert the image to a format that is supported by all browsers
                var img = FileServices.ImageToJpgBase64(imagePath);

                var jsonResult = Json(new { Image = img, Name = imageName });
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (FileNotFoundException)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Couldn't find control to display. Please make sure that control exists!");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
        }

        private bool wBControlExists(Guid rowId, string userName, string experimentID, string membraneID)
        {
            var rows = db.WBMainTable.ToList();
            // Check all rows in database to check if control image with the same primaryAntibodyId already exists
            foreach (var row in rows)
            {
                // Don't check current row (by rowId) and check only for current user
                if ((row.rowID != null) && (row.userName != null) && (row.experimentID != null) && (row.membraneID != null))
                {
                    if ((!row.rowID.Equals(rowId)) && (row.userName.Equals(userName)) && ((row.experimentID.Equals(experimentID))) && (row.membraneID.Equals(membraneID)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        [HttpPost]
        public JsonResult SetWBControlImage(Guid id, string experimentID, string membraneID)
        {
            string errMsg = null;

            WBMainTable row = db.WBMainTable.Find(id);

            // If control from the same type already exists with the same Experiment ID Value and Membrane ID Value for the current user, don't allow to add a control
            if (wBControlExists(row.rowID, row.userName, experimentID, membraneID))
            {
                errMsg = "There is already control with the same Experiment ID value and Membrane ID value in the system.\n" +
                         "Only one control is allowed for each Experiment ID and Membrane ID.";
            }

            if (String.IsNullOrEmpty(errMsg))
            {
                return Json(new { ok = true });
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(errMsg);
            }
        }

        private bool imnstControlExists(Guid rowId, string userName, string primaryAntibodyId, string ctrlType)
        {
            var rows = db.WBMainTable.ToList();
            // Check all rows in database to check if control image with the same primaryAntibodyId already exists
            foreach (var row in rows)
            {
                // Don't check current row (by rowId) and check only for current user
                if (ctrlType == "positive")
                {
                    if ((!(row.rowID.Equals(rowId))) &&
                        (row.userName.Equals(userName)) &&
                        (row.posControl != null) &&
                        ((row.posControl.Value.Equals(true))) &&
                        (row.primaryAntibodyId != null) &&
                        (row.primaryAntibodyId.Equals(primaryAntibodyId)))
                    {
                        return true;
                    }
                }
                else if (ctrlType == "negative")
                {
                    if ((!(row.rowID.Equals(rowId))) &&
                       (row.userName.Equals(userName)) &&
                       (row.negControl != null) &&
                       (row.negControl.Value.Equals(true)) &&
                       ((row.primaryAntibodyId != null) &&
                       (row.primaryAntibodyId == primaryAntibodyId)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        [HttpPost]
        public JsonResult SetImnstControlImage(Guid id, string primaryantibodyvalue, string ctrltype)
        {
            string errMsg = null;

            WBMainTable row = db.WBMainTable.Find(id);

            // If control from the same type already exists for this Primary Antibody Id and the current user, don't allow to add a control
            if (imnstControlExists(row.rowID, row.userName, primaryantibodyvalue, ctrltype))
            {
                errMsg = "There is already " + ctrltype + " control with the same Primary Antibody Id.\n" +
                         "Only one control from each type (positive or negative) is allowed for each Primary Antibody Id.";
            }

            if (String.IsNullOrEmpty(errMsg))
            {
                return Json(new { ok = true });
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(errMsg);
            }
        }

        [HttpPost]
        public JsonResult GetLabel(Guid id)
        {
            try
            {
                WBMainTable wBMainTableRow = db.WBMainTable.Find(id);
                if (wBMainTableRow == null)
                {
                    throw new InvalidOperationException(("Label info can't be found!"));
                }

                string imagePath = wBMainTableRow.storedLabelImagePath;
                if ((imagePath == null) || (imagePath == ""))
                {
                    // There is no label image for this image just return
                    return Json(new { ok = true });
                }
                // Get label - converted to format that is supported by all browsers
                var img = FileServices.ImageToJpgBase64(imagePath);

                var jsonResult = Json(new { Image = img });
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (FileNotFoundException)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Couldn't find label to display!");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetMacro(Guid id)
        {
            try
            {
                WBMainTable wBMainTableRow = db.WBMainTable.Find(id);
                if (wBMainTableRow == null)
                {
                    throw new InvalidOperationException(("Macro info can't be found!"));
                }
                string imagePath = wBMainTableRow.storedMacroImagePath;
                if ((imagePath == null) || (imagePath == ""))
                {
                    // There is no macro image for this image just return
                    return Json(new { ok = true });
                }
                // Get macro - converted to format that is supported by all browsers
                var img = FileServices.ImageToJpgBase64(imagePath);

                var jsonResult = Json(new { Image = img });
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (FileNotFoundException)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Couldn't find image to display. Please make sure that image exists!");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
        }

        private string deleteRowImages(WBMainTable wBMainTable)
        {
            string imagePath = null;
            string errMsg = null;

            try
            {
                // Delete the image   
                // Note: get errMsg only for main image no need to display messages for every sub image.
                if (!string.IsNullOrEmpty(wBMainTable.storedImagePath))
                {
                    imagePath = Url.Content(wBMainTable.storedImagePath);
                    errMsg = FileServices.deleteFile(imagePath);
                }
                // Delete the downsample image
                if (!string.IsNullOrEmpty(wBMainTable.storedDownSampleImagePath))
                {
                    imagePath = Url.Content(wBMainTable.storedDownSampleImagePath);
                    FileServices.deleteFile(imagePath);
                }
                // Delete the label image            
                if (!string.IsNullOrEmpty(wBMainTable.storedLabelImagePath))
                {
                    imagePath = Url.Content(wBMainTable.storedLabelImagePath);
                    FileServices.deleteFile(imagePath);
                }
                // Delete the macro image            
                if (!string.IsNullOrEmpty(wBMainTable.storedMacroImagePath))
                {
                    imagePath = Url.Content(wBMainTable.storedMacroImagePath);
                    FileServices.deleteFile(imagePath);
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return errMsg;
            }

            return errMsg;
        }

        private bool downloadFileFromServer(string filepath, string originalFilename)
        {
            System.IO.Stream iStream = null;

            try
            {
                // Open the file.
                iStream = new System.IO.FileStream(filepath, System.IO.FileMode.Open,
                    System.IO.FileAccess.Read, System.IO.FileShare.Read);

                // Total bytes to read:
                var dataToRead = iStream.Length;

                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + originalFilename);

                // Read the bytes.
                while (dataToRead > 0)
                {
                    // Verify that the client is connected
                    if (Response.IsClientConnected)
                    {
                        // Set 8KB buffer
                        int intBufferSize = 8 * 1024;

                        // Create buffer for reading [intBufferSize] bytes from file
                        byte[] bytBuffers = new System.Byte[intBufferSize];

                        // Read the data and put it in the buffer.
                        int actualReadSize = iStream.Read(buffer: bytBuffers, offset: 0, count: intBufferSize);

                        // Write the data from buffer to the current output stream.
                        Response.OutputStream.Write
                            (buffer: bytBuffers, offset: 0,
                            count: actualReadSize);

                        // Flush the data to output
                        Response.Flush();
                        // Calc. remaining data to read
                        dataToRead = dataToRead - actualReadSize;
                    }
                    else
                    {
                        // Prevent infinite loop if user disconnected!
                        dataToRead = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
            finally
            {
                if (iStream != null)
                {
                    // Close the file.
                    iStream.Close();
                    iStream.Dispose();
                }
                Response.Close();
            }

            return true;
        }

        [HttpGet]
        public async Task<ActionResult> DownloadImage(Guid? id)
        {
            string originalFileName = null;
            if (id == null)
            {
                throw new InvalidOperationException("Error: Couldn't find image - main table id is missing!");
            }

            var mainTable = await db.WBMainTable.FindAsync(id);
            if (mainTable == null)
            {
                return HttpNotFound();
            }

            var filePath = mainTable.storedImagePath;
            originalFileName = mainTable.imageName;

            try
            {
                var rv = downloadFileFromServer(filePath, originalFileName);
                if (rv == true)
                {
                    return Json(new { ok = true });
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return Json("Downloading image: " + originalFileName + " failed", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> GetReference(Guid? id)
        {
            string fileName = null;
            try
            {
                if (id == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find reference - main table id is missing!");
                }
                WBReferencesTable wBReferencesTable = await db.WBReferencesTable.FindAsync(id);
                if (wBReferencesTable == null)
                {
                    return HttpNotFound();
                }

                string location = wBReferencesTable.refLocation;

                if (location == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find reference - file location is missing!");
                }

                fileName = wBReferencesTable.link;
                string filePath = location == "URL" ? wBReferencesTable.fullPath : wBReferencesTable.fullPath;

                if ((fileName == null) || (filePath == null))
                {
                    throw new InvalidOperationException("Error: Couldn't find reference - some data is missing!");
                }
                if (location == "URL")
                {
                    return Redirect(fileName);
                }
                else
                {
                    var rv = downloadFileFromServer(filePath, fileName);
                    if (rv == true)
                    {
                        return Json(new { ok = true });
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

            return Json("Downloading file: " + fileName + " failed", JsonRequestBehavior.AllowGet);
        }

        [EditDelAuthorize]
        public async Task<ActionResult> CreateReference(Guid? id, string refType)
        {
            try
            {
                if (id == null)
                {
                    throw new InvalidOperationException("Error: Couldn't create the requested reference - main table id is missing!");
                }
                WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
                if (wBMainTableRow == null)
                {
                    throw new InvalidOperationException("Error: Couldn't create the requested reference - main table entry not found!");
                }

                if (refType == "URL")
                {
                    WBReferenceCreateURL_VM viewModel = new WBReferenceCreateURL_VM();
                    viewModel.mainTableID = wBMainTableRow.rowID;
                    return PartialView("_RefCreateURL", viewModel);
                }
                else
                {
                    WBReferenceCreateFile_VM viewModel = new WBReferenceCreateFile_VM();
                    viewModel.mainTableID = wBMainTableRow.rowID;
                    return PartialView("_RefCreateFile", viewModel);
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> CreateReferenceURL(Guid? id)
        {
            return await CreateReference(id, "URL");
        }

        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> CreateReferenceFile(Guid? id)
        {
            return await CreateReference(id, "File");
        }

        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateReferenceURL(WBReferenceCreateURL_VM viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    WBReferencesTable wBReferencesTable = new WBReferencesTable();
                    wBReferencesTable.link = viewModel.link;
                    wBReferencesTable.fullPath = viewModel.link;
                    wBReferencesTable.mainTableID = viewModel.mainTableID;
                    wBReferencesTable.refLocation = "URL";
                    wBReferencesTable.rowID = viewModel.rowID;
                    wBReferencesTable.userName = viewModel.userName;
                    wBReferencesTable.referenceID = viewModel.referenceID;
                    db.WBReferencesTable.Add(wBReferencesTable);
                    await db.SaveChangesAsync();
                    return Json(new { ok = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json("Error: add reference to the requested URL failed.");
        }

        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateReferenceFile(WBReferenceCreateFile_VM viewModel)
        {
            string errMsg = "";
            WBReferencesTable wBReferencesTable = new WBReferencesTable();
            try
            {
                if (ModelState.IsValid)
                {
                    HttpPostedFileBase file = viewModel.file; // Get file, added by the user, from the view
                    if ((file != null) && (file.ContentLength > 0))
                    {
                        string refDestPath = FileServices.SetFileDestPath("reference", db);
                        if (String.IsNullOrEmpty(refDestPath))
                        {
                            throw new InvalidOperationException("Uploading image to the server failed.");
                        }
                        string storedFileName = FileServices.FileUpload(file, refDestPath); // Upload the file
                        // If upload failed there is no point to continue with creating the row - print a message and quit
                        if (String.IsNullOrEmpty(storedFileName))
                        {
                            throw new InvalidOperationException("Uploading file: " + file.FileName + " to the server failed.");
                        }
                        wBReferencesTable.link = Path.GetFileName(file.FileName);
                        // Save the path to the file in database
                        wBReferencesTable.fullPath =
                            Path.Combine(Url.Content(refDestPath),
                            storedFileName);
                    }
                    wBReferencesTable.mainTableID = viewModel.mainTableID;
                    wBReferencesTable.referenceID = viewModel.referenceID;
                    wBReferencesTable.refLocation = "Server";
                    wBReferencesTable.rowID = viewModel.rowID;
                    wBReferencesTable.userName = viewModel.userName;
                    db.WBReferencesTable.Add(wBReferencesTable);
                    await db.SaveChangesAsync();
                    return Json(new { ok = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
                }
            }
            catch (Exception ex)
            {
                errMsg = "Error! " + ex.Message + " ";
                if ((ex.InnerException != null) && (ex.InnerException.InnerException != null))
                {
                    errMsg = errMsg + ex.InnerException.InnerException.Message;
                }
            }
            // If create failed check if file was already uploaded to the server and delete it
            if (!String.IsNullOrEmpty(wBReferencesTable.fullPath))
            {
                FileServices.deleteFile(Url.Content(wBReferencesTable.fullPath));
            }
            if (String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Error! add reference to the requested file failed.";
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(errMsg);
        }

        [EditDelAuthorize]
        public async Task<ActionResult> RefCreateFiles(Guid? id)
        {
            try
            {
                if (id == null)
                {
                    throw new InvalidOperationException("Error: Couldn't create the requested reference - main table id is missing!");
                }
                WBMainTable wBMainTableRow = await db.WBMainTable.FindAsync(id);
                if (wBMainTableRow == null)
                {
                    throw new InvalidOperationException("Error: Couldn't create the requested reference - main table entry not found!");
                }

                WBReferenceCreateFile_VM viewModel = new WBReferenceCreateFile_VM();
                viewModel.mainTableID = wBMainTableRow.rowID;
                return View("RefCreateFiles", viewModel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RefCreateFiles(WBReferenceCreateFile_VM viewModel)
        {
            if (ModelState.IsValid)
            {
                return await CreateReferenceFile(viewModel);
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(errors);
            }       
        }
        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> EditReference(Guid? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                WBReferencesTable wBRefTable = await db.WBReferencesTable.FindAsync(id);
                if (wBRefTable == null)
                {
                    return HttpNotFound();
                }

                if (wBRefTable.refLocation == null)
                {
                    throw new InvalidOperationException(("Reference location is missing!"));
                }
                else if (wBRefTable.refLocation == "URL")
                {
                    var viewModel = new WBReferenceEditURL_VM();
                    viewModel.fullPath = wBRefTable.fullPath;
                    viewModel.link = wBRefTable.link;
                    viewModel.mainTableID = wBRefTable.mainTableID;
                    viewModel.referenceID = wBRefTable.referenceID;
                    viewModel.refLocation = wBRefTable.refLocation;
                    viewModel.rowID = wBRefTable.rowID;
                    viewModel.userName = wBRefTable.userName;

                    return PartialView("_RefEditURL", viewModel);
                }
                else
                {
                    throw new InvalidOperationException(("Unknown reference location!"));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditReferenceURL(WBReferenceEditURL_VM viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    WBReferencesTable wBReferencesTable = new WBReferencesTable();
                    wBReferencesTable.link = viewModel.link;
                    wBReferencesTable.fullPath = viewModel.link;
                    wBReferencesTable.mainTableID = viewModel.mainTableID;
                    wBReferencesTable.referenceID = viewModel.referenceID;
                    wBReferencesTable.refLocation = viewModel.refLocation;
                    wBReferencesTable.rowID = viewModel.rowID;
                    wBReferencesTable.userName = viewModel.userName;
                    db.Entry(wBReferencesTable).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return Json(new { ok = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json("Error: edit URL failed.");
        }

        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> DeleteReference(Guid id)
        {
            try
            {
                if (id == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find any reference to delete!");
                }
                WBReferencesTable wBRefTable = await db.WBReferencesTable.FindAsync(id);
                if (wBRefTable == null)
                {
                    return HttpNotFound();
                }
                var viewModel = new EntityDelete_VM
                {
                    EntityId = id,
                    EntityName = wBRefTable.link,
                    ActionName = "DeleteReference",
                    ControllerName = "WesternBlot",

                };

                return PartialView("_EntityDeleteModal", viewModel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteReference(EntityDelete_VM viewModel)
        {
            string errMsg = null;
            try
            {
                WBReferencesTable refItem = await db.WBReferencesTable.FindAsync(viewModel.EntityId);

                if (refItem == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find any reference to delete!");
                }

                // Check if this is an actual file and delete it if yes
                errMsg = DeleteRefFile(refItem);
                if (!string.IsNullOrEmpty(errMsg))
                {
                    // If there was an error deleting the file from the server, display error message and don't delete data from database
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json(errMsg);
                }
                // Delete the data from the database table
                db.WBReferencesTable.Remove(refItem);
                db.SaveChanges();
                return Json(new { success = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = ex.Message });
            }
        }

        private string DeleteRefFile(WBReferencesTable refItem)
        {
            string errMsg = null;

            // If file's location is on the server delete it
            if (refItem.refLocation == "Server")
            {
                errMsg = FileServices.deleteFile(Url.Content(refItem.fullPath));
            }

            return errMsg;
        }
    }
}
