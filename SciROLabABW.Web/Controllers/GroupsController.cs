﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.GroupsUtilities;
using SciROLabABW.Web.TeamsUtilities;
using System.Threading.Tasks;
using SciROLabABW.Web.ViewModels;
using SciROLabABW.Web.ViewModels.Groups;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq.Expressions;

namespace SciROLabABW.Web.Controllers
{
    [Authorize(Roles = "Owner, Admin")]
    public class GroupsController : BaseController
    {
        private SciROLabWebEntities db = new SciROLabWebEntities();

        // GET: Groups
        public ActionResult Index()
        {
            var permittedGroups = Groups.GetPermittedGroupsList(db);

            var groupList = Mapper.Map<List<SciROLabGroup>, List<GroupManage_VM>>(permittedGroups);
            Mapper.AssertConfigurationIsValid();

            return View(groupList);
        }

        // GET: UserGroups
        [OverrideAuthorization]
        [Authorize]
        public ActionResult UserGroups()
        {
            string usrName = User.Identity.GetUserName();
            var permittedGroups = Groups.GetUserGroups(db, usrName);

            var groupList = Mapper.Map<List<SciROLabGroup>, List<GroupManage_VM>>(permittedGroups);
            Mapper.AssertConfigurationIsValid();

            return PartialView("_UserGroups", groupList);
        }

        // GET: Groups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Groups/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GroupCreate_VM groupCreate_VM)
        {
            SciROLabTeam team = Teams.GetUserTeam(db);
            if(team == null)
            {
                ViewBag.GroupExistsMessage = "User must be part of a team to be able to create a group.";
                return View(groupCreate_VM);
            }

            if (ModelState.IsValid)
            {
                var permittedGroups = Groups.GetPermittedGroupsList(db);
                var groupList = Mapper.Map<List<SciROLabGroup>, List<GroupManage_VM>>(permittedGroups);

                foreach (var grp in groupList)
                {
                    if (grp.Name == groupCreate_VM.Name)
                    {
                        ViewBag.GroupExistsMessage = "Group with the same name already exists! Please try another name.";
                        return View(groupCreate_VM);
                    }
                }
                // Add user's information to the row
                groupCreate_VM.userId = User.Identity.GetUserId(); 
                groupCreate_VM.userName = User.Identity.GetUserName();

                SciROLabGroup sciROLabGroup = Mapper.Map<SciROLabGroup>(groupCreate_VM); // Map view model items to domain model
                Mapper.AssertConfigurationIsValid();

                // Add team information to the row
                sciROLabGroup.TeamId = team.Id;
                sciROLabGroup.TeamName = team.Name;

                db.SciROLabGroups.Add(sciROLabGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(groupCreate_VM);
        }

        // GET: Groups/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SciROLabGroup sciROLabGroup = db.SciROLabGroups.Find(id);
            if (sciROLabGroup == null)
            {
                return HttpNotFound();
            }

            GroupManage_VM group = Mapper.Map<GroupManage_VM>(sciROLabGroup);
            Mapper.AssertConfigurationIsValid();

            return View(group);
        }

        // POST: Groups/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GroupManage_VM groupManage_VM)
        {
            if (ModelState.IsValid)
            {
                SciROLabGroup sciROLabGroup = Mapper.Map<SciROLabGroup>(groupManage_VM); // Map view model items to domain model
                Mapper.AssertConfigurationIsValid();

                db.Entry(sciROLabGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(groupManage_VM);
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SciROLabGroup sciROLabGroup = db.SciROLabGroups.Find(id);
            if (sciROLabGroup == null)
            {
                return HttpNotFound();
            }

            GroupManage_VM group = Mapper.Map<GroupManage_VM>(sciROLabGroup);
            Mapper.AssertConfigurationIsValid();
            return View(group);
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            SciROLabGroup sciROLabGroup = db.SciROLabGroups.Find(id);
            foreach (var user in sciROLabGroup.AspNetUsers.ToList())
            {
                sciROLabGroup.AspNetUsers.Remove(user);
            }

            db.SciROLabGroups.Remove(sciROLabGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AddUsers(string groupId)
        {
            try
            {
                if (groupId == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find any group!");
                }

                SciROLabGroup group = db.SciROLabGroups.Find(groupId);

                if (group == null)
                {
                    return HttpNotFound();
                }

                SciROLabTeam team = Teams.GetUserTeam(db);

                List<SelectListItem> listItems = new List<SelectListItem>();
                if (team != null)
                {
                    // Build a list of all users from current team that are not already in the current group
                    foreach (AspNetUser user in team.AspNetUsers)
                    {
                        if (!(Groups.IsUserInGroup(group, user.UserName)))
                        {
                            SelectListItem selectList = new SelectListItem()
                            {
                                Text = user.UserName,
                                Value = user.Id
                            };
                            listItems.Add(selectList);
                        }
                    }
                }

                var viewModel = new GroupManageUsers_VM
                {
                    GroupId = group.Id,
                    GroupName = group.Name,
                    Users = listItems
                };

                return View("GroupAddUsers", viewModel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUsers(GroupManageUsers_VM viewModel, IEnumerable<string> SelectedUsers)
        {
            try
            {
                SciROLabGroup sciROLabGroup = db.SciROLabGroups.Find(viewModel.GroupId);

                if (SelectedUsers != null)
                {
                    foreach (string strUser in SelectedUsers)
                    {
                        AspNetUser user = db.AspNetUsers.Find(strUser);
                        sciROLabGroup.AspNetUsers.Add(user);
                    }
                    db.SaveChanges();
                }
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteUsers(string groupId)
        {
            try
            {
                if (groupId == null)
                {
                    throw new InvalidOperationException("Error: Couldn't find any group!");
                }

                SciROLabGroup group = db.SciROLabGroups.Find(groupId);
               
                if (group == null)
                {
                    return HttpNotFound();
                }

                // Build a list of all users that are in the current group
                List<SelectListItem> listItems = new List<SelectListItem>();
                foreach (AspNetUser user in group.AspNetUsers)
                {
                    SelectListItem selectList = new SelectListItem()
                    {
                        Text = user.UserName,
                        Value = user.Id
                    };
                    listItems.Add(selectList);
                }

                var viewModel = new GroupManageUsers_VM
                {
                    GroupId = group.Id,
                    GroupName = group.Name,
                    Users = listItems
                };

                return View("GroupDeleteUsers", viewModel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUsers(GroupManageUsers_VM viewModel, IEnumerable<string> SelectedUsers)
        {
            SciROLabGroup sciROLabGroup = db.SciROLabGroups.Find(viewModel.GroupId);

            if (SelectedUsers != null)
            {
                foreach (string strUser in SelectedUsers)
                {
                    AspNetUser user = db.AspNetUsers.Find(strUser);
                    sciROLabGroup.AspNetUsers.Remove(user);
                }
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public bool DeleteUserFromAllGroups(string userID)
        {
            AspNetUser user = db.AspNetUsers.Find(userID);
            List<SciROLabGroup> groups = db.SciROLabGroups.ToList();
            foreach (SciROLabGroup grp in groups)
            {
                if (Groups.IsUserInGroup(grp, user.UserName))
                {
                    grp.AspNetUsers.Remove(user);
                }
            }
            db.SaveChanges();
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public IUserStore<ApplicationUser> userStore { get; set; }
    }
}
