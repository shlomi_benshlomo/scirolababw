﻿using AutoMapper;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.PhrasesUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Net;
using System.Threading.Tasks;
using SciROLabABW.Web.ViewModels;
using SciROLabABW.Web.Authorization;
using System.Data.Entity;
using SciROLabABW.Web.ViewModels.Phrases;
using PagedList;

namespace SciROLabABW.Web.Controllers
{
    public class PhrasesController : BaseController
    {
        private SciROLabWebEntities db = new SciROLabWebEntities();

        // GET: Phrases
        public ActionResult Index(string listSortOrder, string currentSortOrder,
                                         string currentSearchText, string freeSearchStr,
                                         int? page, string pageSizeStr,
                                         string startSearch, string currentPageSize)
        {
            PhrasesIndex_VM pagingModel = new PhrasesIndex_VM();
            bool searchState = false;

            // Set the sort parameters
            if (listSortOrder != null)
            {
                currentSortOrder = listSortOrder;
                Session["phrasesSortOrder"] = listSortOrder;
            }
            else if (currentSortOrder != null)
            {
                listSortOrder = currentSortOrder;
                Session["phrasesSortOrder"] = listSortOrder;
            }
            else
            {
                if (Session["phrasesSortOrder"] == null)
                {
                    Session["phrasesSortOrder"] = "Category, Ascending"; // Set to default value
                }
                listSortOrder = Session["phrasesSortOrder"].ToString();
                currentSortOrder = listSortOrder;
            }

            // Set pagination parameters
            if (pageSizeStr != null)
            {
                currentPageSize = pageSizeStr;
                Session["phrasesPageSizeStr"] = pageSizeStr;
            }
            else if (currentPageSize != null)
            {
                pageSizeStr = currentPageSize;
                Session["phrasesPageSizeStr"] = pageSizeStr;
            }
            else
            {
                if (Session["phrasesPageSizeStr"] == null)
                {
                    Session["phrasesPageSizeStr"] = 25; // Set to default value
                }
                pageSizeStr = Session["phrasesPageSizeStr"].ToString();
                currentPageSize = pageSizeStr;
            }

            // Load from the DB server only permitted rows
            var permittedPhrases = Phrases.GetPermittedPhrases(db);

            // If there is something in search boxes, update the query according to this data 
            // Set the page to the first page in case of a new search
            // else use currrent info to update search fields to the value they were set to on the last change
            if (!string.IsNullOrEmpty(freeSearchStr))
            {
                page = 1;
            }
            else
            {
                freeSearchStr = currentSearchText;
            }

            // If user is pressing on search and the search box is empty - reset Session state
            if ((!string.IsNullOrEmpty(startSearch)) && (String.IsNullOrEmpty(freeSearchStr)))
            {
                Session["phrasesFreeSearchStr"] = null;
            }
            else if ((String.IsNullOrEmpty(freeSearchStr)) && (Session["phrasesFreeSearchStr"] != null))
            {
                freeSearchStr = Session["phrasesFreeSearchStr"].ToString();
            }

            if (!String.IsNullOrEmpty(freeSearchStr))
            {
                Session["phrasesFreeSearchStr"] = freeSearchStr;
                // TODO:2: still need to work on this one to make the code more readable using Lambada like I did with the 'ContainsOrExpression'!!!
                permittedPhrases = permittedPhrases.Where(sl =>
                    sl.Category.ToString().Contains(freeSearchStr.ToUpper()) ||
                    sl.Subject.ToString().Contains(freeSearchStr.ToUpper()) ||
                    sl.Phrase.ToString().Contains(freeSearchStr.ToUpper()) ||
                    sl.UserName.ToString().Contains(freeSearchStr.ToUpper()));
                if (permittedPhrases != null)
                {
                    searchState = true;
                }
                searchState = true;
            }

            // Sort data according to the selected value
            switch (listSortOrder)
            {
                case "Category, Ascending":
                default:
                    permittedPhrases = permittedPhrases.OrderBy(wb => wb.Category);
                    break;
                case "Category, Descending":
                    permittedPhrases = permittedPhrases.OrderByDescending(wb => wb.Category);
                    break;
                case "Subject, Ascending":
                    permittedPhrases = permittedPhrases.OrderBy(wb => wb.Subject);
                    break;
                case "Subject, Descending":
                    permittedPhrases = permittedPhrases.OrderByDescending(wb => wb.Subject);
                    break;
                case "Phrase, Ascending":
                    permittedPhrases = permittedPhrases.OrderBy(wb => wb.Phrase);
                    break;
                case "Phrase, Descending":
                    permittedPhrases = permittedPhrases.OrderByDescending(wb => wb.Phrase);
                    break;
                case "User Name, Ascending":
                    permittedPhrases = permittedPhrases.OrderBy(wb => wb.UserName);
                    break;
                case "User Name, Descending":
                    permittedPhrases = permittedPhrases.OrderByDescending(wb => wb.UserName);
                    break;    
            }

            // Set number of items per page according to page size selection
            int pageSize;
            switch (pageSizeStr)
            {
                case "5":
                default:
                    pageSize = 5;
                    break;
                case "10":
                    pageSize = 10;
                    break;
                case "15":
                    pageSize = 15;
                    break;
                case "25":
                    pageSize = 25;
                    break;
                case "50":
                    pageSize = 50;
                    break;
                case "100":
                    pageSize = 100;
                    break;
            }

            // Populate sort dropdownlist and set selected value
            ViewData["ListSortOrder"] = new SelectList(pagingModel.GetPhrasesSortList(), listSortOrder);
            // Populate page size dropdownlist and set selected value
            ViewData["pageSizeStr"] = new SelectList(pagingModel.GetPhrasesPageSizeList(), pageSizeStr);
            // Update page number with page value if not null, otherwise set to 1
            int pageNumber = (page ?? 1);

            ViewBag.CurrentSearchText = freeSearchStr;
            if (searchState == true)
            {
                ViewData["CurrentSearchStatMsg"] = "Search is ON";
            }
            else
            {
                ViewData["CurrentSearchStatMsg"] = "";
            }

            ViewBag.CurrentSortOrder = listSortOrder;
            ViewBag.CurrentPageSize = pageSizeStr;

            var phrasesTableIndex_VM_list = Mapper.Map<List<SciROLabPhrases>, List<PhrasesTableIndex_VM>>(permittedPhrases.ToList());
            Mapper.AssertConfigurationIsValid();

            var model = phrasesTableIndex_VM_list.ToPagedList(pageNumber, pageSize);
            return View(model);

        }

        // GET: Phrases/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Phrases/Create
        [HttpPost]
        public ActionResult Create(PhrasesTableCreate_VM phrases_VM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var permittedPhrases = Phrases.GetPermittedPhrasesList(db);

                    // Add user's information to the row
                    phrases_VM.UserId = User.Identity.GetUserId();
                    phrases_VM.UserName = User.Identity.GetUserName();

                    SciROLabPhrases phrases = Mapper.Map<SciROLabPhrases>(phrases_VM);
                    Mapper.AssertConfigurationIsValid();

                    db.SciROLabPhrases.Add(phrases);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Phrases/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SciROLabPhrases phraseRow = db.SciROLabPhrases.Find(id);
            if (phraseRow == null)
            {
                return HttpNotFound();
            }

            PhrasesTableEdit_VM phrase = Mapper.Map<PhrasesTableEdit_VM>(phraseRow);
            Mapper.AssertConfigurationIsValid();

            return View(phrase);
        }

        // POST: Phrases/Edit/5
        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PhrasesTableEdit_VM phrases_VM)
        {
            if (ModelState.IsValid)
            {
                SciROLabPhrases sciROLabPhrase = Mapper.Map<SciROLabPhrases>(phrases_VM); // Map view model items to domain model
                Mapper.AssertConfigurationIsValid();

                db.Entry(sciROLabPhrase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(phrases_VM);
        }

        // GET: Phrases/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SciROLabPhrases phrasesRow = await db.SciROLabPhrases.FindAsync(id);
            if (phrasesRow == null)
            {
                return HttpNotFound();
            }

            var viewModel = new EntityDelete_VM
            {
                EntityId = phrasesRow.Id,
                EntityName = phrasesRow.Phrase,
                ActionName = "Delete",
                ControllerName = "Phrases",
            };
            return PartialView("_EntityDeleteModal", viewModel);

        }

        // POST: Phrases/Delete/5
        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(EntityDelete_VM viewModel)
        {
            string errMsg = null;

            errMsg = await deletePhrasesTableRow(viewModel.EntityId);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return Json(new { success = false, responseText = errMsg });
            }
            return Json(new { success = true, redirectUrl = Url.Action("Index", "Phrases"), isRedirect = true });
        }

        private async Task<string> deletePhrasesTableRow(Guid id)
        {
            string errMsg = null;
            try
            {
                SciROLabPhrases phrasesTableRow = await db.SciROLabPhrases.FindAsync(id);
                if (phrasesTableRow == null)
                {
                    return "Can't find row in main table";
                }

                // Delete data from table
                db.SciROLabPhrases.Remove(phrasesTableRow);
                await db.SaveChangesAsync();

                return errMsg;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return errMsg;
            }
        }

        [HttpPost]
        [EditDelAuthorize]
        public async Task<JsonResult> DeleteMultiple(Guid[] selectedRows)
        {
            string errMsg = null;
            string combinedErrMsgs = null;
            try
            {
                if (selectedRows == null)
                {
                    return Json(new { success = true, redirectUrl = Url.Action("Index", "Phrases"), isRedirect = true });
                }

                foreach (var row in selectedRows)
                {
                    errMsg = await deletePhraseTableRow(row);
                    if (!string.IsNullOrEmpty(errMsg))
                    {
                        if (string.IsNullOrEmpty(combinedErrMsgs))
                        {
                            combinedErrMsgs = "*** " + errMsg;
                        }
                        else
                        {
                            combinedErrMsgs = combinedErrMsgs + "\n\n" + "*** " + errMsg;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(combinedErrMsgs))
                {
                    combinedErrMsgs = ex.Message;
                }
                else
                {
                    combinedErrMsgs = ex.Message + "\n\n" + combinedErrMsgs;
                }
            }
            if (!string.IsNullOrEmpty(combinedErrMsgs))
            {
                return Json(new { success = false, responseText = combinedErrMsgs });
            }
            return Json(new { success = true, redirectUrl = Url.Action("Index", "Phrases"), isRedirect = true });
        }

        private async Task<string> deletePhraseTableRow(Guid id)
        {
            string errMsg = null;
            try
            {
                SciROLabPhrases phraseTableRow = db.SciROLabPhrases.Find(id);
                if (phraseTableRow == null)
                {
                    return "Can't find requested row in phrases table";
                }

                // Delete data from phrases table
                db.SciROLabPhrases.Remove(phraseTableRow);
                await db.SaveChangesAsync();

                return errMsg;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return errMsg;
            }
        }

        [HttpGet]
        public JsonResult AutoComplete(string prefix)
        {
            var permittedPhrases = Phrases.GetPermittedPhrasesList(db);

            // Get records starting with 'prefix' from the list
            var phrases = (from P in permittedPhrases
                           where P.Phrase.StartsWith(prefix)
                            select new { P.Phrase });
            return Json(phrases, JsonRequestBehavior.AllowGet);
        }  
    }
}
