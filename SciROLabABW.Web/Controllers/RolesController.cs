﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.TeamsUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.Controllers
{
    [Authorize(Roles = "Owner, Admin")]
    public class RolesController : BaseController
    {
        private SciROLabWebEntities db = new SciROLabWebEntities();

        #region Roles
        public List<string> GetPermittedRolesNamesList()
        {
            var roles = db.AspNetRoles;
            List<string> roleNamesList = new List<string> { };
            foreach (var role in roles)
            {
                // In case of Owner user display all roles, else display basic roles only
                if (User.IsInRole("Owner"))
                {
                    roleNamesList.Add(role.Name);
                }
                else if (ProjectCommons.isBasicRole(role.Name))
                {
                    roleNamesList.Add(role.Name);
                }
            }

            return roleNamesList;
        }

        public ActionResult RoleIndex()
        {
            List<string> roleNamesList = GetPermittedRolesNamesList();
            return View(roleNamesList);
        }

        public ActionResult RoleCreate()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RoleCreate(string roleName)
        {
            using (var context = new ApplicationDbContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var roleNamesList = GetPermittedRolesNamesList();

                foreach (var name in roleNamesList)
                {
                    if (name == roleName)
                    {
                        ViewBag.ResultMessageErr = "Role with the same name already exists! Please try another name.";
                        return View();
                    }
                }

                roleManager.Create(new IdentityRole(roleName));
                context.SaveChanges();
            }

            ViewBag.ResultMessageSuccess = "Role created successfully!";
            return RedirectToAction("RoleIndex", "Roles");
        }

        public ActionResult RoleDelete(string roleName)
        {
            using (var context = new ApplicationDbContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var role = roleManager.FindByName(roleName);

                roleManager.Delete(role);
                context.SaveChanges();
            }

            ViewBag.ResultMessageSuccess = "Role deleted succesfully!";
            return RedirectToAction("RoleIndex", "Roles");
        }

        public ActionResult ManageUserRoles()
        {
            List<string> users = new List<string> { };
            List<string> roleNamesList = new List<string> { };
            using (var context = new ApplicationDbContext())
            {
                users = Teams.GetAllPermittedUsers(db);
                roleNamesList = GetPermittedRolesNamesList();
            }

            ViewBag.Roles = new SelectList(roleNamesList);
            ViewBag.Users = new SelectList(users);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageUserRoles(string roleName, string userName)
        {
            List<string> roles;
            List<string> users;
            using (var context = new ApplicationDbContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                users = Teams.GetAllPermittedUsers(db);

                var user = userManager.FindByName(userName);
                if (user == null)
                    throw new Exception("User not found!");

                var role = roleManager.FindByName(roleName);
                if (role == null)
                    throw new Exception("Role not found!");

                if (userManager.IsInRole(user.Id, role.Name))
                {
                    ViewBag.ResultMessageErr = "This user already has the role specified!";
                }
                else
                {
                    userManager.AddToRole(user.Id, role.Name);
                    context.SaveChanges();

                    ViewBag.ResultMessageSuccess = "Username added to the role succesfully!";
                }

                roles = GetPermittedRolesNamesList();
            }

            ViewBag.Roles = new SelectList(roles);
            ViewBag.Users = new SelectList(users);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetRoles(string userName)
        {
            if (!string.IsNullOrWhiteSpace(userName))
            {
                List<string> userRoles = new List<string> { };
                List<string> roles = new List<string> { };
                List<string> users = new List<string> { };
                using (var context = new ApplicationDbContext())
                {
                    var roleStore = new RoleStore<IdentityRole>(context);
                    var roleManager = new RoleManager<IdentityRole>(roleStore);

                    roles = GetPermittedRolesNamesList();

                    var userStore = new UserStore<ApplicationUser>(context);
                    var userManager = new UserManager<ApplicationUser>(userStore);

                    users = Teams.GetAllPermittedUsers(db);

                    var user = userManager.FindByName(userName);
                    if (user == null)
                        throw new Exception("User not found!");

                    var userRoleIds = (from r in user.Roles select r.RoleId);
                    userRoles = (from id in userRoleIds
                                 let r = roleManager.FindById(id)
                                 select r.Name).ToList();
                }

                ViewBag.Roles = new SelectList(roles);
                ViewBag.Users = new SelectList(users);
                ViewBag.RolesForThisUser = userRoles;
            }

            return View("ManageUserRoles");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteRoleFromUser(string userName, string roleName)
        {
            List<string> userRoles;
            List<string> roles;
            List<string> users;
            using (var context = new ApplicationDbContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                roles = GetPermittedRolesNamesList();

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                users = Teams.GetAllPermittedUsers(db);

                var user = userManager.FindByName(userName);
                if (user == null)
                    throw new Exception("User not found!");

                string currentUsrName = User.Identity.GetUserName();
                if (userManager.IsInRole(user.Id, roleName))
                {
                    if ((roleName.Equals("Admin")) && (userName.Equals(currentUsrName)))
                    {
                        ViewBag.ResultMessageErr = "You can't remove your own Admin role!";
                    }
                    else
                    {
                        userManager.RemoveFromRole(user.Id, roleName);
                        context.SaveChanges();
                        ViewBag.ResultMessageSuccess = "Role removed from this user successfully!";
                    }                    
                }
                else
                {
                    ViewBag.ResultMessageErr = "This user doesn't belong to selected role.";
                }

                var userRoleIds = (from r in user.Roles select r.RoleId);
                userRoles = (from id in userRoleIds
                             let r = roleManager.FindById(id)
                             select r.Name).ToList();
            }

            ViewBag.RolesForThisUser = userRoles;
            ViewBag.Roles = new SelectList(roles);
            ViewBag.Users = new SelectList(users);
            return View("ManageUserRoles");
        }
        #endregion
    }
}