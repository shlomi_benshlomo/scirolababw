﻿using AutoMapper;
using SciROLabABW.Web.Authorization;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.ViewModels;
using SciROLabABW.Web.ViewModels.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Linq.Expressions;
using SciROLabABW.Web.TeamsUtilities;
using SciROLabABW.Web.MethodsUtilities;

namespace SciROLabABW.Web.Controllers
{
    public class MethodsController : BaseController
    {
        private SciROLabWebEntities db = new SciROLabWebEntities();

        // GET: Method
        public ActionResult Index()
        {
            var permittedMethods = Methods.GetPermittedMethodsList(db);

            var methodsList = Mapper.Map<List<MethodsTable>, List<ExpMethodsIndex_VM>>(permittedMethods);
            Mapper.AssertConfigurationIsValid();

            return View(methodsList);
        }

        // GET: Method/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Method/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Method/Create
        [HttpPost]
        public ActionResult Create(ExpMethodsCreate_VM create_VM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var permittedMethods = Methods.GetPermittedMethodsList(db);

                    foreach (var method in permittedMethods)
                    {
                        if (method.methodName == create_VM.methodName)
                        {
                            ViewBag.ExpMethodExistsMessage = "Experiment method with the same name already exists! Please try another name.";
                            return View();
                        }
                    }
                    // Add user's information to the row
                    create_VM.userId = User.Identity.GetUserId();
                    create_VM.userName = User.Identity.GetUserName();
                    
                    MethodsTable methodsTable = Mapper.Map<MethodsTable>(create_VM);
                    Mapper.AssertConfigurationIsValid();

                    db.MethodsTables.Add(methodsTable);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Method/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Method/Edit/5
        [HttpPost]
        [EditDelAuthorize]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Method/Delete/5
        [HttpGet]
        [EditDelAuthorize]
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MethodsTable methodsTableRow = await db.MethodsTables.FindAsync(id);
            if (methodsTableRow == null)
            {
                return HttpNotFound();
            }

            var viewModel = new EntityDelete_VM
            {
                EntityId = methodsTableRow.methodId,
                EntityName = methodsTableRow.methodName,
                ActionName = "Delete",
                ControllerName = "Methods",
            };
            return PartialView("_EntityDeleteModal", viewModel);
        }

        // POST: Method/Delete/5
        [HttpPost]
        [EditDelAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(EntityDelete_VM viewModel)
        {
            string errMsg = null;

            errMsg = await deleteMethodsTableRow(viewModel.EntityId);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return Json(new { success = false, responseText = errMsg });
            }
            return Json(new { success = true, redirectUrl = Url.Action("Index", "WesternBlot"), isRedirect = true });
        }

        private async Task<string> deleteMethodsTableRow(Guid id)
        {
            string errMsg = null;
            try
            {
                MethodsTable methodsTableRow = await db.MethodsTables.FindAsync(id);
                if (methodsTableRow == null)
                {
                    return "Can't find row in main table";
                }

                // Delete data from table
                db.MethodsTables.Remove(methodsTableRow);
                await db.SaveChangesAsync();

                return errMsg;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return errMsg;
            }
        }

        // GET: Method/Duplicate/5
        [EditDelAuthorize]
        public async Task<ActionResult> Duplicate(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MethodsTable methodsTableRow = await db.MethodsTables.FindAsync(id);
            if (methodsTableRow == null)
            {
                return HttpNotFound();
            }

            var viewModel = Mapper.Map<ExpMethodsCreate_VM>(methodsTableRow);
            Mapper.AssertConfigurationIsValid();

            return View("Create", viewModel);
        }
    }
}
