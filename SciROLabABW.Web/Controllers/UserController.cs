﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SciROLabABW.Web.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Net;
using SciROLabABW.Web.ViewModels.Users;
using Microsoft.AspNet.Identity;

namespace SciROLabABW.Web.Controllers
{
    // Only Owner can manage users.
    [Authorize(Roles = "Owner")]
    public class UserController : BaseController
    {
        private SciROLabWebEntities db = new SciROLabWebEntities();
        ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            // Build a list of all existing users
            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (AspNetUser user in db.AspNetUsers)
            {
                SelectListItem item = new SelectListItem()
                {
                    Text = user.UserName,
                    Value = user.Id
                };
                listItems.Add(item);
            }

            var viewModel = new UsersManage_VM
            {
                Users = listItems
            };

            return View(viewModel);
        }

        public ActionResult CreateUserByEmail()
        {
            return View("AddUserByEmail");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateUserByEmail(UsersManage_VM model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, EmailConfirmed = true };
                var result = await UserManager.CreateAsync(user); // Create user without password.
                if (result.Succeeded)
                {
                    await SendActivationMail(user);
                    TempData["message"] = "The user has been created and an activation email has been sent to the provided e-mail address.";
                    return RedirectToAction("CreateConfirmation");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            return View("AddUserByEmail", model);
        }

        private async Task SendActivationMail(ApplicationUser user)
        {
            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

            // Using protocol param will force creation of an absolut url. We
            // don't want to send a relative URL by e-mail.
            var callbackUrl = Url.Action(
                "InvitedUserSignUp",
                "Account",
                new { userId = user.Id, code = code },
                protocol: Request.Url.Scheme);

            string body = @"<h4>Welcome to " + ProjectCommons.productName + @"!</h4> 
                            <p>To get started, please <a href=""" + callbackUrl + @""">activate</a> your account.</p>
                            <p>The account must be activated within 24 hours from receving this mail.</p>";

            await UserManager.SendEmailAsync(user.Id, "Welcome to " +ProjectCommons.productName + "!", body);
        }

        public ActionResult CreateConfirmation()
        {
            return View();
        }

        public ActionResult RegisterUser()
        {
            return View("RegisterUser");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterUser(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Note: we don't really do email confirmation in this case but we need mark Email Confirmed as true,
                //       otherwise user won't be able to change password later on.
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, EmailConfirmed = true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    TempData["message"] = "The user has been successfully created.";
                    return RedirectToAction("CreateConfirmation");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            // If we got this far, something failed, redisplay form
            return View("RegisterUser", model);
        }

        [HttpGet]
        public ActionResult RemoveUsers(string id)
        {
            try
            {
                // Build a list of all existing users other than the current user
                // (we don't want current user to delete himself/herself)
                List<SelectListItem> listItems = new List<SelectListItem>();
                foreach (AspNetUser user in db.AspNetUsers)
                {
                    string currentUsrName = User.Identity.GetUserName();
                    if (!user.UserName.Equals(currentUsrName))
                    {
                        SelectListItem item = new SelectListItem()
                        {
                            Text = user.UserName,
                            Value = user.Id
                        };
                        listItems.Add(item);
                    }
                }

                var viewModel = new UsersManage_VM
                {
                    Users = listItems
                };

                return View("RemoveUsers", viewModel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveUsers(UsersManage_VM viewModel, IEnumerable<string> SelectedUsers)
        {
            bool rv = true;

            if (SelectedUsers != null)
            {
                foreach (string strUser in SelectedUsers)
                {
                    AspNetUser user = db.AspNetUsers.Find(strUser);
                    GroupsController grp = new GroupsController();
                    rv = grp.DeleteUserFromAllGroups(user.Id);
                    db.AspNetUsers.Remove(user);
                }
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }        
    }
}