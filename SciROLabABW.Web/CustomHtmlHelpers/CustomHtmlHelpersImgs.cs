﻿using SciROLabABW.Web.ImageUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace SciROLabABW.Web.CustomHtmlHelpers
{
    public static class CustomHtmlHelpersImgs
    {
        public static MvcHtmlString DisplayImageByteArray(this HtmlHelper html, byte[] image)
        {
            try
            {
                if (image == null)
                {
                    return new MvcHtmlString("");
                }
                var img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(image));
                return new MvcHtmlString(img);
            }
            catch (Exception)
            {
                return new MvcHtmlString("");
            }
        }

        public static MvcHtmlString DisplayImageByPath(this HtmlHelper html, string imagePath)
        {
            try
            {
                byte[] data;
                Image image;

                using (var memoryStream = new System.IO.MemoryStream())
                {
                    using (image = System.Drawing.Image.FromFile(imagePath))
                    {
                        image.Save(memoryStream, ImageFormat.Jpeg);
                    }
                    data = memoryStream.ToArray();
                }
                var img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(data));
                return new MvcHtmlString(img);
            }
            catch (Exception)
            {
                return new MvcHtmlString("");
            }

#if false
            try
            {
                TiffReader reader = new TiffReader(imagePath);
                var bitmap = reader.getBmp();

                byte[] byteArray = new byte[0];
                using (MemoryStream stream = new MemoryStream())
                {
                    bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    stream.Close();
                    byteArray = stream.ToArray();
                    var img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(byteArray));
                    return new MvcHtmlString(img);
                }
            }
            catch
            {
                return null;
            }
#endif
        }

        public static MvcHtmlString ZoomViewHref(this HtmlHelper html, string storedImagePath, string text)
        {
            var address = ConfigurationManager.AppSettings["openSlideAddress"].ToString();
            var port = ConfigurationManager.AppSettings["openSlidePort"].ToString();

            string imgName = Path.GetFileName(storedImagePath);
            if (FileServices.isOpenSlideImg(storedImagePath) == false)
            {
                return new MvcHtmlString("");
            }
            MvcHtmlString htmlStr = new MvcHtmlString(null);
            try
            {
                if (imgName == null)
                {
                    return new MvcHtmlString("");
                }

                string str = "<a href='http://" + address + ":" + port + "/" + imgName + "' target='_blank'>" + text + "</a>";
                return new MvcHtmlString(str);
            }
            catch (Exception)
            {
                return new MvcHtmlString("");
            }
        }
    }
}