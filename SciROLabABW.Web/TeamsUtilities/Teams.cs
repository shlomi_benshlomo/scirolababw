﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.TeamsUtilities
{
    public class Teams
    {
        public static SciROLabTeam GetUserTeam(SciROLabWebEntities db)
        {
            string currentUsrName = HttpContext.Current.User.Identity.Name;
            if(string.IsNullOrEmpty(currentUsrName))
            {
                return null;
            }
            var currentUser = db.AspNetUsers.Where(x => x.UserName == currentUsrName).ToList();
            SciROLabTeam team = null;

            if ((currentUser != null) && (currentUser.Count > 0) && (currentUser.FirstOrDefault().TeamId != null))
            {
                team = db.SciROLabTeams.Find(currentUser.FirstOrDefault().TeamId);
                return team;
            }

            return null;
        }

        public static string GetUserTeamId(SciROLabWebEntities db)
        {
            SciROLabTeam team = GetUserTeam(db);

            if (team != null)
            {
                return team.Id;
            }

            return null;
        }

        public static bool IsUserInTeam(SciROLabTeam team, string userName)
        {
            foreach (AspNetUser user in team.AspNetUsers)
            {
                if (userName == user.UserName)
                {
                    return true;
                }
            }

            return false;
        }

        public static List<string> GetAllPermittedUsers(SciROLabWebEntities db)
        {
            List<string> list = new List<string>();
            string currentUsrName = HttpContext.Current.User.Identity.Name;
            var currentUser = db.AspNetUsers.Where(x => x.UserName == currentUsrName).ToList();
            SciROLabTeam team = null;

            // If current user has Owner permission create a list of all users in the system
            if (HttpContext.Current.User.IsInRole("Owner"))
            {
                list = db.AspNetUsers.Select(u => u.UserName).ToList();
            }
            else // Create a list of permitted users
            {
                if ((currentUser != null) && (currentUser.FirstOrDefault().TeamId != null))
                {
                    team = db.SciROLabTeams.Find(currentUser.FirstOrDefault().TeamId);
                }

                // If user is not part of a team just return his/her user name
                if (team == null)
                {
                    list.Add(currentUsrName);
                }
                else
                {
                    // Build a list of all users in current team
                    foreach (AspNetUser usr in team.AspNetUsers)
                    {
                        if (!list.Contains(usr.UserName)) // Add user to the list only once
                        {
                            list.Add(usr.UserName);
                        }
                    }
                }
            }
            return list;
        }

        public static List<string> GetAllUserNamesInMyTeam(SciROLabTeam team)
        {
            List<string> list = new List<string>();
            // Build a list of all users in current team
            foreach (AspNetUser usr in team.AspNetUsers)
            {
                if (!list.Contains(usr.UserName)) // Add user to the list only once
                {
                    list.Add(usr.UserName);
                }
            }

            return list;
        }
    }
}