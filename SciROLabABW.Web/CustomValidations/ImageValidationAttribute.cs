﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.CustomValidations
{
    public class ImageValidationAttribute : ValidationAttribute, IClientValidatable
    {
        const int ImageMinimumBytes = 512;

        public ImageValidationAttribute()
            : base("Invalid Image!")
        {
            return;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var supportedFormats = new[] 
            { ImageFormat.Jpeg, ImageFormat.Gif, ImageFormat.Png, 
              ImageFormat.Bmp, ImageFormat.Tiff 
            };

            HttpPostedFileBase file = value as HttpPostedFileBase;

            // The image is required.
            if (file == null)
            {
                return new ValidationResult("Please add an image!");
            }

            // Image type validation.
            string ext = Path.GetExtension(file.FileName).ToLower();

            if (String.IsNullOrEmpty(ext))
            {
                return new ValidationResult("Invalid image - extension is not readable!");
            }

            if (!FileServices.openSlideSupportedExtensions.Contains(ext) && (!FileServices.supportedFileExtensions.Contains(ext)))
            {
                return new ValidationResult("Invalid image type! (allowed formats: " + string.Join(", ", FileServices.supportedFileExtensions, FileServices.openSlideSupportedExtensions) + ")");
            }
            if (FileServices.openSlideSupportedExtensions.Contains(ext))
            {
                // In case of openslide images that's all we can check at this point
                return ValidationResult.Success;
            }

            // Image MIME type validation.
            string contentType = file.ContentType.ToLower();
            if (String.IsNullOrEmpty(contentType) || (!FileServices.supportedMimeType.Contains(contentType)))
            {
                return new ValidationResult("Invalid content type!");
            }

            // Image raw format validation.
            try
            {
                using (var img = Image.FromStream(file.InputStream))
                {
                    if (!supportedFormats.Contains(img.RawFormat))
                    {
                        return new ValidationResult("Invalid image format!");
                    }
                }
            }
            catch (Exception)
            {
                return new ValidationResult("Invalid image!");
            }
            finally
            {
                file.InputStream.Position = 0;
            }

            // Read file and check the first bytes. // TODO: should i use this one??? check performance!!!
            try
            {
                if (!file.InputStream.CanRead)
                {
                    return new ValidationResult("Can't read image!");
                }

                if (file.ContentLength < ImageMinimumBytes)
                {
                    return new ValidationResult("Invalid image size!");
                }

                byte[] buffer = new byte[ImageMinimumBytes];
                file.InputStream.Read(buffer, 0, ImageMinimumBytes);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                {
                    return new ValidationResult("Invalid content!");
                }
            }
            catch (Exception)
            {
                return new ValidationResult("Invalid image!");
            }
            finally
            {
                file.InputStream.Position = 0;
            }

            return ValidationResult.Success;
        }

        // Client side validation
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(metadata.GetDisplayName());
            rule.ValidationType = "imagevalidation";
            yield return rule;
        }
    }
}