﻿using SciROLabABW.Web.GroupsUtilities;
using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SciROLabABW.Web.RowsUtilities
{
    public class Rows
    {
        private static Expression<Func<WBMainTable, bool>> userNameContainsOrExpression(IEnumerable<string> keywords)
        {
            var predicate = ExpressionBuilder.Begin<WBMainTable>();

            foreach (var keyword in keywords)
            {
                predicate = predicate.Or(x => x.userName.Contains(keyword));
            }
            return predicate;
        }

        private static Expression<Func<WBMainTable, bool>> rowIDsContainsOrExpression(IEnumerable<Guid> keywords)
        {
            var predicate = ExpressionBuilder.Begin<WBMainTable>();

            foreach (var keyword in keywords)
            {
                predicate = predicate.Or(x => x.rowID.Equals(keyword));
            }
            return predicate;
        }

        private List<Guid> mainTableViewPermitUsersList(string usrName, SciROLabWebEntities db)
        {
            List<Guid> rowIDs = new List<Guid> { };
            // Get all the rows from mainTable that the current user has permission to view
            var mainTablePermittedRows = db.WBMainTable.Where(x => x.userName == usrName || x.shareRow == true).ToList();
            var permittedIds = db.WBRowPermissions.Where(x => x.userName == usrName).Select(u => u.mainTableID).ToList();

            foreach (var item in mainTablePermittedRows)
            {
                if (item.userName == usrName)
                {
                    rowIDs.Add(item.rowID);
                }
                else
                {
                    foreach (var id in permittedIds)
                    {
                        if (id == item.rowID)
                        {
                            rowIDs.Add(item.rowID);
                        }
                    }
                }
            }

            return rowIDs;
        }

        private static List<Guid> mainTableViewPermitGrpsList(string usrName, SciROLabWebEntities db)
        {
            // Get a list of all the permitted groups for the current user
            var permittedGroups = Groups.GetPermittedGroupsList(db);
            // List of all users that current user has permission to view their data
            List<string> userNames = new List<string> { };

            userNames = Groups.GetAllUserNamesInMyGroups(permittedGroups, usrName);
            // Current user can always view his/her own data - add user if not in the list yet (can happen in case that the user is not in any group) 
            if (!(userNames.Contains(usrName)))
            {
                userNames.Add(usrName);
            }

            // List of row ids the current user has permission to view
            List<Guid> rowIDs = new List<Guid> { };

            // Get all the rows from mainTable that the current user has permission to view according to groups list
            var mainTablePermittedRows = db.WBMainTable.Where(userNameContainsOrExpression(userNames));

            foreach (var item in mainTablePermittedRows)
            {
                // User can always view his/her own rows and PI can view all rows regardless of permission per row
                if ((item.userName == usrName) || (HttpContext.Current.User.IsInRole("PI")))
                {
                    rowIDs.Add(item.rowID);
                }
                else
                {
                    if (item.shareRow == true)
                    {
                        if (item.groupName != null)
                        {
                            foreach (var grp in permittedGroups)
                            {
                                if ((Groups.IsUserInGroup(grp, item.userName)) && (grp.Name == item.groupName))
                                {
                                    rowIDs.Add(item.rowID);
                                }
                            }
                        }
                        else
                        {
                            rowIDs.Add(item.rowID);
                        }
                    }
                }
            }

            return rowIDs;
        }

        public static IQueryable<WBMainTable> getUserPermmitedRowsViewMainTable(SciROLabWebEntities db)
        {
            string usrName = HttpContext.Current.User.Identity.Name;
            // List of row ids the current user has permission to view
            List<Guid> rowIDs = new List<Guid> { };

            // Get all the rows from mainTable that the current user has permission to view according to groups list
            rowIDs = mainTableViewPermitGrpsList(usrName, db);

            // Get all the rows from mainTable that the current user has permission to view according to users list per row
            // This feature is not in use yet
            //rowIDs = mainTableViewPermitUsersList(usrName);

            // Get all main table rows that the user has permission to view.
            var wbMainTable = db.WBMainTable.Where(rowIDsContainsOrExpression(rowIDs));
            return wbMainTable;
        }

        public static bool userEditMainRowPermission(string rowOwner)
        {
            string currentUserName = HttpContext.Current.User.Identity.Name;

            if (currentUserName != rowOwner)
            {
                return false;
            }

            return true;
        }
    }
}