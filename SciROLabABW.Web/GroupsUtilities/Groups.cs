﻿using SciROLabABW.Web.Models;
using SciROLabABW.Web.TeamsUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SciROLabABW.Web.GroupsUtilities
{
    public class Groups
    {
        private static Expression<Func<SciROLabGroup, bool>> groupsNameEqualsOrExpression(IEnumerable<string> keywords)
        {
            var predicate = ExpressionBuilder.Begin<SciROLabGroup>();

            foreach (var keyword in keywords)
            {
                predicate = predicate.Or(x => x.Name.Equals(keyword));
            }
            return predicate;
        }

        private static Expression<Func<SciROLabGroup, bool>> groupsUserNameContainsOrExpression(IEnumerable<string> keywords)
        {
            var predicate = ExpressionBuilder.Begin<SciROLabGroup>();

            foreach (var keyword in keywords)
            {
                predicate = predicate.Or(x => x.userName.Contains(keyword));
            }
            return predicate;
        }

        public static bool IsUserInGroup(SciROLabGroup group, string userName)
        {
            foreach (AspNetUser user in group.AspNetUsers)
            {
                if (userName == user.UserName)
                {
                    return true;
                }
            }

            return false;
        }

        public static List<SciROLabGroup> GetUserGroups(SciROLabWebEntities db, string userName)
        {
            // Get a list of all the permitted groups for the current user
            var permittedGroups = Groups.GetPermittedGroupsList(db);

            List<SciROLabGroup> groups = null;
            // List of all users from the same team of the current user
            List<string> grpsNamesList = new List<string>();

            foreach (var grp in permittedGroups)
            {
                if (Groups.IsUserInGroup(grp, userName))
                {
                    grpsNamesList.Add(grp.Name);
                }
            }

            groups = db.SciROLabGroups.Where(groupsNameEqualsOrExpression(grpsNamesList)).ToList();

            return groups;
        }

        public static List<string> GetAllUserNamesInMyGroups(List<SciROLabGroup> groups, string usrName)
        {
            List<string> list = new List<string>();
            foreach (SciROLabGroup grp in groups)
            {
                // If current user is in this group, add all users to the list
                if (IsUserInGroup(grp, usrName))
                {
                    foreach (AspNetUser usr in grp.AspNetUsers)
                    {
                        if (!list.Contains(usr.UserName)) // Add user to the list only once
                        {
                            list.Add(usr.UserName);
                        }
                    }
                }
            }

            return list;
        }

        public static List<SciROLabGroup> GetPermittedGroupsList(SciROLabWebEntities db)
        {
            string currentUsrName = HttpContext.Current.User.Identity.Name;
            var currentUser = db.AspNetUsers.Where(x => x.UserName == currentUsrName).ToList();
            SciROLabTeam team = Teams.GetUserTeam(db);
            List<SciROLabGroup> groupsList = null;

            // List of all users from the same team of the current user
            List<string> userNamesList = new List<string>();

            // If current user has Owner permission create a list of all groups in the system
            if (HttpContext.Current.User.IsInRole("Owner"))
            {
                groupsList = db.SciROLabGroups.ToList();
            }
            else
            {
                // If user is part of a team, add all groups that belong to this team to the list
                if (team != null)
                {
                    groupsList = db.SciROLabGroups.Where(x => x.TeamId == team.Id).ToList();
                }
                else // User is not part of a team, add only the groups of the current user to the list
                {
                    userNamesList.Add(currentUser.FirstOrDefault().UserName);
                    groupsList = db.SciROLabGroups.Where(groupsUserNameContainsOrExpression(userNamesList)).ToList();
                }
            }

            return groupsList;

        }

        public static bool RemoveAllTeamGroups(SciROLabWebEntities db, string teamId)
        {
            var groupsList = db.SciROLabGroups.Where(x => x.TeamId == teamId).ToList();

            foreach (var grp in groupsList)
            {
                foreach(var user in grp.AspNetUsers.ToList())
                {
                    grp.AspNetUsers.Remove(user);
                }
                db.SciROLabGroups.Remove(grp);
                db.SaveChanges();
            }

            return true;
        }
    }
}