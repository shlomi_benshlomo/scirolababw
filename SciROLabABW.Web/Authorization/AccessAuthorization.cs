﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace SciROLabABW.Web.Authorization
{
    public static class AccessAuthorization
    {
        public static bool IsUserEditDelAutorized(string loginUser, string dataOwnerUserName)
        {
            if (loginUser == dataOwnerUserName)
            {
                return true;
            }
            return false;
        }
    }

    public class HttpForbiddenResult : HttpStatusCodeResult
    {
        public HttpForbiddenResult()
            : this(null)
        {
        }

        public HttpForbiddenResult(string statusDescription)
            : base(HttpStatusCode.Forbidden, statusDescription)
        {
        }
    }

    // TODO:3: - decide if we need this one!!!
    public class EditDelAuthorize : AuthorizeAttribute
    {
        // Not in use at this point!
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            /*if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                return false;
            }

            var rd = httpContext.Request.RequestContext.RouteData;
            string idStr = "";
            if (rd.Values["id"] != null)
            {
                idStr = rd.Values["id"].ToString();
            }
            else if (httpContext.Request.Params["id"] != null)
            {
                idStr = httpContext.Request.Params["id"];
            }
            else
            {
                return false;
            }

            Guid id = new Guid(idStr);
            var userName = httpContext.User.Identity.Name;

            SciROLabWebEntities db = new SciROLabWebEntities();
            WBMainTable wBMainTable = db.WBMainTable.Find(id);

            return wBMainTable.userName == userName;*/

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpForbiddenResult("Forbidden Access.");
            }
            else
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
    }
}