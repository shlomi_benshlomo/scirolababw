﻿using AutoMapper;
using Mappings;
using SciROLabABW.Web.Controllers;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SciROLabABW.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutoMapperConfig.Configure();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["cloudService"] != "1963")
            {
                // Initializing InitLicenseKey
                LicenseObject objLnc = new LicenseObject(0, Request, Response);
                if (!objLnc.InitLicense())
                {
                    throw new InvalidOperationException(objLnc.m_strErrStr);
                }
                // If user is already authenticated in the system implement login to the license server
                if ((HttpContext.Current != null) && (HttpContext.Current.User != null) && (HttpContext.Current.User.Identity.IsAuthenticated))
                {
                    // Get the Web application configuration object.
                    System.Configuration.Configuration configuration =
                        System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);

                    // Get the section related object.
                    System.Web.Configuration.SessionStateSection sessionStateSection =
                    (System.Web.Configuration.SessionStateSection)configuration.GetSection("system.web/sessionState");
                    sessionStateSection.Cookieless = HttpCookieMode.AutoDetect;

                    var userName = HttpContext.Current.User.Identity.Name;
                    // Log in to the NKLS
                    if (objLnc.NKLSLogin("SciROLab", userName, Session.SessionID, 2, 20))   // 2 min timeout, 20 sec intervals
                    {
                        Session["LoginUserID"] = userName;
                    }
                    else
                    {
                        throw new Exception(objLnc.m_strErrStr);
                    }
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception lastError = Server.GetLastError();
            Server.ClearError();

            int statusCode = 0;

            if (lastError.GetType() == typeof(HttpException))
            {
                statusCode = ((HttpException)lastError).GetHttpCode();
            }
            else
            {
                // Not an HTTP related error so this is a problem in the code, set status to
                // 500 (internal server error)
                statusCode = 500;
            }

            HttpContextWrapper contextWrapper = new HttpContextWrapper(this.Context);

            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Index");
            routeData.Values.Add("statusCode", statusCode);
            if (lastError != null)
            {
                if (lastError.InnerException != null)
                {
                    routeData.Values.Add("exception", lastError.InnerException);
                }
                else
                {
                    routeData.Values.Add("exception", lastError);
                }
            }
            routeData.Values.Add("isAjaxRequet", contextWrapper.Request.IsAjaxRequest());

            IController controller = new ErrorController();

            RequestContext requestContext = new RequestContext(contextWrapper, routeData);

            controller.Execute(requestContext);
            Response.End();
        }
    }
}
