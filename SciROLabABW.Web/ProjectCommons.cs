﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web
{
    public static class ProjectCommons
    {
        public static string productName
        {
            get { return "SciROLab®"; }
        }

        public static string productVersionName
        {
            get { return "SciROLab®ABW"; }
        }

        public static string SciROLabDataImgsPath
        {
            get
            {
                return Path.Combine(ConfigurationManager.AppSettings["storedDataPath"].ToString());
            }
        }
        public static string SciROLabDataRefPath
        {
            get
            {
                return Path.Combine(ConfigurationManager.AppSettings["storedDataPath"].ToString());
            }
        }

        public static string AppImagesPath
        {
            get
            {
                return "~/Content/Images/";
            }
        }

        // List of basic roles in the system
        static private string[] basicRolesArr = { "Admin", "PI", "GroupUser", "User" };

        public static string[] BasicRoles
        {
            get { return basicRolesArr; }
        }

        public static bool isBasicRole(string role)
        {
            for (int i = 0; i < basicRolesArr.Length; i++)
            {
                if (basicRolesArr[i] == role)
                {
                    return true;
                }
            }

            return false;
        }
    }
}