﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.ViewModels.Teams
{
    public class TeamManageUsers_VM
    {
        public TeamManageUsers_VM()
        {
            this.AspNetUsers = new HashSet<AspNetUser>();
        }
    
        public string teamId { get; set; }
        public string teamName { get; set; }
    
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
        public IEnumerable<string> SelectedUsers { get; set; }
        public IEnumerable<SelectListItem> Users { get; set; }
    }
}