﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels
{
    public class WBReferenceCreateURL_VM
    {
        public WBReferenceCreateURL_VM()
        {
            rowID = Guid.NewGuid();
            userName = HttpContext.Current.User.Identity.Name;
        }
        [Display(Name = "URL: ")]
        [Required(ErrorMessage = "This field is required!")]
        [Url(ErrorMessage = "Please enter a valid url")]
        public string link { get; set; }
        public string fullPath { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long referenceID { get; set; }
        public string userName { get; set; }
        public System.Guid rowID { get; set; }
        public System.Guid mainTableID { get; set; }
        public string refLocation { get; set; }

        public virtual WBMainTable WBMainTable { get; set; }
    }
}