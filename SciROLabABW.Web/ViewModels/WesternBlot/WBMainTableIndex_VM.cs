﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SciROLabABW.Web.Models;
using System.ComponentModel.DataAnnotations;
using SciROLabABW.Web.CustomValidations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SciROLabABW.Web.ViewModels
{
    public class WBMainTableIndex_VM
    {
        [Required]
        [Display(Name = "Image ID: ")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long mainID { get; set; }
        [Required]
        [Display(Name = "User Name: ")]
        public string userName { get; set; }
        [Display(Name = "User ID: ")]
        public long userID { get; set; }
        [Display(Name = "Date Added: ")]
        public Nullable<System.DateTime> dateAdded { get; set; }
        [Display(Name = "Experiment ID: ")]
        public string experimentID { get; set; }
        [Display(Name = "Experiment Date: ")]
        public Nullable<System.DateTime> experimentDate { get; set; }
        [Display(Name = "Experimenter: ")]
        public string performer { get; set; }
        [Display(Name = "Experiment Info: ")]
        [DataType(DataType.MultilineText)]
        public string experimentInfo { get; set; }
        [Display(Name = "Membrane Info: ")]
        [DataType(DataType.MultilineText)]
        public string membraneInfo { get; set; }
        [Display(Name = "Membrane ID: ")]
        public string membraneID { get; set; }
        [Display(Name = "Membrane Date: ")]
        public Nullable<System.DateTime> membraneDate { get; set; }
        [Display(Name = "Blot: ")]
        public string blot { get; set; }
        [Display(Name = "Blot Date: ")]
        public Nullable<System.DateTime> blotDate { get; set; }
        [Display(Name = "Comments: ")]
        [DataType(DataType.MultilineText)]
        public string comments { get; set; }
        [Display(Name = "Control: ")]
        public bool control { get; set; }
        [Display(Name = "Image Name: ")]
        public string imageName { get; set; }
        public System.Guid rowID { get; set; }
        [Display(Name = "Image: ")]
        public byte[] image { get; set; }
        [Display(Name = "Slide Date: ")]
        public Nullable<System.DateTime> slideDate { get; set; }
        [Display(Name = "Block ID: ")]
        public string blockID { get; set; }
        [Display(Name = "Prim. Antibody ID: ")]
        public string primaryAntibodyId { get; set; }
        [Display(Name = "Prim. Antibody: ")]
        public string primaryAntibody { get; set; }
        [Display(Name = "Second. Antibody: ")]
        public string secondaryAntibody { get; set; }
        [Display(Name = "Positive Control: ")]
        public bool posControl { get; set; }
        [Display(Name = "Negative Control: ")]
        public bool negControl { get; set; }
        [Display(Name = "Method: ")]
        public string experimentMethod { get; set; }
        [Display(Name = "Share Row: ")]
        public bool shareRow { get; set; }
        public string imagePath { get; set; }
        public string groupID { get; set; }
        [Display(Name = "Share with Group: ")]
        public string groupName { get; set; }
        public string storedImageName { get; set; }
        public string storedImagePath { get; set; }
        public string storedLabelImagePath { get; set; }
        public string storedMacroImagePath { get; set; }
        public string Text1 { get; set; }
        public string Text1Name { get; set; }
        public string Text2 { get; set; }
        public string Text2Name { get; set; }
        public string Text3 { get; set; }
        public string Text3Name { get; set; }
        public string Text4 { get; set; }
        public string Text4Name { get; set; }
        public string Text5 { get; set; }
        public string Text5Name { get; set; }
        public string Text6 { get; set; }
        public string Text6Name { get; set; }
        public string Text7 { get; set; }
        public string Text7Name { get; set; }
        public string Text8 { get; set; }
        public string Text8Name { get; set; }
        public string Text9 { get; set; }
        public string Text9Name { get; set; }
        public string Text10 { get; set; }
        public string Text10Name { get; set; }
        public string Text11 { get; set; }
        public string Text11Name { get; set; }
        public string Text12 { get; set; }
        public string Text12Name { get; set; }
        public string Text13 { get; set; }
        public string Text13Name { get; set; }
        public string Text14 { get; set; }
        public string Text14Name { get; set; }
        public string Text15 { get; set; }
        public string Text15Name { get; set; }
        public Nullable<System.DateTime> Date1 { get; set; }
        public string Date1Name { get; set; }
        public Nullable<System.DateTime> Date2 { get; set; }
        public string Date2Name { get; set; }
        public Nullable<System.DateTime> Date3 { get; set; }
        public string Date3Name { get; set; }
        public bool Bool1 { get; set; }
        public string Bool1Name { get; set; }
        public bool Bool2 { get; set; }
        public string Bool2Name { get; set; }
        public bool Bool3 { get; set; }
        public string Bool3Name { get; set; }
        public bool Bool4 { get; set; }
        public string Bool4Name { get; set; }
        public bool Bool5 { get; set; }
        public string Bool5Name { get; set; }
        public bool Bool6 { get; set; }
        public string Bool6Name { get; set; }
        public bool Bool7 { get; set; }
        public string Bool7Name { get; set; }
        public bool Bool8 { get; set; }
        public string Bool8Name { get; set; }
        public virtual ICollection<WBReferencesTable> WBReferencesTable { get; set; }
    }
}