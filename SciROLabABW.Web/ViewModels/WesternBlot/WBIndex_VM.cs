﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels
{
    public class WBIndex_VM
    {
        public string freeSearchStr { get; set; }
        public string searchUserName { get; set; }
        public string searchMethod { get; set; }
        public string searchExperimenter { get; set; }
        public string searchPrimaryAntibody { get; set; }
        public DateTime? dateAddedMinDate { get; set; }
        public DateTime? dateAddedMaxDate { get; set; }
        public DateTime? expMinDate { get; set; }
        public DateTime? expMaxDate { get; set; }

        public string CurrentSortOrder { get; set; }
        public string CurrentPageSize { get; set; }
        public string CurrentSearchType { get; set; }
        public string CurrentSearchStatMsg { get; set; }
        public string FreeSearchCurrentFilter { get; set; }
        public string UserNameCurrentFilter { get; set; }
        public string MethodCurrentFilter { get; set; }
        public string ExperimenterCurrentFilter { get; set; }
        public string PrimaryAntibodyCurrentFilter { get; set; }
        public DateTime? DateAddedCurrentMinDate { get; set; }
        public DateTime? DateAddedCurrentMaxDate { get; set; }
        public DateTime? ExperimentCurrentMinDate { get; set; }
        public DateTime? ExperimentCurrentMaxDate { get; set; }

        public PagedList.IPagedList<WBMainTableIndex_VM> WBMainTableIndex_VM { get; set; }

        List<string> wbSortListNames = new List<string> { 
                                       "Image ID, Descending", "Image ID, Ascending", 
                                       "User Name, Descending", "User Name, Ascending",
                                       "Image Name, Descending", "Image Name, Ascending",
                                       "Share Row Mode", 
                                       "Date Added, Descending", "Date Added, Ascending",
                                       "Method, Descending", "Method, Ascending",
                                       "Experiment ID, Descending", "Experiment ID, Ascending",
                                       "Experiment Date, Descending", "Experiment Date, Ascending",
                                       "Experimenter, Descending", "Experimenter, Ascending",
                                       "Primary Antibody Id, Descending", "Primary Antibody Id, Ascending", 
                                   };

        public List<string> GetWBSortList()
        {
            return wbSortListNames;
        }

        List<string> wbPageSizeList = new List<string> { "5", "10", "25" };

        public List<string> GetWBPageSizeList()
        {
            return wbPageSizeList;
        }

        public bool ClearFreeSearch(WBIndex_VM model)
        {
            model.freeSearchStr = null;
            return true;
        }

        public bool ClearAdvancedSearch(WBIndex_VM model)
        {
            model.searchUserName = null;
            model.searchMethod = null;
            model.searchExperimenter = null;
            model.searchPrimaryAntibody = null;
            model.dateAddedMinDate = null;
            model.dateAddedMaxDate = null;
            model.expMinDate = null;
            model.expMaxDate = null;
            return true;
        }

        public bool ClearSearch(WBIndex_VM model)
        {
            ClearFreeSearch(model);
            ClearAdvancedSearch(model);
            return true;
        }
    }
}