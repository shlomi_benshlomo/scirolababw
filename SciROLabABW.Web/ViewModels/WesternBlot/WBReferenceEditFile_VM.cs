﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels
{
    public class WBReferenceEditFile_VM
    {
        [Display(Name = "File: ")]
        [Required(ErrorMessage = "This field is required!")]
        public HttpPostedFileBase file { get; set; }
        public string link { get; set; }
        public string fullPath { get; set; }
        public long referenceID { get; set; }
        public string userName { get; set; }
        public System.Guid rowID { get; set; }
        public Nullable<System.Guid> mainTableID { get; set; }
        public string refLocation { get; set; }
    }
}