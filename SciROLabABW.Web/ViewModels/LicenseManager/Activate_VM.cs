﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels.LicenseManager
{
    public class Activate_VM
    {
        [Display(Name = "Registration ID: ")]
        public string strRegistrationID { get; set; }
        [Display(Name = "Activation Key: ")]
        public string strActivationKey { get; set; }
        [Display(Name = "License Key: ")]
        public string strLicenseKey { get; set; }        
    }
}