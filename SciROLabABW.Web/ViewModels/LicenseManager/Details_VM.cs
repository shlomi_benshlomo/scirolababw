﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels.LicenseManager
{
    public class Details_VM
    {
        [Display(Name = "Registration ID: ")]
        public string strRegistrationID { get; set; }
        [Display(Name = "Activation Key: ")]
        public string strActivationKey { get; set; }
        [Display(Name = "License Status: ")]
        public string strLicenseStatus { get; set; }
        [Display(Name = "License Mode: ")]
        public int strLicenseMode { get; set; }        
        [Display(Name = "Max Day Period: ")]
        public string nMaxDayPeriod { get; set; }
        [Display(Name = "Day Count Period: ")]
        public string nDayCountPeriod { get; set; }
        [Display(Name = "Day Left Period: ")]
        public string nDayLeftPeriod { get; set; }
        [Display(Name = "Expired Date: ")]
        public string strExpiredDate { get; set; }
        [Display(Name = "Max Executions: ")]
        public int strMaxExec { get; set; }
        [Display(Name = "Executions Count: ")]
        public int strExecCount { get; set; }        
        [Display(Name = "Max Users: ")]
        public int strMaxUser { get; set; }
    }
}