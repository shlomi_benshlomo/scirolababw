﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.ViewModels.Users
{
    public class UsersManage_VM
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
        public IEnumerable<string> SelectedUsers { get; set; }
        public IEnumerable<SelectListItem> Users { get; set; }
    }
}