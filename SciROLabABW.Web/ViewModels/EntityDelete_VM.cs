﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels
{
    public class EntityDelete_VM
    {
        public Guid EntityId { get; set; }
        public string EntityName { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
    }
}