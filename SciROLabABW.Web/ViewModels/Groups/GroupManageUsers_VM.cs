﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SciROLabABW.Web.ViewModels.Groups
{
    public class GroupManageUsers_VM
    {
        public GroupManageUsers_VM()
        {
            this.AspNetUsers = new HashSet<AspNetUser>();
        }

        public string GroupId { get; set; }
        public string GroupName { get; set; }
    
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
        public IEnumerable<string> SelectedUsers { get; set; }
        public IEnumerable<SelectListItem> Users { get; set; }
    }
}