﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels.Groups
{
    public class GroupManage_VM
    {
        public GroupManage_VM()
        {
            this.AspNetUsers = new HashSet<AspNetUser>();
        }
    
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public string TeamId { get; set; }
        public string TeamName { get; set; }

        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
    }
}