﻿using SciROLabABW.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels.Groups
{
    public class GroupCreate_VM
    {
        public GroupCreate_VM()
        {
            if (string.IsNullOrEmpty(Id))
            {
                Id = Guid.NewGuid().ToString();
            }
            this.AspNetUsers = new HashSet<AspNetUser>();
        }
    
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
    
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
    }
}