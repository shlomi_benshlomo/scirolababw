﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels.Phrases
{
    public class PhrasesTableIndex_VM
    {
        public System.Guid Id { get; set; }
        [Display(Name = "Category: ")]
        public string Category { get; set; }
        [Display(Name = "Subject: ")]
        public string Subject { get; set; }
        [Display(Name = "Phrase: ")]
        [DataType(DataType.MultilineText)]
        public string Phrase { get; set; }
        [Display(Name = "User Name: ")]
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string TeamId { get; set; }
        public string TeamName { get; set; }
    }
}