﻿using SciROLabABW.Web.ViewModels.Phrases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels
{
    public class PhrasesIndex_VM
    {
        public PagedList.IPagedList<PhrasesTableIndex_VM> PhrasesTableIndex_VM { get; set; }

        List<string> phrasesSortListNames = new List<string> { 
                                           "Category, Descending", "Category, Ascending", 
                                           "Subject, Descending", "Subject, Ascending",
                                           "Phrase, Descending", "Phrase, Ascending",
                                           };

        public List<string> GetPhrasesSortList()
        {
            return phrasesSortListNames;
        }

        List<string> phrasesPageSizeList = new List<string> { "5", "10", "25", "50", "100" };

        public List<string> GetPhrasesPageSizeList()
        {
            return phrasesPageSizeList;
        }
    }
}