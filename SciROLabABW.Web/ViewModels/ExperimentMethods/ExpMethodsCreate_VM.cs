﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels.Methods
{
    public class ExpMethodsCreate_VM
    {
        public ExpMethodsCreate_VM()
        {
            methodId = Guid.NewGuid();
        }
        public System.Guid methodId { get; set; }
        [Required]
        [Display(Name = "Method Name: ")]
        [StringLength(60)]
        public string methodName { get; set; }
        [Display(Name = "User Name: ")]
        public string userName { get; set; }
        [Display(Name = "Experiment ID: ")]
        public string experimentID { get; set; }
        [Display(Name = "Experiment Date: ")]
        public Nullable<System.DateTime> experimentDate { get; set; }
        [Display(Name = "Experiment Info: ")]
        public string experimentInfo { get; set; }
        [Display(Name = "Experimenter: ")]
        public string performer { get; set; }
        [Display(Name = "Comments: ")]
        public string comments { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text1Name { get; set; }
        public bool text1Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text2Name { get; set; }
        public bool text2Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text3Name { get; set; }
        public bool text3Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text4Name { get; set; }
        public bool text4Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text5Name { get; set; }
        public bool text5Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text6Name { get; set; }
        public bool text6Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text7Name { get; set; }
        public bool text7Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text8Name { get; set; }
        public bool text8Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text9Name { get; set; }
        public bool text9Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text10Name { get; set; }
        public bool text10Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text11Name { get; set; }
        public bool text11Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text12Name { get; set; }
        public bool text12Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text13Name { get; set; }
        public bool text13Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text14Name { get; set; }
        public bool text14Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        [StringLength(60)]
        public string text15Name { get; set; }
        public bool text15Display { get; set; }
        [Display(Name = "Datebox Name: ")]
        [StringLength(60)]
        public string date1Name { get; set; }
        public bool date1Display { get; set; }
        [Display(Name = "Datebox Name: ")]
        [StringLength(60)]
        public string date2Name { get; set; }
        public bool date2Display { get; set; }
        [Display(Name = "Datebox Name: ")]
        [StringLength(60)]
        public string date3Name { get; set; }
        public bool date3Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool1Name { get; set; }
        public bool bool1Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool2Name { get; set; }
        public bool bool2Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool3Name { get; set; }
        public bool bool3Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool4Name { get; set; }
        public bool bool4Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool5Name { get; set; }
        public bool bool5Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool6Name { get; set; }
        public bool bool6Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool7Name { get; set; }
        public bool bool7Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        [StringLength(60)]
        public string bool8Name { get; set; }
        public bool bool8Display { get; set; }
        public string userId { get; set; }
    }
}