﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ViewModels.ExperimentMethods
{
    public class ExpMethodsDetails
    {
        public System.Guid methodId { get; set; }
        [Display(Name = "Method Name: ")]
        public string methodName { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text1Name { get; set; }
        public Nullable<bool> text1Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text2Name { get; set; }
        public Nullable<bool> text2Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text3Name { get; set; }
        public Nullable<bool> text3Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text4Name { get; set; }
        public Nullable<bool> text4Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text5Name { get; set; }
        public Nullable<bool> text5Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text6Name { get; set; }
        public Nullable<bool> text6Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text7Name { get; set; }
        public Nullable<bool> text7Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text8Name { get; set; }
        public Nullable<bool> text8Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text9Name { get; set; }
        public Nullable<bool> text9Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text10Name { get; set; }
        public Nullable<bool> text10Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text11Name { get; set; }
        public Nullable<bool> text11Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text12Name { get; set; }
        public Nullable<bool> text12Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text13Name { get; set; }
        public Nullable<bool> text13Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text14Name { get; set; }
        public Nullable<bool> text14Display { get; set; }
        [Display(Name = "Textbox Name: ")]
        public string text15Name { get; set; }
        public Nullable<bool> text15Display { get; set; }
        [Display(Name = "Datebox Name: ")]
        public string date1Name { get; set; }
        public Nullable<bool> date1Display { get; set; }
        [Display(Name = "Datebox Name: ")]
        public string date2Name { get; set; }
        public Nullable<bool> date2Display { get; set; }
        [Display(Name = "Datebox Name: ")]
        public string date3Name { get; set; }
        public Nullable<bool> date3Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool1Name { get; set; }
        public Nullable<bool> bool1Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool2Name { get; set; }
        public Nullable<bool> bool2Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool3Name { get; set; }
        public Nullable<bool> bool3Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool4Name { get; set; }
        public Nullable<bool> bool4Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool5Name { get; set; }
        public Nullable<bool> bool5Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool6Name { get; set; }
        public Nullable<bool> bool6Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool7Name { get; set; }
        public Nullable<bool> bool7Display { get; set; }
        [Display(Name = "Checkbox Name: ")]
        public string bool8Name { get; set; }
        public Nullable<bool> bool8Display { get; set; }
        [Display(Name = "User Name: ")]
        public string userName { get; set; }
    }
}