﻿using SciROLabABW.Web.Models;
using SciROLabABW.Web.TeamsUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SciROLabABW.Web.MethodsUtilities
{
    public class Methods
    {
        private static Expression<Func<MethodsTable, bool>> methodsUserNameContainsOrExpression(IEnumerable<string> keywords)
        {
            var predicate = ExpressionBuilder.Begin<MethodsTable>();

            foreach (var keyword in keywords)
            {
                predicate = predicate.Or(x => x.userName.Contains(keyword));
            }
            return predicate;
        }

        public static List<MethodsTable> GetPermittedMethodsList(SciROLabWebEntities db)
        {
            string currentUsrName = HttpContext.Current.User.Identity.Name;
            var currentUser = db.AspNetUsers.Where(x => x.UserName == currentUsrName).ToList();
            SciROLabTeam team = Teams.GetUserTeam(db);

            // In case of Owner user display all Methods in the system
            if (HttpContext.Current.User.IsInRole("Owner"))
            {
                var methods = db.MethodsTables.ToList();
                return methods.ToList();
            }
            else // else display only permitted methods
            {
                // List of all users from the same team of the current user
                List<string> userNamesList = new List<string>();

                // If user is part of a team, add all users that belong to this team to the list
                if (team != null)
                {
                    userNamesList = Teams.GetAllUserNamesInMyTeam(team);
                }
                else // User is not part of a team, add only the userName of the current user to the list
                {
                    userNamesList.Add(currentUser.FirstOrDefault().UserName);
                }

                var methods = db.MethodsTables.Where(methodsUserNameContainsOrExpression(userNamesList));

                return methods.ToList();
            }
        }
    }
}