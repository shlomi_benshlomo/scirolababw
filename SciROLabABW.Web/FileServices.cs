﻿using SciROLabABW.Web.Models;
using SciROLabABW.Web.TeamsUtilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace SciROLabABW.Web
{
    public class FileServices
    {
        static public string[] openSlideSupportedExtensions = new[] 
            { 
                ".svs", ".tif", ".tiff",
                ".vms", ".vmu", ".ndpi",
                ".scn", ".mrxs", ".svslide",
                ".bif"
            };
        static public string[] supportedFileExtensions = new[] 
            { 
                ".jpeg", ".jpg", ".png", 
                ".gif", ".bmp", ".tif", 
                ".tiff"
            };
        static public string[] supportedMimeType = new[] 
            { 
                "image/jpeg", "image/jpg", "image/pjpeg", 
                "image/gif", "image/png", "image/x-png", 
                "image/bmp", "image/tiff" 
            };
 
        public static byte[] ConvertImageToThumbnailJpeg(MemoryStream input, MemoryStream output)
        {
            int thumbnailsize = 300;
            int width;
            int height;

            try 
            {
                using (var originalImage = new Bitmap(input))
                {
                    if ((originalImage.Width < thumbnailsize) && (originalImage.Height < thumbnailsize))
                    {
                        width = originalImage.Width;
                        height = originalImage.Height;
                    }
                    else if (originalImage.Width > originalImage.Height)
                    {
                        width = thumbnailsize;
                        height = thumbnailsize * originalImage.Height / originalImage.Width;
                    }
                    else
                    {
                        height = thumbnailsize;
                        width = thumbnailsize * originalImage.Width / originalImage.Height;
                    }

                    using (Bitmap thumbnailImage = new Bitmap(width, height))
                    {
                        using (Graphics graphics = Graphics.FromImage(thumbnailImage))
                        {
                            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            graphics.SmoothingMode = SmoothingMode.AntiAlias;
                            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            graphics.DrawImage(originalImage, 0, 0, width, height);
                        }

                        thumbnailImage.Save(output, ImageFormat.Jpeg);
                        var outBytes = output.ToArray();
                        return outBytes;
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        public static byte[] imageToThumbnail(HttpPostedFileBase file, string storedFilePath)
        {
            string ext = Path.GetExtension(file.FileName).ToLower();
            byte[] buffer = null;
            if ((file != null) && (file.ContentLength > 0))
            {
                // If image has one of the openslide supported extensions,
                // try to create thumbnail using the openslide library
                if (openSlideSupportedExtensions.Contains(ext))
                {
                    buffer = OpenSlide.OpenSlideGetThumbnail(storedFilePath);
                }
                // If image has another supported extension,
                // try to create thumbnail from the original image
                if ((buffer == null) && (supportedFileExtensions.Contains(ext)))
                {
                    using (MemoryStream target = new MemoryStream())
                    {
                        file.InputStream.CopyTo(target);
                        byte[] data = target.ToArray();
                        using (var inStream = new MemoryStream(data))
                        {
                            using (var outStream = new MemoryStream())
                            {
                                buffer = ConvertImageToThumbnailJpeg(inStream, outStream);
                            }
                        }
                    }
                }
            }
            return buffer;
        }

        private static string getAssociatedImg(string storedImagePath, string imgName)
        {
            byte[] data;
            string img = null;
            // TODO: consider to check first if this is an openslide image and to do this only if yes
            string ext = Path.GetExtension(storedImagePath).ToLower();

            // Do this only if image has one of the openslide supported extensions
            if (!openSlideSupportedExtensions.Contains(ext))
            {
                return null;
            }
            data = OpenSlide.OpenSlideGetAssociatedImage(storedImagePath, imgName);
            if (data != null)
            {
                img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(data));
            }
            return img;          
        }

        public static string uploadDownSampleImage(string origImgFullPath, string destPath)
        {
            string ext = Path.GetExtension(origImgFullPath).ToLower();

            // If image has one of the openslide supported extensions,
            // try to get the downsample image using the openslide library
            // Note: At this point we can get downsample image only in case that image is supported by 'openslide'
            if (openSlideSupportedExtensions.Contains(ext))
            {
                byte[] data;
                string origImgNameNoExt = Path.GetFileNameWithoutExtension(origImgFullPath).ToLower();
                // Keep original name and add DS for the downsample image
                string fileName = origImgNameNoExt + "_DS.jpg";

                data = OpenSlide.OpenSlideGetImage(origImgFullPath);
                if (data != null)
                {
                    File.WriteAllBytes(Path.Combine(destPath, fileName), data);
                    return fileName;
                }
            }

            return null;
        }

        public static string uploadAssociatedImage(string storedImagePath, string destPath, string imgName)
        {
            string ext = Path.GetExtension(storedImagePath).ToLower();
            string fileName = "" ;

            // If image has one of the openslide supported extensions,
            // try to get the image using the openslide library
            // Note: At this point we can get downsample image only in case that image is supported by 'openslide'
            if (openSlideSupportedExtensions.Contains(ext))
            {
                byte[] data;
                string origImgNameNoExt = Path.GetFileNameWithoutExtension(storedImagePath).ToLower();
                // Keep original name and add LB for label image or MC for macro image
                if(imgName == "label")
                {
                    fileName = origImgNameNoExt + "_LB.jpg";
                }
                else
                {
                    fileName = origImgNameNoExt + "_MC.jpg";
                }

                data = OpenSlide.OpenSlideGetAssociatedImage(storedImagePath, imgName);
                if (data != null)
                {
                    File.WriteAllBytes(Path.Combine(destPath, fileName), data);
                    return fileName;
                }
            }

            return null;
        }

        public static string SetFileDestPath(string fileType, SciROLabWebEntities db)
        {
            string destPath = null;
            string currentUsrName = HttpContext.Current.User.Identity.Name;
            var currentUser = db.AspNetUsers.Where(x => x.UserName == currentUsrName).ToList();
            SciROLabTeam team = Teams.GetUserTeam(db);
            string teamName = null;
            if(team != null)
            {
                teamName = team.Name;
            }
            else
            {
                teamName = "AAANoTeam";
            }

            destPath = Path.Combine(ProjectCommons.SciROLabDataImgsPath, teamName, currentUsrName);
            try
            {
                if (fileType.Equals("image"))
                {
                    destPath = Path.Combine(destPath, "Images");
                }
                else if (fileType.Equals("reference"))
                {
                    destPath = Path.Combine(destPath, "Reference");
                }
                else
                {
                    return null;
                }

                Random randomNumber = new Random();
                // Create a random folder name to use it to store the images in multiple subfolders
                string folderName = randomNumber.Next(0, 999).ToString();

                destPath = Path.Combine(destPath, folderName);

                if (!Directory.Exists(destPath))
                {
                    Directory.CreateDirectory(destPath);
                }

                return destPath;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string SetRefDestPath()
        {
            var imgDestPath = ProjectCommons.SciROLabDataRefPath;

            try
            {
                Random randomNumber = new Random();
                // Create a random folder name to use it to store the images in multiple subfolders
                string folderName = randomNumber.Next(0, 999).ToString();

                imgDestPath = Path.Combine(imgDestPath, folderName);

                if (!Directory.Exists(imgDestPath))
                {
                    Directory.CreateDirectory(imgDestPath);
                }

                return imgDestPath;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Uploads the requested file to the server
        /// </summary>
        /// <param name="file"></param>
        /// <param name="destPath"></param>
        /// <returns> The stored unique (Guid) filename </returns>
        public static string FileUpload(HttpPostedFileBase file, string destPath)
        {
            try
            {
                if (file != null)
                {
                    string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    string path = Path.Combine(destPath, fileName);

                    // Save the file.
                    file.SaveAs(path);
                    return fileName;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static string ImageToJpgBase64(string imagePath)
        {
            string imgStr = null;
            byte[] data;
            using (Image image = Image.FromFile(imagePath))
            {
                using (Bitmap bmp = new Bitmap(image.Width, image.Height))
                {
                    using (Graphics gr = Graphics.FromImage(bmp))
                    {
                        gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        gr.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                        gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        gr.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

                        Rectangle rectDestination = new Rectangle(0, 0, image.Width, image.Height);
                        gr.DrawImage(image, rectDestination, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                        using (var outMemoryStream = new MemoryStream())
                        {
                            bmp.Save(outMemoryStream, ImageFormat.Jpeg);
                            data = outMemoryStream.ToArray();
                        }
                    }
                }
            }

            imgStr = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(data));
            return imgStr;
        }

        public static bool isOpenSlideImg(string storedImagePath)
        {
            string vendor = null;

            vendor = OpenSlide.OpenslideDetectVendor(storedImagePath);
            if(vendor != null)
            {
                return true;
            }
            return false;
        }

        public static string deleteFile(string filePath)
        {
            string errMsg = null;
            try
            {
                System.IO.File.Delete(filePath);
            }
            catch (Exception ex)
            {
                errMsg = "Deleting image from the server failed - " + ex.Message;
                return errMsg;
            }

            return errMsg;
        }
    }
}