﻿using AutoMapper;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.ViewModels;
using SciROLabABW.Web.ViewModels.Groups;
using SciROLabABW.Web.ViewModels.Methods;
using SciROLabABW.Web.ViewModels.Phrases;
using SciROLabABW.Web.ViewModels.Teams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.Mappings
{
    public class ViewModelToDomainModelMapProfile : Profile
    {
        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<WBMainTableCreate_VM, WBMainTable>()
                .ForMember(dt => dt.control, options => options.Ignore())
                .ForMember(dt => dt.msgIcon, options => options.Ignore())
                .ForMember(dt => dt.msgsNum, options => options.Ignore())
                .ForMember(dt => dt.newMsgsNum, options => options.Ignore())
                .ForMember(dt => dt.WBReferencesTable, options => options.Ignore())
                .ForMember(dt => dt.storedImageName, options => options.Ignore())
                .ForMember(dt => dt.storedImagePath, options => options.Ignore())
                .ForMember(dt => dt.storedDownSampleImagePath, options => options.Ignore())
                .ForMember(dt => dt.storedLabelImagePath, options => options.Ignore())
                .ForMember(dt => dt.icc, options => options.Ignore())
                .ForMember(dt => dt.posControl, options => options.Ignore())
                .ForMember(dt => dt.negControl, options => options.Ignore())
                .ForMember(dt => dt.storedMacroImagePath, options => options.Ignore());

            Mapper.CreateMap<WBMainTableEdit_VM, WBMainTable>()
                .ForMember(dt => dt.msgIcon, options => options.Ignore())
                .ForMember(dt => dt.msgsNum, options => options.Ignore())
                .ForMember(dt => dt.newMsgsNum, options => options.Ignore())
                .ForMember(dt => dt.icc, options => options.Ignore())
                .ForMember(dt => dt.WBReferencesTable, options => options.Ignore());

            // TODO: this should be here only if i'm going to edit from main view.
            Mapper.CreateMap<WBMainTableIndex_VM, WBMainTable>()
                .ForMember(dt => dt.imageType, options => options.Ignore())
                .ForMember(dt => dt.msgIcon, options => options.Ignore())
                .ForMember(dt => dt.msgsNum, options => options.Ignore())
                .ForMember(dt => dt.newMsgsNum, options => options.Ignore())
                .ForMember(dt => dt.storedDownSampleImagePath, options => options.Ignore())
                .ForMember(dt => dt.storedLabelImagePath, options => options.Ignore())
                .ForMember(dt => dt.icc, options => options.Ignore())
                .ForMember(dt => dt.storedMacroImagePath, options => options.Ignore());

            Mapper.CreateMap<WBMainTableDelete_VM, WBMainTable>()
                .ForMember(dt => dt.msgIcon, options => options.Ignore())
                .ForMember(dt => dt.msgsNum, options => options.Ignore())
                .ForMember(dt => dt.newMsgsNum, options => options.Ignore())
                .ForMember(dt => dt.WBReferencesTable, options => options.Ignore())
                .ForMember(dt => dt.storedImageName, options => options.Ignore())
                .ForMember(dt => dt.storedImagePath, options => options.Ignore())
                .ForMember(dt => dt.storedDownSampleImagePath, options => options.Ignore())
                .ForMember(dt => dt.storedLabelImagePath, options => options.Ignore())
                .ForMember(dt => dt.icc, options => options.Ignore())
                .ForMember(dt => dt.storedMacroImagePath, options => options.Ignore());

            Mapper.CreateMap<TeamCreate_VM, SciROLabTeam>()
                .ForMember(dt => dt.ActualUsersNum, options => options.Ignore())
                .ForMember(dt => dt.MaxUsers, options => options.Ignore());

            Mapper.CreateMap<TeamManage_VM, SciROLabTeam>()
                .ForMember(dt => dt.ActualUsersNum, options => options.Ignore())
                .ForMember(dt => dt.MaxUsers, options => options.Ignore());

            Mapper.CreateMap<GroupCreate_VM, SciROLabGroup>()
                .ForMember(dt => dt.TeamId, options => options.Ignore())
                .ForMember(dt => dt.TeamName, options => options.Ignore());

            Mapper.CreateMap<GroupManage_VM, SciROLabGroup>();

            Mapper.CreateMap<ExpMethodsIndex_VM, MethodsTable>()
                .ForMember(dt => dt.userId, options => options.Ignore());

            Mapper.CreateMap<ExpMethodsCreate_VM, MethodsTable>();

            Mapper.CreateMap<PhrasesTableIndex_VM, SciROLabPhrases>();

            Mapper.CreateMap<PhrasesTableCreate_VM, SciROLabPhrases>();

            Mapper.CreateMap<PhrasesTableEdit_VM, SciROLabPhrases>();
        }
    }
}