﻿using AutoMapper;
using SciROLabABW.Web.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mappings
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<DomainModelToViewModelMapProfile>();
                config.AddProfile<ViewModelToDomainModelMapProfile>();
            });
        }
    }
}