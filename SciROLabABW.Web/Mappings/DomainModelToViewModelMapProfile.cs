﻿using AutoMapper;
using SciROLabABW.Web.Models;
using SciROLabABW.Web.ViewModels;
using SciROLabABW.Web.ViewModels.Groups;
using SciROLabABW.Web.ViewModels.Methods;
using SciROLabABW.Web.ViewModels.Teams;
using SciROLabABW.Web.ViewModels.Phrases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.Mappings
{
    public class DomainModelToViewModelMapProfile : Profile
    {
        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<WBMainTable, WBMainTableCreate_VM>()
                .ForMember(dt => dt.SelectedGroup, options => options.Ignore())
                .ForMember(dt => dt.Groups, options => options.Ignore())
                .ForMember(dt => dt.MethodsList, options => options.Ignore())
                .ForMember(dt => dt.SelectedMethod, options => options.Ignore())
                .ForMember(dt => dt.file, options => options.Ignore());

            Mapper.CreateMap<WBMainTable, WBMainTableEdit_VM>()
                .ForMember(dt => dt.MethodsList, options => options.Ignore())
                .ForMember(dt => dt.SelectedMethod, options => options.Ignore())
                .ForMember(dt => dt.previousUrl, options => options.Ignore());

            Mapper.CreateMap<WBMainTable, WBMainTableIndex_VM>();

            Mapper.CreateMap<WBMainTable, WBMainTableDetails_VM>();

            Mapper.CreateMap<WBMainTable, WBMainTableDelete_VM>();

            Mapper.CreateMap<SciROLabTeam, TeamCreate_VM>();

            Mapper.CreateMap<SciROLabTeam, TeamManage_VM>();

            Mapper.CreateMap<SciROLabGroup, GroupCreate_VM>();

            Mapper.CreateMap<SciROLabGroup, GroupManage_VM>();

            Mapper.CreateMap<MethodsTable, ExpMethodsIndex_VM>()
                .ForMember(dt => dt.experimentID, options => options.Ignore())
                .ForMember(dt => dt.experimentDate, options => options.Ignore())
                .ForMember(dt => dt.experimentInfo, options => options.Ignore())
                .ForMember(dt => dt.performer, options => options.Ignore())
                .ForMember(dt => dt.comments, options => options.Ignore());

            Mapper.CreateMap<MethodsTable, ExpMethodsCreate_VM>()
                .ForMember(dt => dt.experimentID, options => options.Ignore())
                .ForMember(dt => dt.experimentDate, options => options.Ignore())
                .ForMember(dt => dt.experimentInfo, options => options.Ignore())
                .ForMember(dt => dt.performer, options => options.Ignore())
                .ForMember(dt => dt.comments, options => options.Ignore());

            Mapper.CreateMap<SciROLabPhrases, PhrasesTableIndex_VM>();

            Mapper.CreateMap<SciROLabPhrases, PhrasesTableCreate_VM>();

            Mapper.CreateMap<SciROLabPhrases, PhrasesTableEdit_VM>();
        }
    }
}