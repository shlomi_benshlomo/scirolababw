﻿var imgOrigWidth;
var imgOrigHeight;
var ctrlOrigWidth;
var ctrlOrigHeight;
var imgBoxPos;
var imgBoxRight;
var imgBoxBottom;
var experimentMethod;

$(function () {
    displayImage();

    $("#image").on("load", function () {
        imgOrigWidth = $("#image").width();
        imgOrigHeight = $("#image").height();
        setImageView(imgOrigWidth, imgOrigHeight);
        setImagePage();
    });

    $("#ctrlImage").on("load", function () {
        ctrlOrigWidth = $('#ctrlImage').width();
        ctrlOrigHeight = $('#ctrlImage').height();
        // Set width and height of the ctrlImgBox based
        // on the initial size of the ctrlImage.
        $("#ctrlImgBox").width($('#ctrlImage').width());
        $("#ctrlImgBox").height($('#ctrlImage').height());
    });

    $("#ctrlImgBox").on("mousemove", function (evt) {
        evt.preventDefault();
    });

    $(document).on('change', '#showPosCtrlCheckBox', function (evt) {
        document.getElementById("showNegCtrlCheckBox").checked = false;
        var ctrlCheckBox = document.getElementById("showPosCtrlCheckBox");
        var url = $('#showPosCtrlCheckBox').data('url');
        var id = $('#showPosCtrlCheckBox').data('id');
        displayImnstControl(ctrlCheckBox, "positive", url, id);
    });

    $(document).on('change', '#showNegCtrlCheckBox', function (evt) {
        document.getElementById("showPosCtrlCheckBox").checked = false;
        var ctrlCheckBox = document.getElementById("showNegCtrlCheckBox");
        var url = $('#showNegCtrlCheckBox').data('url');
        var id = $('#showNegCtrlCheckBox').data('id');
        displayImnstControl(ctrlCheckBox, "negative", url, id);
    });

    $(document).on('click', '#imgZoomOutButton', function (evt) {
        imgViewZoom(0.9);
    });

    $(document).on('click', '#imgZoomInButton', function (evt) {
        imgViewZoom(1.1);
    });

    $(document).on('click', '#imgZoomResetButton', function (evt) {
        imgViewReset();
    });

    $(document).on('change', '#imgZoomModelList', function (evt) {
        imgViewReset();
    });

    $(document).on('change', '#showCtrlCheckBox', function (evt) {
        displayControl();
    });

    $(document).on('change', '#showLabelCheckBox', function (evt) {
        displayLabel();
    });

    $(document).on('change', '#showMacroCheckBox', function (evt) {
        displayMacro();
    });
});

function changeImageViewDisplay() {
    displayImage();
    setImageView(imgOrigWidth, imgOrigHeight);
    setImagePage();
}

function setImagePage() {
    displayLabel();
    displayMacro();
    if (experimentMethod === "Immunostaining") {
        $("#showPosCtrlCheckBox").prop('checked', false);
        $("#showNegCtrlCheckBox").prop('checked', false);
        $("#showImnstCtrlCheckBoxDiv").show();
    }
    else if (experimentMethod === "Western Blot") {
        $("#showCtrlCheckBox").prop('checked', false);
        $("#showCtrlCheckBoxDiv").show();
    }
}

function displayImage()
{
    var url = $('#imgNameTextBox').data('url');
    var id = $('#imgNameTextBox').data('id');
    var dataPost = { id: id };
    // Save experiment method for later use
    experimentMethod = $('#imgNameTextBox').data('method');

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: dataPost,
        success: function (data) {
            $("#spinnerImage").hide();
            if (data.Image == null) {
                alert("Invalid image content")
            }
            else if (data != '') {
                var img = document.getElementById('image');
                img.setAttribute('src', data.Image);
                img.setAttribute('alt', "");
            }
            else {
                alert("Can't find image to display")
            }
        },
        error: function (xhr) {
            $("#spinnerImage").hide();
            err = err = xhr.responseJSON;
            alert("Error! " + err);
        },
    })
}

function setImageView(imgWidth, imgHeight) {
    var imgBox = $("#imgBox");
    var image = $("#image");
    var imgDraggable = false;
    var ImgMouseDeltaX = 0;
    var ImgMouseDeltaY = 0;
    var imgPos = 0;
    var ctrlImgBox = $("#ctrlImgBox");

    // Set width and height of the imgBox based
    // on the initial size of the image.
    imgBox.width(imgWidth);
    imgBox.height(imgHeight);

    // Calc the positions of the right side and the bottom side of the imgBox
    imgBoxPos = imgBox.offset();
    imgBoxRight = imgBoxPos.left + imgBox.width();
    imgBoxBottom = imgBoxPos.top + imgBox.height();

    $(document).mouseup(
    function (evt) {
        imgDraggable = false; // Image is not draggable if mouse is up
    });

    imgBox.on("mouseover", function (evt) {
        // In case of zoom mode show the cursor as a pointer since user can grab and move the image around, otherwise display a default cursor.
        $("#imgZoomModelList").val() == "zoom" ? $('#imgBox').css('cursor', 'pointer') : $('#imgBox').css('cursor', 'default');
    });

    imgBox.on("mousedown", function (evt) {
        evt.preventDefault();

        // Image is draggable in Zoom mode and if mouse is down and inside the imgBox area
        $("#imgZoomModelList").val() == "zoom" ? imgDraggable = true : imgDraggable = false;

        // Calc the position of the upper left corner of the image relatively to the cursor
        imgPos = image.offset();
        ImgMouseDeltaX = evt.pageX - imgPos.left;
        ImgMouseDeltaY = evt.pageY - imgPos.top;
    });

    ctrlImgBox.on("mousedown", function (evt) {
        evt.preventDefault();
    });

    imgBox.on("mousemove", function (evt) {
        evt.preventDefault();
        if (imgDraggable == false) {
            return;
        }
        // Get image's current position
        imgPos = image.offset();
        // Calc image's new position according to the new position of the mouse
        var imgX = evt.pageX - ImgMouseDeltaX;
        var imgY = evt.pageY - ImgMouseDeltaY;

        imgBoxPos = imgBox.offset();
        // Calc the positions of the right side and the bottom side of the imgBox
        imgBoxRight = imgBoxPos.left + imgBox.width();
        imgBoxBottom = imgBoxPos.top + imgBox.height();

        // Prevent image's corners from scrolling into the imgBox area
        imgX = Math.min(imgX, imgBoxPos.left);
        imgY = Math.min(imgY, imgBoxPos.top);
        imgX = Math.max(imgX, (imgBoxRight - image.width()));
        imgY = Math.max(imgY, (imgBoxBottom - image.height()));

        // Finally set image at the new position
        image.offset({ top: imgY, left: imgX })
    });    
}

function displayImnstControl(ctrlCheckBox, ctrlType, url, id) {
    var val = ctrlCheckBox.checked;
    // If check box is checked
    if (val == true) {
        imgViewReset();
        var ctrlName = document.getElementById('ctrlName');

        var dataPost = { id: id, ctrltype: ctrlType };

        // Populate image only once on the first request. 
        // If control was already found and added to the view there is no need to do it again.
        //if (($('#ctrlImage').attr('src') === undefined) || ($('#ctrlImage').attr('src') === '')) { // It doesn't work well in case of more than one control that user can switch between them
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: dataPost,
            success: function (data) {
                $("#spinnerCtrl").hide();
                $('#ctrlImgDiv').show();
                if (data.Image == null) {
                    alert("Invalid control content")
                }
                else if (data != '') {
                    var ctrlImg = document.getElementById('ctrlImage');
                    ctrlImg.setAttribute('src', data.Image);
                    ctrlImg.setAttribute('alt', "");
                    ctrlName.value = data.Name;
                    ctrlName.setAttribute('title', data.Name);
                }
                else {
                    $('#ctrlImgDiv').hide();
                    ctrlCheckBox.checked = false;
                }
            },
            error: function (xhr) {
                err = xhr.responseJSON;
                alert(err);
                $('#ctrlImgDiv').hide();
                ctrlCheckBox.checked = false;
            },
        })
        //}
    }
        // If check box is unchecked
    else {
        $('#ctrlImgDiv').hide();
    }
}

function displayControl() {
    var val = document.getElementById("showCtrlCheckBox").checked;
    // If check box is checked
    if (val == true) {
        var ctrlName = document.getElementById('ctrlName');
        var url = $('#showCtrlCheckBox').data('url');
        var id = $('#showCtrlCheckBox').data('id');
        var dataPost = { id: id };

        $('#ctrlImgDiv').show();
        // Populate image only once on the first request. 
        // If control was already found and added to the view there is no need to do it again.
        if (($('#ctrlImage').attr('src') === undefined) || ($('#ctrlImage').attr('src') === '')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: "json",
                data: dataPost,
                success: function (data) {
                    $("#spinnerCtrl").hide();
                    if (data.Image == null) {
                        alert("Invalid control content")
                    }
                    else if (data != '') {
                        var ctrlImg = document.getElementById('ctrlImage');                        
                        ctrlImg.setAttribute('src', data.Image);
                        ctrlImg.setAttribute('alt', "");
                        ctrlName.value = data.Name;
                        ctrlName.setAttribute('title', data.Name);                        
                    }
                    else {
                        $('#ctrlImgDiv').hide();
                        $("#showCtrlCheckBox").prop("checked", false);
                    }
                },
                error: function (xhr) {
                    err = xhr.responseJSON;
                    alert(err);
                    $('#ctrlImgDiv').hide();
                    $("#showCtrlCheckBox").prop("checked", false);
                },
            })
        }
    }
    // If check box is unchecked
    else {
        $('#ctrlImgDiv').hide();
    }
}

function imgViewZoom(zm) {
    var image = $("#image");
    var imgBox = $("#imgBox");

    // If 'zoom' mode, change only image size inside a fixed imgBox
    if (document.getElementById("imgZoomModelList").value == "zoom") {
        imgZoom(zm, imgBox, image);
    }
    else {
        // If 'resize' mode, change the size of the image and its box 
        // and if control is displayed change the size of the control and its box
        imgResize(zm, imgBox, image, imgOrigWidth, imgOrigHeight);
        if ($('#ctrlImgDiv').css('display') !== 'none') {
            imgResize(zm, $("#ctrlImgBox"), $("#ctrlImage"), ctrlOrigWidth, ctrlOrigHeight);
        }
    }
}

function imgViewReset() {
    var image = $("#image");
    var imgBox = $("#imgBox");

    image.width(imgOrigWidth);
    image.height(imgOrigHeight);
    imgBox.width(imgOrigWidth);
    imgBox.height(imgOrigHeight);

    // Get control's current position
    imgBoxPos = imgBox.offset();
    image.offset({ top: imgBoxPos.top, left: imgBoxPos.left });

    $("#ctrlImage").width(ctrlOrigWidth);
    $("#ctrlImage").height(ctrlOrigHeight);
    $("#ctrlImgBox").width(ctrlOrigWidth);
    $("#ctrlImgBox").height(ctrlOrigHeight);
}

function displayLabel() {
    var val = document.getElementById("showLabelCheckBox").checked;
    // If check box is checked
    if (val == true) {
        var url = $('#showLabelCheckBox').data('url');
        var id = $("#imgNameTextBox").data('id');
        var dataPost = { id: id };

        $('#labelDiv').show();
        // Populate label only once on the first request. 
        // If label was already found and added to the view there is no need to do it again.
        if (($('#labelImage').attr('src') === undefined) || ($('#labelImage').attr('src') === '')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: "json",
                data: dataPost,
                success: function (data) {
                    $("#spinnerLabel").hide();
                    if (data.Image == null) {
                        $('#labelDiv').hide();
                        $("#showLabelCheckBox").prop("checked", false);
                    }
                    else if (data != '') {
                        var labelImg = document.getElementById('labelImage');
                        labelImg.setAttribute('src', data.Image);
                        labelImg.setAttribute('alt', "");
                        $("#showLabelCheckBox").prop("checked", true);
                    }
                    else {
                        $('#labelDiv').hide();
                        $("#showLabelCheckBox").prop("checked", false);
                    }
                },
                error: function (xhr) {
                    err = xhr.responseJSON;
                    alert(err);
                    $('#labelDiv').hide();
                    $("#showLabelCheckBox").prop("checked", false);
                },
            })
        }
    }
        // If check box is unchecked
    else {
        $('#labelDiv').hide();
    }
}

function displayMacro() {
    var val = document.getElementById("showMacroCheckBox").checked;
    // If check box is checked
    if (val == true) {
        var macroImage = $('#macroImage');
        var url = $('#showMacroCheckBox').data('url');
        var id = $("#imgNameTextBox").data('id');
        var dataPost = { id: id };

        $('#macroDiv').show();
        // Populate macro only once on the first request. 
        // If macro was already found and added to the view there is no need to do it again.
        if (($('#macroImage').attr('src') === undefined) || ($('#macroImage').attr('src') === '')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: "json",
                data: dataPost,
                success: function (data) {
                    $("#spinnerMacro").hide();
                    if (data.Image == null) {
                        $('#macroDiv').hide();
                        $("#showMacroCheckBox").prop("checked", false);
                    }
                    else if (data != '') {
                        var macroImg = document.getElementById('macroImage');
                        macroImg.setAttribute('src', data.Image);
                        macroImg.setAttribute('alt', "");
                        $("#showMacroCheckBox").prop("checked", true);
                    }
                    else {
                        $('#macroDiv').hide();
                        $("#showMacroCheckBox").prop("checked", false);
                    }
                },
                error: function (xhr) {
                    err = xhr.responseJSON;
                    alert(err);
                    $('#macroDiv').hide();
                    $("#showMacroCheckBox").prop("checked", false);
                },
            })
        }
    }
        // If check box is unchecked
    else {
        $('#macroDiv').hide();
    }
}