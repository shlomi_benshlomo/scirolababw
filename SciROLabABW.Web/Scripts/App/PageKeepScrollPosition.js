﻿$(function () {
    var currentLoc = document.URL;

    var historyBack = localStorage['historyBack'];
    var url = localStorage['lastViewURL'];
    var position = localStorage['lastViewPos'];
    if (historyBack) {
        if (url === currentLoc) {
            $(document).scrollTop(position);
        }
    }

    localStorage.removeItem('historyBack');
    localStorage.removeItem('lastViewURL');
    localStorage.removeItem('lastViewPos');
});

$(document).on('click', '.keepMainViewLocation', function (evt) {
    localStorage['lastViewURL'] = document.URL;
    localStorage['lastViewPos'] = $(document).scrollTop();
});
