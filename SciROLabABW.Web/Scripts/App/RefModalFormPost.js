﻿$(function () {
    // Using jquery.form to POST references
    var options = {
        type: "POST",
        beforeSubmit: beforeSubmit,
        success: function (result) {
            if (result.isRedirect)
                window.location = result.redirectUrl;
        },
        error: function (xhr) {
            if (xhr.status == 403) {
                err = "You do not have permission to access this page.";
            }
            else
            {
                err = xhr.responseJSON;
            }
            alert(err);
        },
        complete: function () {
            $('#refModal').modal('hide');
            HideProgressModal();
        }
    };

    // Bind form using 'ajaxForm'
    $('#postForm').ajaxForm(options);

    $('input').keydown(function (e) {
        if (e.keyCode == 13) {
            $("input[value='OK']").focus().click();
            return false;
        }
    });
});

// Pre-submit callback 
function beforeSubmit(formData, jqForm, options) {
    ShowProgressModal();
    document.getElementById('btnReferencesSubmit').focus()
    return true;
}
