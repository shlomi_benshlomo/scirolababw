﻿$(function () {
    $(document).on('keyup keypress', 'form input[type="checkbox"]', function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    });
    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    });

});