﻿ var isDirty = false;
    $(document).ready(function(){
        $(":input").change(function (evt) {
            // In some cases the input is not really part of the data like in case of a search box.
            // In this cases we don't want to change isDirty to true otherwise it will trigger the leave validation message on unload of the page
            // only because the text in the text box was changed
            if (evt.target.classList.contains("skipLeavePageValid")) {
                return;
            }

            if (!isDirty) {
                isDirty = true;
            }
        });

        $(window).on('beforeunload', function() {
            if(isDirty){ 
                return 'You have unsaved changes!\nIf you choose to leave this page your changes will be lost!';
            } 
        });

        $(".submitBtn").on("click", function () {
            isDirty = false;
        });
    });