﻿$(function () {
    var array = [];
    var primaryAntibodyId;

    // Find all the rows where imnstCtrlImgCheckBox are checked
    $("Input[data-name='imnstCtrlImgCheckBox']:checked").each(function () {
        array.push($(this).data('id'));
    })
    if ((array) && (array.length > 0)) {
        // Set to readOnly all primaryAntibodyId in rows where imnstCtrlImgCheckBox are checked
        for (var i = 0; i < array.length; i++) {
            primaryAntibodyId = document.getElementById("primaryAntibodyId" + array[i]);
            primaryAntibodyId.readOnly = true;
        }
    }
});

function ImnstSetCtrlImg(val) {
    var url = $(val).data('url');
    var id = $(val).data('id');
    var ctrlType = $(val).data('ctrltype');
    var primaryAntibodyId = document.getElementById("primaryAntibodyId" + id);

    setImnstCtrlImg(ctrlType, url, id, primaryAntibodyId)
}

function setImnstCtrlImg(ctrlType, url, id, primaryAntibodyId) {
    if (ctrlType === "positive") {
        var currentCheckBox = document.getElementById("pos" + id);
        var otherCheckBox = document.getElementById("neg" + id);
    }
    else {
        var currentCheckBox = document.getElementById("neg" + id);
        var otherCheckBox = document.getElementById("pos" + id);
    }

    // If checkbox was unchecked change primaryAntibodyId textbox readonly state to false, no need to do anything else
    if (!currentCheckBox.checked) {
        primaryAntibodyId.readOnly = false;
        return;
    }

    // Do as much as possible validation on the client side
    if (!primaryAntibodyId.value) {
        alert("Can't create a control without Primary Antibody Id.");
        if (currentCheckBox.checked === true)
        {
            currentCheckBox.checked = false;
        }
        return;
    }

    if (otherCheckBox.checked === true) {
        alert("Row can't have more then one type of control.");
        currentCheckBox.checked = false;
        return;
    }

    // Prevent user from adding an existing primaryAntibodyId for the same type of control image more than once
    var count = 0;
    var rowIdArray = [];
    
    // Find all ids of all the selected rows from the same control type (current row is included)
    if (ctrlType === 'positive') {
        $("Input[data-ctrltype='positive']:checked").each(function () {
            rowIdArray.push($(this).data('id'));
        })
    }
    else {
        $("Input[data-ctrltype='negative']:checked").each(function () {
            rowIdArray.push($(this).data('id'));
        })
    }

    // Iterate over the selected rows and check if the number inserted by the user
    // into the current primaryAntibodyId already exist in another row
    if ((rowIdArray) && (rowIdArray.length > 0)) {
        for (var i = 0; i < rowIdArray.length; i++) {
            var PrimABId = document.getElementById("primaryAntibodyId" + rowIdArray[i]);
            if ((PrimABId.id !== primaryAntibodyId.id) && (PrimABId.value === primaryAntibodyId.value)) {
                count++
            }
        }
    }

    // If count is bigger than 0 it means that the number in primaryAntibodyId already exist. 
    // Print a message and cancel the checking of the checkbox
    if (count > 0) {
        alert("There is already " + ctrlType + " control with the same Primary Antibody Id.\n" +
              "Only one control from each type (positive or negative) is allowed for each Primary Antibody Id.")
        currentCheckBox.checked = false;
        return;
    }

    // Do the rest of the validation on the server side    
    // Send rowId and the current primaryAntibodyId (the one on the page since the user may have changed it) to the server for the validation
    var dataPost = { id: id, primaryantibodyvalue: primaryAntibodyId.value, ctrltype: ctrlType };
    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        dataType: "json",
        data: dataPost,
        success: function () {
            // If everything went well change the check box status to checked and 
            // change primaryAntibodyId textbox to read only to prevent changing the value when it is defined as a control image row
            currentCheckBox.checked = true;
            primaryAntibodyId.readOnly = true;
        },
        error: function (xhr) {
            if (currentCheckBox.checked === true) {
                currentCheckBox.checked = false;
            }
            // Print error message we got from the server
            err = xhr.responseJSON;
            if (err.message != undefined) {
                alert(err.message);
            }
            else {
                alert(err);
            }
        },
    });
}
