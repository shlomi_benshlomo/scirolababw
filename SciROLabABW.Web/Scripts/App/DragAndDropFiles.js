﻿$(function () {
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);
    var statusMessage = document.getElementById('statusText');
    var statusErrorMessage = document.getElementById('statusError');
    var uploadCanceled = false;

    Dropzone.options.dropzoneForm = {
        maxFilesize: 2000, // Allow uploads of files up to 2GB
        maxFiles: null,
        parallelUploads: 3,
        createImageThumbnails: true, // Display thumbnails
        previewTemplate: previewTemplate,
        dictDefaultMessage: "Click on the Add button or drop files here to upload.",
        clickable: "#addFiles",
        init: function () {
            var addFileZone = this; // Closure

            // Init dropzone controllers
            dropZoneInitView();

            addFileZone.on("addedfile", function (file) {
                // Client side validation
                // Note:
                // At this point it is not clear how to prevent the call to the server from here. 
                // Therefore there is no point to perform a client side validation.  
            });

            addFileZone.on("totaluploadprogress", function (progress) {
                // Update the total progress bar
                document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
            });

            addFileZone.on("sending", function (file) {
                dropZoneProccessingSetView();
            });

            // Called when all files in the queue finish uploading
            addFileZone.on("queuecomplete", function (progress) {
                if (uploadCanceled === true) {
                    dropZoneInitView();
                    uploadCanceled = false;
                }
                else {
                    // Note: for some reason this event is also triggered if there is an error in the first file, 
                    // so we need to check if upload was really completed here before setting the view
                    if (isUploadCompleted()) {
                        dropZoneUploadCompletedSetView();
                    }
                }
            });

            addFileZone.on("success", function (file) {
                file.previewElement.querySelector(".progress-bar").style.background = "green";
            });

            addFileZone.on("error", function (file) {
                file.previewElement.querySelector(".progress-bar").style.width = "100%"
                file.previewElement.querySelector(".progress-bar").style.background = "red";
            });

            $("button#cancelAll").on("click", function () {
                if (!confirm("Are you sure want to cancel upload?")) {
                    return false; // cancel the submit if the user clicked the Cancel button
                }
                // Remove all files (uploaded or not) from the list
                addFileZone.removeAllFiles(true);
                uploadCanceled = true;
            });

            $("button#delAll").on("click", function () {
                // Remove all files other than the uploading one (uploaded or not) from the list
                addFileZone.removeAllFiles();
                statusMessage.textContent = "";
                statusErrorMessage.textContent = "";
            });

            $(".backActionLink").on("click", function (e) {
                if (isUploadCompleted()) {
                    return;
                }
                if (!confirm("This will cancel the upload! Are you sure want to continue?")) {
                    e.preventDefault();
                }
            });

            function dropZoneInitView() {
                $("button#delAll").hide();
                $("button#cancelAll").hide();
                $("#total-progress-container").hide();
                document.querySelector("#total-progress .progress-bar").style.width = "0%";
                statusMessage.textContent = "";
                statusErrorMessage.textContent = "";
            }

            function dropZoneProccessingSetView() {
                $("button#delAll").hide();
                $("button#cancelAll").show();
                $("#total-progress-container").show();
                statusErrorMessage.textContent = "";
                statusMessage.textContent = "Uploading data to the server! Please wait...";
            }

            function dropZoneUploadCompletedSetView() {
                var errorsCount = addFileZone.getFilesWithStatus(Dropzone.ERROR).length;

                $("button#cancelAll").hide();
                $("button#delAll").show();
                $("#total-progress-container").hide();
                document.querySelector("#total-progress .progress-bar").style.width = "0%";

                if (errorsCount === 0) {
                    statusErrorMessage.textContent = "";
                    statusMessage.textContent = "Upload completed: No errors found!!!";
                }
                else {
                    statusMessage.textContent = "";
                    statusErrorMessage.textContent = "Upload completed: " + errorsCount + " errors found!!! Please review the list.";
                }
            }

            function isUploadCompleted() {
                var queuedCount = addFileZone.getFilesWithStatus(Dropzone.QUEUED).length;
                var uploadingCount = addFileZone.getFilesWithStatus(Dropzone.UPLOADING).length;

                if ((queuedCount === 0) && (uploadingCount === 0)) {
                    return true;
                }

                return false;
            }
        }
    };
});