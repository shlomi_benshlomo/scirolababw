﻿$(function () {
    $(".datePicker").datepicker({
        dateFormat: "mm/dd/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-30:+10"
    });
});

function setDatePicker() {
    $('.datePicker').datepicker({
        //...
    });
}