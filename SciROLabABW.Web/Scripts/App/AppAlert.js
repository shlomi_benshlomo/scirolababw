﻿function alertModal(title, message, refreshView) {
    $("<pre></pre>").dialog({
        buttons: { "Ok": function () { $(this).dialog("close"); } },
        close: function (event, ui) { $(this).remove(); },
        resizable: false,
        title: title,
        modal: true,
        height: 700,
        width: 700,
        dialogClass: 'errorMsgDialog',
        close: function () {
            if (refreshView == true) {
                window.location = window.location.href; // Refresh the view.
            }
        }
    }).text(message);
}
