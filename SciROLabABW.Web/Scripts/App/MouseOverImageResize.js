﻿$(function () {
    var img = document.getElementById("appImageId");
    if (img == null) {
        return;
    }
    width = img.width
    height = img.height
    $(".resizedImageCl").mouseover(function () {
        $(this).animate({ height: '+=50%', width: '+=50%' }, 'slow');
    });
    $(".resizedImageCl").mouseout(function () {
        $(this).animate({ height: '-=50%', width: '-=50%' }, 'slow');
    });
});