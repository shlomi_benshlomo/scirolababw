﻿$(function () {
    $('#experimentMethodEditView').change(function () {
        var url = $('#experimentMethodEditView').data('url');
        var id = $('#experimentMethodEditView').data('id');
        var selectedMethodId = $(this).val();
        if (selectedMethodId === undefined)
        {
            return;
        }
        var dataPost = { id: id, selectedmethodid: selectedMethodId };
        $.ajax({
            url: url,
            type: 'GET',
            data: dataPost,
            success: function (data) {
                if (data.result == false)
                {
                    alert("Invalid experiment method.");
                    return;
                }
                $('#expMethodEditPartialView').html(data);
                $('#expMethodEditPartialView').fadeIn('fast');
                setDatePicker(); // Enable datepicker script
            },
            error: function (xhr) {
                err = xhr.statusText;
                alert(err);
            },
        })
    });

    $('#experimentMethodCreateView').change(function () {
        var url = $('#experimentMethodCreateView').data('url');
        var selectedMethodId = $(this).val();
        if (selectedMethodId === undefined) {
            return;
        }
        var dataPost = { selectedmethodid: selectedMethodId };
        $.ajax({
            url: url,
            type: 'GET',
            data: dataPost,
            success: function (data) {
                if (data.ok == false) {
                    alert("Invalid experiment method.");
                    return;
                }
                $('#expMethodCreatePartialView').html(data);
                $('#expMethodCreatePartialView').fadeIn('fast');
                if (data.method == "no method") {
                    $("#MethodHelper").hide();
                }
                else {
                    $("#MethodHelper").show();
                }
                setDatePicker(); // Enable datepicker script
            },
            error: function (xhr) {
                err = xhr.statusText;
                alert(err);
            },
        })
    });
});

function methodSelect(val) {
    var url = $(val).data('url');
    var id = $(val).data('id');
    var selectedMethodId = "Immunostaining";
    if (selectedMethodId === undefined) {
        return;
    }
    var dataPost = { id: id, selectedmethodid: selectedMethodId };

    $.ajax({
        url: url,
        type: 'GET',
        data: dataPost,
        success: function (data) {
            if (data.result == false) {
                alert("Invalid experiment method.");
                return;
            }
            $("#" + id).load(url);
            $("#" + id).html(data);
            $("#" + id).fadeIn('fast');
            setDatePicker(); // Enable datepicker script
        },
        error: function (xhr) {
            err = xhr.statusText;
            alert(err);
        },
    })
}