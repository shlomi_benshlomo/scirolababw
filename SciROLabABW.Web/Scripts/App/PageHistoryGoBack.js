﻿$(document).on('click', ".mainListGoBack", function (evt) {
    evt.preventDefault();

    var url = localStorage['lastViewURL'];
    if (url) {
        window.location = url;
        localStorage['historyBack'] = 1;
    }
});

$(document).on('click', ".historyGoBack", function (evt) {
    evt.preventDefault();
    history.go(-1);
});

$(document).on('click', ".submitBtnHistoryBack", function (evt) {
    localStorage['historyBack'] = 1;
});