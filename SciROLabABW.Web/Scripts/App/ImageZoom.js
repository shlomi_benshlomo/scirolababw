﻿function imgZoom(zm, imgBox, image) {
    var imgPos = image.offset();
    var imgBoxPos = imgBox.offset();
    var prevImgWidth = image.width();
    var prevImgHeight = image.height();

    // Calc the positions of the right side and the bottom side of the imgBox
    imgBoxRight = imgBoxPos.left + imgBox.width();
    imgBoxBottom = imgBoxPos.top + imgBox.height();

    // zm = 0 => reset image size
    var newImgWidth = zm == 0 ? imgOrigWidth : (prevImgWidth * zm);
    var newImgHeight = zm == 0 ? imgOrigHeight : (prevImgHeight * zm);

    // Minimum image size = original size
    newImgWidth = Math.max(newImgWidth, imgOrigWidth);
    newImgHeight = Math.max(newImgHeight, imgOrigHeight);

    // Maximum image size = 5 times original size
    newImgWidth = Math.min(newImgWidth, imgOrigWidth * 5);
    newImgHeight = Math.min(newImgHeight, imgOrigHeight * 5);
    // Calc image's new position - keep image in center
    var imgX = imgPos.left - ((newImgWidth - prevImgWidth) / 2);
    var imgY = imgPos.top - ((newImgHeight - prevImgHeight) / 2);

    // Prevent image's corners from scrolling into the imgBox area
    imgX = Math.min(imgX, imgBoxPos.left);
    imgY = Math.min(imgY, imgBoxPos.top);
    imgX = Math.max(imgX, imgBoxRight - newImgWidth);
    imgY = Math.max(imgY, imgBoxBottom - newImgHeight);

    // Finally set image's new size and position
    image.width(newImgWidth);
    image.height(newImgHeight);
    image.offset({ top: imgY, left: imgX });
}

function imgResize(zm, imgBox, image, imgOrigWidth, imgOrigHeight) {
    // zm = 0 => reset image size
    var newWidth = zm == 0 ? imgOrigWidth : (image.width() * zm);
    var newHeight = zm == 0 ? imgOrigHeight : (image.height() * zm);

    // Minimum image size = original size
    newWidth = Math.max(newWidth, imgOrigWidth);
    newHeight = Math.max(newHeight, imgOrigHeight);

    // Maximum image size = 5 times original size
    newWidth = Math.min(newWidth, imgOrigWidth * 5);
    newHeight = Math.min(newHeight, imgOrigHeight * 5);

    // Set image's new size and position
    image.width(newWidth);
    image.height(newHeight);

    // Set width and height of the imgBox based
    // on the size of the image.
    imgBox.width(image.width());
    imgBox.height(image.height());
}