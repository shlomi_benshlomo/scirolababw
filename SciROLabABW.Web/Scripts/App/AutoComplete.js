﻿$(function () {
    autoComplete();

    $('#experimentMethodCreateView').change(function () {
        autoComplete();
    });

    function autoComplete() {
        $(".autoComplete").autocomplete({
            source: function (request, response) {
                var prefix = request.term;
                $.ajax({
                    url: "/Phrases/AutoComplete",
                    type: "GET",
                    dataType: "json",
                    cache: false,
                    data: { prefix: prefix },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return { label: item.Phrase, value: item.Phrase };
                        }))
                    }
                })
            },
        });
    }
});