﻿$(function () {
    $(document).on("click", '#buttonShare', function (e) {
        e.preventDefault();
        var array = [];
        $("Input[name='selectRow']:checked").each(function () {
            array.push($(this).val());
        })
        var url = $(this).data('url');
        if (array.length > 0) {
            var answer = confirm("You are about to share the selected rows with other users. Is that ok?")
            if (answer) {
                ShowProgressModal();
            }
            else {
                return;
            }
        }
        else {
            alert("No row has been selected!");
            return;
        }

        setSharedRows(true, url, array);
    });

    $(document).on("click", '#buttonUnshare', function (e) {
        e.preventDefault();
        var array = [];
        $("Input[name='selectRow']:checked").each(function () {
            array.push($(this).val());
        })
        var url = $(this).data('url');
        if (array.length > 0) {
            ShowProgressModal();
        }
        else {
            alert("No row has been selected!");
            return;
        }

        setSharedRows(false, url, array);
    });
});

function setSharedRows(status, url, array) {
    $.ajax({
        url: url,
        type: "POST",
        dataType: "html",
        traditional: true,
        data: { selectedrows: array, status: status },
        cache: false,
        success: function (response) {
            var results = JSON.parse(response);
            if (results != null && results.success) {
                window.location = window.location.href; // Refresh the view.
                $('#result').html();
            }
            else {
                alertModal("Error!", results.responseText, true);
            }            
        },
        error: function (response) {
            var err = JSON.parse(response.responseText);
            alert("Error! " + err.message)
        },
        complete: function () {
            HideProgressModal();
        }
    });
}