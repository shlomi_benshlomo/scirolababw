﻿$(function () {
    $(document).on("click", '#buttonEditSelected', function (e) {
        e.preventDefault();
        var array = [];
        $("Input[name='selectRow']:checked").each(function () {
            array.push($(this).val());
        })
        var url = $(this).data('url');
        if (array.length == 0) {
            alert("No row has been selected!");
            return;
        }

        editSelectedRows(url, array);
    });
});

function editSelectedRows(url, array) {
    $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        traditional: true,
        data: { selectedrows: array },
        cache: false,
        success: function (data) {
            // Populate modal with data
            $('#editSelectedRowsContent').html(data);
            $('#editSelectedRowsModal').modal({
                keyboard: true
            }, 'show');
            setDatePicker(); // Enable datepicker script
            },
        error: function (xhr) {
            if (xhr.status == 403) {
                err = "You do not have permission to access this page.";
            }
            else {
                err = xhr.responseText
            }
            alert(err);
        },
        complete: function () {
            HideProgressModal();
        }
    });
}