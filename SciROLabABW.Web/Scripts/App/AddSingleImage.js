﻿// Old code, not in use! Was replaced by 'dropzone'.
$(function () {
    $(document).on('change', '#fileNameTextBox', function (evt) {
        //document.getElementById('submitBtnId').focus() // Change focus to force error message to refresh

        /* At this point we don't display a preview of the selected image - 
          TIFF is not supported on some of the major browsers and it takes a lot of time to display a big image */
        /*if (!evt) // If event doesn't exist.
        {
            evt = window.event; // Try to use IE fallback. // TODO: write a helper function to do this instead of checking everywhere!!!
        }

        try {
            var file = evt.target.files[0] || evt.srcElement.files[0]; // Read the first and the only file from the list.
        }
        catch (e) {
            // Something went wrong or no file was chosen - remove current image and return.
            removeImg();
            return;
        }

        try {
            if (isImage(file)) { // Add only valid images
                var reader = new FileReader();
                reader.onloadend = (function () {
                    return function (event) {
                        var img = document.getElementById('img-preview-id');
                        img.src = event.target.result;
                    };
                })(file);
                reader.readAsDataURL(file); // Read in the image file as a data URL.
            }
            else {
                removeImg(); // If there is already image remove it
            }
        }
        catch (e) {
            alert(e.message+"!");
            return;
        }*/
    });
});

// Function to remove existing image
/*function removeImg() {
    var img = document.getElementById('img-preview-id');
    if (img != null) {
        img.src = "";
    }
}*/
