﻿$(function () {
    $(document).on("click", '.modifyReferences', function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        var id = $(this).data('id');

        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            data: { id: id },
            success: function (data) {
                $('#refModalContent').html(data);
                $('#refModal').modal({
                    keyboard: true
                }, 'show');
            },
            error: function (xhr) {
                if (xhr.status == 403)
                {
                    err = "You do not have permission to access this page.";
                }
                else
                {
                    err = xhr.responseJSON;
                }
                alert(err);
            },
        });
    });
});
