﻿$(function () {
    var checkAllBox = $("#selecetAll");

    checkAllBox.click(function () {
        // Get checked status
        var status = checkAllBox.prop('checked');
        toggleChecked(status);
    });

    $("Input[name='selectRow']").click(function() {
        if ($("input[name='selectRow']").length == $("Input[name='selectRow']:checked").length) {
            checkAllBox.prop("checked", true);
        }
        else {
            checkAllBox.prop("checked", false);
        }
    });
});

function toggleChecked(status) {
    $("Input[name='selectRow']").each(function () {
        // Set the status of each 'selectRow' checkbox
        // to match the status of the select all checkbox.
        $(this).prop("checked", status);
    });
}