﻿$(function () {
    // If control image check box is checked, change experimentID and membraneID textboxes to read only 
    // to prevent changing the values when it is defined as a control image
    var ctrlImgCheckBox = document.getElementById("ctrlImgCheckBox");
    var experimentID = document.getElementById("experimentID");
    var membraneID = document.getElementById("membraneID");
    if ((ctrlImgCheckBox) && (ctrlImgCheckBox.checked)) {
        if (experimentID) {
            experimentID.readOnly = true;
        }
        if (membraneID) {
            membraneID.readOnly = true;
        }
    }

    $(document).on("click", "#ctrlImgCheckBox", function (evt) {
        var url = $("#ctrlImgCheckBox").data('url');
        var id = $("#ctrlImgCheckBox").data('id');
        setWBCtrlImg(evt, url, id)
    });
});

function setWBCtrlImg(evt, url, id, checkBox) {
    var currentCheckBox = document.getElementById("ctrlImgCheckBox");

    // If checkbox was unchecked change experimentID and membraneID textbox readonly state to false, no need to do anything else
    if (!currentCheckBox.checked) {
        document.getElementById("experimentID").readOnly = false;
        document.getElementById("membraneID").readOnly = false;
        return;
    }

    // In case that user changed it to checked, prevent default and do some validation 
    evt.preventDefault();

    // Do as much as possible validation on the client side
    var experimentIDValue = document.getElementById("experimentID").value;
    var membraneIDValue = document.getElementById("membraneID").value;

    if ((!experimentIDValue) && (!membraneIDValue)) {
        alert("Can't create a control without Experiment Id and Membrane Id.");
        return;
    }
    else if (!experimentIDValue) {
        alert("Can't create a control without Experiment Id.");
        return;
    }
    else if (!membraneIDValue) {
        alert("Can't create a control without Membrane Id.");
        return;
    }

    // Do the rest of the validation on the server side    
    // Send rowId, current experimentIDValue and current membraneIDValue (the ones on the page since the user may have changed it) to the server for the validation
    var dataPost = { id: id, experimentID: experimentIDValue, membraneID: membraneIDValue };
    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        dataType: "json",
        data: dataPost,
        success: function () {
            // If everything went well change the check box status to checked and 
            // change experimentID and membraneID textboxes to read only to prevent changing the values when it is defined as a control image row
            currentCheckBox.checked = true;
            document.getElementById("experimentID").readOnly = true;
            document.getElementById("membraneID").readOnly = true;
        },
        error: function (xhr) {
            // Print error message we got from the server
            err = xhr.responseJSON;
            alert(err);
        },
    });
}