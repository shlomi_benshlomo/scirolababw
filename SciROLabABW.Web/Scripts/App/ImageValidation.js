﻿/// <reference path="../jquery.validate.js" />
/// <reference path="../jquery.validate.unobtrusive.js" />

$.validator.addMethod("imagevalidation", function (value, element, params) {

    var file = element.files[0]; // Read the first and the only file from the list.

    if (isImage(file)) {
        return true;
    }

    return false;
});

$.validator.unobtrusive.adapters.addBool("imagevalidation");
function isValidImage(file) {
    var supportedExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'tif', 'tiff'];
    var fileExtesion = file.name.split('.').pop().toLowerCase();
    var fileSize = file.size / 1024 / 1024; // Converted from bytes to MBytes

    if (!file.type.match('image.*')) {
        return false;
    }
    if (supportedExtension.indexOf(fileExtesion) == -1) {
        return false;
    }

    if (fileSize >  100) {
        return false;
    }  
  
    return true;
}

function isImage(file) {

    if (!file) {
        return false;
    }

    return isValidImage(file);
}
