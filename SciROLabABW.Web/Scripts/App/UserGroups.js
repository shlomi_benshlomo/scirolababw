﻿$(function () {
    $(document).on("click", '.getUserGrous', function (e) {
        e.preventDefault();
        var url = $(this).data('url');

        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            success: function (data) {
                $('#userGroupsContent').html(data);
                $('#userGroupsModal').modal({
                    keyboard: true
                }, 'show');
            },
            error: function (xhr) {
                if (xhr.status == 403) {
                    err = "You do not have permission to access this page.";
                }
                else {
                    err = xhr.responseJSON;
                }
                alert(err);
            },
        });
    });
});
