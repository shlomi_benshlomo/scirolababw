﻿$(function () {
    $(document).on("change", '#small', function (e) {
        var id = $(this).data('id');
        var url = $(this).data('url');
        var viewStyle = "small";

        $("Input[name='viewStyle']:checked").each(function () {
            displaySelectedRows(url, id, viewStyle);
        })
    });

    $(document).on("change", '#mid', function (e) {
        var id = $(this).data('id');
        var url = $(this).data('url');
        var viewStyle = "mid";

        $("Input[name='viewStyle']:checked").each(function () {
            displaySelectedRows(url, id, viewStyle);            
        })
    });

    $(document).on("change", '#large', function (e) {
        var id = $(this).data('id');
        var url = $(this).data('url');
        var viewStyle = "large";

        $("Input[name='viewStyle']:checked").each(function () {
            displaySelectedRows(url, id, viewStyle);
        })
    });
});

function displaySelectedRows(url, id, viewStyle) {
    var dataPost = { id: id, viewstyle: viewStyle };
    $.ajax({
        url: url,
        type: 'GET',
        data: dataPost,
        success: function (data) {
            if (data.result == false) {
                alert("Invalid experiment method.");
                return;
            }
            $('#imageViewPartialView').html(data);
            $('#imageViewPartialView').fadeIn('fast');
            $("#image")
            changeImageViewDisplay();
        },
        error: function (xhr) {
            err = xhr.statusText;
            alert(err);
        },
    })
}