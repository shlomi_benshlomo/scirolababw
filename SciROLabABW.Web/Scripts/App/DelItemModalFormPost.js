﻿$(function () {
    // Using jquery.form to POST delete item
    var options = {
        type: "POST",
        beforeSubmit: beforeSubmit,
        success: function (results) {
            if (results != null && results.success) {
                $('#refModal').modal('hide');
                HideProgressModal();
                window.location.reload(true); // Refresh the view.
            }
            else {
                alert(results.responseText);
            }            
        },
        error: function (xhr) {
            if (xhr.status == 403) {
                err = "You do not have permission to access this page.";
            }
            else {
                err = xhr.responseJSON;
            }
            alert(err);
            $('#refModal').modal('hide');
            HideProgressModal();
        },
        complete: function () {
            $('#refModal').modal('hide');
            HideProgressModal();
        }
    };

    // Bind form using 'ajaxForm'
    $('#postForm').ajaxForm(options);

    $('input').keydown(function (e) {
        if (e.keyCode == 13) {
            $("input[value='OK']").focus().click();
            return false;
        }
    });
});

// Pre-submit callback 
function beforeSubmit(formData, jqForm, options) {
    ShowProgressModal();
    document.getElementById('btnDelItemSubmit').focus()
    return true;
}
