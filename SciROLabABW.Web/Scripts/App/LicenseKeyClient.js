﻿var updatUrl = $("#updateURL").val();
var refreshUrl = $("#refreshURL").val();
var errorUrl = $("#errorURL").val();
var currentUrl = $("#currentURL").val();

// Set timeout for the next time the app should send a heartbeat (live signal) to the server
$(function (evt) {
    $.ajax(
    {
        type: "POST",
        url: updatUrl,
        success: function (response) {
            //var results = JSON.parse(response);
            if (response != null && response.success) {
                setTimeout(RefreshLicenseServer, 20000); // 20 sec timeout
            }
            else {
                var errorText = response.responseText;
                // If current url is already 'error url' don't call it again.
                if (errorUrl !== currentUrl) {
                    window.location.href = errorUrl += '?error=' + errorText;
                }
            }
        },
        error: function (xhr) {
            var errorText = xhr.responseJSON;
            // If current url is already 'error url' don't call it again.
            if (errorUrl !== currentUrl) {
                window.location.href = errorUrl += '?error=' + errorText.message;
            }
        },
        complete: function () {

        }
    });
});

function RefreshLicenseServer() {
    $.ajax(
    {
        type: "POST",
        url: refreshUrl,
        success: function (response) {
            //var results = JSON.parse(response);
            if (response != null && response.success) {
                sessionStorage.Alert = "";
            }
            else {
                var errorText = response.responseText;
                // If current url is already 'error url' don't call it again.
                if (errorUrl !== currentUrl) {
                    window.location.href = errorUrl += '?error=' + errorText;
                }
            }
        },
        error: function (xhr) {
            var errorText = xhr.responseJSON;
            // If current url is already 'error url' don't call it again.
            if (errorUrl !== currentUrl) {
                window.location.href = errorUrl += '?error=' + errorText.message;
            }
        },
        complete: function () {
            setTimeout(RefreshLicenseServer, 20000); // 20 sec timeout
        }
    });
};

