﻿var ButtonKeys = { "EnterKey": 13 };
$(function () {
    $("#MainForm").keypress(function (e) {
        // In case that 'enter' was pressed make the button with id="SubmitButton" the default button
        if (e.which == ButtonKeys.EnterKey) {
            var defaultButtonId = $(this).attr("defaultbutton");
            $("#" + defaultButtonId).click();
            return false;
        }
    });
});