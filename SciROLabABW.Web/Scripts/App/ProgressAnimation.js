﻿function ShowProgressModal() {
    $("#processingModal").fadeIn();
}

function HideProgressModal() {
    $("#processingModal").fadeOut();
}