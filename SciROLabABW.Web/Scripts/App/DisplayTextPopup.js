﻿$(function () {
    $(document).on("click", '.displayText', function (e) {
        e.preventDefault();
        var filePath = $("#file-path").val();
        var modalTitle = $("#modal-title").val();
        var url = $(this).data('url');

        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            data: { file_path: filePath, modal_title: modalTitle },
            success: function (data) {
                if (data) {
                    $('#displayTextModalContent').html(data);
                    $('#displayTextModal').modal({
                        keyboard: true
                    }, 'show');
                }
                else {
                    alert("Nothing to display!");
                }
            },
            error: function (xhr) {
                if (xhr.status == 403) {
                    err = "You do not have permission to access this page.";
                }
                else {
                    err = xhr.responseJSON;
                    alert(err.message);
                }
            },
        });
    });
});