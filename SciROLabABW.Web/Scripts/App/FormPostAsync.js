﻿// File Post Async using jquery.form
$(function () {
    var options = {
        type: "POST",
        beforeSubmit: beforeSubmit,
        success: function (result) {
            if (result.isRedirect) {
                window.location = result.redirectUrl;
            }
        },
        error: function (xhr) {
            err = xhr.responseJSON;
            alert(err);
        },
        complete: function () {
            HideProgressModal();
        }
    };
    // Bind form using 'ajaxForm'
    $('#postForm').ajaxForm(options);
});

// Pre-submit callback 
function beforeSubmit(formData, jqForm, options) {
    ShowProgressModal();
    return true;
}