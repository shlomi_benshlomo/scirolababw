﻿$(function () {
    $(document).on("click", '#buttonSelectedTileView', function (e) {
        var array = [];
        $("Input[name='selectRow']:checked").each(function () {
            array.push($(this).val());
        })
        var url = $(this).data('url');
        if (array.length == 0) {
            alert("No row has been selected!");
            return;
        }

        displaySelectedRows(url, array);
    });
});

function displaySelectedRows(url, array) {
    $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        traditional: true,
        data: { selectedrows: array },
        cache: false,
        success: function (data) {
            if (data.ok == false) {
                alert("Can't display files in Tile View.");
                return;
            }
            else {
                // Populate modal with data
                $('#tileViewSelectedRowsContent').html(data);
                $('#tileViewSelectedRowsModal').modal({
                    keyboard: true
                }, 'show');
            }
        },
        error: function (xhr) {
            if (xhr.status == 403) {
                err = "You do not have permission to access this page.";
            }
            else {
                err = xhr.responseText
            }
            alert(err);
        },
        complete: function () {
            HideProgressModal();
        }
    });
}