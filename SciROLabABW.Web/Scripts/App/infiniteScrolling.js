﻿$(function (event) {
    var pageId = 1;
    var inCallback = false;

    function loadData(url) {
        if ((pageId > 0) && (!inCallback)) {
            inCallback = true;

            $.ajax({
                type: 'GET',
                url: url,
                data: { "id": pageId },
                cache: false,
                success: function (results) {
                    if ((results != null) && (results != '')) {
                        $(".infiniteScrolling").append(results);
                        pageId++;
                    }
                    else {
                        pageId = 0;
                    }
                },
                beforeSend: function () {
                    ShowProgressModal();
                },
                complete: function () {
                    inCallback = false;
                    HideProgressModal();
                },
                error: function () {
                    alert("Error while retrieving data from the server!");
                }
            });
        }
    }

    $(window).scroll(function () {        
        var url = window.location.pathname;

        if ($(window).scrollTop() >= $(document).height() - $(window).innerHeight()-100) {
            loadData(url);
        }
    });
});