﻿$(function () {
    // If control image check box is checked, change primaryAntibodyId textbox to read only 
    // to prevent changing the value when it is defined as a control image
    var posCtrlImgCheckBox = document.getElementById("posCtrlImgCheckBox");
    var negCtrlImgCheckBox = document.getElementById("negCtrlImgCheckBox");
    var primaryAntibodyId = document.getElementById("primaryAntibodyId");
    if (((posCtrlImgCheckBox) && (posCtrlImgCheckBox.checked)) || ((negCtrlImgCheckBox) && (negCtrlImgCheckBox.checked))) {
        if (primaryAntibodyId) {
            primaryAntibodyId.readOnly = true;
        }
    }

    $(document).on("click", "#posCtrlImgCheckBox", function (evt) {
        var ctrlType = "positive";
        var url = $("#posCtrlImgCheckBox").data('url');
        var id = $("#posCtrlImgCheckBox").data('id');
        setImnstCtrlImg(evt, ctrlType, url, id)
    });

    $(document).on("click", "#negCtrlImgCheckBox", function (evt) {
        var ctrlType = "negative";
        var url = $("#negCtrlImgCheckBox").data('url');
        var id = $("#negCtrlImgCheckBox").data('id');
        setImnstCtrlImg(evt, ctrlType, url, id)
    });
});

function setImnstCtrlImg(evt, ctrlType, url, id, checkBox) {
    if (ctrlType === "positive") {
        var currentCheckBox = document.getElementById("posCtrlImgCheckBox");
        var otherCheckBox = document.getElementById("negCtrlImgCheckBox");
    }
    else {
        var currentCheckBox = document.getElementById("negCtrlImgCheckBox");
        var otherCheckBox = document.getElementById("posCtrlImgCheckBox");
    }

    // If checkbox was unchecked change primaryAntibodyId textbox readonly state to false, no need to do anything else
    if (!currentCheckBox.checked) {
        document.getElementById("primaryAntibodyId").readOnly = false;
        return;
    }

    // In case that user changed it to checked, prevent default and do some validation 
    evt.preventDefault();

    // Do as much as possible validation on the client side
    var primaryAntibodyValue = document.getElementById("primaryAntibodyId").value;
    if (!primaryAntibodyValue) {
        alert("Can't create a control without Primary Antibody Id.");
        return;
    }

    if (otherCheckBox.checked === true) {
        alert("Row can't have more then one type of control.");
        return;
    }

    // Do the rest of the validation on the server side    
    // Send rowId and the current primaryAntibodyId (the one on the page since the user may have changed it) to the server for the validation
    var dataPost = { id: id, primaryantibodyvalue: primaryAntibodyValue, ctrltype: ctrlType };
    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        dataType: "json",
        data: dataPost,
        success: function () {
            // If everything went well change the check box status to checked and 
            // change primaryAntibodyId textbox to read only to prevent changing the value when it is defined as a control image row
            currentCheckBox.checked = true;
            //ctrlImgCheckBox.prop('checked', true);
            document.getElementById("primaryAntibodyId").readOnly = true;
        },
        error: function (xhr) {
            // Print error message we got from the server
            err = xhr.responseJSON;
            alert(err);
        },
    });
}