﻿$(function () {
    $(document).on("click", '#buttonDel', function (e) {
        e.preventDefault();
        var array = [];
        $("Input[name='selectRow']:checked").each(function () {
            array.push($(this).val());
        })
        var url = $(this).data('url');
        if (array.length > 0) {
            var answer = confirm("Are you sure that you want to delete the " + array.length + " selected rows?")
            if (answer) {
                DeleteSelected(url, array);
            }
            else {
                return;
            }
        }
        else {
            alert("No row has been selected!");
            return;
        }
    });
});

function DeleteSelected(url, array) {
    ShowProgressModal();
    $.ajax({
        url: url,
        data: { selectedrows: array },
        type: "POST",
        dataType: "html",
        traditional: true,
        cache: false,
        success: function (response) {
            var results = JSON.parse(response);
            if (results != null && results.success) {
                window.location.reload(true); // Refresh the view.
                $('#result').html();
            }
            else {
                alertModal("Error!", results.responseText, true);
            }
        },
        error: function (response) {
            var err = JSON.parse(response.responseText);
            alert("Error! " + err.message)
        },
        complete: function () {

        }
    });
    HideProgressModal();
}
