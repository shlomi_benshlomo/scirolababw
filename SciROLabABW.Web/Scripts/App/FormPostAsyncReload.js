﻿// File Post Async using jquery.form
$(function () {
    var options = {
        type: "POST",
        beforeSubmit: beforeSubmit,
        success: function (result) {
            if (result.isRedirect) {
                window.location = result.redirectUrl;
            }
        },
        error: function (xhr) {
            if (xhr.responseText) {
                alert(xhr.responseText);
            }
            else {
                err = xhr.responseJSON;
                alert(err);
            }
        },
        complete: function () {
            window.location.reload(true); // Refresh the view.
            HideProgressModal();
        }
    };
    // Bind form using 'ajaxForm'
    $('#postForm').ajaxForm(options);
});

// Pre-submit callback 
function beforeSubmit(formData, jqForm, options) {
    ShowProgressModal();
    return true;
}