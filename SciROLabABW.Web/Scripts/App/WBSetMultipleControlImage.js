﻿$(function () {
    var array = [];
    var experimentId;
    var membraneId;

    // Find all the rows where ctrlImgCheckBox are checked
    $("Input[data-name='wbCtrlImgCheckBox']:checked").each(function () {
        array.push($(this).data('id'));
    })
    if ((array) && (array.length > 0)) {
        // Set to readOnly all membraneId and membraneId in rows where wbCtrlImgCheckBox are checked
        for (var i = 0; i < array.length; i++) {
            experimentId = document.getElementById("experimentId" + array[i]);
            experimentId.readOnly = true;
            membraneId = document.getElementById("membraneId" + array[i]);
            membraneId.readOnly = true;
        }
    }
});

function WBSetCtrlImg(val) {
    var url = $(val).data('url');
    var id = $(val).data('id');
    var experimentId = document.getElementById("experimentId" + id);
    var membraneId = document.getElementById("membraneId" + id);    

    setWBCtrlImg(url, id, experimentId, membraneId)
}

function setWBCtrlImg(url, id, experimentId, membraneId) {
    var currentCheckBox = document.getElementById("ctrlImgCheckBox" + id);

    // If checkbox was unchecked change experimentID and membraneId textboxes readonly state to false, no need to do anything else
    if (!currentCheckBox.checked) {
        experimentId.readOnly = false;
        membraneId.readOnly = false;
        return;
    }

    if ((!experimentId.value) && (!membraneId.value)) {
        alert("Can't create a control without Experiment Id and Membrane Id.");
        if (currentCheckBox.checked === true) {
            currentCheckBox.checked = false;
        }
        return;
    }
    else if (!experimentId.value) {
        alert("Can't create a control without Experiment Id.");
        if (currentCheckBox.checked === true) {
            currentCheckBox.checked = false;
        }
        return;
    }
    else if (!membraneId.value) {
        alert("Can't create a control without Membrane Id.");
        if (currentCheckBox.checked === true) {
            currentCheckBox.checked = false;
        }
        return;
    }

    // Prevent user from adding an existing membraneId or membraneId more than once
    var experimentIdCount = 0;
    var membraneIdCount = 0;
    var rowIdArray = [];

    // Find all ids of all the selected ctrlImgCheckBoxes rows (current row is included)
    $("Input[data-name='wbCtrlImgCheckBox']:checked").each(function () {
        rowIdArray.push($(this).data('id'));
    })

    // Iterate over the selected rows and check if the number inserted by the user 
    // into the current experimentId or membraneId already exist in another row
    if ((rowIdArray) && (rowIdArray.length > 0)) {
        for (var i = 0; i < rowIdArray.length; i++) {
            var exprmntId = document.getElementById("experimentId" + rowIdArray[i]);
            if ((exprmntId.id !== experimentId.id) && (exprmntId.value === experimentId.value)) {
                experimentIdCount++
            }
            var mmbrneId = document.getElementById("membraneId" + rowIdArray[i]);
            if ((mmbrneId.id !== membraneId.id) && (mmbrneId.value === membraneId.value)) {
                membraneIdCount++
            }
        }
    }

    // If both counters are bigger than 0 it means that control from the same type already exists 
    // with the same Experiment ID Value and Membrane ID Value, don't allow to add a control
    // Print a message and cancel the checking of the checkbox
    if ((experimentIdCount > 0) && (membraneIdCount > 0)) {
        alert("There is already control with the same Experiment ID value and Membrane ID value.\n" +
              "Only one control is allowed for each Experiment ID and Membrane ID.");
        currentCheckBox.checked = false;
        return;
    }

    // Do the rest of the validation on the server side    
    // Send rowId and the current experimentId and membraneId 
    // (the one on the page since the user may have changed it) to the server for the validation
    var dataPost = { id: id, experimentID: experimentId.value, membraneID: membraneId.value };
    $.ajax({
        url: url,
        cache: false,
        type: 'POST',
        dataType: "json",
        data: dataPost,
        success: function () {
            // If everything went well change the check box status to checked and 
            // change experimentID and membraneID textboxes to read only to prevent changing the values when it is defined as a control image row
            currentCheckBox.checked = true;
            experimentId.readOnly = true;
            membraneId.readOnly = true;
        },
        error: function (xhr) {
            if (currentCheckBox.checked === true) {
                currentCheckBox.checked = false;
            }
            // Print error message we got from the server
            err = xhr.responseJSON;
            if (err.message != undefined) {
                alert(err.message);
            }
            else {
                alert(err);
            }
        },
    });
}
