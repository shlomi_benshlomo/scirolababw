﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Hosting;

namespace SciROLabABW.Web
{
    public class OpenSlide
    {
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr openslide_detect_vendor(string filename);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr openslide_open(string filename);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void openslide_close(IntPtr openslidePtr);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void openslide_get_level0_dimensions(IntPtr openslidePtr, ref Int64 width, ref Int64 height);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void openslide_get_level_dimensions(IntPtr openslidePtr, Int32 level, ref Int64 width, ref Int64 height);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern Int32 openslide_get_level_count(IntPtr openslidePtr);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr openslide_get_associated_image_names(IntPtr openslidePtr);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr openslide_get_comment(IntPtr openslidePtr);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void openslide_get_associated_image_dimensions(IntPtr openslidePtr, string name, ref Int64 width, ref Int64 height);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void openslide_read_associated_image(IntPtr openslidePtr, string name, [In, Out] UInt32[] buff);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void openslide_read_region(IntPtr openslidePtr, [In, Out] UInt32[] buff, Int64 xCoordinate, Int64 yCoordinate, Int32 level, Int64 width, Int64 height);
        [DllImport("libopenslide-0.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern Int32 openslide_get_best_level_for_downsample(IntPtr openslidePtr, double downsample);

        [StructLayout(LayoutKind.Sequential)]
        struct OpenslideLevel
        {
            double downsample;  // zero value is filled in automatically from dimensions

            Int64 w;
            Int64 h;

            // only for tile geometry properties; 0 to omit.
            // all levels must set these, or none
            Int64 tile_w;
            Int64 tile_h;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct OpenSlideImg
        {
            public IntPtr ops;
            public IntPtr levels;
            public IntPtr data;
            public Int32 level_count;
            public IntPtr associated_images;
            public IntPtr associated_image_names;
            public IntPtr properties;
            public IntPtr property_names;
            public IntPtr cache;
            public IntPtr error;
        };

        private static Int32 openslideGetLevelCount(IntPtr openslidePtr)
        {
            Int32 levelCount = openslide_get_level_count(openslidePtr);
            return levelCount;
        }

        private static byte[] openslideRegionToJpeg(UInt32[] imgBuff, Int32 imgWidth, Int32 imgHeight)
        {
            // Convert to byte array
            var buffSize = imgBuff.Length;
            byte[] byteBuff = new byte[buffSize];
            Buffer.BlockCopy(imgBuff, 0, byteBuff, 0, byteBuff.Length);

            // Create a bitmap with PixelFormat.Format32bppArgb
            using (Bitmap bitmap = new Bitmap(imgWidth, imgHeight, PixelFormat.Format32bppArgb))
            {
                var bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, bitmap.PixelFormat);
                byte[] outBytes = new byte[imgBuff.Length];
                Marshal.Copy(byteBuff, 0, bitmapData.Scan0, outBytes.Length);
                bitmap.UnlockBits(bitmapData);

                // Build a new byte array with the converted format and return
                using (var outStream = new MemoryStream())
                {
                    bitmap.Save(outStream, ImageFormat.Jpeg);
                    outBytes = outStream.ToArray();
                }
                return outBytes;
            }
        }

        private static Int32 getBestLevelForDownsample(IntPtr openslidePtr, double downSample, Int64 maxBuffSize)
        {
            Int64 imgWidth = 0;
            Int64 imgHeight = 0;
            Int32 levelCount = -1;
            Int32 bestLevel = -1;
            Int32 level = -1;

            levelCount = openslide_get_level_count(openslidePtr);
            bestLevel = openslide_get_best_level_for_downsample(openslidePtr, downSample);
            for (level = bestLevel; level < levelCount; level++)
            {
                // Get dimensions
                openslide_get_level_dimensions(openslidePtr, level, ref imgWidth, ref imgHeight);
                if ((imgWidth > 0) && (imgHeight > 0) && (imgWidth <= Int32.MaxValue) && (imgHeight <= Int32.MaxValue))
                {
                    // Set buffer big enough to accommodate the whole image
                    Int64 buffSize = imgWidth * imgHeight * 4;
                    if (buffSize <= maxBuffSize)
                    {
                        return level;
                    }
                }
            }

            return -1;
        }

        private static byte[] OpenSlideGetDownsampleImage(IntPtr openslidePtr, string imgFullPath, double downSample)
        {
            Int64 xCoordinate = 0;
            Int64 yCoordinate = 0;
            Int64 imgWidth = 0;
            Int64 imgHeight = 0;
            Int64 maxBuffSize = 20000000;


            Int32 bestlevel = getBestLevelForDownsample(openslidePtr, downSample, maxBuffSize);

            if(bestlevel == -1)
            {
                return null;
            }
            openslide_get_level_dimensions(openslidePtr, bestlevel, ref imgWidth, ref imgHeight);
            Int64 buffSize = imgWidth * imgHeight * 4;
            UInt32[] imgBuff = new UInt32[buffSize];
            // Read the entire image
            openslide_read_region(openslidePtr, imgBuff, xCoordinate, yCoordinate, bestlevel, imgWidth, imgHeight);
            // Convert to Jpeg
            return openslideRegionToJpeg(imgBuff, (Int32)imgWidth, (Int32)imgHeight);
        }

        public static string OpenslideDetectVendor(string storedImagePath)
        {
            string vendorStr = null;

            if (String.IsNullOrEmpty(storedImagePath))
            {
                return null;
            }
            IntPtr vendorPtr = IntPtr.Zero;
            vendorPtr = openslide_detect_vendor(storedImagePath);
            vendorStr = Marshal.PtrToStringAnsi(vendorPtr);

            return vendorStr;
        }

        public static byte[] OpenSlideGetImage(string storedImagePath)
        {
            string vendorName = null;
            // Call to OpenslideDetectVendor first.
            // A quick way to efficiently check whether a slide file is recognized by OpenSlide.
            vendorName = OpenslideDetectVendor(storedImagePath);
            if(String.IsNullOrEmpty(vendorName))
            {
                return null;
            }
            if (String.IsNullOrEmpty(storedImagePath))
            {
                return null;
            }
            if (String.IsNullOrEmpty(storedImagePath))
            {
                return null;
            }

            IntPtr openslidePtr = IntPtr.Zero;
            try
            {
                // Call openslide_open to get the pointer to the main structure
                openslidePtr = openslide_open(storedImagePath);
                if (openslidePtr == IntPtr.Zero)
                {
                    return null;
                }

                byte[] byteBuff = null;
                // Read the entire downsampled image and convert to Jpeg to be able to display in the browser
                byteBuff = OpenSlideGetDownsampleImage(openslidePtr, storedImagePath, 100);
                if(byteBuff == null)
                {
                    // If system couldn't find downsample image to display try to get the thumbnail
                    byteBuff = OpenSlideGetThumbnail(storedImagePath);
                }

                return byteBuff;
            }
            finally
            {
                if (openslidePtr != IntPtr.Zero)
                {
                    openslide_close(openslidePtr);
                }
            }
        }

        private static byte[] openSlideGetAssociatedImageToJpeg(IntPtr openslidePtr, string imgFullPath, string associatedName)
        {
            // Read all associaged names from openslide and create a local list
            var associatedNamesPtr = IntPtr.Zero;
            List<String> associatedNamesList = new List<String>();
            try
            {
                associatedNamesPtr = openslide_get_associated_image_names(openslidePtr);
                if (associatedNamesPtr != IntPtr.Zero)
                {
                    IntPtr namePtr = Marshal.ReadIntPtr(associatedNamesPtr);
                    while (namePtr != IntPtr.Zero)
                    {
                        string name = Marshal.PtrToStringAnsi(namePtr);
                        if (!(String.IsNullOrWhiteSpace(name)))
                        {
                            associatedNamesList.Add(name);
                        }
                        associatedNamesPtr = IntPtr.Add(associatedNamesPtr, IntPtr.Size);
                        namePtr = Marshal.ReadIntPtr(associatedNamesPtr);
                    }
                }
            }
            finally
            {
                //Marshal.FreeHGlobal(ptr);
            }
            // Check if the reqested name exists in the list
            foreach (string name in associatedNamesList)
            {
                // If name found read the associated image
                if (name == associatedName)
                {
                    Int64 assocWidth = 0;
                    Int64 assocHeight = 0;

                    openslide_get_associated_image_dimensions(openslidePtr, associatedName, ref assocWidth, ref assocHeight);

                    if ((assocWidth < 0) || (assocHeight < 0) || (assocWidth > Int32.MaxValue) || (assocHeight > Int32.MaxValue))
                    {
                        return null;
                    }

                    UInt32[] buff = new UInt32[assocWidth * assocHeight * 4];
                    openslide_read_associated_image(openslidePtr, associatedName, buff);
                    // Convert to Jpeg to be able to display in the browser
                    return openslideRegionToJpeg(buff, (Int32)assocWidth, (Int32)assocHeight);
                }
            }

            return null;
        }

        public static byte[] OpenSlideGetAssociatedImage(string storedImagePath, string associatedName)
        {
            IntPtr openslidePtr = IntPtr.Zero;

            try
            {
                openslidePtr = openslide_open(storedImagePath);
                if (openslidePtr == IntPtr.Zero)
                {
                    return null;
                }
                return openSlideGetAssociatedImageToJpeg(openslidePtr, storedImagePath, associatedName);
            }
            finally
            {
                if (openslidePtr != IntPtr.Zero)
                {
                    openslide_close(openslidePtr);
                }
            }
        }

        public static byte[] OpenSlideGetThumbnail(string storedImagePath)
        {
            IntPtr openslidePtr = IntPtr.Zero;

            try
            {
                openslidePtr = openslide_open(storedImagePath);
                if (openslidePtr == IntPtr.Zero)
                {
                    return null;
                }
                byte[] buff = null;
                buff = openSlideGetAssociatedImageToJpeg(openslidePtr, storedImagePath, "thumbnail");
                if (buff == null)
                {
                    // There is no actual thumbnail for this image, use downsample image instead. 
                    buff = OpenSlideGetDownsampleImage(openslidePtr, storedImagePath, 200);
                }
                return buff;
            }
            finally
            {
                if (openslidePtr != IntPtr.Zero)
                {
                    openslide_close(openslidePtr);
                }
            }
        }
    }
}