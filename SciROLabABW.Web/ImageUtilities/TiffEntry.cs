﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace SciROLabABW.Web.ImageUtilities
{
    public class TiffEntry
    {
        private TIFFInfo TagId;
        private TIFFType Type;
        private Int32 Count;
        private UInt32 ValueOffset;

        public TiffEntry(Stream stream, bool isLittleEndian)
        {
            byte[] Buff = new byte[TagSize()];
            stream.Read(Buff, 0, TagSize());

            if (!isLittleEndian)
            {
                Array.Reverse(Buff, 0, sizeof(Int16));
                Array.Reverse(Buff, sizeof(Int16), sizeof(Int16));
                Array.Reverse(Buff, 2 * sizeof(Int16), sizeof(Int32));
            }
            TagId = (TIFFInfo)BitConverter.ToInt16(Buff, 0);
            Type = (TIFFType)BitConverter.ToInt16(Buff, sizeof(Int16));
            Count = BitConverter.ToInt32(Buff, 2 * sizeof(Int16));

            int valIdx = sizeof(Int16) + sizeof(Int16) + sizeof(Int32);

            if (Count * bytesForType(Type) > 4)//This is an offset to the value
            {
                if (!isLittleEndian)
                {
                    Array.Reverse(Buff, valIdx, sizeof(Int32));
                }
                ValueOffset = BitConverter.ToUInt32(Buff, valIdx);


            }
            else//This is the whole value, so just mark the offset now and read the value later
            {
                ValueOffset = (UInt32)(stream.Position - 4);
            }
        }

        public UInt16 GetUInt16AsLittleEndian(Stream stream, bool isLittleEndian)
        {
            byte[] byteBuff = new byte[sizeof(UInt16)];
            stream.Read(byteBuff, 0, sizeof(UInt16));
            if (!isLittleEndian)
                Array.Reverse(byteBuff, 0, sizeof(UInt16));
            return BitConverter.ToUInt16(byteBuff, 0);
        }

        public UInt32 GetUInt32AsLittleEndian(Stream stream, bool isLittleEndian)
        {
            byte[] byteBuff = new byte[sizeof(UInt32)];
            stream.Read(byteBuff, 0, sizeof(UInt32));
            if (!isLittleEndian)
                Array.Reverse(byteBuff, 0, sizeof(UInt32));
            return BitConverter.ToUInt32(byteBuff, 0);
        }

        static Int32 bytesForType(TIFFType type)
        {
            switch (type)
            {
                case TIFFType.Byte:
                case TIFFType.ASCII:
                case TIFFType.SignedByte:
                case TIFFType.Undefined: return 1;

                case TIFFType.Short:
                case TIFFType.SignedShort: return 2;

                case TIFFType.Long:
                case TIFFType.SignedLong:
                case TIFFType.Float: return 4;

                case TIFFType.Rational:
                case TIFFType.SignedRational:
                case TIFFType.Double: return 8;

                default: return 0;
            }
        }

        struct Rational
        {
            public UInt32 Numerator, Denominator;
        }

        struct SignedRational
        {
            public Int32 Numerator, Denominator;
        }

#if false
        public static Int32 DataTypeSize(int dataType)
        {
            Int32[] DataTypeArr = { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8 };

            if (dataType < 1 || dataType > 13)//TODO: Add error handling
                return 0;

            return DataTypeArr[dataType];
        }
#endif

        public static int TagSize()
        {
            return 12;
        }

        public TIFFInfo tagValue()
        {
            return TagId;
        }

        public Int32 tagCount()
        {
            return Count;
        }

        public TIFFType tagType()
        {
            return Type;
        }

#if false
        public byte[] byteArr()
        {
            return byteBuff;
        }

        public Int32[] asInt32()
        {
            Int32[] rv = new Int32[Count];

            return rv;
        }

        public Int32[] toInt32()
        {
            int i;
            Int32[] value = new Int32[Count];
            switch (Type)
            {
                case TIFFType.Short:
                    for (i = 0; i < Count; i++)
                    {
                        value[i] = Convert.ToInt32(BitConverter.ToInt16(byteArr(), i * sizeof(Int16)));
                    }
                    break;
                case TIFFType.Long:
                    for (i = 0; i < Count; i++)
                    {
                        value[i] = BitConverter.ToInt32(byteArr(), i * sizeof(Int32));
                    }
                    break;
                default:
                    {
                        //TODO: error message
                    }
                    break;
            }
            return value;
        }
#endif
        public object GetIFDData(Stream stream, bool isLittleEndian)
        {
            stream.Seek(ValueOffset, SeekOrigin.Begin);
            BinaryReader reader = new BinaryReader(stream, Encoding.ASCII);
            int i;

            switch (Type)
            {
                case TIFFType.Byte:
                case TIFFType.Undefined:
                    {
                        Byte[] buff = new Byte[Count];
                        return stream.Read(buff, 0, Count * sizeof(Byte));
                    }
                case TIFFType.ASCII:
                    {
                        Byte[] buff = new Byte[Count];
                        stream.Read(buff, 0, Count * sizeof(Byte));
                        return Encoding.ASCII.GetString(buff);
                    }
                case TIFFType.Short:
                    {
                        UInt16[] buff = new UInt16[Count];
                        for (i = 0; i < Count; i++)
                        {
                            buff[i] = GetUInt16AsLittleEndian(stream, isLittleEndian);
                        }
                        return buff;
                    }
                case TIFFType.Long:
                    {
                        UInt32[] buff = new UInt32[Count];
                        for (i = 0; i < Count; i++)
                        {
                            buff[i] = GetUInt32AsLittleEndian(stream, isLittleEndian);
                        }
                        return buff;
                    }
                case TIFFType.Rational:
                    {
                        Rational[] buff = new Rational[Count];
                        for (i = 0; i < Count; i++)
                        {
                            buff[i].Numerator = GetUInt32AsLittleEndian(stream, isLittleEndian);
                            buff[i].Denominator = GetUInt32AsLittleEndian(stream, isLittleEndian);
                        }
                        return buff;
                    }
                case TIFFType.SignedByte:
                    {
                        sbyte[] buff = new sbyte[Count];
                        for (i = 0; i < Count; i++)
                            buff[i] = reader.ReadSByte();
                        return buff;
                    }

                case TIFFType.SignedShort:
                    {
                        short[] ret = new short[Count];
                        for (i = 0; i < Count; i++)
                            ret[i] = reader.ReadInt16();
                        return ret;
                    }
                case TIFFType.SignedLong:
                    {
                        int[] ret = new int[Count];
                        for (i = 0; i < Count; i++)
                            ret[i] = reader.ReadInt32();
                        return ret;
                    }
                case TIFFType.SignedRational:
                    {
                        SignedRational[] ret = new SignedRational[Count];
                        for (i = 0; i < Count; i++)
                        {
                            ret[i].Numerator = reader.ReadInt32();
                            ret[i].Denominator = reader.ReadInt32();
                        }
                        return ret;
                    }
                case TIFFType.Float:
                    {
                        float[] ret = new float[Count];
                        for (i = 0; i < Count; i++)
                            ret[i] = reader.ReadSingle();
                        return ret;
                    }
                case TIFFType.Double:
                    {
                        double[] ret = new double[Count];
                        for (i = 0; i < Count; i++)
                            ret[i] = reader.ReadDouble();
                        return ret;
                    }

                default:
                    return null;
            }

#if false
            reader.BaseStream.Seek(base_offset + FirstValueOffset, SeekOrigin.Begin);

            switch (Type)
            {
                case TIFFType.Byte:
                case TIFFType.Undefined:
                    {
                        byte[] ret = reader.ReadBytes(Count);
                        return ret;
                    }
                case TIFFType.ASCII:
                    {
                        byte[] bytes = reader.ReadBytes(Count);
                        return Encoding.ASCII.GetString(bytes);
                    }
                case TIFFType.Short:
                    {
                        ushort[] ret = new ushort[Count];
                        for (int i = 0; i < Count; i++)
                            ret[i] = reader.ReadUInt16();
                        return ret;
                    }
                case TIFFType.Long:
                    {
                        uint[] ret = new uint[Count];
                        for (int i = 0; i < Count; i++)
                            ret[i] = reader.ReadUInt32();
                        return ret;
                    }
                case TIFFType.Rational:
                    {
                        Rational[] ret = new Rational[Count];
                        for (int i = 0; i < Count; i++)
                        {
                            ret[i].Numerator = reader.ReadUInt32();
                            ret[i].Denominator = reader.ReadUInt32();
                        }
                        return ret;
                    }
                case TIFFType.SignedByte:
                    {
                        sbyte[] ret = new sbyte[Count];
                        for (int i = 0; i < Count; i++)
                            ret[i] = reader.ReadSByte();
                        return ret;
                    }
                case TIFFType.SignedShort:
                    {
                        short[] ret = new short[Count];
                        for (int i = 0; i < Count; i++)
                            ret[i] = reader.ReadInt16();
                        return ret;
                    }
                case TIFFType.SignedLong:
                    {
                        int[] ret = new int[Count];
                        for (int i = 0; i < Count; i++)
                            ret[i] = reader.ReadInt32();
                        return ret;
                    }
                case TIFFType.SignedRational:
                    {
                        SignedRational[] ret = new SignedRational[Count];
                        for (int i = 0; i < Count; i++)
                        {
                            ret[i].Numerator = reader.ReadInt32();
                            ret[i].Denominator = reader.ReadInt32();
                        }
                        return ret;
                    }
                case TIFFType.Float:
                    {
                        float[] ret = new float[Count];
                        for (int i = 0; i < Count; i++)
                            ret[i] = reader.ReadSingle();
                        return ret;
                    }
                case TIFFType.Double:
                    {
                        double[] ret = new double[Count];
                        for (int i = 0; i < Count; i++)
                            ret[i] = reader.ReadDouble();
                        return ret;
                    }
                default:
                    return null;
            }
#endif
        }

        public UInt32 GetGenericScalar(Stream stream, bool isLittleEndian)
        {
            object data = GetIFDData(stream, isLittleEndian);

            if (data is Int32[])
                return (UInt32)((Int32[])data)[0];
            if (data is Int16[])
                return (UInt32)((Int16[])data)[0];
            if (data is Byte[])
                return ((Byte[])data)[0];
            if (data is UInt32[])
                return ((UInt32[])data)[0];
            if (data is UInt16[])
                return ((UInt16[])data)[0];
            if (data is SByte[])
                return (UInt32)((SByte[])data)[0];
            if (data is Rational[])
            {
                Rational r = ((Rational[])data)[0];

                return (UInt32)(r.Numerator / r.Denominator);
            }
            if (data is SignedRational[])
            {
                SignedRational r = ((SignedRational[])data)[0];

                return (UInt32)(r.Numerator / r.Denominator);
            }
            if (data is Single[])
                return (UInt32)((Single[])data)[0];
            if (data is Double[])
                return (UInt32)((Double[])data)[0];

            if (data is String)
                try
                {
                    return UInt32.Parse(data.ToString());
                }
                catch (FormatException)
                {
                    return 0;
                }

            throw new OutOfMemoryException(("The data could not be interpreted."));
        }

        public UInt32[] GetGenericArray(Stream stream, bool isLittleEndian)
        {
            object data = GetIFDData(stream, isLittleEndian);
            Int32 i;
            if (data is Array)
            {
                Array dataArray = (Array)data;

                UInt32[] buff = new UInt32[dataArray.Length];

                for (i = 0; i < buff.Length; i++)
                    buff[i] = Convert.ToUInt32(dataArray.GetValue(i));

                return buff;
            }
            else
                throw new OutOfMemoryException(("The data could not be interpreted."));
        }

        public enum TIFFType
        {
            NOTYPE = 0, /* placeholder */
            Byte = 1,   /* 8-bit unsigned integer */
            ASCII = 2,  /* 8-bit bytes w/ last byte null */
            Short = 3,  /* 16-bit unsigned integer */
            Long = 4,   /* 32-bit unsigned integer */
            Rational = 5, /* 64-bit unsigned fraction */
            SignedByte = 6,  /* !8-bit signed integer */
            Undefined = 7,    /* !8-bit untyped data */
            SignedShort = 8, /* !16-bit signed integer */
            SignedLong = 9,  /* !32-bit signed integer */
            SignedRational = 10, /* !64-bit signed fraction */
            Float = 11, /* !32-bit IEEE floating point */
            Double = 12 /* !64-bit IEEE floating point */
        };

        public enum TIFFInfo
        {
            SUBFILETYPE = 254,         /* subfile data descriptor */
            OSUBFILETYPE = 255,       /* +kind of data in subfile */
            IMAGEWIDTH = 256,          /* image width in pixels */
            IMAGELENGTH = 257,         /* image height in pixels */
            BITSPERSAMPLE = 258,      /* bits per channel (sample) */
            COMPRESSION = 259,         /* data compression technique */
            PHOTOMETRIC = 262, /* photometric interpretation */
            THRESHHOLDING = 263, /* +thresholding used on data */
            CELLWIDTH = 264, /* +dithering matrix width */
            CELLLENGTH = 265, /* +dithering matrix height */
            FILLORDER = 266, /* data order within a byte */
            DOCUMENTNAME = 269, /* name of doc. image is from */
            IMAGEDESCRIPTION = 270, /* info about image */
            MAKE = 271, /* scanner manufacturer name */
            MODEL = 272, /* scanner model name/number */
            STRIPOFFSETS = 273, /* offsets to data strips */
            ORIENTATION = 274, /* +image orientation */
            SAMPLESPERPIXEL = 277, /* samples per pixel */
            ROWSPERSTRIP = 278, /* rows per strip of data */
            STRIPBYTECOUNTS = 279, /* bytes counts for strips */
            MINSAMPLEVALUE = 280, /* +minimum sample value */
            MAXSAMPLEVALUE = 281, /* +maximum sample value */
            XRESOLUTION = 282, /* pixels/resolution in x */
            YRESOLUTION = 283, /* pixels/resolution in y */
            PLANARCONFIG = 284, /* storage organization */
            PAGENAME = 285, /* page name image is from */
            XPOSITION = 286, /* x page offset of image lhs */
            YPOSITION = 287, /* y page offset of image lhs */
            FREEOFFSETS = 288, /* +byte offset to free block */
            FREEBYTECOUNTS = 289, /* +sizes of free blocks */
            GRAYRESPONSEUNIT = 290, /* $gray scale curve accuracy */
            GRAYRESPONSECURVE = 291, /* $gray scale response curve */
            GROUP3OPTIONS = 292, /* 32 flag bits */
            GROUP4OPTIONS = 293, /* 32 flag bits */
            RESOLUTIONUNIT = 296, /* units of resolutions */
            PAGENUMBER = 297, /* page numbers of multi-page */
            COLORRESPONSEUNIT = 300, /* $color curve accuracy */
            TRANSFERFUNCTION = 301, /* !colorimetry info */
            SOFTWARE = 305, /* name & release */
            DATETIME = 306, /* creation date and time */
            ARTIST = 315, /* creator of image */
            HOSTCOMPUTER = 316, /* machine where created */
            PREDICTOR = 317, /* prediction scheme w/ LZW */
            WHITEPOINT = 318, /* image white point */
            PRIMARYCHROMATICITIES = 319, /* !primary chromaticities */
            COLORMAP = 320, /* RGB map for pallette image */
            HALFTONEHINTS = 321, /* !highlight+shadow info */
            TILEWIDTH = 322, /* !rows/data tile */
            TILELENGTH = 323, /* !cols/data tile */
            TILEOFFSETS = 324, /* !offsets to data tiles */
            TILEBYTECOUNTS = 325, /* !byte counts for tiles */
            BADFAXLINES = 326, /* lines w/ wrong pixel count */
            CLEANFAXDATA = 327, /* regenerated line info */
            CONSECUTIVEBADFAXLINES = 328, /* max consecutive bad lines */
            INKSET = 332, /* !inks in separated image */
            INKNAMES = 333, /* !ascii names of inks */
            DOTRANGE = 336, /* !0% and 100% dot codes */
            TARGETPRINTER = 337, /* !separation target */
            EXTRASAMPLES = 338, /* !info about extra samples */
            SAMPLEFORMAT = 339, /* !data sample format */
            SMINSAMPLEVALUE = 340, /* !variable MinSampleValue */
            SMAXSAMPLEVALUE = 341, /* !variable MaxSampleValue */
            JPEGPROC = 512, /* !JPEG processing algorithm */
            JPEGIFOFFSET = 513, /* !pointer to SOI marker */
            JPEGIFBYTECOUNT = 514, /* !JFIF stream length */
            JPEGRESTARTINTERVAL = 515, /* !restart interval length */
            JPEGLOSSLESSPREDICTORS = 517, /* !lossless proc predictor */
            JPEGPOINTTRANSFORM = 518, /* !lossless point transform */
            JPEGQTABLES = 519, /* !Q matrice offsets */
            JPEGDCTABLES = 520, /* !DCT table offsets */
            JPEGACTABLES = 521, /* !AC coefficient offsets */
            YCBCRCOEFFICIENTS = 529, /* !RGB -> YCbCr transform */
            YCBCRSUBSAMPLING = 530, /* !YCbCr subsampling factors */
            YCBCRPOSITIONING = 531, /* !subsample positioning */
            REFERENCEBLACKWHITE = 532, /* !colorimetry info */
            /* tags 32952-32956 are private tags registered to Island Graphics */
            REFPTS = 32953, /* image reference points */
            REGIONTACKPOINT = 32954, /* region-xform tack point */
            REGIONWARPCORNERS = 32955, /* warp quadrilateral */
            REGIONAFFINE = 32956, /* affine transformation mat */
            /* tags 32995-32999 are private tags registered to SGI */
            MATTEING = 32995, /* $use ExtraSamples */
            DATATYPE = 32996, /* $use SampleFormat */
            IMAGEDEPTH = 32997, /* z depth of image */
            TILEDEPTH = 32998 /* z depth/data tile */
        };
    }
}