﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ImageUtilities
{
    public class TiffIFD
    {
        //NOTE: at the end of the reading here we should verify that the position is the end of the current IFD and not something else
        //So if we read only the current IFD it is ok, otherwise we need to set the position back to the end of the current IFD
        public TiffIFD(Stream stream, bool isLittleEndian)
        {
            int idx;
            byte[] countBuffer = new byte[sizeof(Int16)];
            TiffTagList = new Dictionary<Int16, TiffEntry>();

            stream.Read(countBuffer, 0, sizeof(Int16));
            if (!isLittleEndian)
                Array.Reverse(countBuffer, 0, sizeof(Int16));

            Int16 numOfTags = BitConverter.ToInt16(countBuffer, 0);
            //            byte[] tagBuff = new byte[numOfTags * TiffTag.TagSize()];
            //            stream.Read(tagBuff, 0, numOfTags * TiffTag.TagSize());

            for (idx = 0; idx < numOfTags; idx++)
            {
                TiffEntry newTag = new TiffEntry(stream, isLittleEndian);
                TiffTagList.Add((Int16)newTag.tagValue(), newTag);
            }
        }

        public bool tiffTagContainsKey(Int16 key)
        {
            return TiffTagList.ContainsKey(key);
        }

        public TiffEntry tiffTagForKey(Int16 key)
        {
            TiffEntry ret;
            TiffTagList.TryGetValue(key, out ret);
            return ret;
        }

        private Dictionary<Int16, TiffEntry> TiffTagList;
    }
}