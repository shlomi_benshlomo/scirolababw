﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace SciROLabABW.Web.ImageUtilities
{
#if false // TODO: not in use for now - using unsafe which is not working with deployment
    public class TiffReader
    {
        const int TiffHeaderLength = 8;
        const int TiffBigEndian = 0x4d4d;
        const int TiffLittleEndian = 0x4949;
        const Int16 TiffId = 42;

        //private Int16 byteOrder;

        private Bitmap bmp;
        private Boolean isLittleEndian;

        public unsafe TiffReader(string fileNameStr)
        {
            Stream stream = new FileStream(fileNameStr, FileMode.Open, FileAccess.Read);
            BitmapData bmpData = null;
            List<TiffIFD> IFDList;

            //Read header
            /************/
            byte[] buffer = new byte[TiffHeaderLength];

            stream.Read(buffer, 0, TiffHeaderLength);
            Int16 byteOrder = BitConverter.ToInt16(buffer, 0);
            if (byteOrder == TiffBigEndian)
            {
                isLittleEndian = false;
            }
            else if (byteOrder == TiffLittleEndian)
            {
                isLittleEndian = true;
            }
            else
            {
                // TODO: put an error message
                return;
            }

            //Read the Megicbytes
            /********************/
            if (!isLittleEndian)
                Array.Reverse(buffer, 2, sizeof(Int16));

            Int16 magicBytes = BitConverter.ToInt16(buffer, 2);
            if (magicBytes != TiffId)
                throw new OutOfMemoryException(("Invalid file format (File ID is not 42)"));
            //Read the offset to IFD
            if (!isLittleEndian)
                Array.Reverse(buffer, 4, sizeof(Int32));

            //Read IFDs 
            /**********/
            Int32 IfdOffset = BitConverter.ToInt32(buffer, 4);
            Int32 imgIdx = 0;
            IFDList = new List<TiffIFD>();
            while (IfdOffset != 0)//Read all IFDs
            {
                stream.Seek(IfdOffset, SeekOrigin.Begin);

                IFDList.Add(new TiffIFD(stream, isLittleEndian));
                //NOTE: After we read the stream the position is at the end of the IFD which is the pointer to the next IFD
                stream.Read(buffer, 0, sizeof(Int32));
                if (!isLittleEndian)
                    Array.Reverse(buffer, 0, sizeof(Int32));
                IfdOffset = BitConverter.ToInt32(buffer, 0);

                /********************************************************************************************/
                //TODO: Take care on the case that we don't have width and height!!!!!
                TiffIFD currentIfd = IFDList[imgIdx];
                Int32 width = (int)currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.IMAGEWIDTH).
                    GetGenericScalar(stream, isLittleEndian);
                Int32 height = (int)currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.IMAGELENGTH).
                    GetGenericScalar(stream, isLittleEndian);


                Int32 rowsPerStrip, samplesPerPixel, extraSamples, fillOrder, orientation;
                UInt32[] bitsPerSample;

                if (currentIfd.tiffTagContainsKey((short)TiffEntry.TIFFInfo.ROWSPERSTRIP))
                    rowsPerStrip = (int)
                      currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.ROWSPERSTRIP).
                       GetGenericScalar(stream, isLittleEndian);
                else
                    rowsPerStrip = int.MaxValue;

                if (currentIfd.tiffTagContainsKey((short)TiffEntry.TIFFInfo.BITSPERSAMPLE))
                    bitsPerSample = currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.BITSPERSAMPLE).
                       GetGenericArray(stream, isLittleEndian);
                else
                {
                    bitsPerSample = new uint[1];
                    bitsPerSample[0] = 1;
                }

                if (currentIfd.tiffTagContainsKey((short)TiffEntry.TIFFInfo.SAMPLESPERPIXEL))
                    samplesPerPixel = (int)
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.SAMPLESPERPIXEL).
                       GetGenericScalar(stream, isLittleEndian);
                else
                    samplesPerPixel = 1;

                if (currentIfd.tiffTagContainsKey((short)TiffEntry.TIFFInfo.EXTRASAMPLES))
                    extraSamples = (int)
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.EXTRASAMPLES).
                       GetGenericScalar(stream, isLittleEndian);
                else
                    extraSamples = 0;

                if (currentIfd.tiffTagContainsKey((short)TiffEntry.TIFFInfo.FILLORDER))
                    fillOrder = (int)
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.FILLORDER).
                       GetGenericScalar(stream, isLittleEndian);
                else
                    fillOrder = 1;

                if (currentIfd.tiffTagContainsKey((short)TiffEntry.TIFFInfo.ORIENTATION))
                    orientation = (int)
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.ORIENTATION).
                       GetGenericScalar(stream, isLittleEndian);
                else
                    orientation = 1;

                UInt32[] stripOffsets =
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.STRIPOFFSETS).
                       GetGenericArray(stream, isLittleEndian);

                UInt32[] stripByteCounts =
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.STRIPBYTECOUNTS).
                       GetGenericArray(stream, isLittleEndian);

                TIFFPhotometric photometric = (TIFFPhotometric)
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.PHOTOMETRIC).
                       GetGenericScalar(stream, isLittleEndian);

                TIFFCompression compression = (TIFFCompression)
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.COMPRESSION).
                       GetGenericScalar(stream, isLittleEndian);

                UInt32[] colorMap = null;

                if (currentIfd.tiffTagContainsKey((short)TiffEntry.TIFFInfo.COLORMAP))
                    colorMap =
                       currentIfd.tiffTagForKey((short)TiffEntry.TIFFInfo.COLORMAP).
                       GetGenericArray(stream, isLittleEndian);

                Int64 numStrips;
                /*To avoid overflow we need to use long instead of int when adding the 2 parameters below*/
                Int64 tmpRowsPerStrip = rowsPerStrip;
                Int64 tmpHeight = height;
                numStrips = (tmpHeight + tmpRowsPerStrip - 1) / tmpRowsPerStrip;

                if (fillOrder != 1)
                    throw new OutOfMemoryException(("Unsupported fill order"));

                if (orientation != 1)
                    throw new OutOfMemoryException(("Unsupported orientation"));

                if ((stripOffsets.Length != numStrips) || (stripByteCounts.Length != numStrips))
                    throw new OutOfMemoryException(("Invalid file format (strip data is invalid)"));

                if (bitsPerSample.Length < samplesPerPixel)
                {
                    uint[] newBitsPerSample = new uint[samplesPerPixel];

                    Array.Copy(bitsPerSample, 0, newBitsPerSample, 0, bitsPerSample.Length);
                    for (int i = bitsPerSample.Length; i < newBitsPerSample.Length; i++)
                        newBitsPerSample[i] = bitsPerSample[0];

                    bitsPerSample = newBitsPerSample;
                }

                if (bitsPerSample.Length != samplesPerPixel)
                    throw new OutOfMemoryException(("Invalid file format (incorrect number of samples described in BitsPerSample)"));

                if (((photometric == TIFFPhotometric.Bilevel_BlackIsZero)
                  || (photometric == TIFFPhotometric.Bilevel_WhiteIsZero))
                 && (samplesPerPixel - extraSamples != 1))
                    throw new OutOfMemoryException(("Invalid file format (bilevel image has more than one sample per pixel)"));

                if (photometric == TIFFPhotometric.RGB_Palette)
                {
                    if (samplesPerPixel != 1)
                        throw new OutOfMemoryException(("Invalid file format (palettized image has more than one sample per pixel)"));

                    int expectedColorMapLength = (1 << (int)bitsPerSample[0]) * 3;

                    if (colorMap.Length != expectedColorMapLength)
                        throw new OutOfMemoryException(("Invalid file format (palette does not contain the correct number of entries)"));
                }

#if false // I found at least one valid case that will throw an exception here so I need to comment out this one
                if (((photometric == TIFFPhotometric.RGB)
                  || (photometric == TIFFPhotometric.YCbCr))
                 && (samplesPerPixel - extraSamples != 3))
                    throw new OutOfMemoryException(("Invalid file format (image does not have 3 components)"));
#endif

                if ((photometric == TIFFPhotometric.CMYK)
                 && (samplesPerPixel - extraSamples != 4))
                    throw new OutOfMemoryException(("Invalid file format (CMYK image does not have 4 components)"));

                int stripIndex = 0;

                bool sixteenBits = false;

                for (Int32 i = 0; i < samplesPerPixel; i++)
                {
                    if (bitsPerSample[i] > 8)
                    {
                        sixteenBits = true;
                        break;
                    }
                }

                int pixelBytes = (sixteenBits ? 8 : 4);

                if (sixteenBits)
                {
                    //bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                    bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                }
                else
                {
                    //TODO: Decide if we really need the code for 8 bit or leave the return in case of not sixteenBits.
                    return;
#if false
                    if (photometric == TIFFPhotometric.CMYK)
                    {
                        bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                    }
                    else
                    {
                        bmp = new Bitmap(width, height, PixelFormat.Format32bppRgb);
                    }
#endif
                }


                Int32 pixelBits = 0;
                for (UInt32 i = 0; i < samplesPerPixel; i++)
                {
                    pixelBits += (int)bitsPerSample[i];
                }

                Int32 strideBits = (int)(width * pixelBits);

                Int32 strideBytes = (strideBits + 7) >> 3;

                try
                {
                    bmpData = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                    if (bmpData == null)
                        throw new OutOfMemoryException(("Unable to lock bitmap data"));

                    byte* bmpDataPtr = (byte*)bmpData.Scan0.ToPointer();

                    Int32 bmpStride = bmpData.Stride;
                    Int32 bmpFixup = bmpStride - bmpData.Width * (sixteenBits ? 8 : 4);

                    for (Int64 y = 0; y < height; y += rowsPerStrip, stripIndex++)
                    {
                        //TODO: what to do with offset???
                        Int32 stripPosition = (int)(/*offset +*/ stripOffsets[stripIndex]);
                        Int32 rowsThisStrip = rowsPerStrip;
                        Int64 stripEnd = y + rowsPerStrip;
                        if (stripEnd > height)
                        {
                            stripEnd = height;
                            rowsThisStrip = (int)(stripEnd - y);
                        }

                        Int32 bmpDataOffset = (int)(y * bmpStride);

                        stream.Seek(stripPosition, SeekOrigin.Begin);

                        Byte[] stripBytes = new byte[stripByteCounts[stripIndex]];
                        stream.Read(stripBytes, 0, (int)stripByteCounts[stripIndex]);

                        switch (compression)
                        {
                            case TIFFCompression.Uncompressed:
                                break;
                            case TIFFCompression.CCITT_1D:
                                stripBytes = TIFFHuffman.Decode(stripBytes, width, rowsThisStrip, strideBytes);
                                break;
                            case TIFFCompression.PackBits:
                                stripBytes = TIFFPackBits.Decode(stripBytes, width, rowsThisStrip, strideBytes);
                                break;
                            default:
                                throw new OutOfMemoryException(("Unsupported compression method"));
                        }

                        switch (photometric)
                        {
                            case TIFFPhotometric.Bilevel_BlackIsZero:
                            case TIFFPhotometric.Bilevel_WhiteIsZero:
                            case TIFFPhotometric.TransparencyMask:
                                {
                                    using (MemoryStream memStream = new MemoryStream(stripBytes))
                                    {
                                        byte[] tmpBuff = new byte[sizeof(UInt16)];

                                        for (int i = 0; i < (height * width); i++)
                                        {
                                            memStream.Read(tmpBuff, 0, sizeof(UInt16));
                                            if (!isLittleEndian)
                                                Array.Reverse(tmpBuff, 0, sizeof(UInt16));
                                            UInt16 val = BitConverter.ToUInt16(tmpBuff, 0);
                                            if (photometric == TIFFPhotometric.Bilevel_WhiteIsZero)
                                                val ^= 0xFFFF;
                                            for (int j = 0; j < 4; j++)
                                            {
                                                if (j != 3)
                                                {
                                                    bmpDataPtr[bmpDataOffset++] = (byte)((val & 0xff00) >> 8);
                                                }
                                                else
                                                {
                                                    bmpDataPtr[bmpDataOffset++] = (byte)(0xff);
                                                }
                                            }

#if false
                                            bmpDataPtr[bmpDataOffset++] = (byte)((val >> 8) & 0x00ff);
                                            bmpDataPtr[bmpDataOffset++] = (byte)(val & 0x00ff);
                                            bmpDataPtr[bmpDataOffset++] = (byte)(0x00ff);
                                            bmpDataPtr[bmpDataOffset++] = (byte)(0x00ff);
                                            bmpDataPtr[bmpDataOffset++] = (byte)(0x00ff);
                                            bmpDataPtr[bmpDataOffset++] = (byte)(0x00ff);
                                            bmpDataPtr[bmpDataOffset++] = (byte)(0x00ff);
                                            bmpDataPtr[bmpDataOffset++] = (byte)(0x00ff);
#endif
                                        }
#if false
                                    using (MemoryStream memStream = new MemoryStream(stripBytes))
                                    {
                                        BitReader bits = new BitReader(memStream);

                                        for (long yy = y; yy < stripEnd; yy++)
                                        {
                                            for (int xx = 0; xx < width; xx++)
                                            {
                                                byte bit = 0;

                                                if ((bitsPerSample[0] == 8) && (extraSamples == 0))
                                                    bit = (byte)(0xFF ^ memStream.ReadByte());
                                                else
                                                    unchecked
                                                    {
                                                        byte[] tmpBuff = new byte[sizeof(UInt16)];

                                                        memStream.Read(tmpBuff, 0, sizeof(UInt16));

                                                        tmpBuff[0] = (byte)(0xFF ^ tmpBuff[0]);
                                                        tmpBuff[1] = (byte)(0xFF ^ tmpBuff[1]);

                                                        if (tmpBuff[0] != 255 || tmpBuff[1] != 255)
                                                            tmpBuff[0] = tmpBuff[0];
                                                        if (!isLittleEndian)
                                                            Array.Reverse(tmpBuff, 0, sizeof(UInt16));
                                                        UInt16 rt = BitConverter.ToUInt16(tmpBuff, 0);

                                                        //rt = (UInt16)((rt << 4)&0xff00);
                                                        bit = (byte)((rt>>8));
#if false
                                                        rt = (byte)(0xFF ^ memStream.ReadByte());
                                                        bit = (byte)((rt & 0xff) >> 4);
                                                        //bit |= (byte)((rt & 0x0f) << 4);

                                                        rt = (byte)(0xFF ^ memStream.ReadByte());
                                                        bit2 = (byte)((rt & 0xff) >> 4);
                                                        bit2 |= (byte)((rt & 0x0f) << 4);
#endif
#if false

                                                        for (int i = 0; i < bitsPerSample[0]; i++)
                                                        {

                                                            bool rt = bits.ReadBit();
                                                            bit = (byte)((bit << 1) | (rt ? 0 : 1));
                                                        }
#endif

                                                        switch (bitsPerSample[0])
                                                        {
                                                            case 1:
                                                                bit = (byte)(bit * 255);
                                                                break;
                                                            case 4:
                                                                bit = (byte)(bit * 17);
                                                                break;
                                                        }

                                                        for (int i = 1; i < samplesPerPixel; i++) // skip any extra samples
                                                            for (int j = 0; j < bitsPerSample[i]; j++)
                                                                bits.ReadBit();
                                                    }

                                                if (photometric == TIFFPhotometric.Bilevel_BlackIsZero)
                                                    bit ^= 0xFF;

                                                if (sixteenBits)
                                                {
                                                    for (int i = 0; i < 8; i++)
                                                    {
                                                        bmpDataPtr[bmpDataOffset++] = bit;
                                                    }
                                                }
                                                else
                                                {
                                                    for (int i = 0; i < 4; i++)
                                                    {
                                                        bmpDataPtr[bmpDataOffset++] = bit;
                                                    }
                                                }
                                            }

                                            bits.SyncToNextByte();

                                            bmpDataOffset += bmpFixup;
                                        }
#endif

                                    }
                                    break;
                                }
                            case TIFFPhotometric.RGB_Palette:
                                {
                                    using (MemoryStream memStream = new MemoryStream(stripBytes))
                                    {
                                        BitReader bits = new BitReader(memStream);

                                        for (long yy = y; yy < stripEnd; yy++)
                                        {
                                            for (int xx = 0; xx < width; xx++)
                                            {
                                                int idx = 0;

                                                unchecked
                                                {
                                                    if ((bitsPerSample[0] == 8) && (extraSamples == 0))
                                                        idx = stream.ReadByte();
                                                    else
                                                    {
                                                        for (int i = 0; i < bitsPerSample[0]; i++)
                                                            idx = ((idx << 1) | (bits.ReadBit() ? 1 : 0));

                                                        for (int i = 1; i < samplesPerPixel; i++) // skip any extra samples
                                                            for (int j = 0; j < bitsPerSample[i]; j++)
                                                                bits.ReadBit();
                                                    }

                                                    ushort r = (ushort)colorMap[idx + (0 << (int)bitsPerSample[0])];
                                                    ushort g = (ushort)colorMap[idx + (1 << (int)bitsPerSample[0])];
                                                    ushort b = (ushort)colorMap[idx + (2 << (int)bitsPerSample[0])];

                                                    if (sixteenBits)
                                                    {
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(b & 0xFF);
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(b >> 8);
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(g & 0xFF);
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(g >> 8);
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(r & 0xFF);
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(r >> 8);
                                                        bmpDataPtr[bmpDataOffset++] = 0;
                                                        bmpDataPtr[bmpDataOffset++] = 0;
                                                    }
                                                    else
                                                    {
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(b >> 8);
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(g >> 8);
                                                        bmpDataPtr[bmpDataOffset++] = (byte)(r >> 8);
                                                        bmpDataPtr[bmpDataOffset++] = 0;
                                                    }
                                                }
                                            }

                                            bits.SyncToNextByte();

                                            bmpDataOffset += bmpFixup;
                                        }
                                    }
                                    break;
                                }
                            case TIFFPhotometric.RGB:
                            case TIFFPhotometric.YCbCr:
                            case TIFFPhotometric.CIELab:
                                {
                                    using (MemoryStream memStream = new MemoryStream(stripBytes))
                                    {
                                        BitReader bits = new BitReader(memStream);

                                        for (long yy = y; yy < stripEnd; yy++)
                                        {
                                            for (int xx = 0; xx < width; xx++)
                                            {
                                                unchecked
                                                {
                                                    for (int i = 0; i < 3; i++)
                                                    {
                                                        int chan = 0;

                                                        if (bitsPerSample[i] < 16)
                                                        {
                                                            for (int j = 0; j < bitsPerSample[i]; j++)
                                                                chan = ((chan << 1) | (bits.ReadBit() ? 1 : 0));

                                                            for (int j = (int)bitsPerSample[i]; j < 16; j += (int)bitsPerSample[i])
                                                                chan = (chan << (int)bitsPerSample[i]) | chan;

                                                            if ((16 % bitsPerSample[i]) != 0)
                                                            {
                                                                chan >>= (int)(bitsPerSample[i] - (16u % bitsPerSample[i]) - 1);
                                                                chan = (chan + 1) >> 1;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            for (int j = 0; j < 16; j++)
                                                                chan = ((chan << 1) | (bits.ReadBit() ? 1 : 0));

                                                            for (int j = 16; j < bitsPerSample[i]; j++)
                                                                bits.ReadBit(); // discard sample noise
                                                        }

                                                        //TODO: I don't need this!!!
                                                        if ((bitsPerSample[i] > 8) && (isLittleEndian == false))
                                                            chan = (ushort)((chan >> 8) | (chan << 8));

                                                        if ((i > 0) && (photometric == TIFFPhotometric.CIELab))
                                                            chan = (ushort)(chan + 0x8000); // bias the 'a' and 'b' channels

                                                        if (sixteenBits)
                                                        {
                                                            bmpDataPtr[bmpDataOffset + (2 - i) * 2] = (byte)(chan & 0xFF);
                                                            bmpDataPtr[bmpDataOffset + (2 - i) * 2 + 1] = (byte)(chan >> 8);
                                                        }
                                                        else
                                                            bmpDataPtr[bmpDataOffset + (2 - i)] = (byte)(chan >> 8);
                                                    }

                                                    for (int i = 3; i < samplesPerPixel; i++) // skip any extra samples
                                                        for (int j = 0; j < bitsPerSample[i]; j++)
                                                            bits.ReadBit();

                                                    if (sixteenBits)
                                                    {
                                                        bmpDataOffset += 6;
                                                        bmpDataPtr[bmpDataOffset++] = 0;
                                                    }
                                                    else
                                                        bmpDataOffset += 3;
                                                    bmpDataPtr[bmpDataOffset++] = 0;
                                                }
                                            }

                                            bits.SyncToNextByte();

                                            bmpDataOffset += bmpFixup;
                                        }
                                    }
                                    break;
                                }
                            case TIFFPhotometric.CMYK:
                                {
                                    using (MemoryStream memStream = new MemoryStream(stripBytes))
                                    {
                                        BitReader bits = new BitReader(memStream);

                                        for (long yy = y; yy < stripEnd; yy++)
                                        {
                                            for (int xx = 0; xx < width; xx++)
                                            {
                                                unchecked
                                                {
                                                    for (int i = 0; i < 4; i++)
                                                    {
                                                        int chan = 0;

                                                        if (bitsPerSample[i] < 16)
                                                        {
                                                            for (int j = 0; j < bitsPerSample[i]; j++)
                                                                chan = ((chan << 1) | (bits.ReadBit() ? 1 : 0));

                                                            for (int j = (int)bitsPerSample[i]; j < 16; j += (int)bitsPerSample[i])
                                                                chan = (chan << (int)bitsPerSample[i]) | chan;

                                                            if ((16 % bitsPerSample[i]) != 0)
                                                            {
                                                                chan >>= (int)(bitsPerSample[i] - (16u % bitsPerSample[i]) - 1);
                                                                chan = (chan + 1) >> 1;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            for (int j = 0; j < 16; j++)
                                                                chan = ((chan << 1) | (bits.ReadBit() ? 1 : 0));

                                                            for (int j = 16; j < bitsPerSample[i]; j++)
                                                                bits.ReadBit(); // discard sample noise
                                                        }

                                                        //if ((bitsPerSample[i] > 8) && (header.IntelByteOrder == true))
                                                        //    chan = (ushort)((chan >> 8) | (chan << 8));

                                                        if (sixteenBits)
                                                        {
                                                            bmpDataPtr[bmpDataOffset + i * 2] = (byte)(chan & 0xFF);
                                                            bmpDataPtr[bmpDataOffset + i * 2 + 1] = (byte)(chan >> 8);
                                                        }
                                                        else
                                                            bmpDataPtr[bmpDataOffset + i] = (byte)(chan >> 8);
                                                    }

                                                    for (int i = 4; i < samplesPerPixel; i++) // skip any extra samples
                                                        for (int j = 0; j < bitsPerSample[i]; j++)
                                                            bits.ReadBit();
                                                }
                                            }

                                            bits.SyncToNextByte();

                                            bmpDataOffset += bmpFixup;
                                        }
                                    }
                                    break;
                                }
                            default:
                                throw new OutOfMemoryException(("Unknown photometric interpretation."));
                        }
                    }
                }
                finally
                {
                    // Close stream
                    stream.Close();
                    if (bmpData != null)
                        bmp.UnlockBits(bmpData);

                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                }

                TIFFImageInfo ret = new TIFFImageInfo();

                ret.BitmapData = bmp;
                ret.BitsPerComponent = sixteenBits ? 16 : 8;
                if (photometric == TIFFPhotometric.CMYK)
                    ret.ColorSpace = TIFFColorSpace.CMYK;
                else if (photometric == TIFFPhotometric.CIELab)
                    ret.ColorSpace = TIFFColorSpace.Lab;
                else
                    ret.ColorSpace = TIFFColorSpace.RGB;

                //                return ret;


                /*********************************************************************************************************/

                imgIdx++;
            }

            //            Int32 width = widthOfImage(0);
            //            Int32 length = lengthOfImage(0);
            //            Int32 pixels = width * length;
        }

        public Bitmap getBmp()
        {
            return bmp;
        }

#if faslse
        public Int32 widthOfImage(int imgIdx)
        {
            TiffIFD currentIfd = IFDList[imgIdx];
            TiffEntry currentTag = currentIfd.tiffTagForKey(256);
            Int32[] widthArr = currentTag.toInt32();
            Int32 width = widthArr[0];
            return width;
        }

        public Int32 lengthOfImage(int imgIdx)
        {
            TiffIFD currentIfd = IFDList[imgIdx];
            TiffEntry currentTag = currentIfd.tiffTagForKey(257);
            Int32[] lengthArr = currentTag.toInt32();
            Int32 length = lengthArr[0];
            return length;
        }
#endif

        public enum TIFFColorSpace
        {
            RGB,
            CMYK,
            Lab,
        }

        public class TIFFImageInfo
        {
            public TIFFColorSpace ColorSpace;
            public int BitsPerComponent;
            public Bitmap BitmapData;
        }

        enum TIFFPhotometric
        {
            Bilevel_WhiteIsZero = 0,
            Bilevel_BlackIsZero = 1,
            RGB = 2,
            RGB_Palette = 3,
            TransparencyMask = 4,
            CMYK = 5,
            YCbCr = 6,
            CIELab = 8,
        }

        enum TIFFCompression
        {
            Uncompressed = 1,
            CCITT_1D = 2,
            Group3Fax = 3,
            Group4Fax = 4,
            LZW = 5,
            JPEG = 6,
            PackBits = 32773,
        }
        class TIFFPackBits
        {
            public static byte[] Decode(byte[] input, int width, int rows, int stride)
            {
                byte[] output = new byte[rows * stride];

                int i = 0;

                for (int y = 0; y < rows; y++)
                {
                    int o = y * stride;

                    int run = 0;
                    int run_value = 0;

                    int x = 0;

                    while (x < stride)
                    {
                        if (i >= input.Length)
                            return output;

                        sbyte n = unchecked((sbyte)input[i++]);

                        if (n >= 0)
                        {
                            run = 1 + n;
                            run_value = -1;
                        }
                        else if (n > -128)
                        {
                            run = 1 - n;
                            run_value = input[i++];
                        }
                        else
                            continue;

                        if (run_value < 0)
                        {
                            if (run + x > stride)
                                Array.Copy(input, i, output, o, stride - x);
                            else
                                Array.Copy(input, i, output, o, run);

                            i += run;
                        }
                        else
                        {
                            if (run + x > stride)
                                run = stride - x;

                            for (int q = 0; q < run; q++)
                                output[o + q] = unchecked((byte)run_value);
                        }

                        x += run;
                        o += run;
                    }
                }

                return output;
            }
        }

        class BitReader
        {
            private readonly Stream BaseStream;

            public BitReader(Stream to_wrap)
            {
                BaseStream = to_wrap;
            }

            int bits_left = 0, bits;

            public bool ReadBit()
            {
                if (bits_left == 0)
                {
                    bits = BaseStream.ReadByte();
                    bits_left = 8;
                }

                bits <<= 1;
                bits_left--;

                return (0 != (bits & 0x100));
            }

            public void SyncToNextByte()
            {
                while (bits_left > 0)
                    ReadBit();
            }
        }

        class BitWriter
        {
            private readonly Stream BaseStream;

            public BitWriter(Stream to_wrap)
            {
                BaseStream = to_wrap;
            }

            int bits_left = 8, bits;

            public void WriteBit(bool bit)
            {
                bits = (bits << 1) | (bit ? 1 : 0);
                bits_left--;

                if (bits_left == 0)
                {
                    BaseStream.WriteByte((byte)bits);
                    bits_left = 8;
                    bits = 0;
                }
            }

            public void SyncToNextByte()
            {
                while (bits_left != 8)
                    WriteBit(false);
            }

            public void Seek(int position)
            {
                if (bits_left < 8)
                    BaseStream.WriteByte((byte)bits);

                bits_left = 8;
                bits = 0;

                BaseStream.Seek(position, SeekOrigin.Begin);
            }
        }

        class TIFFHuffman
        {
            enum CodeType
            {
                White,
                Black,
                Both,
            }

            struct Code
            {
                public int Value, Length;
                public CodeType Type;

                public Code(int value, int length, CodeType type)
                {
                    Value = value;
                    Length = length;
                    Type = type;
                }
            }

            static Code[][] codes_by_length = new Code[][]
        {
          new Code[0], // length 0 does not exist
          new Code[0], // length 1 does not exist
          new Code[]   // length 2
          {
            new Code(3, 2, CodeType.Black), // 11
            new Code(1, 3, CodeType.Black), // 10
          },
          new Code[]   // length 3
          {
            new Code(2, 1, CodeType.Black), // 010
            new Code(6, 4, CodeType.Black), // 011
          },
          new Code[]   // length 4
          {
            new Code(14, 2, CodeType.White), // 0111
            new Code(1, 3, CodeType.White),  // 1000
            new Code(13, 4, CodeType.White), // 1011
            new Code(3, 5, CodeType.White),  // 1100
            new Code(7, 6, CodeType.White),  // 1110
            new Code(15, 7, CodeType.White), // 1111
            new Code(12, 5, CodeType.Black),  // 0011
            new Code(4, 6, CodeType.Black),   // 0010
          },
          new Code[]   // length 5
          {
            new Code(25, 8, CodeType.White),  // 10011
            new Code(5, 9, CodeType.White),   // 10100
            new Code(28, 10, CodeType.White), // 00111
            new Code(2, 11, CodeType.White),  // 01000
            new Code(27, 64, CodeType.White), // 11011
            new Code(9, 128, CodeType.White), // 10010
            new Code(24, 7, CodeType.Black),  // 00011
          },
          new Code[]   // length 6
          {
            new Code(56, 1, CodeType.White),   // 000111
            new Code(4, 12, CodeType.White),   // 001000
            new Code(48, 13, CodeType.White),  // 000011
            new Code(11, 14, CodeType.White),  // 110100
            new Code(43, 15, CodeType.White),  // 110101
            new Code(21, 16, CodeType.White),  // 101010
            new Code(53, 17, CodeType.White),  // 101011
            new Code(58, 192, CodeType.White), // 010111
            new Code(6, 1664, CodeType.White), // 011000
            new Code(40, 8, CodeType.Black),   // 000101
            new Code(8, 9, CodeType.Black),    // 000100
          },
          new Code[]   // length 7
          {
            new Code(114, 18, CodeType.White),  // 0100111
            new Code(24, 19, CodeType.White),   // 0001100
            new Code(8, 20, CodeType.White),    // 0001000
            new Code(116, 21, CodeType.White),  // 0010111
            new Code(96, 22, CodeType.White),   // 0000011
            new Code(16, 23, CodeType.White),   // 0000100
            new Code(10, 24, CodeType.White),   // 0101000
            new Code(106, 25, CodeType.White),  // 0101011
            new Code(100, 26, CodeType.White),  // 0010011
            new Code(18, 27, CodeType.White),   // 0100100
            new Code(12, 28, CodeType.White),   // 0011000
            new Code(118, 256, CodeType.White), // 0110111
            new Code(16, 10, CodeType.Black),   // 0000100
            new Code(80, 11, CodeType.Black),   // 0000101
            new Code(112, 12, CodeType.Black),  // 0000111
          },
          new Code[]   // length 8
          {
            new Code(172, 0, CodeType.White),   // 00110101
            new Code(64, 29, CodeType.White),   // 00000010
            new Code(192, 30, CodeType.White),  // 00000011
            new Code(88, 31, CodeType.White),   // 00011010
            new Code(216, 32, CodeType.White),  // 00011011
            new Code(72, 33, CodeType.White),   // 00010010
            new Code(200, 34, CodeType.White),  // 00010011
            new Code(40, 35, CodeType.White),   // 00010100
            new Code(168, 36, CodeType.White),  // 00010101
            new Code(104, 37, CodeType.White),  // 00010110
            new Code(232, 38, CodeType.White),  // 00010111
            new Code(20, 39, CodeType.White),   // 00101000
            new Code(148, 40, CodeType.White),  // 00101001
            new Code(84, 41, CodeType.White),   // 00101010
            new Code(212, 42, CodeType.White),  // 00101011
            new Code(52, 43, CodeType.White),   // 00101100
            new Code(180, 44, CodeType.White),  // 00101101
            new Code(32, 45, CodeType.White),   // 00000100
            new Code(160, 46, CodeType.White),  // 00000101
            new Code(80, 47, CodeType.White),   // 00001010
            new Code(208, 48, CodeType.White),  // 00001011
            new Code(74, 49, CodeType.White),   // 01010010
            new Code(202, 50, CodeType.White),  // 01010011
            new Code(42, 51, CodeType.White),   // 01010100
            new Code(170, 52, CodeType.White),  // 01010101
            new Code(36, 53, CodeType.White),   // 00100100
            new Code(164, 54, CodeType.White),  // 00100101
            new Code(26, 55, CodeType.White),   // 01011000
            new Code(154, 56, CodeType.White),  // 01011001
            new Code(90, 57, CodeType.White),   // 01011010
            new Code(218, 58, CodeType.White),  // 01011011
            new Code(82, 59, CodeType.White),   // 01001010
            new Code(210, 60, CodeType.White),  // 01001011
            new Code(76, 61, CodeType.White),   // 00110010
            new Code(204, 62, CodeType.White),  // 00110011
            new Code(44, 63, CodeType.White),   // 00110100
            new Code(108, 320, CodeType.White), // 00110110
            new Code(236, 384, CodeType.White), // 00110111
            new Code(38, 448, CodeType.White),  // 01100100
            new Code(166, 512, CodeType.White), // 01100101
            new Code(22, 576, CodeType.White),  // 01101000
            new Code(230, 640, CodeType.White), // 01100111
            new Code(32, 13, CodeType.Black),   // 00000100
            new Code(224, 14, CodeType.Black),  // 00000111
          },
          new Code[]   // length 9
          {
            new Code(102, 704, CodeType.White),  // 011001100
            new Code(358, 768, CodeType.White),  // 011001101
            new Code(150, 832, CodeType.White),  // 011010010
            new Code(406, 896, CodeType.White),  // 011010011
            new Code(86, 960, CodeType.White),   // 011010100
            new Code(342, 1024, CodeType.White), // 011010101
            new Code(214, 1088, CodeType.White), // 011010110
            new Code(470, 1152, CodeType.White), // 011010111
            new Code(54, 1216, CodeType.White),  // 011011000
            new Code(310, 1280, CodeType.White), // 011011001
            new Code(182, 1344, CodeType.White), // 011011010
            new Code(438, 1408, CodeType.White), // 011011011
            new Code(50, 1472, CodeType.White),  // 010011000
            new Code(306, 1536, CodeType.White), // 010011001
            new Code(178, 1600, CodeType.White), // 010011010
            new Code(434, 1728, CodeType.White), // 010011011
            new Code(48, 15, CodeType.Black),    // 000011000
          },
          new Code[]   // length 10
          {
            new Code(944, 0, CodeType.Black),  // 0000110111
            new Code(928, 16, CodeType.Black), // 0000010111
            new Code(96, 17, CodeType.Black),  // 0000011000
            new Code(64, 18, CodeType.Black),  // 0000001000
            new Code(960, 64, CodeType.Black), // 0000001111
          },
          new Code[]   // length 11
          {
            new Code(1840, 19, CodeType.Black),  // 00001100111
            new Code(176, 20, CodeType.Black),   // 00001101000
            new Code(432, 21, CodeType.Black),   // 00001101100
            new Code(1888, 22, CodeType.Black),  // 00000110111
            new Code(160, 23, CodeType.Black),   // 00000101000
            new Code(1856, 24, CodeType.Black),  // 00000010111
            new Code(192, 25, CodeType.Black),   // 00000011000
            new Code(0, -1, CodeType.Black),     // 00000000000
            new Code(128, 1792, CodeType.Both),  // 00000001000
            new Code(384, 1856, CodeType.Both),  // 00000001100
            new Code(1408, 1920, CodeType.Both), // 00000001101
          },
          new Code[]   // length 12
          {
            new Code(2048, -1, CodeType.White),  // 000000000001
            new Code(1328, 26, CodeType.Black),  // 000011001010
            new Code(3376, 27, CodeType.Black),  // 000011001011
            new Code(816, 28, CodeType.Black),   // 000011001100
            new Code(2864, 29, CodeType.Black),  // 000011001101
            new Code(352, 30, CodeType.Black),   // 000001101000
            new Code(2400, 31, CodeType.Black),  // 000001101001
            new Code(1376, 32, CodeType.Black),  // 000001101010
            new Code(3424, 33, CodeType.Black),  // 000001101011
            new Code(1200, 34, CodeType.Black),  // 000011010010
            new Code(3248, 35, CodeType.Black),  // 000011010011
            new Code(688, 36, CodeType.Black),   // 000011010100
            new Code(2736, 37, CodeType.Black),  // 000011010101
            new Code(1712, 38, CodeType.Black),  // 000011010110
            new Code(3760, 39, CodeType.Black),  // 000011010111
            new Code(864, 40, CodeType.Black),   // 000001101100
            new Code(2912, 41, CodeType.Black),  // 000001101101
            new Code(1456, 42, CodeType.Black),  // 000011011010
            new Code(3504, 43, CodeType.Black),  // 000011011011
            new Code(672, 44, CodeType.Black),   // 000001010100
            new Code(2720, 45, CodeType.Black),  // 000001010101
            new Code(1696, 46, CodeType.Black),  // 000001010110
            new Code(3744, 47, CodeType.Black),  // 000001010111
            new Code(608, 48, CodeType.Black),   // 000001100100
            new Code(2656, 49, CodeType.Black),  // 000001100101
            new Code(1184, 50, CodeType.Black),  // 000001010010
            new Code(3232, 51, CodeType.Black),  // 000001010011
            new Code(576, 52, CodeType.Black),   // 000000100100
            new Code(3776, 53, CodeType.Black),  // 000000110111
            new Code(448, 54, CodeType.Black),   // 000000111000
            new Code(3648, 55, CodeType.Black),  // 000000100111
            new Code(320, 56, CodeType.Black),   // 000000101000
            new Code(416, 57, CodeType.Black),   // 000001011000
            new Code(2464, 58, CodeType.Black),  // 000001011001
            new Code(3392, 59, CodeType.Black),  // 000000101011
            new Code(832, 60, CodeType.Black),   // 000000101100
            new Code(1440, 61, CodeType.Black),  // 000001011010
            new Code(1632, 62, CodeType.Black),  // 000001100110
            new Code(3680, 63, CodeType.Black),  // 000001100111
            new Code(304, 128, CodeType.Black),  // 000011001000
            new Code(2352, 192, CodeType.Black), // 000011001001
            new Code(3488, 256, CodeType.Black), // 000001011011
            new Code(3264, 320, CodeType.Black), // 000000110011
            new Code(704, 384, CodeType.Black),  // 000000110100
            new Code(2752, 448, CodeType.Black), // 000000110101
            new Code(1152, 1984, CodeType.Both), // 000000010010
            new Code(3200, 2048, CodeType.Both), // 000000010011
            new Code(640, 2112, CodeType.Both),  // 000000010100
            new Code(2688, 2176, CodeType.Both), // 000000010101
            new Code(1664, 2240, CodeType.Both), // 000000010110
            new Code(3712, 2304, CodeType.Both), // 000000010111
            new Code(896, 2368, CodeType.Both),  // 000000011100
            new Code(2944, 2432, CodeType.Both), // 000000011101
            new Code(1920, 2496, CodeType.Both), // 000000011110
            new Code(3968, 2560, CodeType.Both), // 000000011111
          },
          new Code[]   // length 13
          {
            new Code(1728, 512, CodeType.Black),  // 0000001101100
            new Code(5824, 576, CodeType.Black),  // 0000001101101
            new Code(2624, 640, CodeType.Black),  // 0000001001010
            new Code(6720, 704, CodeType.Black),  // 0000001001011
            new Code(1600, 768, CodeType.Black),  // 0000001001100
            new Code(5696, 832, CodeType.Black),  // 0000001001101
            new Code(2496, 896, CodeType.Black),  // 0000001110010
            new Code(6592, 960, CodeType.Black),  // 0000001110011
            new Code(1472, 1024, CodeType.Black), // 0000001110100
            new Code(5568, 1088, CodeType.Black), // 0000001110101
            new Code(3520, 1152, CodeType.Black), // 0000001110110
            new Code(7616, 1216, CodeType.Black), // 0000001110111
            new Code(2368, 1280, CodeType.Black), // 0000001010010
            new Code(6464, 1344, CodeType.Black), // 0000001010011
            new Code(1344, 1408, CodeType.Black), // 0000001010100
            new Code(5440, 1472, CodeType.Black), // 0000001010101
            new Code(2880, 1536, CodeType.Black), // 0000001011010
            new Code(6976, 1600, CodeType.Black), // 0000001011011
            new Code(1216, 1664, CodeType.Black), // 0000001100100
            new Code(5312, 1728, CodeType.Black), // 0000001100101
          }
        };

            class Tree
            {
                public Tree Bit0, Bit1;
                public int Length;

                public int GetLength(BitReader stream)
                {
                    if ((Bit0 == null) && (Bit1 == null))
                        return Length;

                    bool bit = stream.ReadBit();

                    Tree child = (bit == false) ? Bit0 : Bit1;

                    if (child == null)
                        throw new OutOfMemoryException(("Invalid Huffman code"));

                    return child.GetLength(stream);
                }

                public void Insert(int bits, int num_bits_remaining, int length)
                {
                    if (num_bits_remaining == 0)
                    {
                        if ((Bit0 != null) || (Bit1 != null))
                            throw new OutOfMemoryException(("Huffman code table corrupt"));

                        Length = length;

                        return;
                    }

                    bool bit = (0 != (bits & 1));

                    bits >>= 1;
                    num_bits_remaining--;

                    if (bit == false)
                    {
                        if (Bit0 == null)
                            Bit0 = new Tree();

                        Bit0.Insert(bits, num_bits_remaining, length);
                    }
                    else
                    {
                        if (Bit1 == null)
                            Bit1 = new Tree();

                        Bit1.Insert(bits, num_bits_remaining, length);
                    }
                }
            }

            static Tree white_tree, black_tree;

            static void build_trees()
            {
                white_tree = new Tree();
                black_tree = new Tree();

                for (int length = 0; length < codes_by_length.Length; length++)
                {
                    Code[] codes = codes_by_length[length];

                    for (int i = 0; i < codes.Length; i++)
                    {
                        Code code = codes[i];

                        if ((code.Type == CodeType.White) || (code.Type == CodeType.Both))
                            white_tree.Insert(code.Value, length, code.Length);
                        if ((code.Type == CodeType.Black) || (code.Type == CodeType.Both))
                            black_tree.Insert(code.Value, length, code.Length);
                    }
                }
            }

            static TIFFHuffman()
            {
                build_trees();
            }

            public static byte[] Decode(byte[] input, int width, int rows, int stride)
            {
                byte[] output = new byte[rows * stride];

                using (MemoryStream input_stream = new MemoryStream(input),
                         output_stream = new MemoryStream(output))
                {
                    BitReader reader = new BitReader(input_stream);
                    BitWriter writer = new BitWriter(output_stream);

                    for (int y = 0; y < rows; y++)
                    {
                        int o = y * stride;
                        int code = 0;

                        int x = 0;

                        while (x < width)
                        {
                            code = white_tree.GetLength(reader);

                            if (code < 0) // end-of-line
                                break;

                            for (int i = 0; i < code; i++)
                            {
                                if (x < width)
                                    writer.WriteBit(false);
                                x++;
                            }

                            if (x >= width)
                            {
                                if (code >= 64)
                                {
                                    code = white_tree.GetLength(reader);

                                    if (code != 0)
                                        throw new OutOfMemoryException(("Invalid Huffman data"));
                                }
                                break;
                            }

                            if (code >= 64)
                                continue;

                            do
                            {
                                code = black_tree.GetLength(reader);

                                if (code < 0) // end-of-line
                                    break;

                                for (int i = 0; i < code; i++)
                                {
                                    if (x < width)
                                        writer.WriteBit(true);
                                    x++;
                                }
                            }
                            while (code >= 64);
                        }

                        reader.SyncToNextByte();
                        writer.SyncToNextByte();
                    }
                }

                return output;
            }
        }
    }
#endif
}
