﻿CREATE TABLE [dbo].[WBMainTable](
	[mainID] [bigint] NOT NULL,
	[userID] [bigint] NOT NULL,
	[experimentID] [nvarchar](max) NULL,
	[experimentDate] [datetime] NULL,
	[membraneInfo] [nvarchar](max) NULL,
	[membraneID] [nvarchar](max) NULL,
	[membraneDate] [datetime] NULL,
	[blot] [nvarchar](max) NULL,
	[blotDate] [datetime] NULL,
	[antibodyInfo] [nvarchar](max) NULL,
	[performer] [nvarchar](max) NULL,
	[comments] [nvarchar](max) NULL,
	[control] [bit] NULL,
	[image] [varbinary](max) NULL,
	[imageName] [nvarchar](max) NULL,
	[imagePath] [nvarchar](max) NULL,
	[imageType] NVARCHAR(50) NULL,
	[userName] [nvarchar](max) NOT NULL,
	[rowID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__tmp_ms_xx__rowID__36B12243]  DEFAULT (newid()),
	[msgIcon] [varbinary](max) NULL,
	[msgsNum] [int] NULL,
	[newMsgsNum] [int] NULL,
 CONSTRAINT [PK_WBMainTable] PRIMARY KEY CLUSTERED 
(
	[rowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

