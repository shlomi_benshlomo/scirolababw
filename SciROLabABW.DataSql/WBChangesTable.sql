﻿CREATE TABLE [dbo].[WBChangesTable](
	[blotID] [bigint] NULL,
	[experimentID] [nvarchar](max) NULL,
	[experimentDate] [datetime] NULL,
	[membraneInfo] [nvarchar](max) NULL,
	[membraneID] [nvarchar](max) NULL,
	[membraneDate] [datetime] NULL,
	[blot] [nvarchar](max) NULL,
	[blotDate] [datetime] NULL,
	[antibodyInfo] [nvarchar](max) NULL,
	[performer] [nvarchar](max) NULL,
	[comments] [nvarchar](max) NULL,
	[control] [bit] NULL,
	[image] [varbinary](max) NULL,
	[imagePath] [nvarchar](max) NULL,
	[imageType] [nchar](10) NULL,
	[userName] [nvarchar](max) NULL,
	[changeDate] [datetime] NULL,
	[changeStatus] [nvarchar](50) NULL,
	[rowID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_WBChangesTable] PRIMARY KEY CLUSTERED 
(
	[rowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]