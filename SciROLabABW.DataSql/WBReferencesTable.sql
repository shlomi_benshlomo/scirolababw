﻿CREATE TABLE [dbo].[WBReferencesTable](
	[link] [nvarchar](max) NULL,
	[fullPath] [nvarchar](max) NULL,
	[referenceID] [bigint] NOT NULL,
	[userName] [nvarchar](max) NULL,
	[rowID] [uniqueidentifier] NOT NULL,
	[mainTableID] [uniqueidentifier] NULL,
	[refLocation] [nvarchar](50) NULL,
 CONSTRAINT [PK_referencesTable] PRIMARY KEY CLUSTERED 
(
	[rowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
